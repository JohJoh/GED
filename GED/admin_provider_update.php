<?php
include ('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include('./scripts/head.php');
include('./scripts/menu.php');
include('./scripts/banner.php');

$defaut_lang = 'fr';
if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == 'en')) {
            $lang = $_GET['lang'];
        } else {
            $lang = $defaut_lang;
        }
    } else {
        $lang = $defaut_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_provider_update = array(
    'fr' => array(
        'title_form' => 'Modifier des fournisseurs',
        'provider' => 'Fournisseur',
        'code_provider' => 'Code fournisseur ',
        'nom_provider' => 'Nom du fournisseur',
        'update' => 'Modifier'
    ),
    'en' => array(
        'title_form' => 'Update providers ',
        'provider' => 'Provider',
        'code_provider' => 'Code of prrovider',
        'nom_provider' => 'Name of provider ',
        'update' => 'Update'
    )
);

//Partie SQL
$reponse_fournisseur = $link->query("SELECT * FROM fournisseurs ORDER BY lib_four ASC");

if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-horizontal" name="formUpdateZone" data-toggle="validator" role="form" action="admin_provider_update_infos.php" method="post">
                        <fieldset>
                            <legend><?php echo $trad_admin_provider_update[$lang]['title_form']; ?></legend>
                            <div class="form-group">
                                <label for="select" class="col-md-4 control-label"><?php echo $trad_admin_provider_update[$lang]['provider']; ?></label>
                                <div class="col-md-4">
                                    <select id="provider" name="provider" class="form-control">
                                        <?php
                                        while ($donnees_fournisseur = $reponse_fournisseur->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_fournisseur['code_four'] . "'>" . $donnees_fournisseur['code_four'] . ' - ' . $donnees_fournisseur['lib_four'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            </br>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_update"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_update" class="btn btn-primary" name="btn_update"><?php echo $trad_admin_provider_update[$lang]['update']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
} else {
    echo '<h2 class="lead section-lead-has-error">' . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>