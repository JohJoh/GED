<?php
$dir_zip = "./zip/";
require_once './scripts/dbc.php';
$type_download = filter_input(INPUT_GET, 'fic', FILTER_SANITIZE_STRING);
$fichier_a_zipper = array();

$req_zip = "SELECT " . $type_download . " FROM produits WHERE " . $type_download . " IS NOT NULL;";

$fichier_a_zipper_prep = $link->prepare($req_zip);
if ($fichier_a_zipper_prep->execute() !== false) {
    $resultats = $fichier_a_zipper_prep->fetchAll(PDO::FETCH_ASSOC);
    foreach ($resultats as $fichier) {
        if (file_exists($fichier[$type_download])) {
            if (mb_substr($fichier[$type_download], 0, 2, "UTF-8") === "./") {
                $fichier[$type_download] = mb_substr($fichier[$type_download], 2, null, "UTF-8");
            }
            $fichier_a_zipper[] = $fichier[$type_download];
        }
    }
    $files = $fichier_a_zipper;
    $zipname = $dir_zip.$type_download . '.zip';
    
    $zip = new ZipArchive;
    $zip->open($zipname, ZipArchive::OVERWRITE|ZipArchive::CREATE);
    foreach ($files as $file) {
        $zip->addFile($file);
    }

    $zip->close();

    $size = filesize($zipname);
//    var_dump($zipname);die;
    header('Content-type: application/zip');
    header("Content-length: $size");
    header('Content-Disposition: attachment; filename="'.$type_download . '.zip";');
    header('Expires: 0');
    readfile($zipname);
} else {
    throw new Exception("La requête ne s'est pas bien exécutée.");
}