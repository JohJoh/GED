<?php
require_once('./scripts/dbc.php');
page_protect();
$allCountries = loadPays();

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_product_edit = array(
    'fr' => array(
        'title' => 'Modifier des produits',
        'code_sap' => 'Code SAP',
        'provider' => 'Fournisseur',
        'code_prod' => 'Code Produit Fournisseur',
        'designation' => 'Désignation',
        'division' => 'Division',
        'pays_prov' => 'Pays de provenance',
        'code_post' => 'Code Postal',
        'ville' => 'Ville',
        'pays_origine' => 'Pays d\'origine',
        'produit_non_pref' => 'Ce produit ne bénéficie pas de préférence',
        'date_debut' => 'Date de début de validité',
        'date_fin' => 'Date de fin de validité',
        'zone_pref' => 'Zones préférentielles',
        'pays_zone' => 'Pays par zone',
        'code_douane' => 'Code douanier',
        'definition_code' => 'Définition du code ',
        'danger' => 'DANGER',
        'produit_concerne' => 'Ce produit n\'est pas concerné par cette partie',
        'classe_danger' => 'Classe de danger',
        'code_onu' => 'Code ONU',
        'code_emballage' => 'Code emballage',
        'mode_transport' => 'Mode de transport',
        'conditionnement' => 'Conditionnement du fournisseur',
        'DLT' => 'Fiche DLT',
		'DLT_PEMD' => 'Fiche DLT PEMD',
		'DE' => 'Fiche DE',		
        'FDS_fr' => 'Fiche FDS Français',
        'info_fds_en' => 'Je ne dispose pas de cette fiche en anglais',
        'FDS_en' => 'Fiche FDS Anglais',
        'DOF' => 'Fiche Déclaration d\'origine',
        'error_sapcode_1' => 'Le code SAP existe déjà.',
        'error_sapcode_2' => 'Le code SAP ne doit pas être vide',
        'save' => 'Enregistrer',
        'contenance' => 'Quantité par emballage intérieur',
		'type' => 'Type d\'emballage',
        'explication_contenance' => 'Fournir la quantité de produit dangereux (en mL, L, g, kg) contenu par emballage - exemple : 500 mL, 5L',
		'explication_type' => 'Fournir la typologie de l\'emballage - exemple : aérosol métal, bidon plastique, flacon de verre',
        'explication_conditionnement' => 'Ex: Si paquet de 4 stylos, indiquer 4+',
        'add' => 'Ajouter',
        'code_sap_label' => 'Code(s) SAP',
    ),
    'en' => array(
        'title' => 'Edit products',
        'code_sap' => 'SAP code',
        'provider' => 'Provider',
        'code_prod' => 'Supplier product code',
        'designation' => 'Designation',
        'division' => 'Division',
        'pays_prov' => 'Country of provenance',
        'code_post' => 'Postal Code',
        'ville' => 'City',
        'pays_origine' => 'Country of origin',
        'produit_non_pref' => 'This product does not benefit from preference',
        'date_debut' => 'Starting date of validity',
        'date_fin' => 'Expiration date',
        'zone_pref' => 'Preferential area',
        'pays_zone' => 'Area\'s countries',
        'code_douane' => 'Customs code',
        'definition_code' => 'Definition of the code',
        'danger' => 'DANGER',
        'produit_concerne' => 'This product is not concerned by that party',
        'classe_danger' => 'Class of danger',
        'code_onu' => 'UN code',
        'code_emballage' => 'Packaging code',
        'mode_transport' => 'Mode of transport',
        'conditionnement' => 'Packaging provider',
        'DLT' => 'LTH File',
		'DLT_PEMD' => 'DLT PEMD File',
		'DE' => 'DC File',		
        'FDS_fr' => 'French SDS File',
        'info_fds_en' => 'I do not have to this file in English',
        'FDS_en' => 'English SDS File',
        'DOF' => 'Origin declaration File',
        'error_sapcode_1' => 'The SAP code already exists.',
        'error_sapcode_2' => 'The SAP code should not be empty',
        'save' => 'Save',
        'contenance' => 'Quantity per inner packaging',
		'type' => 'Type of packaging',
        'explication_contenance' => 'Provide the amount of hazardous material ( in mL , L, g, kg) content by packaging - example: 500 mL, 5L',
		'explication_contenance' => 'Provide the typology of the packaging - example: metal aerosol canister, plastic , glass bottle',
        'explication_conditionnement' => 'Ex : If pack of 4 pens, indicate 4+',
        'add' => 'Add',
        'code_sap_label' => 'SAP code(s)',
    )
);

//Partie SQL
//$reponse_fournisseur = $link->query("SELECT * FROM fournisseurs ORDER BY lib_four");
//$reponse_division = $link->query('SELECT sap_prod, division_prod FROM produit');
//$reponse_pays_provenance = $link->query('SELECT * from pays ORDER BY lib_pays');
//$reponse_pays_origine = $link->query('SELECT * from pays ORDER BY lib_pays');
//$reponse_zone_pref = $link->query('SELECT * from zone');
//$reponse_mode_transport = $link->query("SELECT * from mode_tpt");
//$reponse_code_douanier = $link->query('SELECT * from douane');
//$reponse_classe_danger = $link->query("SELECT * from danger");
//$reponse_code_onu = $link->query('SELECT * from onu');
//$reponse_code_emballage = $link->query("SELECT * from emballage");
$codes_sap = explode(',', $_POST['code_sap']);
foreach ($codes_sap as $i => $code) {
    $codes_sap[$i] = filter_var($code, FILTER_SANITIZE_NUMBER_INT);
}

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="result_req3" class="lead section-lead has-success"></div>
                    <script>
                        $(document).ready(function () {
                            var popover = $("[data-toggle=popover]").popover({
                                trigger: 'hover',
                                placement: 'bottom',
                                html: 'true'
                            }).on('show.bs.popover', function () {
                                $.ajax({
                                    url: 'admin_provider_create.php',
                                    success: function (html) {
                                        popover.attr('data-content', html);
                                    }
                                });
                            });
                        });
                    </script>
                    <form class="form-horizontal" name="update_products" action="ajax_update_products.php" method="post" data-toggle="validator" role="form" id="update_products" enctype="multipart/form-data">
                        <fieldset>

                            <!-- Modifier des produits -->
                            <legend><?= $trad_admin_product_edit[$lang]['title']; ?></legend>

                            <!-- Code SAP-->
                            <div class="form-group">
                                <input type="hidden" id="code_actuel" name="code_actuel" value="<?= join(",", $codes_sap) ?>">
                            </div>

                            <style>
                                .btn-glyphicon {
                                    padding:8px;
                                    background:#ffffff;
                                    margin-right:4px;
                                }
                                .icon-btn {
                                    padding: 1px 15px 3px 2px;
                                    border-radius:50px;
                                }
                            </style>

                            <!--Fichier DLT -->
                            <div class="form-group" id="groupDLT">
                                <label class="col-md-4 control-label" for="fiche_DLT"><?= $trad_admin_product_edit[$lang]['DLT']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DLT" class="input-file" name="fiche_DLT" type="file" accept="application/pdf"/>
                                </div>
                            </div>

							<!--Fichier DLT PEMD -->
                            <div class="form-group" id="groupDLTPEMD">
                                <label class="col-md-4 control-label" for="fiche_DLT_PEMD"><?= $trad_admin_product_edit[$lang]['DLT_PEMD']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DLT_PEMD" class="input-file" name="fiche_DLT_PEMD" type="file" accept="application/pdf"/>
                                </div>
                            </div>
							
							<!--Fichier DE -->
                            <div class="form-group" id="groupDE">
                                <label class="col-md-4 control-label" for="fiche_DE"><?= $trad_admin_product_edit[$lang]['DE']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DE" class="input-file" name="fiche_DE" type="file" accept="application/pdf"/>
                                </div>
                            </div>							

                            <!-- Fichier FDS Français -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_FDS_fr"><?= $trad_admin_product_edit[$lang]['FDS_fr']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_FDS_fr" class="input-file" name="fiche_FDS_fr" type="file" accept="application/pdf">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="no_english_file"></label>
                                <div class="col-md-4">
                                    <label class="checkbox-inline" for="no_english_file">
                                        <input id="no_english_file" name="no_english_file" type="checkbox" value="0">
                                        <?= $trad_admin_product_edit[$lang]['info_fds_en']; ?>
                                    </label>
                                </div>
                            </div>

                            <!-- Fichier FDS Anglais -->
                            <div class="form-group" id="fds_en">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="fiche_FDS_en"><?= $trad_admin_product_edit[$lang]['FDS_en']; ?></label>
                                    <div class="col-md-4">
                                        <input id="fiche_FDS_en" class="input-file" name="fiche_FDS_en" type="file" accept="application/pdf">
                                    </div>
                                </div>
                            </div>

                            <!-- Fichier DOF -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_DOF"><?= $trad_admin_product_edit[$lang]['DOF']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DOF" class="input-file" name="fiche_DOF" type="file" accept="application/pdf">
                                </div>
                            </div>

                            <!-- Bouton Valider -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?= $trad_admin_product_edit[$lang]['save'] . "  "; ?><span class="glyphicon glyphicon-floppy-disk"></span></button>
                                </div>
                            </div>

                            <!--<button type="button" id="id_test" class="btn btn-primary" name="id_test">Test</button>-->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/jquery-admin_product.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
?>