<?php
include('./scripts/dbc.php');
page_protect();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

if (isset($_SESSION['user_level'])) {
    if ($_SESSION['user_level'] == 1) {
        $req_produit_non_renseigne_check = "select * from produits p, pays pa where p.code_pays_origine = pa.code_pays and code_four = '" . $_SESSION['user_code_four'] . "';";
    } else {
        $req_produit_non_renseigne_check = "select * from produits p, pays pa where p.code_pays_origine = pa.code_pays;";
    }

    $i = 0;
    $j = 0;
    $k = 0;

    $req_produit_non_renseigne = $link->query($req_produit_non_renseigne_check);

    /* while($produit_non_renseigne = $req_produit_non_renseigne->fetch(PDO::FETCH_BOTH)){

      $vErreur = false;

      if($produit_non_renseigne['is_active'] == 1){

      $j++;
      $requete_compte_tpt =$link->query("select id_transporter from transporter where sap_prod = '".$produit_non_renseigne['sap_prod']."';");
      $req_compte_tpt=$requete_compte_tpt->rowCount();
      if($req_compte_tpt == 0){$vErreur = true;}

      if($produit_non_renseigne['code_pays_origine'] == ""||$produit_non_renseigne['code_pays_origine'] == "0"){
      $vErreur = true;
      }

      if($produit_non_renseigne['code_douanier'] == 0){
      $vErreur = true;
      }
      if(($produit_non_renseigne['code_onu'] == "") && ($produit_non_renseigne['is_dangerous'] == 1)){
      $vErreur = true;
      }
      if(($produit_non_renseigne['code_danger'] == "") && ($produit_non_renseigne['is_dangerous'] == 1)){
      $vErreur = true;
      }
      if(($produit_non_renseigne['code_embal'] == "") && ($produit_non_renseigne['is_dangerous'] == 1)){
      $vErreur = true;
      }
      if(($produit_non_renseigne['fic_dlt'] == "") && ($produit_non_renseigne['estEurope'] == 1)){
      $vErreur = true;
      }
	  if(($produit_non_renseigne['fic_dlt_pemd'] == "") && ($produit_non_renseigne['estEurope'] == 1)){
      $vErreur = true;
      }
	  if(($produit_non_renseigne['fic_de'] == "") && ($produit_non_renseigne['estEurope'] == 1)){
      $vErreur = true;
      }	  
      if(($produit_non_renseigne['fic_fds_fr'] == "") && ($produit_non_renseigne['is_dangerous'] == 1)){
      $vErreur = true;
      }
      if(($produit_non_renseigne['fic_fds_en'] == "") && ($produit_non_renseigne['is_dangerous'] == 1)){
      $vErreur = true;
      }
      if(($produit_non_renseigne['fic_decl_four'] == "") && ($produit_non_renseigne['estEurope'] == 0)){
      $vErreur = true;
      }

      if($produit_non_renseigne['is_dangerous'] == 1){
      $req_compte_mode_tpt = $link->query("select id_transporter from transporter where sap_prod = '".$produit_non_renseigne['sap_prod']."';");
      $compte_mode_tpt =$req_compte_mode_tpt->rowCount();
      if($compte_mode_tpt == 0){$vErreur = true;}
      }

      if($produit_non_renseigne['estEurope'] == 1){

      $i++;

      $req_compte_zone =$link->query("select id_lien_produit_zone from lien_produit_zone where sap_prod = '".$produit_non_renseigne['sap_prod']."';");
      $compte_zone =$req_compte_zone->rowCount();
      if($compte_zone == 0){$vErreur = true;}

      if($produit_non_renseigne['date_deb_validite'] == "0000-00-00"){
      $vErreur = true;
      }
      if($produit_non_renseigne['date_fin_validite'] == "0000-00-00"){
      $vErreur = true;
      }
      }

      }

      if($vErreur){$k++;}

      } */

    /*
      LANGAGE
     */
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        $lang = 'en';
    }

    if ($j > 1) {
        $s1 = "s";
    } else {
        $s1 = "";
    }
    if ($i > 1) {
        $s2 = "s";
    } else {
        $s2 = "";
    }
    if ($k > 1) {
        $s3 = "s";
    } else {
        $s3 = "";
    }

    $trad = array(
        'fr' => array(
            'title' => 'GED Lyreco',
            'welcome' => 'Bonjour, ',
            'p1' => 'Bienvenue sur la plateforme de gestion des données de Lyreco.',
            'p2' => 'Elle permet à Lyreco de répertorier les données relatives aux origines, provenances et dangerosités de vos produits. Il est donc important que vous remplissiez les informations demandées avec le plus de précision possible.',
            'p3' => 'Nous vous demanderons de remplir les données au moins 1 fois par an en début d\'année, avec les données concernant les nouveaux produits et pour mise à jour des documents officiels, notamment les déclarations long terme; ou dès que vous avez un changement d\'origine d\'un ou plusieurs de vos produits par exemple.',
            'p4' => 'Vous pouvez accéder à cette plateforme directement avec votre identifiant et mot de passe, qui sont modifiables à tout moment dans le menu.',
            'p5' => 'Vous trouverez dans la rubrique "Aide" un guide donnant plus d\'explications sur les informations à renseigner.',
            'p6' => 'Si malgré cela, des questions persistent, n\'hésitez pas à vous rapprocher du chef de produit en charge de votre compte chez Lyreco France.',
            'p7' => 'Merci.',
            'p8' => 'Cordialement,<br>L\'équipe Lyreco France.',
            'data' => '<h3>Synthèse de vos produits.</h3>',
            's1' => "Vous avez un total de " . $j . " produit" . $s1 . ".<br>",
            's2' => "Vous avez un total de " . $i . " produit" . $s2 . " d'origine européenne.<br>",
            's3' => "Vous avez un total de " . $k . " produit" . $s3 . " misinformed.<br /><br />",
            'error' => "Vous n'êtes pas connecté."
        ),
        'en' => array(
            'title' => 'GED Lyreco',
            'welcome' => 'Hi, ',
            'p1' => 'Welcome to Lyreco\'s online database.',
            'p2' => 'This database is designed to host data related to products\' sources, origins and hazards. It is really important that you precisely fill in any requested information.',
            'p3' => 'We ask you to fill in the necessary information regarding your products at least once a year in January with the new products data and with the updating of official documents such as Long-term declarations, or whenever there is a change in the origin of one of your products, for example.',
            'p4' => 'You can access to this online database directly with your login and password, which can be changed at any moment in the menu.',
            'p5' => 'You will find in the "Aide" guidelines on the data required.',
            'p6' => 'Should you have any questions, please contact directly the product manager in charge of your account at Lyreco France.',
            'p7' => 'Thanks.',
            'p8' => 'Best Regards,<br>Lyreco France team.',
            'data' => '<h3>Summary of your products.</h3>',
            's1' => "You have a total of " . $j . " product" . $s1 . ".<br>",
            's2' => "You have a total of " . $i . " product" . $s2 . " from EU.<br>",
            's3' => "You have a total of " . $k . " product" . $s3 . " misinformed.<br /><br />",
            'error' => "You are not login."
        )
    );


    /*
      LANGAGE
     */
    ?>

    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2><?php echo $trad[$lang]['welcome'] . " " . $_SESSION['user_name']; ?></h2>
                    <h3><?php echo $trad[$lang]['p1']; ?></h3>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p2']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p3']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p4']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p5']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p6']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p7']; ?></p>
                    <p class="lead section-lead"><?php echo $trad[$lang]['p8']; ?></p>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--</td>
        </tr>
        <tr>
            <td valign="top" style="padding:15px;">

           </td>
            <td valign="top" style="padding:15px;">
           </td>
        </tr>
        <tr>
           <td valign="top" style="padding:15px;">
               <h3>Paramètres du compte.</h3>
               <a href='update_password.php'>Modifier mon mot de passe</a><br /><br /><br />
           </td>
           <td valign="top" style="padding:15px;">
               <h3>Account settings.</h3>
               <a href='update_password.php'>Change my password</a><br /><br /><br />
           </td>
       </tr>
    </table>
    </div>

    </body>-->

    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
