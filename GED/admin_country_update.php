<?php
include ('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include('./scripts/head.php');
include('./scripts/menu.php');
include('./scripts/banner.php');

$defaut_lang = 'fr';
if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == 'en')) {
            $lang = $_GET['lang'];
        } else {
            $lang = $defaut_lang;
        }
    } else {
        $lang = $defaut_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_country_update = array(
    'fr' => array(
        'title_form' => 'Modifier des pays',
        'pays' => 'Pays',
        'code_country' => 'Code du pays ',
        'nom_country' => 'Nom du pays',
        'update' => 'Modifier'
    ),
    'en' => array(
        'title_form' => 'Update countries',
        'pays' => 'Country',
        'code_counrty' => 'Code of country',
        'nom_country' => 'Name of country',
        'update' => 'Update'
    )
);

//Partie SQL
$reponse_pays = $link->query("SELECT * from pays ORDER BY lib_pays ASC");

if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-horizontal" name="formUpdateZone" data-toggle="validator" role="form" action="admin_country_update_infos.php" method="post">
                        <fieldset>
                            <legend><?php echo $trad_admin_country_update[$lang]['title_form']; ?></legend>
                            <div class="form-group">
                                <label for="select" class="col-md-4 control-label"><?php echo $trad_admin_country_update[$lang]['pays'] ?> : </label>
                                <div class="col-md-4">
                                    <select id="country" name="country" class="form-control">
    <?php
    while ($donnees_pays = $reponse_pays->fetch(PDO::FETCH_BOTH)) {
        echo "<option value='" . $donnees_pays['code_pays'] . "'>" . $donnees_pays['lib_pays'] . "</option>";
    }
    ?>
                                    </select>
                                </div>
                            </div>
                            </br>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_update"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_update" class="btn btn-primary" name="btn_update"><?php echo $trad_admin_country_update[$lang]['update']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
} else {
    echo '<h2 class="lead section-lead-has-error">' . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>