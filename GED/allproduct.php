<?php
include('./scripts/dbc.php');
page_protect();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$lst_Countrys = loadPays();
$lst_Suppliers = loadSupplier();
$lst_zones = loadZone();
$lst_modes = loadMode();
$lst_dangers = loadDang();
$lst_embals = loadEmbal();
$url = explode("?", $_SERVER['REQUEST_URI']);

if (empty($url[1])) {
    $url[1] = "page=1";
}

//var_dump($_SERVER['REQUEST_URI'], $url[1]);die;

/* foreach ($lst_Countrys as $key => $value) {
  var_dump($key);
  var_dump($value);
  }
  die; */
/* var_dump($lst_zones);
  die; */

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}


$trad_allproduct = array(
    'fr' => array(
        'title' => 'Mes produits',
        'th_sap' => 'Code SAP',
        'th_four' => 'Fournisseur',
        'th_desi' => 'Designation',
        'th_c_prod' => 'Code produit',
        'th_prov' => 'Provenance',
        'th_prov_pays' => 'Pays',
        'th_prov_cp' => 'Code Postal',
        'th_prov_vill' => 'Ville',
        'th_orig' => 'Pays d\'origine',
        'th_zone' => 'Zones préférentielles',
        'th_preu' => 'Preuve d\'origine préférentielle',
        'th_preu_deb' => 'Début',
        'th_preu_fin' => 'Fin',
        'th_c_doua' => 'Code douanier',
        'th_dang' => 'Danger',
        'th_dang_clas' => 'Classe de danger',
        'th_dang_onu' => 'Numéro ONU',
        'th_dang_emba' => 'Groupe d\'emballage',
        'th_dang_mode' => 'Mode de transport',
        'th_fic' => 'Fichiers',
        'th_fic_dlt' => 'DLT',
		'th_fic_dlt_pemd' => 'DLT PEMD',
		'th_fic_de' => 'DE',
		'th_fic_fds_fr' => 'FDS FR',
        'th_fic_fds_en' => 'FDS EN',
        'th_fic_dof' => 'Déclaration d\'origine',
        'search' => 'Recherche (code produit, description, ...)',
        'hide_title' => 'Masquer les colonnes',
        'filter_title' => 'Filtrer les produits'
    ),
    'en' => array(
        'title' => 'My Products',
        'th_sap' => 'Code SAP',
        'th_four' => 'Supplier',
        'th_desi' => 'Description',
        'th_c_prod' => 'Supplier art. nber.',
        'th_prov' => 'Shipping',
        'th_prov_pays' => 'Country',
        'th_prov_cp' => 'Zip code',
        'th_prov_vill' => 'Town',
        'th_orig' => 'Country of origin',
        'th_zone' => 'Preferential zones',
        'th_preu' => 'Proof of preferential origin',
        'th_preu_deb' => 'Start',
        'th_preu_fin' => 'End',
        'th_c_doua' => 'Customs code',
        'th_dang' => 'Hazard',
        'th_dang_clas' => 'Hazard class',
        'th_dang_onu' => 'UN Number',
        'th_dang_emba' => 'Packing group',
        'th_dang_mode' => 'Transport mode',
        'th_fic' => 'Files',
        'th_fic_dlt' => 'LTH',
		'th_fic_dlt_pemd' => 'DLT PEMD',
		'th_fic_de' => 'DC',
		'th_fic_fds_fr' => 'SDF FR',
        'th_fic_fds_en' => 'SDF EN',
        'th_fic_dof' => 'Origin declaration',
        'search' => 'Search (sku, description, ...)',
        'hide_title' => 'Hide Columns',
        'filter_title' => 'Filter products'
    )
);

//Partie SQL
$req_douanier = $link->query("SELECT sap_prod FROM produits");

if (isset($_SESSION['user_level'])) {

    $pdo_nb_produits = $link->query("Select sap_prod from produits");
    $nb_produits = $pdo_nb_produits->rowCount();
    $nb_pages = ceil($nb_produits / 100);
    ?>

    <style>
        .center-block {
            width:100px;
            float: none;
            margin-left: auto;
            margin-right: auto;
        }

        .input-group .icon-addon .form-control {
            border-radius: 0;
        }

        .icon-addon {
            position: relative;
            color: #555;
            display: block;
        }

        .icon-addon:after,
        .icon-addon:before {
            display: table;
            content: " ";
        }

        .icon-addon:after {
            clear: both;
        }

        .icon-addon.addon-md .glyphicon,
        .icon-addon .glyphicon, 
        .icon-addon.addon-md .fa,
        .icon-addon .fa {
            position: absolute;
            z-index: 2;
            left: 10px;
            font-size: 14px;
            width: 20px;
            margin-left: -2.5px;
            text-align: center;
            padding: 10px 0;
            top: 1px
        }

        .icon-addon.addon-lg .form-control {
            line-height: 1.33;
            height: 46px;
            font-size: 18px;
            padding: 10px 16px 10px 40px;
        }

        .icon-addon.addon-sm .form-control {
            height: 30px;
            padding: 5px 10px 5px 28px;
            font-size: 12px;
            line-height: 1.5;
        }

        .icon-addon.addon-lg .fa,
        .icon-addon.addon-lg .glyphicon {
            font-size: 18px;
            margin-left: 0;
            left: 11px;
            top: 4px;
        }

        .icon-addon.addon-md .form-control,
        .icon-addon .form-control {
            padding-left: 30px;
            float: left;
            font-weight: normal;
        }

        .icon-addon.addon-sm .fa,
        .icon-addon.addon-sm .glyphicon {
            margin-left: 0;
            font-size: 12px;
            left: 5px;
            top: -1px
        }

        .icon-addon .form-control:focus + .glyphicon,
        .icon-addon:hover .glyphicon,
        .icon-addon .form-control:focus + .fa,
        .icon-addon:hover .fa {
            color: #2580db;
        }
        .danger-td {
            background-color: #d37a7a;
        }
        .selection-multiple{
            text-align: left;
        }
        #search_code_sap {
            height: 22px;
            font-size: 14px;
            line-height: 22px;
        }
        #prod_page {
            height: 22px;
            font-size: 10px;
            line-height: 22px;
        }
        #search_icon {
            height: 7px;
            font-size: 7px;
            line-height: 7px;
        }
    </style>
    <section id="content_page">
        <div class="row-fluid" style="text-align:center">
            <h1 style="padding-top: 0px;margin-top: 0px;"><?= $trad_allproduct[$lang]['title']; ?></h1>
            <div id="filtre" class="col-md-12" style="border-width: 1px; border-style: dotted; border-color: black; padding: 5px;">
                <br>
                <legend style="font-size: 14px;"><span id="form_hide_allproduct_gly" class="glyphicon glyphicon-collapse-down" aria-hidden="true"></span><?= $trad_allproduct[$lang]['hide_title']; ?></legend>
                <div id="hide_columns">
                    <?php
                    if (isset($_SESSION["user_level"])) {
                        if ($_SESSION["user_level"] > 1) {
                            echo '<div class="col-md-2"><input name="remote" remote-for="col_sap" id="switch-sap" type="checkbox" data-label-text="' . mb_strimwidth($trad_allproduct[$lang]['th_sap'], 0, 17, "...") . '"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>';
                            echo '<div class="col-md-2"><input name="remote" remote-for="col_four" id="switch-four" type="checkbox" data-label-text="' . mb_strimwidth($trad_allproduct[$lang]['th_four'], 0, 17, "...") . '"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>';
                        }
                    } else {
                        logout();
                    }
                    ?>
                    <div class="col-md-2"><input name="remote" remote-for="col_c_prod" id="switch-c-prod" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_c_prod'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_desi" id="switch-desi" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_desi'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_prov" id="switch-prov" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_prov'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_orig" id="switch-orig" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_orig'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_zone" id="switch-zone" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_zone'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_preu" id="switch-preu" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_preu'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_c_doua" id="switch-doua" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_c_doua'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_dang" id="switch-dang" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_dang'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <div class="col-md-2"><input name="remote" remote-for="col_fic" id="switch-fic" type="checkbox" data-label-text="<?= mb_strimwidth($trad_allproduct[$lang]['th_fic'], 0, 17, "..."); ?>"  data-label-width="100" data-size="mini" data-off-color="danger" data-on-color="success" checked></div>
                    <br><br><br>
                </div>
                <legend style="font-size: 14px;"><span id="form_filter_allproduct_gly" class="glyphicon glyphicon-collapse-down" aria-hidden="true"></span><?= $trad_allproduct[$lang]['filter_title']; ?><span id="resume_filter"></span></legend>

                <form class="form-horizontal form-condensed" id="form_filter_allproduct">
                    <fieldset id="filter_fieldset">

                        <!-- Form Name -->

                        <?php
                        if (isset($_SESSION["user_level"])) {
                            if ($_SESSION["user_level"] > 1) {
                                ?>
                                <!-- Select Multiple -->
                                <div class="form-group col-md-2">
                                    <label class="control-label" for="selectmultiple">Code SAP</label><br>
                                    <div class="icon-addon addon-md center-block">
                                        <input type="text" placeholder="" class="form-control" id="filter_sap" name="sap_prod">
                                        <label for="filter_sap" class="glyphicon glyphicon-search" rel="tooltip" title="Code SAP"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="control-label" for="selectmultiple">Fournisseur</label><br>
                                    <select id="filter_four" name="code_four" class="selection-multiple" multiple="multiple">
                                        <option value="NULL">Vide</option>
                                        <?php
                                        foreach ($lst_Suppliers as $key => $value) {
                                            echo '<option value="' . $key . '">' . $value['lib_four'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                        } else {
                            logout();
                        }
                        ?>

                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Code produit fournisseur</label><br>
                            <div class="icon-addon addon-md center-block">
                                <input type="text" placeholder="" class="form-control" id="filter_code_art_four_prod" name="code_art_four_prod">
                                <label for="filter_code_art_four_prod" class="glyphicon glyphicon-search" rel="tooltip" title="Code Produit"></label>
                            </div>
                        </div>                                

                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Pays Provenance</label><br>
                            <select id="filter_pap" name="code_pays" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_Countrys as $key => $value) {
                                    echo '<option value="' . $key . '">' . $value['lib_pays'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Code Postal</label><br>
                            <select id="filter_cp" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Ville</label><br>
                            <select id="filter_ville" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Pays d'Origine</label><br>
                            <select id="filter_pao" name="selectmultiple" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_Countrys as $key => $value) {
                                    echo '<option value="' . $key . '">' . $value['lib_pays'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Zones</label><br>
                            <select id="filter_zone" name="selectmultiple" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_zones as $key => $value) {
                                    echo '<option value="' . $key . '">' . $value['lib_zone_pref'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Date Début</label><br>
                            <select id="filter_date_deb" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                                <option value="obsolete">Périmé</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Date fin</label><br>
                            <select id="filter_date_fin" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                                <option value="obsolete">Périmé</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Code Douanier</label><br>
                            <select id="filter_douane" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Classe de danger</label><br>
                            <select id="filter_danger" name="selectmultiple" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_dangers as $key => $value) {
                                    echo '<option value="' . $key . '">' . $key . ' ' . $value['lib_danger'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Code ONU</label><br>
                            <select id="filter_onu" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="null">Vide</option>
                                <option value="not_null">Non vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Emballage</label><br>
                            <select id="filter_embal" name="selectmultiple" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_embals as $key => $value) {
                                    echo '<option value="' . $key . '">' . $key . ' ' . $value['lib_embal'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Mode transport</label><br>
                            <select id="filter_tpt" name="selectmultiple" class="selection-multiple" multiple="multiple">
                                <option value="NULL">Vide</option>
                                <?php
                                foreach ($lst_modes as $key => $value) {
                                    echo '<option value="' . $key . '">' . $key . ' ' . $value['lib_tpt'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">DLT</label><br>
                            <select id="filter_dlt" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
						<!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">DLT PEMD</label><br>
                            <select id="filter_dlt_pemd" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
						<!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">DE</label><br>
                            <select id="filter_de" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">FDS FR</label><br>
                            <select id="filter_fds_fr" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">FDS EN</label><br>
                            <select id="filter_fds_en" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
                        <!-- Select Multiple -->
                        <div class="form-group col-md-2">
                            <label class="control-label" for="selectmultiple">Décl. d'origine</label><br>
                            <select id="filter_dof" name="selectmultiple" class="selection-multiple">
                                <option value="">None selected</option>
                                <option value="not_null">OK</option>
                                <option value="null">Vide</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <br>
                            <button id="update_filter" name="singlebutton" class="btn btn-primary">
                                Gerer les fiches
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-12" id="content"><img src="images/loading.gif" alt="loading..."></div>
            <script>
                $(document).ready(function () {
                    $("#form_filter_allproduct").hide(0);
                    $("#hide_columns").hide(0);
                    $('input[name="remote"]').bootstrapSwitch();

                    $.ajax({
                        url: './scripts/allproduct_hide.php',
                        type: 'GET',
                        data: 'page=1',
                        dataType: 'html',
                        success: function (code_html, statut) { // code_html contient le HTML renvoyé
                            $('#content').html(code_html);
                        },
                        error: function (resultat, statut, erreur) {
                            alert('Erreur : ' + erreur);
                        }
                    });
                });

                $('input[name="remote"]').on('switchChange.bootstrapSwitch', function (event, state) {
                    if (state) {
                        //alert(state);
                        $('td[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                        $('th[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                    } else {
                        //alert(state);
                        $('td[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                        $('th[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                    }
                });

                $("#form_filter_allproduct_gly").click(function () {
                    if ($("#form_filter_allproduct").is(":visible")) {
                        $("#form_filter_allproduct_gly").attr('class', 'glyphicon glyphicon-collapse-down');
                        $("#form_filter_allproduct").hide(500);
                    } else {
                        $("#form_filter_allproduct_gly").attr('class', 'glyphicon glyphicon-collapse-up');
                        $("#form_filter_allproduct").show(500);
                    }

                });
                $("#form_hide_allproduct_gly").click(function () {
                    if ($("#hide_columns").is(":visible")) {
                        $("#form_hide_allproduct_gly").attr('class', 'glyphicon glyphicon-collapse-down');
                        $("#hide_columns").hide(500);
                    } else {
                        $("#form_hide_allproduct_gly").attr('class', 'glyphicon glyphicon-collapse-up');
                        $("#hide_columns").show(500);
                    }

                });

                $("#filter_sap").keyup(function () {
                    loadFilterResult("filter_sap");
//                    console.log("KEYUP : filter_sap");
                });

                $("#filter_code_art_four_prod").keyup(function () {
                    loadFilterResult("filter_code_art_four_prod");
//                    console.log("KEYUP : filter_code_art_four_prod");
                });
            </script>
        </div>
    </section>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
