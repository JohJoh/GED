<?php

// Connexion à la base de données 
include('./scripts/dbc.php');

$res = $link->query('SELECT 
    produits.sap_prod as code_sap, 
    fournisseurs.lib_four as fournisseur,
    produits.code_art_four_prod as code_art_four,
    produits.designation as design,
    pap.lib_pays as pays_prov,
    produits.code_postal_prov as cp,
    produits.ville_prov as ville,
    pao.lib_pays as pays_orig,
    group_concat(DISTINCT lien_produit_zone_pays.code_zone_pref order by lien_produit_zone_pays.code_zone_pref asc separator ",") as "zone",
    produits.date_deb_validite as date_deb,
    produits.date_fin_validite as date_fin,
    produits.code_douanier as code_doua,
    produits.code_danger as code_dang,
    produits.code_onu as onu,
    produits.code_embal as code_emba,
    group_concat(DISTINCT transporter.code_tpt order by transporter.code_tpt asc separator ",") as "mode",
    contenance_prod as contenance,
	type_prod as type,
    condi_four as conditionnement,
    IF(produits.is_dangerous = 0, "NON", "OUI") as dangereux,
    IF(produits.fic_dlt IS NULL, "NON", "OUI") as dlt,
	IF(produits.fic_dlt_pemd IS NULL, "NON", "OUI") as dlt_pemd,
	IF(produits.fic_de IS NULL, "NON", "OUI") as de,
	IF(produits.fic_fds_fr = 0, "OUI", "NON") as fds_fr,
    IF(produits.fic_fds_en = 0, "OUI", "NON") as fds_en,
    IF(produits.fic_decl_four = 0, "OUI", "NON") as decl_four
FROM 
    produits
LEFT JOIN pays pao ON produits.code_pays_origine = pao.code_pays
LEFT JOIN pays pap ON produits.code_pays = pap.code_pays
LEFT JOIN douane ON produits.code_douanier = douane.code_douanier
LEFT JOIN fournisseurs ON produits.code_four = fournisseurs.code_four
LEFT JOIN onu ON produits.code_onu = onu.code_onu
LEFT JOIN transporter ON produits.sap_prod = transporter.sap_prod
LEFT JOIN lien_produit_zone_pays ON produits.sap_prod = lien_produit_zone_pays.sap_prod
WHERE produits.is_active = 1
GROUP BY produits.sap_prod;');

//headers
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');
header('Content-Type: text/csv');
header("Content-disposition: attachment; filename=produits " . date("d-m-Y") . ".csv");
header('Content-Transfer-Encoding: binary');

//open file pointer to standard output
$fp = fopen('php://output', 'w');

//add BOM to fix UTF-8 in Excel
fputs($fp, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
if ($fp) {
    fputcsv($fp, array("Code SAP", "Fourniseur", "Code produit", "Designation", "Pays de Provenance", "Code postal", "Ville", "Pays d'origine", "Zones", "Date début", "Date fin", "Code douanier", "Code danger", "Code ONU", "Code emballage", "Mode de transport","Contenance","Type" , "Conditionnement", "Produit dangereux ?", "Fichier DLT", "Fichier DLT_PEMD", "Fichier DE", "Fichier FDS FR", "Fichier FDS EN", "Declaration fournisseur"), ";");
    if ($res->rowCount() != 0) {

// on insère les données de la table 
        while ($arrSelect = $res->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($fp, $arrSelect, ";");
        }
    }
}

fclose($fp);
