<?php
include('./scripts/dbc.php');
page_protect(); 
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php"); 
include("./scripts/menu.php"); 
include("./scripts/banner.php"); 
	
	$default_lang = 'fr';
	
	if(!isset($_SESSION['lang'])){
		if(isset($_GET['lang'])){
			if(($_GET['lang']=="fr")||($_GET['lang']=="en")){
				$lang = $_GET['lang'];
			}else{
				$lang = $default_lang;
			}
		}
		else{
			$lang = $default_lang;
		}
	}else{
			$lang = $_SESSION['lang'];
	}
	
	$trad_admin_code_create = array(
		'fr' => array(
			'title' => 'Modifier les renseignements',
			'code' => 'Code douanier',
			'name' =>'Nom de la douane',
			'save' => 'Enregistrer',
			'error_douanecode_1'=>'Le code douanier existe déja',
			'error_douanecode_2'=>'Le code douanier ne peut contenir que des chiffres',
			'retour'=>'Retour'
		),
		'en' => array(
			'title'=>'Edit Information',
			'code'=>'Customs Code',
			'name' => 'Name of the customs',
			'save' => 'Save',
			'error_douanecode_1'=>'The customs code already exists',
			'error_douanecode_2'=>'The customs code may contain numbers',
			'retour'=>'Cancel'
		)
	);
	$code_recupere=$_GET['code'];
	
	//MYSQL
	$req_code=$link->query("SELECT * FROM douane WHERE code_douanier=".$code_recupere.";");
	$infos_code=$req_code->fetch(PDO::FETCH_BOTH);
	if(isset($_SESSION['user_level'])){
?>
<!-- Content Section -->
<section>
    <div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div id="result_req3" class="lead section-lead has-success"></div>
				
				<form class="form-horizontal" name="update_code" data-toggle="validator" role="form" id="update_code">
					<fieldset>
	
						<!-- Form Name -->
						<legend><?php echo $trad_admin_code_create[$lang]['title'];?></legend>
	
						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="code_douanier"><?php echo $trad_admin_code_create[$lang]['code'];?></label>  
							<div class="col-md-4">
								<input 
									id="code_douanier" 
									class="form-control input-md" 
									name="code_douanier" 
									type="text" 
									placeholder="<?php echo $infos_code['code_douanier'];?>" 
									data-remote="./scripts/req_douaniercode.php" 
									data-remote-error="<?php echo $trad_admin_code_create[$lang]['error_douanecode_1'];?>"
									pattern="[0-9]+"
									data-native-error="<?php echo $trad_admin_code_create[$lang]['error_douanecode_2'];?>"
									maxlength="8"
								>
								
								<span class="help-block with-errors"></span>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="nom_douanier"><?php echo $trad_admin_code_create[$lang]['name'];?></label>  
							<div class="col-md-4">
								<input id="nom_douanier" class="form-control input-md" name="nom_douanier" type="text" placeholder="<?php echo $infos_code['lib_douanier'];?>">
								<input id="code_actuel" type="hidden" name="code_actuel" value="<?php echo $code_recupere;?>">
							</div>
						</div>

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="btn_registrer"></label>
							<div class="col-md-4">    
								<button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_code_create[$lang]['save'];?></button><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href='admin_code_douanier_update.php'" ><?php echo $trad_admin_code_create[$lang]['retour'];?></button>
							</div>
						</div>
					
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</section>
<script src="./js/validator.js"></script>
<script src="./js/jquery-admin_code_douanier.js"></script>
<?php 
}else{
	echo "<h2 class='lead section-lead has-error'>".$trad[$lang]['error']."</h2>";
} 
include("./scripts/footer.php");
?>