<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_country_update = array(
    'fr' => array(
        'title' => 'Modifier les informations',
        'code' => 'Code du pays',
        'name' => 'Nom du pays',
        'europeen' => 'Pays européen',
        'yes' => 'Oui',
        'no' => 'Non',
        'error_payscode_1' => 'Le code pays existe déjà.',
        'error_payscode_2' => 'Le code pays ne doit contenir que des lettres',
        'save' => 'Enregistrer',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Edit informations',
        'code' => 'Code of the country',
        'name' => 'Name of the country',
        'europeen' => 'European country',
        'yes' => 'Yes',
        'no' => 'No',
        'error_payscode_1' => 'The country code already exists.',
        'error_payscode_2' => 'The country code must contain only letters ',
        'save' => 'Save',
        'retour' => 'Cancel'
    )
);
$code_country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);

$infos_country = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT * FROM pays WHERE code_pays = :code_country;", 
    [':code_country' => $code_country]
);

//$infos_country = $req_country->fetch(PDO::FETCH_ASSOC);
if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req3" class="lead section-lead has-success"></div>

                    <form id="update_country" class="form-horizontal" name="update_country" data-toggle="validator" role="form">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_country_update[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_pays"><?php echo $trad_admin_country_update[$lang]['code']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_pays" 
                                        class="form-control input-md" 
                                        name="code_pays" 
                                        type="text" 
                                        placeholder="<?php echo $infos_country[0]['code_pays'] ?>" 
                                        data-remote="./scripts/req_payscode.php" 
                                        data-remote-error="<?php echo $trad_admin_country_update[$lang]['error_payscode_1']; ?>"
                                        pattern="[a-zA-Z]+"
                                        data-native-error="<?php echo $trad_admin_country_update[$lang]['error_payscode_2']; ?>"
                                        maxlength="2"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_pays"><?php echo $trad_admin_country_update[$lang]['name']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom_pays" class="form-control input-md" name="nom_pays" type="text" placeholder="<?php echo $infos_country[0]['lib_pays'] ?>">
                                </div>
                            </div>

                            <!-- Multiple Radios (inline) -->
                            <div class="form-group" id="est_europe">
                                <label class="col-md-4 control-label" for="pays_europeen"><?php echo $trad_admin_country_update[$lang]['europeen']; ?></label>
                                <div class="col-md-4"> 
                                    <label class="radio-inline" for="pays_europeen">
                                        <input id="pays_europeen_yes" name="pays_europeen" value="1" <?php echo ($infos_country[0]['estEurope'] === '1')?"CHECKED='checked'":"" ?> type="radio">
                                        <?php echo $trad_admin_country_update[$lang]['yes']; ?>
                                    </label> 
                                    <label class="radio-inline" for="pays_europeen">
                                        <input id="pays_europeen_no" name="pays_europeen" value="0" type="radio"  <?php echo ($infos_country[0]['estEurope'] === '0')?"CHECKED='checked'":"" ?> >
                                        <?php echo $trad_admin_country_update[$lang]['no']; ?>
                                    </label>
                                    <input type="hidden" id="code_actuel" name="code_actuel" value="<?php echo $code_country; ?>">
                                </div>
                            </div>


                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">    
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_country_update[$lang]['save']; ?></button><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'admin_country_update.php'"><?php echo $trad_admin_country_update[$lang]['retour']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_country.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>