<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_user_update_infos = array(
    'fr' => array(
        'title' => 'Modifier les informations',
        'lastname' => 'Nom',
        'firstname' => 'Prénom',
        'username' => 'Nom d\'utilisateur',
        'username_help' => 'Utiliser pour se connecter à la place de l\'adresse email',
        'email' => 'Adresse e-mail',
        'langage' => 'Langue',
        'newpwd1' => 'Nouveau mot de passe',
        'caracteres' => '**6 caractères minimum',
        'newpwd2' => 'Confirmer nouveau mot de passe',
        'account' => 'Type de compte',
        'packer' => 'Emballeur',
        'provider' => 'Fournisseur',
        'userlyreco' => 'Utilisateur Lyreco',
        'administrator' => 'Administrateur',
        'valid_email' => '**email valide',
        'save' => 'Enregistrer',
        'errorpwd1' => 'Votre mot de passe doit comporter 6 caractères minimum.',
        'errorpwd2' => 'Vos mots de passe doivent être identiques.',
        'erroremail1' => 'Votre adresse e-mail n\'est pas valide.',
        'erroremail2' => 'Cette adresse e-mail est déjà utilisée.',
        'errorusrnme1' => 'Le nom d\'utilisateur existe déjà.',
        'errorusrnme2' => 'Votre nom d\'utilisateur doit contenir au moins 6 caractères.',
        'errorusrnme3' => 'Uniquement des lettres et chiffres autorisés.',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Edit informations',
        'lastname' => 'Name',
        'firstname' => 'First name',
        'username' => 'Username',
        'username_help' => 'Use to connect instead of the email address',
        'email' => 'Email address',
        'langage' => 'Language',
        'newpwd1' => 'New Password',
        'caracteres' => '**6 characters minimum',
        'newpwd2' => 'Confirm New Password',
        'account' => 'Type of account',
        'packer' => 'Packer',
        'provider' => 'Provider',
        'userlyreco' => 'User of Lyreco',
        'administrator' => 'Administrator',
        'valid_email' => '**email valid',
        'save' => 'Save',
        'errorpwd1' => 'Your password must contain at least 6 characters long.',
        'errorpwd2' => 'Your passwords must match.',
        'erroremail1' => 'Your e-mail address is not valid.',
        'erroremail2' => 'This email address is already in use.',
        'errorusrnme1' => 'Your username is already used.',
        'errorusrnme2' => 'Your username must be at least 6 characters.',
        'errorusrnme3' => 'Only letters and white space allowed.',
        'retour' => 'Cancel'
    )
);
$id_user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
//MYSQL
$reponse = $link->query("SELECT * FROM fournisseurs ORDER BY lib_four ASC;");
$infos_users = safeParameteredSQLRequestFetch($link, 'SELECT * FROM users WHERE id = :id_user;', [':id_user' => $id_user]);

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req3" class="lead section-lead has-success"></div>

                    <form class="form-horizontal" name="update_user" id="update_user" data-toggle="validator" role="form">
                        <fieldset>
                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_user_update_infos[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom"><?php echo $trad_admin_user_update_infos[$lang]['lastname']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom" class="form-control input-md" name="nom" type="text" placeholder="<?php echo $infos_users[0]['nom_user']; ?>">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="prenom"><?php echo $trad_admin_user_update_infos[$lang]['firstname']; ?></label>  
                                <div class="col-md-4">
                                    <input id="prenom" class="form-control input-md" name="prenom" type="text" placeholder="<?php echo $infos_users[0]['pnom_user']; ?>" >
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group" id="verif-username">
                                <label class="col-md-4 control-label" for="username"><?php echo $trad_admin_user_update_infos[$lang]['username']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="username" 
                                        name="username" 
                                        type="text"
                                        class="form-control input-md" 
                                        data-remote="./scripts/req_username.php"
                                        data-remote-error="<?php echo $trad_admin_user_update_infos[$lang]['errorusrnme1']; ?>"
                                        data-minlength="6" 
                                        data-minlength-error="<?php echo $trad_admin_user_update_infos[$lang]['errorusrnme2']; ?>" 
                                        placeholder="<?php echo $infos_users[0]['user_name']; ?>"
                                        >
                                    <span class="help-block with-errors"><?php echo $trad_admin_user_update_infos[$lang]['username_help']; ?></span>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="email"><?php echo $trad_admin_user_update_infos[$lang]['email']; ?></label>  
                                <div class="col-md-4">
                                    <input id="email" class="form-control input-md" name="email" type="text" placeholder="<?php echo $infos_users[0]['user_email'] ?>"
                                           data-remote="./scripts/req_email.php" 
                                           data-remote-error="<?php echo $trad_admin_user_update_infos[$lang]['errorusrnme1']; ?>"
                                           data-minlength="6" 
                                           data-minlength-error="<?php echo $trad_admin_user_update_infos[$lang]['errorusrnme2']; ?>" 
                                           >
                                    <span class="help-block"><?php echo $trad_admin_user_update_infos[$lang]['valid_email']; ?></span>
                                </div>
                            </div>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password1"><?php echo $trad_admin_user_update_infos[$lang]['newpwd1']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="password1" 
                                        class="form-control input-md" 
                                        name="password1" 
                                        type="password" placeholder=""  
                                        data-minlength="6" 
                                        data-error="<?php echo $trad_admin_user_update_infos[$lang]['errorpwd1']; ?>"
                                        >
                                    <span class="help-block with-errors" id="valid-pwd1"></span>
                                </div>
                            </div>
                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password2"><?php echo $trad_admin_user_update_infos[$lang]['newpwd2']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="password2" 
                                        class="form-control input-md" 
                                        name="password2" 
                                        type="password" 
                                        placeholder="" 
                                        data-error="<?php echo $trad_admin_user_update_infos[$lang]['errorpwd2']; ?>"
                                        data-match="#password1" 
                                        data-match-error="<?php echo $trad_admin_user_update_infos[$lang]['errorpwd2']; ?>"
                                        >
                                    <span class="help-block with-errors" id="valid-pwd2"></span>
                                </div>
                            </div>

                            <div class="form-group"  id="language-group">
                                <label class="col-md-4 control-label" for="language"><?php echo $trad_admin_user_update_infos[$lang]['langage']; ?></label>
                                <div class="col-md-4" >
                                    <div class="radio">
                                        <label for="langage-0">
                                            <input type="radio" name="language" id="langage-fr" value="fr" <?php
                                            if ($infos_users[0]['lang'] == "fr") {
                                                echo 'checked="checked"';
                                            }
                                            ?>>
                                            Francais
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label for="langage-1">
                                            <input type="radio" name="language" id="langage-en" value="en" <?php
                                            if ($infos_users[0]['lang'] == "en") {
                                                echo 'checked="checked"';
                                            }
                                            ?>>
                                            English
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="type_compte"><?php echo $trad_admin_user_update_infos[$lang]['account']; ?></label>
                                <div class="col-md-4">
                                    <select id="type_compte" class="form-control" name="type_compte" required="">
                                        <?php
                                        if ($_SESSION['user_level'] == 5) {
                                            switch ($infos_users[0]['user_level']) {
                                                case 0:
                                                    echo '<option value="0" selected="selected">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="1">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="2">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    echo '<option value="5">' . $trad_admin_user_update_infos[$lang]['administrator'] . '</option>';
                                                    break;
                                                case 1:
                                                    echo '<option value="1" selected="selected">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="0">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="2">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    echo '<option value="5">' . $trad_admin_user_update_infos[$lang]['administrator'] . '</option>';
                                                    break;
                                                case 2:
                                                    echo '<option value="2" selected="selected">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    echo '<option value="0">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="1">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="5">' . $trad_admin_user_update_infos[$lang]['administrator'] . '</option>';
                                                    break;
                                                case 5:
                                                    echo '<option value="5" selected="selected">' . $trad_admin_user_update_infos[$lang]['administrator'] . '</option>';
                                                    echo '<option value="0">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="1">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="2">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                            }
                                        } else {
                                            switch ($infos_users[0]['user_level']) {
                                                case 0:
                                                    echo '<option value="0" selected="selected">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="1">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="2">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    break;
                                                case 1:
                                                    echo '<option value="1" selected="selected">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                                    echo '<option value="0">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="2">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    break;
                                                case 2:
                                                    echo '<option value="2" selected="selected">' . $trad_admin_user_update_infos[$lang]['userlyreco'] . '</option>';
                                                    echo '<option value="0">' . $trad_admin_user_update_infos[$lang]['packer'] . '</option>';
                                                    echo '<option value="1">' . $trad_admin_user_update_infos[$lang]['provider'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group" id="group_fournisseur">
                                <label class="col-md-4 control-label" for="fournisseur"><?php echo $trad_admin_user_update_infos[$lang]['provider']; ?></label>
                                <div class="col-md-4">
                                    <select id="fournisseur" class="form-control" name="fournisseur">
                                        <?php
                                        //var_dump($donnees['code_four'] == $infos_users[0]['code_four']);
                                        while ($donnees = $reponse->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<option value='" . $donnees['code_four'] . "'";
                                            //var_dump($donnees['code_four']);
                                            if ($donnees['code_four'] === $infos_users[0]['code_four']) {
                                                echo ' selected="selected" ';
                                            }
                                            echo ">" . $donnees['code_four'] . " - " . $donnees['lib_four'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="id_user" name="id_user" value="<?php echo $id_user; ?>">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_user_update_infos[$lang]['save']; ?></button><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'admin_user_update.php'" ><?php echo $trad_admin_user_update_infos[$lang]['retour']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_user.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>