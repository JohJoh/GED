<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");
fullDebug();
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_products_import = array
    (
    'fr' => array
        (
        'title' => 'Charger des produits',
        'import' => 'Fichier CSV',
        'valider' => 'Importer',
        'error_inject' => 'Vérifiez votre tableau Excel.',
        'inject_valid' => 'Produit(s) ajouté(s) ou modifié(s)',
        'error_sap_empty' => 'Code SAP vide dans votre tableau Excel.',
        'stoppedByTime' => 'Insertion stoppée, trop de lignes dans le fichier.',
    ),
    'en' => array
        (
        'title' => 'Load products',
        'import' => 'CSV File',
        'valider' => 'Import',
        'error_inject' => 'Check your Excel sheet',
        'inject_valid' => 'Product(s) creating or updating ',
        'error_sap_empty' => 'SAP code is empty in your Excel sheet.',
        'stoppedByTime' => 'Stopped creating/updating: too many lines in Excel file.',
    )
);


if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo '<div class="panel panel-default">';
                    echo '<div class="panel-heading">';
                    echo '<h3 class="panel-title" id="titre_panel">Injecter ou modifier plusieurs produits à la fois </h3>';
                    echo '</div>';
                    echo '<div class="panel-body" id="contenu_panel"><a href="./exemple_injection_produits_csv.csv">Exemple</a>'
                    . '<br><br>Dans le fichier csv :'
                    . '<br>Si vous laissez un champ vide, celui-ci va garder son ancienne valeur. Pour supprimer celle-ci vous devez mettre \'null\' sans apostrophes ni guillemets.<br> '
                    . 'Pour supprimer les fichiers DLT/DLT_PEMD/DE/FDS/DECLARATION D\'ORIGINE mettre les champs concernés à null également.'
                    . '</div>';
                    echo '</div>';
                    ?>
                    <form enctype="multipart/form-data" class="form-horizontal" name="import_csv"  action="admin_import_csv_products.php" data-toggle="validator" role="form" id="import_csv" method="post">
                        <fieldset>
                            <legend><?php echo $trad_admin_products_import[$lang]['title']; ?></legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_csv">
                                    <?php echo $trad_admin_products_import[$lang]['import']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_csv" class="input-file" name="fiche_csv" type="file" accept=".csv">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_valider"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_valider" class="btn btn-primary" name="btn_valider"><?php echo $trad_admin_products_import[$lang]['valider']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
    if (isset($_POST['btn_valider'])) {
        $compteur_de_fours_ajoutes = 0;
        //On ouvre le fichier à importer en lecture seule
        if (is_uploaded_file($_FILES['fiche_csv']['tmp_name'])) {
            $fp = fopen($_FILES['fiche_csv']['tmp_name'], "r");
            $numero_ligne = 0;
            $nb_enreg = 0;
            $prep_insert_four_auto = $link->prepare('INSERT INTO fournisseurs (code_four, lib_four) VALUES (:code_four, :lib_four);');

            /**
             * in_array
             */
            $q_codes_sap = $link->query('SELECT sap_prod FROM produits;', PDO::FETCH_COLUMN, 0);
            $codes_sap = array_fill_keys(array_values($q_codes_sap->fetchAll()), 0);

            $q_code_four = $link->query("SELECT code_four FROM fournisseurs;", PDO::FETCH_COLUMN, 0);
            $codes_four = array_fill_keys(array_values($q_code_four->fetchAll()), 0);

            $q_code_pays = $link->query("SELECT code_pays FROM pays;", PDO::FETCH_COLUMN, 0);
            $codes_pays = array_fill_keys(array_values($q_code_pays->fetchAll()), 0);

            $q_code_douanier = $link->query("SELECT code_douanier FROM douane;", PDO::FETCH_COLUMN, 0);
            $codes_douanier = array_fill_keys(array_values($q_code_douanier->fetchAll()), 0);

            $prep_check_pays_zone_euro = $link->prepare('SELECT code_pays FROM pays WHERE estEurope = 1 and code_pays = :code_pays;');

            $startTime = time();
            $maxTime = ini_get('max_execution_time');
            $stoppedByTime = false;
            while (!feof($fp)) { //on importe le fichier
                $currentTime = time();
                if (($currentTime - $startTime) >= ($maxTime - 5)) {
                    $stoppedByTime = true;
                    break;
                }
                $valeurs = $champs = [];
                $numero_ligne++;

                $ligne = fgets($fp, 4096); //tant qu'on est pas à la fin du fichier, on lit une ligne
                $liste = explode(";", utf8_encode($ligne)); // On récupère les champs séparés par ; dans liste
                if ($numero_ligne > 1) { // On saute la ligne d'en-tête
                    //On assigne les variables
                    if ($liste[0] !== '' && $ligne !== false) {
                        foreach ($liste as $i => $l) {
                            if (trim(strtolower($l)) === 'null') {
                                $liste[$i] = null;
                            } else {
                                $liste[$i] = filter_var(trim($l), FILTER_SANITIZE_STRING);
                            }
                        }
                        list($sap_prod,
                                $code_four,
                                $designation,
                                $code_art_four_prod,
                                $division_prod,
                                $code_pays,
                                $code_postal_prov,
                                $ville_prov,
                                $code_douanier,
                                $code_onu,
                                $code_emballage,
                                $code_danger,
                                $pays_origine,
                                $date_debut_validite,
                                $date_fin_validite,
                                $conditionnement,
                                $contenance,
                                $dlt,
								$dlt_pemd,
								$de,
								$fdsfr,
                                $fdsen,
                                $dof
                                ) = $liste;
                        //var_dump($liste);
                        if ($code_four !== '') {
                            $code_four = str_pad($code_four, 5, "0", STR_PAD_LEFT);
                        }
                        if (!isset($codes_sap[(int) $sap_prod])) {

                            /**
                             * INSERT INTO
                             */
                            //Code SAP
                            if ($sap_prod !== '') {
                                $champs[] = 'sap_prod';
                                $valeurs[':sap_prod'] = $sap_prod;
                            }
                            //CODE PRODUIT FOURNISSEUR
                            if ($code_art_four_prod !== '') {
                                $champs[] = 'code_art_four_prod';
                                $valeurs[':code_art_four_prod'] = $code_art_four_prod;
                            }
                            //DESIGNATION
                            if ($designation !== '') {
                                $champs[] = 'designation';
                                $valeurs[':designation'] = $designation;
                            }
                            //DIVISION PRODUIT
                            if ($division_prod !== '') {
                                $champs[] = 'division_prod';
                                $valeurs[':division_prod'] = $division_prod;
                            }
                            //CODE FOUR
                            if ($code_four !== '') {
                                if (!isset($codes_four[$code_four])) {
                                    // Création du fournisseur avec un libellé vide
                                    if ($prep_insert_four_auto->execute([':code_four' => $code_four, ':lib_four' => 'CREATION AUTOMATIQUE ' . $code_four]) !== false) {
                                        $compteur_de_fours_ajoutes++;
                                        $codes_four[$code_four] = 0; // On l'ajoute
                                    }
                                }
                                $champs[] = 'code_four';
                                $valeurs[':code_four'] = $code_four;
                            }

                            if ($code_pays !== null) {
                                if (isset($codes_pays[(int) $code_pays])) {
                                    $champs[] = 'code_pays';
                                    $valeurs[':code_pays'] = $code_pays;
                                }
                            } else {
                                $champs[] = 'code_pays';
                                $valeurs[':code_pays'] = null;
                            }

                            if ($code_postal_prov !== '') {
                                $champs[] = 'code_postal_prov';
                                $valeurs[':code_postal_prov'] = $code_postal_prov;
                            }

                            if ($ville_prov !== '') {
                                $champs[] = 'ville_prov';
                                $valeurs[':ville_prov'] = $ville_prov;
                            }

                            if ($code_douanier !== null) {
                                if (isset($codes_douanier[(int) $code_douanier])) {
                                    $champs[] = 'code_douanier';
                                    $valeurs[':code_douanier'] = $code_douanier;
                                }
                            } else {
                                $champs[] = 'code_douanier';
                                $valeurs[':code_douanier'] = null;
                            }

                            if ($pays_origine !== null) {
                                if (isset($codes_pays[(int) $pays_origine])) {
                                    $champs[] = 'code_pays_origine';
                                    $valeurs[':code_pays_origine'] = $pays_origine;
                                    $champs[] = 'is_not_pref';
                                    $valeurs[':is_not_pref'] = ($prep_check_pays_zone_euro->execute([':code_pays' => $pays_origine]) !== false && !empty($res_check_pays_zone_euro = $prep_check_pays_zone_euro->fetchAll(PDO::FETCH_ASSOC))) ? 0 : 1;
                                }
                            } else {
                                $champs[] = 'code_pays_origine';
                                $valeurs[':code_pays_origine'] = null;
                            }

                            if ($code_onu !== '') {
                                $champs[] = 'code_onu';
                                $valeurs[':code_onu'] = ($code_onu !== null) ? str_pad($code_onu, 4, '0', STR_PAD_LEFT) : null;
                            }

                            if ($code_emballage !== '') {
                                $champs[] = 'code_embal';
                                $valeurs[':code_embal'] = $code_emballage;
                            }

                            $champs[] = 'is_dangerous';
                            if ($code_danger !== '') {
                                $champs[] = 'code_danger';
                                $valeurs[':code_danger'] = $code_danger;
                                $valeurs[':is_dangerous'] = 1;
                            } else {
                                $valeurs[':is_dangerous'] = 0;
                            }

                            if ($date_debut_validite !== '') {
                                $champs[] = 'date_deb_validite';
                                $valeurs[':date_deb_validite'] = ($date_debut_validite !== null) ? date_eu($date_debut_validite) : null;
                            }

                            if ($date_fin_validite !== '') {
                                $champs[] = 'date_fin_validite';
                                $valeurs[':date_fin_validite'] = ($date_fin_validite !== null) ? date_eu($date_fin_validite) : null;
                            }

                            if ($conditionnement !== '') {
                                $champs[] = 'condi_four';
                                $valeurs[':condi_four'] = $conditionnement;
                            }

                            if ($contenance !== '') {
                                $champs[] = 'contenance_prod';
                                $valeurs[':contenance_prod'] = $contenance;
                            }

                            if ($dlt === null) {
                                $champs[] = 'fic_dlt';
                                $valeurs[':fic_dlt'] = $dlt;
                            }

							if ($dlt_pemd === null) {
                                $champs[] = 'fic_dlt_pemd';
                                $valeurs[':fic_dlt_pemd'] = $dlt_pemd;
                            }
							
							if ($de === null) {
                                $champs[] = 'fic_de';
                                $valeurs[':fic_de'] = $de;
                            }

							if ($fdsfr === null) {
                                $champs[] = 'fic_fds_fr';
                                $valeurs[':fic_fds_fr'] = $dlt;
                            }

                            if ($fdsen === null) {
                                $champs[] = 'fic_fds_en';
                                $valeurs[':fic_fds_en'] = $dlt;
                            }

                            if ($dof === null) {
                                $champs[] = 'fic_decl_four';
                                $valeurs[':fic_decl_four'] = $dlt;
                            }

                            $champs[] = 'is_not_pref';
                            $valeurs[':is_not_pref'] = ($prep_check_pays_zone_euro->execute([':code_pays' => $pays_origine]) !== false && !empty($res_check_pays_zone_euro = $prep_check_pays_zone_euro->fetchAll(PDO::FETCH_ASSOC))) ? 0 : 1;

                            $champs[] = 'is_active';
                            $valeurs[':is_active'] = 1;

                            $sql = "INSERT INTO produits (";
                            foreach ($champs as $champ) {
                                $sql .= $champ . ',';
                            }
                            $sql = mb_substr($sql, 0, -1) . ') VALUES ('; // On retire la virgule, avant
                            foreach ($champs as $champ) {
                                $sql .= sprintf(':%s,', $champ);
                            }

                            $sql = mb_substr($sql, 0, -1) . ');'; // On retire la virgule, avant

                            $prep = $link->prepare($sql);
                            if ($prep->execute($valeurs) !== false) {
                                $nb_enreg++;
                            }

                            //UPDATE
                        } else {

                            //CODE PRODUIT FOURNISSEUR
                            if ($code_art_four_prod !== '') {
                                $champs[] = 'code_art_four_prod';
                                $valeurs[':code_art_four_prod'] = $code_art_four_prod;
                            }

                            //DESIGNATION
                            if ($designation !== '') {
                                $champs[] = 'designation';
                                $valeurs[':designation'] = $designation;
                            }

                            //DIVISION PRODUIT
                            if ($division_prod !== '') {
                                $champs[] = 'division_prod';
                                $valeurs[':division_prod'] = $division_prod;
                            }

                            //CODE FOUR
                            if ($code_four !== '') {
                                if (!isset($codes_four[$code_four])) {
                                    // Création du fournisseur avec un libellé vide
                                    if ($prep_insert_four_auto->execute([':code_four' => $code_four, ':lib_four' => 'CREATION AUTOMATIQUE ' . $code_four]) !== false) {
                                        $compteur_de_fours_ajoutes++;
                                        $codes_four[$code_four] = 0;
                                    }
                                }
                                $champs[] = 'code_four';
                                $valeurs[':code_four'] = $code_four;
                            }

                            if ($code_pays !== null) {
                                if (isset($codes_pays[$code_pays])) {
                                    $champs[] = 'code_pays';
                                    $valeurs[':code_pays'] = $code_pays;
                                }
                            } else {
                                $champs[] = 'code_pays';
                                $valeurs[':code_pays'] = null;
                            }

                            if ($code_postal_prov !== '') {
                                $champs[] = 'code_postal_prov';
                                $valeurs[':code_postal_prov'] = $code_postal_prov;
                            }

                            if ($ville_prov !== '') {
                                $champs[] = 'ville_prov';
                                $valeurs[':ville_prov'] = $ville_prov;
                            }

                            if ($code_douanier !== null) {
                                if (isset($codes_douanier[(int) $code_douanier])) {
                                    $champs[] = 'code_douanier';
                                    $valeurs[':code_douanier'] = $code_douanier;
                                }
                            } else {
                                $champs[] = 'code_douanier';
                                $valeurs[':code_douanier'] = null;
                            }

                            if ($pays_origine !== null) {
                                if (isset($codes_pays[$pays_origine])) {
                                    $champs[] = 'code_pays_origine';
                                    $valeurs[':code_pays_origine'] = $pays_origine;
                                    $champs[] = 'is_not_pref';
                                    $valeurs[':is_not_pref'] = ($prep_check_pays_zone_euro->execute([':code_pays' => $pays_origine]) !== false && !empty($res_check_pays_zone_euro = $prep_check_pays_zone_euro->fetchAll(PDO::FETCH_ASSOC))) ? 0 : 1;
                                }
                            } else {
                                $champs[] = 'code_pays_origine';
                                $valeurs[':code_pays_origine'] = $pays_origine;

                                $champs[] = 'is_not_pref';
                                $valeurs[':is_not_pref'] = 0;
                            }

                            if ($code_onu !== '') {
                                $champs[] = 'code_onu';
                                $valeurs[':code_onu'] = ($code_onu !== null) ? str_pad($code_onu, 4, '0', STR_PAD_LEFT) : null;
                            }

                            if ($code_emballage !== '') {
                                $champs[] = 'code_embal';
                                $valeurs[':code_embal'] = $code_emballage;
                            }

                            if ($code_danger !== '') {
                                $champs[] = 'code_danger';
                                $valeurs[':code_danger'] = $code_danger;
                                $champs[] = 'is_dangerous';
                                $valeurs[':is_dangerous'] = 1;
                            }

                            if ($date_debut_validite !== '') {
                                $champs[] = 'date_deb_validite';
                                $valeurs[':date_deb_validite'] = ($date_debut_validite !== null) ? date_eu($date_debut_validite) : null;
                            }

                            if ($date_fin_validite !== '') {
                                $champs[] = 'date_fin_validite';
                                $valeurs[':date_fin_validite'] = ($date_fin_validite !== null) ? date_eu($date_fin_validite) : null;
                            }

                            if ($conditionnement !== '') {
                                $champs[] = 'condi_four';
                                $valeurs[':condi_four'] = $conditionnement;
                            }

                            if ($contenance !== '') {
                                $champs[] = 'contenance_prod';
                                $valeurs[':contenance_prod'] = $contenance;
                            }

                            if ($dlt === null) {
                                $champs[] = 'fic_dlt';
                                $valeurs[':fic_dlt'] = $dlt;
                            }

							if ($dlt_pemd === null) {
                                $champs[] = 'fic_dlt_pemd';
                                $valeurs[':fic_dlt_pemd'] = $dlt_pemd;
                            }
							
							if ($de === null) {
                                $champs[] = 'fic_de';
                                $valeurs[':fic_de'] = $de;
                            }

							if ($fdsfr === null) {
                                $champs[] = 'fic_fds_fr';
                                $valeurs[':fic_fds_fr'] = $dlt;
                            }

                            if ($fdsen === null) {
                                $champs[] = 'fic_fds_en';
                                $valeurs[':fic_fds_en'] = $dlt;
                            }

                            if ($dof === null) {
                                $champs[] = 'fic_decl_four';
                                $valeurs[':fic_decl_four'] = $dlt;
                            }

                            $champs[] = 'is_active';
                            $valeurs[':is_active'] = 1;

                            $valeurs[':sap_prod'] = $sap_prod;

                            /**
                             * Génération de la requête SQL et exécution
                             */
                            $sql = "UPDATE produits SET ";
                            foreach ($champs as $champ) {
                                $sql .= sprintf('%1$s = :%1$s,', $champ);
                            }
                            $sql = mb_substr($sql, 0, -1) . ' WHERE sap_prod = :sap_prod;'; // On retire la virgule, avant
                            //var_dump($sql, $valeurs);
                            $prep = $link->prepare($sql);
                            if ($prep->execute($valeurs) !== false) {
                                $nb_enreg++;
                            } else {
                                
                            }
                        } // fin else
                    } else {
                        if ($liste[0] === '' && $ligne !== false) {
                            echo "<p style='text-align:center; color:#FF0000;'>" . $trad_admin_products_import[$lang]['error_sap_empty'] . "</p>";
                        }
                    }
                } // FIN On saute la ligne d'en-tête
            } // FIN WHILE
            fclose($fp); // On ferme le fichier
            if ($nb_enreg > 0) {
                if ($stoppedByTime) {
                    echo "<p style='text-align:center; font-weight:bold; color:#800000;'>" . $trad_admin_products_import[$lang]['stoppedByTime'] . "</p>";
                }
                echo "<p style='text-align:center; color:#008000;'>" . $nb_enreg . " " . $trad_admin_products_import[$lang]['inject_valid'] . "</p>";
            } else {
                echo "<p style='text-align:center; color:#FF0000;'>" . $trad_admin_products_import[$lang]['error_inject'] . "</p>";
            }
        } // FIN si le fichier est un fichier uploadé
    }
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
