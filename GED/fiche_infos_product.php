<?php

include('./scripts/dbc.php');
page_protect();
$code_sap = filter_input(INPUT_GET, "q", FILTER_SANITIZE_STRING);

//var_dump($code_sap);die();
if (!checkAdmin() && !checkFourniseurSpool($link, $code_sap)) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");
$allCountries = loadPays();

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_product_edit = array(
    'fr' => array(
        'title' => 'Modifier ce produit',
        'code_sap' => 'Code SAP',
        'provider' => 'Fournisseur',
        'code_prod' => 'Code Produit Fournisseur',
        'designation' => 'Désignation',
        'division' => 'Division',
        'pays_prov' => 'Pays de provenance',
        'code_post' => 'Code Postal',
        'ville' => 'Ville',
        'pays_origine' => 'Pays d\'origine',
        'produit_non_pref' => 'Ce produit ne bénéficie pas de préférence',
        'date_debut' => 'Date de début de validité',
        'date_fin' => 'Date de fin de validité',
        'zone_pref' => 'Zones préférentielles',
        'pays_zone' => 'Pays par zone',
        'code_douane' => 'Code douanier',
        'definition_code' => 'Définition du code ',
        'danger' => 'DANGER',
		'produit_dlt_concerne' => 'Signature manuscrite présente ?',
        'produit_concerne' => 'Ce produit est concerné par cette partie ?',
		'produit_concerne0' => 'NON',
		'produit_concerne1' => 'OUI',
        'classe_danger' => 'Classe de danger',
        'code_onu' => 'Code ONU',
        'code_emballage' => 'Code emballage',
        'mode_transport' => 'Mode de transport',
        'conditionnement' => 'Lot de conditionnement du fournisseur',
        'DLT' => 'Fiche DLT',
		'DLT_PEMD' => 'Fiche DLT PEMD',
		'DE_Radio' => '',
		'DE_PEMD_Radio' => '',
		'DE' => 'Fiche Déclaration Engagement',
		'FDS_fr' => 'Fiche FDS Français',
        'info_fds_en' => 'Je ne dispose pas de cette fiche en anglais',
        'FDS_en' => 'Fiche FDS Anglais',
        'DOF' => 'Fiche Déclaration d\'origine',
        'error_sapcode_1' => 'Le code SAP existe déjà.',
        'error_sapcode_2' => 'Le code SAP ne doit pas être vide',
		'error_fournisseur' => 'Sélectionnez un fournisseur dans la liste.',
		'error_code_fournisseur' => 'Le code Produit Fournisseur ne doit pas être vide.',
		'error_designation' => 'La désignation ne doit pas être vide.',
		'error_division' => 'Sélectionnez une division dans le liste.',
		'error_pays_provenance' => 'Sélectionnez un pays de provenance.',
		'error_code_postal' => 'Le code postal ne doit pas être vide.',
		'error_ville' => 'La ville ne doit pas être vide.',
		'error_pays_origine' => 'Sélectionnez un pays d\'origine.',
		'error_date_debut' => 'La date de début ne doit pas être vide.',
		'error_date_fin' => 'La date de fin ne doit pas être vide.',
		'error_code_douanier' => 'Sélectionnez un code douanier dans la liste.',
		'error_classe_danger' => 'Sélectionnez une classe de danger dans la liste.',
		'error_code_onu' => 'Sélectionnez un code onu dans la liste.',
		'error_code_emballage' => 'Sélectionnez un code emballage dans la liste.',
		'error_quantite' => 'La quantité par emballage intérieur ne doit pas être vide.',
		'error_type_emballage' => 'Le type d\'emballage ne doit pas être vide.',
		'error_conditionnement_fournisseur' => 'Le conditionnement du fournisseur ne doit pas être vide.',
		'error_dlt' => 'L\'ajout de la fiche DLT est obligatoire.',
		'error_dlt_pemd' => 'L\'ajout de la fiche DLT PEMD est obligatoire.',
		'error_de' => 'L\'ajout de la fiche DE est obligatoire.',
		'error_fds_fr' => 'L\'ajout de la fiche FDS est obligatoire.',
		'error_fds_en' => 'L\'ajout de la fiche FDS est obligatoire.',
		'error_dof' => 'L\'ajout de la fiche DOF est obligatoire.',
        'save' => 'Enregistrer',
        'contenance' => 'Quantité par emballage',
		'type' => 'Type d\'emballage',
        'explication_contenance' => 'Fournir la quantité de produit dangereux (en mL, L, g, kg) - exemple : 500 mL, 5L, 100ml',
		'explication_type' => 'Fournir le type d\'emballage - exemple : aérosol métal, bidon plastique, flacon de verre',
        'explication_conditionnement' => 'Ex: Si paquet de 4 stylos, indiquer 4',
        'add' => 'Ajouter',
        'retour' => 'Retour',
        'produit_inconnu' => 'Ce produit n’existe pas.'
    ),
    'en' => array(
        'title' => 'Edit this product',
        'code_sap' => 'SAP code',
        'provider' => 'Provider',
        'code_prod' => 'Supplier Product Code',
        'designation' => 'Designation',
        'division' => 'Division',
        'pays_prov' => 'Country of origin',
        'code_post' => 'Postal code',
        'ville' => 'City',
        'pays_origine' => 'Native country',
        'produit_non_pref' => 'Country of origin product does not benefit from preference',
        'date_debut' => 'Starting date of validity',
        'date_fin' => 'Expiration date',
        'zone_pref' => 'Preferential areas',
        'pays_zone' => 'Country by zone',
        'code_douane' => 'Customs Code',
        'definition_code' => 'Code definition',
        'danger' => 'DANGER',
		'produit_dlt_concerne' => 'Handwritten signature present ?',
        'produit_concerne' => 'This product is affected by this part ?',
		'produit_concerne0' => 'NO',
		'produit_concerne1' => 'YES',
        'classe_danger' => 'Hazard class',
        'code_onu' => 'ONU Code',
        'code_emballage' => 'Packing code',
        'mode_transport' => 'Mode of transport',
        'conditionnement' => 'Supplier\'s packaging package',
        'DLT' => 'DLT plug',
		'DLT_PEMD' => 'DLT PEMD plug',
		'DE_Radio' => '',
		'DE_PEMD_Radio' => '',
		'DE' => 'Statement Statement Commitment',
		'FDS_fr' => 'French FDS file',
        'info_fds_en' => 'I do not have this card in English',
        'FDS_en' => 'FDS Sheet English',
        'DOF' => 'Declaration of Origin form',
        'error_sapcode_1' => 'The SAP code already exists.',
        'error_sapcode_2' => 'The SAP code must not be empty',
		'error_fournisseur' => 'Select a provider from the list.',
		'error_code_fournisseur' => 'The Supplier Product Code must not be empty.',
		'error_designation' => 'The designation must not be empty.',
		'error_division' => 'Select a division from the list.',
		'error_pays_provenance' => 'Select a country of origin.',
		'error_code_postal' => 'The postal code must not be empty.',
		'error_ville' => 'The city must not be empty.',
		'error_pays_origine' => 'Select a country of origin.',
		'error_date_debut' => 'The start date must not be empty.',
		'error_date_fin' => 'The end date must not be empty.',
		'error_code_douanier' => 'Select a customs code from the list.',
		'error_classe_danger' => 'Select a hazard class from the list.',
		'error_code_onu' => 'Select a ONU code from the list.',
		'error_code_emballage' => 'Select a packaging code from the list.',
		'error_quantite' => 'The quantity per inner package must not be empty.',
		'error_type_emballage' => 'The type of packaging must not be empty.',
		'error_conditionnement_fournisseur' => 'The supplier\'s packaging must not be empty.',
		'error_dlt' => 'The addition of the DLT form is mandatory.',
		'error_dlt_pemd' => 'The addition of the DLT PEMD record is mandatory.',
		'error_de' => 'The addition of the DE form is mandatory.',
		'error_fds_fr' => 'The addition of the FDS form is mandatory.',
		'error_fds_en' => 'The addition of the FDS form is mandatory.',
		'error_dof' => 'The addition of the DOF form is mandatory.',
        'save' => 'Record',
        'contenance' => 'Quantity per package',
		'type' => 'Type of packaging',
        'explication_contenance' => 'Provide the amount of hazardous product (in mL, L, g, kg) - example: 500 mL, 5L, 100ml',
		'explication_type' => 'Provide the type of packaging - example: metal aerosol, plastic can, glass vial',
        'explication_conditionnement' => 'Ex: If pack of 4 pens, indicate 4',
        'add' => 'Add',
        'retour' => 'Return',
        'produit_inconnu' => 'This product does not exist.'
    )
);
//Recuperation des données du produit
$sap = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING);
//$url = explode("?", $_SERVER['REQUEST_URI']);

$produit = safeParameteredSQLRequestFetch($link, 'SELECT * FROM produits WHERE sap_prod = :sap_prod;', [':sap_prod' => $sap]);
if (!empty($produit)) {
    $donnees_produit = $produit[0];
    $quantite = explode(' ', $donnees_produit['contenance_prod']);
	$type = explode(' ', $donnees_produit['type_prod']);
} else {
    die($trad_admin_product_edit[$lang]['produit_inconnu']);
}

$reponse_produit_fournisseur = safeParameteredSQLRequestFetch(
        $link, "SELECT * FROM fournisseurs WHERE code_four IN (select code_four from produits where sap_prod = :sap_prod) ORDER BY lib_four", [':sap_prod' => $sap]
);
$donnees_produit_fournisseur = !empty($reponse_produit_fournisseur) ? $reponse_produit_fournisseur[0] : false;


$reponse_produit_pays_provenance = safeParameteredSQLRequestFetch(
        $link, 'SELECT * from pays WHERE code_pays IN (Select code_pays from produits where sap_prod = :sap_prod) ORDER BY lib_pays;', [':sap_prod' => $sap]
);
$donnees_produit_pays_provenance = !empty($reponse_produit_pays_provenance) ? $reponse_produit_pays_provenance[0] : false;


$reponse_produit_pays_origine = safeParameteredSQLRequestFetch(
        $link, 'SELECT * from pays WHERE code_pays IN (select code_pays_origine from produits where sap_prod = :sap_prod) ORDER BY lib_pays;', [':sap_prod' => $sap]
);
$donnees_produit_pays_origine = !empty($reponse_produit_pays_origine) ? $reponse_produit_pays_origine[0] : false;

$reponse_produit_douane = safeParameteredSQLRequestFetch(
        $link, "SELECT * from douane WHERE code_douanier IN (SELECT code_douanier from produits where sap_prod = :sap_prod);", [':sap_prod' => $sap]
);
$donnees_produit_douane = !empty($reponse_produit_douane) ? $reponse_produit_douane[0] : false;

$reponse_produit_danger = safeParameteredSQLRequestFetch(
        $link, 'Select * from danger WHERE code_danger IN (SELECT code_danger from produits where sap_prod = :sap_prod);', [':sap_prod' => $sap]
);
$donnees_produit_danger = !empty($reponse_produit_danger) ? $reponse_produit_danger[0] : false;

$reponse_produit_onu = safeParameteredSQLRequestFetch(
        $link, "Select * from onu WHERE code_onu IN (SELECT code_onu from produits where sap_prod = :sap_prod);", [':sap_prod' => $sap]
);
$donnees_produit_onu = !empty($reponse_produit_onu) ? $reponse_produit_onu[0] : false;

$reponse_produit_emballage = safeParameteredSQLRequestFetch(
        $link, 'SELECT * from emballage WHERE code_embal IN (SELECT code_embal from produits where sap_prod = :sap_prod);', [':sap_prod' => $sap]
);
$donnees_produit_emballage = !empty($reponse_produit_emballage) ? $reponse_produit_emballage[0] : false;

//Partie SQL
$reponse_fournisseur = safeParameteredSQLRequestFetch(
        $link, "SELECT * FROM fournisseurs WHERE code_four NOT IN (select code_four from produits where sap_prod = :sap_prod) ORDER BY lib_four;", [':sap_prod' => $sap]
);

$reponse_code_douanier = safeParameteredSQLRequestFetch(
        $link, 'SELECT * from douane WHERE code_douanier NOT IN (SELECT code_douanier from produits where sap_prod = :sap_prod AND code_douanier != null);', [':sap_prod' => $sap]
);

$reponse_division = $link->query('SELECT sap_prod, division_prod FROM produits;');
$reponse_pays_provenance = $link->query('SELECT * from pays ORDER BY lib_pays;');
$reponse_pays_origine = $link->query('SELECT * from pays order by lib_pays;');
$reponse_zone_pref = $link->query('SELECT * from zone;');
$reponse_mode_transport = $link->query("SELECT * from mode_tpt;");
$reponse_classe_danger = $link->query("SELECT * from danger;");
$reponse_code_onu = $link->query('SELECT * from onu;');
$reponse_code_emballage = $link->query("SELECT * from emballage;");

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="result_req3" class="lead section-lead has-success"></div>
                    <script>
                        /*var popover = $("[data-toggle=popover]").popover({
                         trigger : 'hover',
                         placement : 'bottom',
                         html: 'true'
                         }).on('show.bs.popover', function() {
                         $.ajax({
                         url : 'admin_provider_create.php',
                         success : function(html) {
                         popover.attr('data-content', html);
                         }
                         });
                         });
                         */
                        $(document).ready(function () {
                            var popover = $("[data-toggle=popover]").popover({
                                trigger: 'hover',
                                placement: 'bottom',
                                html: 'true'
                            }).on('show.bs.popover', function () {
                                $.ajax({
                                    url: 'fiche_infos_product.php',
                                    success: function (html) {
                                        popover.attr('data-content', html);
                                    }
                                });
                            });
                        });
                    </script>
                    <form class="form-horizontal" method='post' action="ajax_fiche_infos_product.php" name="fiche_product" enctype="multipart/form-data" data-toggle="validator" role="form" id="fiche_product">
                        <fieldset>

                            <!-- Modifier le produit -->
                            <legend><?= $trad_admin_product_edit[$lang]['title']; ?></legend>

                            <!-- Code SAP-->
                            <div class="form-group">
                                <input type="hidden" id="code_actuel" name="code_actuel" value="<?= $sap ?>">
                            </div>

                            <style>
                                .btn-glyphicon {
                                    padding:8px;
                                    background:#ffffff;
                                    margin-right:4px;
                                }
                                .icon-btn {
                                    padding: 1px 15px 3px 2px;
                                    border-radius:50px;
                                }
                            </style>

                            <?php if(checkAdmin()){ ?>
                            
                            <!-- Fournisseur -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fournisseur"><?= $trad_admin_product_edit[$lang]['provider']; ?></label>
                                <div class="col-md-4">
                                    <select id="fournisseur" class="form-control" name="fournisseur" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_fournisseur']; ?>">
                                        <option value="<?= $donnees_produit_fournisseur['code_four']; ?>"><?= $donnees_produit_fournisseur['lib_four']; ?></option>
										<?php
											foreach ($reponse_fournisseur as $donnees_fournisseur) {
												echo "<option value='" . $donnees_fournisseur['code_four'] . "'>" . $donnees_fournisseur['lib_four'] . "</option>";
											}
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>
                            
                            <!-- Code produit fournisseur -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_produit_fournisseur"><?= $trad_admin_product_edit[$lang]['code_prod']; ?></label>
                                <div class="col-md-4">
									<?php 
									if($donnees_produit['code_art_four_prod'] == null){
										?>
										<input id="code_produit_fournisseur" class="form-control input-md" name="code_produit_fournisseur" type="text" placeholder="<?php echo $donnees_produit['code_art_four_prod']; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_code_fournisseur']; ?>">
										<span class="help-block with-errors"></span>
										<?php
									}else{
										?>
										<input id="code_produit_fournisseur" class="form-control input-md" name="code_produit_fournisseur" type="text" placeholder="<?php echo $donnees_produit['code_art_four_prod']; ?>">
										<?php
									}
									?>                                    
                                </div>
                            </div>

                            <!-- Désignation -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="designation"><?= $trad_admin_product_edit[$lang]['designation']; ?></label>
                                <div class="col-md-4">
									<?php 
									if($donnees_produit['designation'] == null){
										?>
										<input id="designation" class="form-control input-md" name="designation" type="text" placeholder="<?php echo $donnees_produit['designation']; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_designation']; ?>">
										<span class="help-block with-errors"></span>
										<?php
									}else{
										?>
										<input id="designation" class="form-control input-md" name="designation" type="text" placeholder="<?php echo $donnees_produit['designation']; ?>">
										<?php
									}
									?>                                    
                                </div>
                            </div>
                            
                            <!-- Division -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="division"><?= $trad_admin_product_edit[$lang]['division']; ?></label>
                                <div class="col-md-4">
                                    <select id="division" class="form-control" name="division" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_division']; ?>">
                                        <?php
                                        if ($donnees_produit['division_prod'] == '1000') {
                                            ?>
                                            <option value="1000">France</option>
                                            <option value="2000">Group</option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="2000">Group</option>
                                            <option value="1000">France</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>
                            
                            <?php } ?>
                            
                            <!-- Pays de provenance -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pays_provenance"><?= $trad_admin_product_edit[$lang]['pays_prov']; ?></label>
                                <div class="col-md-4">
                                    <select id="pays_provenance" class="form-control" name="pays_provenance" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_pays_provenance']; ?>">
                                        <option value="<?= $donnees_produit_pays_provenance['code_pays']; ?>"><?= $donnees_produit_pays_provenance['lib_pays']; ?></option>
                                        <?php
                                        while ($donnees_pays_provenance = $reponse_pays_provenance->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_pays_provenance['code_pays'] . "'>" . $donnees_pays_provenance['lib_pays'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Code postal -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_postal"><?= $trad_admin_product_edit[$lang]['code_post']; ?></label>
                                <div class="col-md-4">
									<?php 
									if($donnees_produit['code_postal_prov'] == null){
										?>
										<input id="code_postal" class="form-control input-md" name="code_postal" type="text" placeholder="<?php echo $donnees_produit['code_postal_prov'] ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_code_postal']; ?>">
										<span class="help-block with-errors"></span>
										<?php
									}else{
										?>
										<input id="code_postal" class="form-control input-md" name="code_postal" type="text" placeholder="<?php echo $donnees_produit['code_postal_prov'] ?>">
										<?php
									}
									?>
                                </div>
                            </div>

                            <!-- Ville -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="ville"><?= $trad_admin_product_edit[$lang]['ville']; ?></label>
                                <div class="col-md-4">
									<?php 
									if($donnees_produit['ville_prov'] == null){
										?>
										<input id="ville" class="form-control input-md" name="ville" type="text" placeholder="<?php echo $donnees_produit['ville_prov']; ?>"  required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_ville']; ?>">
										<span class="help-block with-errors"></span>
										<?php
									}else{
										?>
										<input id="ville" class="form-control input-md" name="ville" type="text" placeholder="<?php echo $donnees_produit['ville_prov']; ?>">
										<?php
									}
									?>                                    
                                </div>
                            </div>

                            <!-- Pays d'origine -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pays_origine"><?= $trad_admin_product_edit[$lang]['pays_origine']; ?></label>
                                <div class="col-md-4">
                                    <select id="pays_origine" class="form-control" name="pays_origine" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_pays_origine']; ?>">
                                        <option value="<?php echo $donnees_produit_pays_origine['code_pays']; ?>"><?php echo $donnees_produit_pays_origine['lib_pays']; ?></option>
                                        <?php
											while ($donnees_pays_origine = $reponse_pays_origine->fetch(PDO::FETCH_BOTH)) {
												echo "<option value='" . $donnees_pays_origine['code_pays'] . "'>" . $donnees_pays_origine['lib_pays'] . "</option>";
											}
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

							<div class="form-group" id="group_europe">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="no_prefer"></label>
                                    <div class="col-md-4">
                                        <label class="checkbox-inline" for="prefer-0">
                                            <input id="prefer-0" name="prefer-0" type="checkbox" value="0"<?= $donnees_produit['is_not_pref'] == "1" ? ' checked' : '' ?>>
                                            <?= $trad_admin_product_edit[$lang]['produit_non_pref']; ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="group-prefer">

                                    <!-- Date de début -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="date_debut_validite"><?= $trad_admin_product_edit[$lang]['date_debut']; ?></label>
                                        <div class="col-md-4">
                                            <input id="date_debut_validite" class="form-control input-md" name="date_debut_validite" type="date" value="<?php echo $donnees_produit['date_deb_validite']; ?>"required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_date_debut']; ?>">
											<span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <!-- Date de fin -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="date_fin_validite"><?= $trad_admin_product_edit[$lang]['date_fin']; ?></label>
                                        <div class="col-md-4">
                                            <input id="date_fin_validite" class="form-control input-md" name="date_fin_validite" type="date" value="<?php echo $donnees_produit['date_fin_validite']; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_date_fin']; ?>">
											<span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <!-- Zones préférentielles -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="zone_pref"><?= $trad_admin_product_edit[$lang]['zone_pref']; ?></label>
                                        <div class="col-md-4">
                                            <?php
											$zp = '';
                                            $donnees_zone_pref = $reponse_zone_pref->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($donnees_zone_pref as $requete2) {
                                                $checked = '';
                                                $req_liste_zone = "select code_zone_pref from lien_produit_zone_pays where sap_prod = :sap ;";
                                                $req_liste_zone_check = $link->prepare($req_liste_zone);
                                                $req_liste_zone_check -> execute([":sap" => $sap]);
                                                while ($lgn_liste_zone = $req_liste_zone_check->fetch(PDO::FETCH_ASSOC)) {
                                                    //var_dump($requete2, $lgn_liste_zone);
                                                    if (($requete2['code_zone_pref'] === $lgn_liste_zone['code_zone_pref']) && ( $donnees_produit['is_not_pref'] == 0)) {
                                                        $checked = " checked ";
                                                    }
                                                }

                                                echo '<label class="checkbox-inline" for="zone_' . $requete2['code_zone_pref'] . '">';
                                                echo '<input id="zone_' . $requete2['code_zone_pref'] . '" name="zone_' . $requete2['code_zone_pref'] . '" value="' . $requete2['code_zone_pref'] . '" type="checkbox" ' . $checked . '/>';
                                                echo $requete2['code_zone_pref'];
												$zp .= '#zone_'.$requete2['code_zone_pref'] . ' ';
												?>
												<script type="text/javascript">
													var zp='<?php echo $zp;?>'
												</script>
												<?php
                                                echo '</label>';
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class="form-group" id="group_pays">
                                        <label class="col-md-4 control-label" for="pays-zone"><?= $trad_admin_product_edit[$lang]['pays_zone']; ?></label>
                                        <div class="col-md-4">
                                            <span id="pays_par_zone" name="pays_par_zone"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Code douanier-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_douanier"><?= $trad_admin_product_edit[$lang]['code_douane']; ?></label>
                                <div class="col-md-4">
                                    <select id="code_douanier" class="form-control" name="code_douanier" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_code_douanier']; ?>">
                                        <option value="<?php echo $donnees_produit_douane['code_douanier']; ?>"><?php echo $donnees_produit_douane['code_douanier']; ?></option>
                                        <?php
                                        foreach ($reponse_code_douanier as $donnees_code_douanier) {
                                            echo "<option value='" . $donnees_code_douanier['code_douanier'] . "'>" . $donnees_code_douanier['code_douanier'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                    <span class="help-block" id="definition_code_douanier" name="definition_code_douanier"></span>
                                </div>
                            </div>

                            <!-- Partie danger-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="danger"><?= $trad_admin_product_edit[$lang]['danger']; ?></label>
                                <div class="col-md-4">
                                    <label class="checkbox-inline" for="danger-0">
                                        <?php echo $trad_admin_product_edit[$lang]['produit_concerne']; ?> <br>
                                        <input id="danger-0" name="danger-0" value="0" type="radio">
                                        <?php echo $trad_admin_product_edit[$lang]['produit_concerne0']; ?> <br>
										<input id="danger-1" name="danger-0" required="" value="0" type="radio">
                                        <?php echo $trad_admin_product_edit[$lang]['produit_concerne1']; ?>
										<span class="help-block with-errors"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row" id="group_danger">
                                <section class="col-sm-12 table-responsive">
                                    <table class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th><?= $trad_admin_product_edit[$lang]['classe_danger']; ?></th>
                                                <th><?= $trad_admin_product_edit[$lang]['code_onu']; ?></th>
                                                <th><?= $trad_admin_product_edit[$lang]['code_emballage']; ?></th>
                                                <th><?= $trad_admin_product_edit[$lang]['mode_transport']; ?></th>
                                                <th><?= $trad_admin_product_edit[$lang]['contenance']; ?></th>
												<th><?= $trad_admin_product_edit[$lang]['type']; ?></th>
                                                <th><?= $trad_admin_product_edit[$lang]['conditionnement']; ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="classe_danger" name="classe_danger" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_classe_danger']; ?>">
                                                                <?php if ($donnees_produit['code_danger'] == "NULL") { ?>
                                                                    <option></option>
                                                                    <?php
                                                                    while ($donnees_classe_danger = $reponse_classe_danger->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_classe_danger['code_danger'] . "'>" . $donnees_classe_danger['code_danger'] . "</option>";
                                                                    }
                                                                    ?>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $donnees_produit_danger['code_danger']; ?>"><?php echo $donnees_produit_danger['code_danger']; ?></option>
                                                                    <?php
                                                                    while ($donnees_classe_danger = $reponse_classe_danger->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_classe_danger['code_danger'] . "'>" . $donnees_classe_danger['code_danger'] . "</option>";
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="code_onu" name="code_onu" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_code_onu']; ?>">
                                                                <?php if ($donnees_produit['code_onu'] == "NULL") { ?>
                                                                    <option></option>
                                                                    <?php
                                                                    while ($donnees_code_onu = $reponse_code_onu->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_code_onu['code_onu'] . "'>" . $donnees_code_onu['code_onu'] . "</option>";
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <option value="<?php echo $donnees_produit_onu['code_onu']; ?>"><?php echo $donnees_produit_onu['code_onu']; ?></option>
                                                                    <?php
                                                                    while ($donnees_code_onu = $reponse_code_onu->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_code_onu['code_onu'] . "'>" . $donnees_code_onu['code_onu'] . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="code_emballage" name="code_emballage" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_code_emballage']; ?>">
                                                                <?php if ($donnees_produit['code_embal'] == "NULL") { ?>
                                                                    <option></option>
                                                                    <?php
                                                                    while ($donnees_code_emballage = $reponse_code_emballage->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_code_emballage['code_embal'] . "'>" . $donnees_code_emballage['code_embal'] . "</option>";
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <option value="<?php echo $donnees_produit_emballage['code_embal']; ?>"><?php echo $donnees_produit_emballage['code_embal']; ?></option>
                                                                    <?php
                                                                    while ($donnees_code_emballage = $reponse_code_emballage->fetch(PDO::FETCH_BOTH)) {
                                                                        echo "<option value='" . $donnees_code_emballage['code_embal'] . "'>" . $donnees_code_emballage['code_embal'] . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12" id="transporter">
                                                            <?php
															$tpt = ''; 
                                                            $donnees_mode_transport = $reponse_mode_transport->fetchAll();
                                                            foreach ($donnees_mode_transport as $requete3) {
                                                                $checked = '';
                                                                $req_liste_mode = "select * from transporter where sap_prod=" . $donnees_produit['sap_prod'];
                                                                $req_liste_mode_check = $link->query($req_liste_mode);
                                                                $lgn_liste_mode = $req_liste_mode_check->fetch();
                                                                while ($lgn_liste_mode) {
                                                                    if (($requete3['code_tpt'] == $lgn_liste_mode['code_tpt'])) {
                                                                        $checked = "checked";
                                                                    }
                                                                    $lgn_liste_mode = $req_liste_mode_check->fetch();
                                                                }
                                                                echo '<label class="checkbox-inline" for="mode-' . $requete3['code_tpt'] . '">';
                                                                echo '<input name="mode_' . $requete3['code_tpt'] . '" value="' . $requete3['code_tpt'] . '" type="checkbox" ' . $checked . '>';
                                                                echo $requete3['code_tpt'];
																$tpt .= '#id_mode_'.$requete3['code_tpt'] . ' ';
																?>
																<script type="text/javascript">
																	var tpt='<?php echo $tpt;?>'
																</script>
																<?php
                                                                echo '</label>';
                                                            }
                                                            ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10">
                                                            <?php 
															if($quantite[0] == null){
																?>
																<input id="contenance" name="contenance" type="text" placeholder="<?php echo $quantite[0]; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_quantite']; ?>">
																<span class="help-block with-errors"></span>
																<?php
															}else{
																?>
																<input id="contenance" name="contenance" type="text" placeholder="<?php echo $quantite[0]; ?>">
																<?php
															}
															?>															
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select id="unite" name="unite" required="">
                                                                <option></option>
                                                                <option value='ml' <?php if(isset($quantite[1])){ if($quantite[1] === 'ml'){ echo "Selected='selected'"; }} ?> >ml</option>
                                                                <option value="l" <?php if(isset($quantite[1])){ if($quantite[1] === 'l'){ echo "Selected='selected'"; }} ?> >l</option>
                                                                <option value='g' <?php if(isset($quantite[1])){ if($quantite[1] === 'g'){ echo "Selected='selected'"; }} ?> >g</option>
                                                                <option value="kg" <?php if(isset($quantite[1])){ if($quantite[1] === 'kg'){ echo "Selected='selected'"; }} ?> >kg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </td>
												<td>
													<div class="form-group">
														<div class="col-md-8">
                                                            <?php 
															if($type[0] == null){
																?>
																<input id="type" name="type" type="text" placeholder="<?php echo $type[0]; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_type_emballage']; ?>">
																<span class="help-block with-errors"></span>
																<?php
															}else{
																?>
																<input id="type" name="type" type="text" placeholder="<?php echo $type[0]; ?>">
																<?php
															}
															?>
                                                        </div>
													</div>
												</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
															<?php 
															if($donnees_produit['condi_four'] == null){
																?>
																<input id="conditionnement_fournisseur" name="conditionnement_fournisseur" type="text" placeholder="<?php echo $donnees_produit['condi_four']; ?>" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_conditionnement_fournisseur']; ?>">
																<span class="help-block with-errors"></span>
																<?php
															}else{
																?>
																<input id="conditionnement_fournisseur" name="conditionnement_fournisseur" type="text" placeholder="<?php echo $donnees_produit['condi_four']; ?>">
																<?php
															}
															?>                                                           
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="help-block" id="definition_classe_danger" name="definition_classe_danger"></span></td>
                                                <td><span class="help-block" id="definition_code_onu" name="definition_code_onu"></span></td>
                                                <td><span class="help-block" id="definition_code_emballage" name="definition_code_emballage"></span></td>
                                                <td>
                                                    <?php
                                                    foreach ($donnees_mode_transport as $requete4) {
                                                        echo $requete4['code_tpt'] . '=' . $requete4['lib_tpt'] . '</br>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?= $trad_admin_product_edit[$lang]['explication_contenance']; ?>
                                                </td>
												<td>
                                                    <?= $trad_admin_product_edit[$lang]['explication_type']; ?>
                                                </td>
                                                <td>
                                                    <?= $trad_admin_product_edit[$lang]['explication_conditionnement']; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </div>

							<!--Fichier DLT -->
							<div class="form-group" id="groupDLT">
								<label class="col-md-4 control-label" for="fiche_DLT"><?= $trad_admin_product_edit[$lang]['DLT']; ?></label>
								<div class="col-md-4">
									<?php 
									if($donnees_produit['fic_dlt'] == null){
									?>
										<input id="fiche_DLT" class="input-file" name="fiche_DLT" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_dlt']; ?>" type="file" accept="application/pdf"/>
										<span class="help-block with-errors"></span>
									<?php
									}else{
									?>
										<input id="fiche_DLT" class="input-file" name="fiche_DLT" type="file" accept="application/pdf"/>
									<?php
									}
									?>
									<?php
										if (($donnees_produit['fic_dlt'] != "") and ( $donnees_produit['fic_dlt'] !== 'NULL')) {
											echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_dlt' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
										}
									?>
								</div>
							</div>

							<!-- Fichier DE Radio --> 
                            <div class="form-group" id="groupDE_Radio">
                                <label class="col-md-4 control-label" for="fiche_DE_Radio"><?php echo $trad_admin_product_edit[$lang]['DE_Radio']; ?></label>
                                <div class="col-md-4">
                                    <?php echo $trad_admin_product_edit[$lang]['produit_dlt_concerne']; ?> <br>
									<input id="dlt-0" name="dlt-0" value="0" type="radio">
									<?php echo $trad_admin_product_edit[$lang]['produit_concerne0']; ?> <br>
									<input id="dlt-1" name="dlt-0" required="" value="1" type="radio">
									<?php echo $trad_admin_product_edit[$lang]['produit_concerne1']; ?>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>
							
							<!--Fichier DLT PEMD -->
							<div class="form-group" id="groupDLTPEMD">
								<label class="col-md-4 control-label" for="fiche_DLT_PEMD"><?= $trad_admin_product_edit[$lang]['DLT_PEMD']; ?></label>
								<div class="col-md-4">
									<?php 
									if($donnees_produit['fic_dlt_pemd'] == null){
									?>
										<input id="fiche_DLT_PEMD" class="input-file" name="fiche_DLT_PEMD" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_dlt_pemd']; ?>" type="file" accept="application/pdf"/>
										<span class="help-block with-errors"></span>
									<?php
									}else{
									?>
										<input id="fiche_DLT_PEMD" class="input-file" name="fiche_DLT_PEMD" type="file" accept="application/pdf"/>
									<?php
									}
									?>									
									<?php
										if (($donnees_produit['fic_dlt_pemd'] != "") and ( $donnees_produit['fic_dlt_pemd'] !== 'NULL')) {
											echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_dlt_pemd' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
										}
									?>
								</div>
							</div>

							<!-- Fichier DE_PEMD Radio --> 
                            <div class="form-group" id="groupDE_PEMD_Radio">
                                <label class="col-md-4 control-label" for="fiche_DE_PEMD_Radio"><?php echo $trad_admin_product_edit[$lang]['DE_PEMD_Radio']; ?></label>
                                <div class="col-md-4">
                                    <?php echo $trad_admin_product_edit[$lang]['produit_dlt_concerne']; ?> <br>
									<input id="dlt_pemd-0" name="dlt_pemd-0" value="0" type="radio">
									<?php echo $trad_admin_product_edit[$lang]['produit_concerne0']; ?> <br>
									<input id="dlt_pemd-1" name="dlt_pemd-0" required="" value="1" type="radio">
									<?php echo $trad_admin_product_edit[$lang]['produit_concerne1']; ?>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>
							
							<!--Fichier DE -->
							<div class="form-group" id="groupDE">
								<label class="col-md-4 control-label" for="fiche_DE"><?= $trad_admin_product_edit[$lang]['DE']; ?></label>
								<div class="col-md-4">
									<?php 
									if($donnees_produit['fic_de'] == null){
									?>
										<input id="fiche_DE" class="input-file" name="fiche_DE" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_de']; ?>"/>
										<span class="help-block with-errors"></span>
									<?php
									}else{
									?>
										<input id="fiche_DE" class="input-file" name="fiche_DE" type="file" accept="application/pdf"/>
									<?php
									}
									?>
									<?php
									if (($donnees_produit['fic_de'] != "") and ( $donnees_produit['fic_de'] !== 'NULL')) {
										echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_de' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
									}
									?>
								</div>
							</div>

							<!-- Fichier FDS Français -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_FDS_fr"><?= $trad_admin_product_edit[$lang]['FDS_fr']; ?></label>
                                <div class="col-md-4">
                                    <?php 
									if($donnees_produit['fic_fds_fr'] == null){
									?>
										<input id="fiche_FDS_fr" class="input-file" name="fiche_FDS_fr" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_fds_fr']; ?>"/>
										<span class="help-block with-errors"></span>
									<?php
									}else{
									?>
										<input id="fiche_FDS_fr" class="input-file" name="fiche_FDS_fr" type="file" accept="application/pdf"/>
									<?php
									}
									?>
                                    <?php
                                    if (($donnees_produit['fic_fds_fr'] != "") and ( $donnees_produit['fic_fds_fr'] !== 'NULL')) {
                                        echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_fds_fr' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="no_english_file"></label>
                                <div class="col-md-4">
                                    <label class="checkbox-inline" for="no_english_file">
                                        <?php if ($donnees_produit['fic_fds_en'] == "NULL") { ?>
                                            <input id="no_english_file" name="no_english_file" type="checkbox" value="0" checked>
                                            <?= $trad_admin_product_edit[$lang]['info_fds_en']; ?>
                                        <?php } else { ?>
                                            <input id="no_english_file" name="no_english_file" type="checkbox" value="0" >
                                            <?= $trad_admin_product_edit[$lang]['info_fds_en']; ?>
                                        <?php } ?>
                                    </label>
                                </div>
                            </div>

                            <!-- Fichier FDS Anglais -->
                            <div class="form-group" id="fds_en">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="fiche_FDS_en"><?= $trad_admin_product_edit[$lang]['FDS_en']; ?></label>
                                    <div class="col-md-4">
                                        <?php 
										if($donnees_produit['fic_fds_en'] == null){
										?>
											<input id="fiche_FDS_en" class="input-file" name="fiche_FDS_en" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_fds_en']; ?>"/>
											<span class="help-block with-errors"></span>
										<?php
										}else{
										?>
											<input id="fiche_FDS_en" class="input-file" name="fiche_FDS_en" type="file" accept="application/pdf"/>
										<?php
										}
										?>
                                        <?php
                                        if (($donnees_produit['fic_fds_en'] != "") and ( $donnees_produit['fic_fds_en'] != 'NULL')) {
                                            echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_fds_en' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Fichier DOF -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_DOF"><?= $trad_admin_product_edit[$lang]['DOF']; ?></label>
                                <div class="col-md-4">
                                    <?php 
										if($donnees_produit['fic_decl_four'] == null){
										?>
											<input id="fiche_DOF" class="input-file" name="fiche_DOF" type="file" accept="application/pdf" required="" data-native-error="<?php echo $trad_admin_product_edit[$lang]['error_dof']; ?>"/>
											<span class="help-block with-errors"></span>
										<?php
										}else{
										?>
											<input id="fiche_DOF" class="input-file" name="fiche_DOF" type="file" accept="application/pdf"/>
										<?php
										}
										?>
                                    <?php
                                    if (($donnees_produit['fic_decl_four'] != "") and ( $donnees_produit['fic_decl_four'] != 'NULL')) {
                                        echo "<a href='suppr_fic.php?art=" . $donnees_produit['sap_prod'] . "&fic=fic_decl_four' onclick='return confirm(\"Etes-vous certain de vouloir supprimer cette fiche ?\");'>Supprimer</a>";
                                    }
                                    ?>
                                </div>
                            </div>

                            <!-- Bouton Valider -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?= $trad_admin_product_edit[$lang]['save'] . "  "; ?><span class="glyphicon glyphicon-floppy-disk"></span></button>
                                    <button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'allproduct.php'" ><?php echo $trad_admin_product_edit[$lang]['retour']; ?></button>
                                </div>
                            </div>

                            <!--<button type="button" id="id_test" class="btn btn-primary" name="id_test">Test</button>-->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/jquery-admin_product.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
?>

<script>
	var ma_lang = "<?php echo $lang ?>";
</script>
