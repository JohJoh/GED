<?php
include('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_zone_update = array(
    'fr' => array(
        'title' => 'Modifier les informations',
        'code' => 'Code de la zone',
        'name' => 'Nom de la zone',
        'pays' => 'Pays de la zone',
        'error_zonecode_1' => 'Le code zone existe déjà.',
        'error_zonecode_2' => 'Le code zone ne doit contenir que des lettres',
        'save' => 'Enregistrer',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Edit informations',
        'code' => 'Code of the zone',
        'name' => 'Name of the zone',
        'pays' => 'Country of the zone',
        'error_zonecode_1' => 'The zone code already exists.',
        'error_zonecode_2' => 'The zone code must contain only letters ',
        'save' => 'Save',
        'retour' => 'Cancel'
    )
);
$code_recupere = filter_input(INPUT_POST, 'zone', FILTER_SANITIZE_STRING);
$infos_zone = safeParameteredSQLRequestFetch($link, "SELECT * FROM zone WHERE code_zone_pref = :code_zone_pref;", [':code_zone_pref' => $code_recupere]);
if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req3" class="lead section-lead has-success"> </div>
                    <form id="update_zone" class="form-horizontal" name="update_zone" data-toggle="validator" role="form">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_zone_update[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_zone"><?php echo $trad_admin_zone_update[$lang]['code']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_zone" 
                                        class="form-control input-md" 
                                        name="code_zone" 
                                        type="text" 
                                        placeholder="<?php echo $infos_zone[0]['code_zone_pref']; ?>" 
                                        data-remote-error="<?php echo $trad_admin_zone_update[$lang]['error_zonecode_1']; ?>"
                                        pattern="[a-zA-Z]+"
                                        data-native-error="<?php echo $trad_admin_zone_update[$lang]['error_zonecode_2']; ?>"
                                        maxlength="8"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_zone"><?= $trad_admin_zone_update[$lang]['name']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="nom_zone" 
                                        class="form-control input-md" 
                                        name="nom_zone" 
                                        type="text" 
                                        placeholder="<?= $infos_zone[0]['lib_zone_pref']; ?>" 
                                        >
                                    <input type="hidden" name="code_actuel" id="code_actuel" value="<?php echo $code_recupere; ?>">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">    
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_zone_update[$lang]['save']; ?></button>
                                    <button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'admin_zone_update.php'" ><?php echo $trad_admin_zone_update[$lang]['retour']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_zone.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>
