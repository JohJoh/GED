<?php
    require("dbc.php");
    page_protect();
	
	$i=0;

	$all = htmlspecialchars($_GET['all']);
	$eu = htmlspecialchars($_GET['eu']);
	$wrong = htmlspecialchars($_GET['wrong']);
	$sap = htmlspecialchars($_GET['sap']);
	$four = htmlspecialchars($_GET['four']);
	$org = htmlspecialchars($_GET['org']);
	$ppp = htmlspecialchars($_GET['ppp']);
	$page = htmlspecialchars($_GET['page']);
	
	$pays_lib = array();
	$pays_est_europe = array();
	
	$pays_lib['p'] = "Origine Multiple";
	$pays_est_europe['p'] = "0";
	
	$req_pays_pref = "select * from pays";
	$req_pays_pref_check = $link->query($req_pays_pref);
	while($lib_pays = $req_pays_pref_check->fetch(PDO::FETCH_BOTH)){
		$pays_lib[$lib_pays['code_pays']] = $lib_pays['lib_pays'];
		$pays_est_europe[$lib_pays['code_pays']] = $lib_pays['estEurope'];
	}

	
	$limit_deb = ((($page*$ppp)-$ppp));
	
	if($all == "1"){
		$requete = "SELECT * 
				FROM produit
				WHERE 1 ";
	}
	
	if($eu == "1"){
		$requete = "SELECT * 
				FROM `produit` pr
				INNER JOIN pays pa ON pr.code_pays_origine = pa.code_pays
				WHERE pa.estEurope = 1 ";
	}
	
	//a voir
	if($wrong == "1"){
		$requete = "SELECT * 
			FROM produit
			WHERE 1 ";
	}
	
	if($sap !== "0"){
		$requete = "SELECT * 
				FROM produit
				WHERE sap_prod LIKE :sap_1 ";
	}
	
	if($four !== "0"){
		$requete = $requete." AND code_four like :four ";
	}
	
	if($org !== "0"){
			$requete = $requete." AND code_pays_origine like :org ";
	}
	
	$req_num_ligne_check = $link->prepare($requete);
	if($sap !== "0"){
		$sap = "%".$sap."%";
		$req_num_ligne_check->bindValue(':sap_1', $sap, PDO::PARAM_STR);
	}
	if($four !== "0"){
		$req_num_ligne_check->bindValue(':four', $four, PDO::PARAM_STR);
	}
	if($org !== "0"){
		$req_num_ligne_check->bindValue(':org', $org, PDO::PARAM_STR);
	}
	$req_num_ligne_check->execute();
	
	$nb_ligne = $req_num_ligne_check->rowCount();	
	$req_num_ligne_check->closeCursor();	
	$req_num_ligne_check=null;

	//echo $nb_ligne;	
	
	$requete = $requete." LIMIT :lim_deb, :lim_taille";
	$req_lignes = $link->prepare($requete);
	if($sap !== "0"){
		$i++;
		$req_lignes->bindValue(':sap_1', $sap, PDO::PARAM_STR);
	}
	if($four !== "0"){
		$i++;
		$req_lignes->bindValue(':four', $four, PDO::PARAM_STR);
	}
	if($org !== "0"){
		$i++;
		$req_lignes->bindValue(':org', $org, PDO::PARAM_STR);
	}
	$i++;
	$req_lignes->bindValue(':lim_deb', $limit_deb, PDO::PARAM_INT);
	$i++;
	$req_lignes->bindValue(':lim_taille', intval($ppp), PDO::PARAM_INT);
	//echo $requete."</br>".$i;
	$req_lignes->execute();	
	
	?>
    <table border="1" bordercolor="#000000">
        <tr bgcolor="#6699FF">
        <?php
			if(isset($_SESSION['user_level'])){
				if($_SESSION['user_level'] > 2){
					echo "<th rowspan='2'><a href='#' class='info'>Code SAP<span>Lyreco Code</span></a></th>";
					echo "<th rowspan='2'><a href='#' class='info'>Fournisseur<span>Supplier</span></a></th>";
				}
			}
        ?>
            <th rowspan="2"><a href='#' class='info'>Code produit<span>Supplier art. nber</span></a></th>
            <th rowspan="2"><a href='#' class='info'>Designation<span>Description</span></a></th>
            <th colspan="3"><a href='#' class='info'>Provenance<span>Shipping</span></a></th>
            <th rowspan="2"><a href='#' class='info'>Pays d'origine<span>Country of origin</span></a></th>
            <th rowspan="2"><a href='#' class='info'>Zones préférentielles<span>Preferential Zones</span></a></th>
            <th colspan="2"><a href='#' class='info'>Preuve d'origine preferentielle<span>Proof of preferential origin</span></a></th>
            <th rowspan="2"><a href='#' class='info'>Code douanier<span>Customs code</span></a></th>
            <th colspan="4"><a href='#' class='info'>Danger<span>Hazard</span></a></th>
            <th colspan="4"><a href='#' class='info'>Fichiers<span>Files</span></a></th>
            
        </tr>
        <tr bgcolor="#6699FF">
            <th><a href='#' class='info'>Pays<span>Country</span></a></th>
            <th><a href='#' class='info'>Code Postal<span>Zip code</span></a></th>
            <th><a href='#' class='info'>Ville<span>Town</span></a></th>
            <th><a href='#' class='info'>Début<span>Start</span></a></th>
            <th><a href='#' class='info'>Fin<span>End</span></a></th>
            <th><a href='#' class='info'>Classe de danger<span>Hazard class</span></a></th>
            <th><a href='#' class='info'>Numéro ONU<span>UN Number</span></a></th>
            <th><a href='#' class='info'>Groupe d'emballage<span>Packing Group</span></a></th>
            <th><a href='#' class='info'>Mode de transport<span>Transport Mode</span></a></th>
            <th><a href='#' class='info'>DLT<span>Lth</span></a></th>
			<th><a href='#' class='info'>DLT_PEMD<span>Lth</span></a></th>
			<th><a href='#' class='info'>DE<span>Lth</span></a></th>
            <th><a href='#' class='info'>FDS FR<span>SDF</span></a></th>
            <th><a href='#' class='info'>FDS EN<span>SDF</span></a></th>
            <th><a href='#' class='info'>Déclaration d'origine<span>Origin Declaration</span></a></th>
        </tr>
		
        <?php
			$couleur_paire = "#FFFF99";
			$couleur_impaire = "#B9CEFF";
			$couleur_erreur = "#FFB3B3";
		
			if(isset($_SESSION['user_level'])){
				
				$couleur = $couleur_impaire;
				//print_r($req_lignes->errorinfo());
				//echo $req_lignes->rowCount();
				/*echo "\nPDO::errorInfo():\n";
				print_r($req_lignes->errorInfo());*/
				$ligne = 1;
				
				while($result_produits = $req_lignes->fetch(PDO::FETCH_BOTH)){
					
					if($couleur==$couleur_paire){
						$couleur=$couleur_impaire;
					}else{
						$couleur=$couleur_paire;
					}
					
					$est_dangereux = $result_produits['is_dangerous'];
					
					echo "<tr bgcolor='".$couleur."'>";
						if(isset($_SESSION['user_level'])){
							if($_SESSION['user_level'] > 2){
								echo "<td><a href='myproduct.php?num_product=".$result_produits['sap_prod']."'>".$result_produits['sap_prod']."</a></td>";
								$req_four = "select lib_four from fournisseurs where code_four = :code_four;";
								$req_four_check = $link->prepare($req_four);
								$req_four_check->bindValue(":code_four", $result_produits['code_four'], PDO::PARAM_STR);
								$req_four_check->execute();
								if($lib_four = $req_four_check->fetch(PDO::FETCH_BOTH)){
									echo "<td>".$lib_four['lib_four']."</td>";
								}else{
									echo "<td bgcolor='".$couleur_erreur."'>inconnu</td>";
								}
								//echo "<td>".$result_produits['code_four']."</td>";
							}
						}
						if(trim($result_produits['code_art_four_prod'])==""){
							echo "<td bgcolor='".$couleur_erreur."'><a href='myproduct.php?num_product=".$result_produits['sap_prod']."'>non communiqué</a></td>";
						}else{
							echo "<td><a href='myproduct.php?num_product=".$result_produits['sap_prod']."'>".$result_produits['code_art_four_prod']."</a></td>";
						}
						
						if(strlen($result_produits['designation']) < 21){
							$designation = $result_produits['designation'];
						}
						else{
							$designation = substr($result_produits['designation'], 0, 17)."...";
						}
						
						echo "<td>".$designation."</td>";
						
						if($result_produits['code_pays']=="0"||$result_produits['code_pays']==""){
							echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
						}else{
							if(isset($pays_lib[$result_produits['code_pays']])){
								echo "<td>".$pays_lib[$result_produits['code_pays']]."</td>";
							}else{
								echo "<td bgcolor='".$couleur_erreur."'>inconnu</td>";
							}
						}
						
						echo "<td>".$result_produits['code_postal_prov']."</td>";
						
						echo "<td>".$result_produits['ville_prov']."</td>";
						
						if($result_produits['code_pays_origine']=="0"||$result_produits['code_pays_origine']==""){
							echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							$estEurope = 1;
						}else{
							if($result_produits['code_pays_origine']=="p"){
								$estEurope = 0;
								echo "<td>Origine Multiple</td>";
							}else{
								if(isset($pays_lib[$result_produits['code_pays_origine']])){
									$estEurope = $pays_est_europe[$result_produits['code_pays_origine']];
									echo "<td>".$pays_lib[$result_produits['code_pays_origine']]."</td>";
								}else{
									
									echo "<td bgcolor='".$couleur_erreur."'>inconnu</td>";
									$estEurope = 0;
								}
							}
						}
						
						$req_zone = "select * from lien_produit_zone where sap_prod = :code_sap;";
						$req_zone_check = $link->prepare($req_zone);
						$req_zone_check->bindValue(":code_sap", $result_produits['sap_prod'], PDO::PARAM_STR);
						
						$zones = "";
						while($zone = $req_zone_check->fetch(PDO::FETCH_BOTH)){
							$zones = $zones.$zone['code_zone_pref']."/";
						}
						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if($zones==""){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									echo "<td>".substr($zones, 0, (strlen($zones)-1))."</td>";
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}
						
						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if(($result_produits['date_deb_validite']=="0000-00-00") or ($result_produits['date_deb_validite']=="00/00/0000") or ($result_produits['date_deb_validite']=="")){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									$datetime_base = new DateTime($result_produits['date_fin_validite']);
									$datetime_today = new DateTime(date("Y-m-d"));
									$interval = $datetime_base->diff($datetime_today);
									if($interval->format('%R')=="+"){
										echo "<td bgcolor='".$couleur_erreur."'>Périmé</td>";
									}else{
										echo "<td>".date_fr($result_produits['date_deb_validite'])."</td>";
									}
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}
						
						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if(($result_produits['date_fin_validite']=="0000-00-00") or ($result_produits['date_fin_validite']=="00/00/0000") or ($result_produits['date_fin_validite']=="")){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									$datetime_base = new DateTime($result_produits['date_fin_validite']);
									$datetime_today = new DateTime(date("Y-m-d"));
									$interval = $datetime_base->diff($datetime_today);
									if($interval->format('%R')=="+"){
										echo "<td bgcolor='".$couleur_erreur."'>Périmé</td>";
									}else{
										echo "<td>".date_fr($result_produits['date_fin_validite'])."</td>";
									}
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}
						
						if(($result_produits['code_douanier']=="") or ($result_produits['code_douanier']=="0")){
							echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
						}else{
							echo "<td>".$result_produits['code_douanier']."</td>";
						}
						if($est_dangereux == 1){
							if($result_produits['code_danger']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								$req_code_danger = "select lib_danger from danger where code_danger = :danger;";
								$req_code_danger_check = $link->prepare($req_code_danger);
								$req_code_danger_check->bindValue(':danger', $result_produits['code_danger'], PDO::PARAM_STR);
								while($lib_danger = $req_code_danger_check->fetch()){
									echo "<td>".$lib_danger['lib_danger']."</td>";
								}
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}
						
						if($est_dangereux == 1){
							if($result_produits['code_onu']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								echo "<td>".$result_produits['code_onu']."</td>";
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}
						
						if($est_dangereux == 1){
							if($result_produits['code_embal']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								$req_code_embal = "select lib_embal from emballage where code_embal = :embal;";
								$req_code_embal_check = $link->prepare($req_code_embal);
								$req_code_embal_check->bindValue(':embal', $result_produits['code_embal'], PDO::PARAM_STR);
								while($lib_embal = $req_code_embal_check->fetch()){
									echo "<td>".$lib_embal['lib_embal']."</td>";
								}
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}

						if($est_dangereux == 1){
							$req_tpt = "select * from transporter where sap_prod = :sap_prod;";
							$req_tpt_check = $link->prepare($req_tpt);
							$req_tpt_check->bindValue(':sap_prod', $result_produits['sap_prod'], PDO::PARAM_STR);
							$tpts = "";
							while($tpt = $req_tpt_check->fetch()){
								$tpts = $tpts.$tpt['code_tpt']."/";
							}
							
							if($tpts==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								echo "<td>".substr($tpts, 0, (strlen($tpts)-1))."</td>";
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}
					
						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if($result_produits['fic_dlt']==""){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									$datetime_base = new DateTime($result_produits['date_fin_validite']);
									$datetime_today = new DateTime(date("Y-m-d"));
									$interval = $datetime_base->diff($datetime_today);
									if($interval->format('%R')=="+"){
										echo "<td bgcolor='".$couleur_erreur."'>Périmé</td>";
									}else{
										echo "<td><a href='".$result_produits['fic_dlt']."' target='_blank'>OK</a></td>";
									}
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}

						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if($result_produits['fic_dlt_pemd']==""){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									$datetime_base = new DateTime($result_produits['date_fin_validite']);
									$datetime_today = new DateTime(date("Y-m-d"));
									$interval = $datetime_base->diff($datetime_today);
									if($interval->format('%R')=="+"){
										echo "<td bgcolor='".$couleur_erreur."'>Périmé</td>";
									}else{
										echo "<td><a href='".$result_produits['fic_dlt_pemd']."' target='_blank'>OK</a></td>";
									}
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}
						
						if($estEurope == 1){
							if($result_produits['is_not_pref']==1){
								echo "<td>non necessaire</td>";
							}else{         
								if($result_produits['fic_de']==""){
									echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
								}else{
									$datetime_base = new DateTime($result_produits['date_fin_validite']);
									$datetime_today = new DateTime(date("Y-m-d"));
									$interval = $datetime_base->diff($datetime_today);
									if($interval->format('%R')=="+"){
										echo "<td bgcolor='".$couleur_erreur."'>Périmé</td>";
									}else{
										echo "<td><a href='".$result_produits['fic_de']."' target='_blank'>OK</a></td>";
									}
								}
							}
						}else{
							echo "<td>non necessaire</td>";
						}
						
						if($est_dangereux == 1){
							if($result_produits['fic_fds_fr']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								echo "<td><a href='".$result_produits['fic_fds_fr']."' target='_blank'>OK</a></td>";
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}
						
						if($est_dangereux == 1){
							if($result_produits['fic_fds_en']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								echo "<td><a href='".$result_produits['fic_fds_en']."' target='_blank'>OK</a></td>";
							}
						}else{
							echo "<td>Produit non dangereux</td>";
						}
						
						if($estEurope == 1){
							echo "<td>non necessaire</td>";
						}else{
							if($result_produits['fic_decl_four']==""){
								echo "<td bgcolor='".$couleur_erreur."'>non communiqué</td>";
							}else{
								echo "<td><a href='".$result_produits['fic_decl_four']."' target='_blank'>OK</a></td>";
							}
						}
						
					echo "</tr>";
					
					$ligne++;

				}
			}
        ?>
    </table><br /><br />
        <?php 
		$nb_page = ceil($nb_ligne / $ppp);
		if($nb_page > 1){
			echo "<center>Page : <select id='f_page' name='f_page' class='f_select'>"; 
			//echo $nb_page;
			for($i=1; $i<=$nb_page; $i++){
				echo "<option value='$i'";
					if($i==$page){echo " selected='selected'";}
				echo ">$i</option>";
			}
			echo "</select></center>";
		}
		$req_lignes->closeCursor();	
		$req_lignes=null;
		$link = null;
		

	?>
	
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="./js/jquery-allproduct.js"></script>
	<script src="./js/scripts.js"></script>
