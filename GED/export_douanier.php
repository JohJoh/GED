<?php

// Connexion à la base de données 
include('./scripts/dbc.php');

$res = $link->query('SELECT * FROM douane;');

//headers
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');
header('Content-Type: text/csv');
header("Content-disposition: attachment; filename=code douanier " . date("d-m-Y") . ".csv");
header('Content-Transfer-Encoding: binary');

//open file pointer to standard output
$fp = fopen('php://output', 'w');

//add BOM to fix UTF-8 in Excel
fputs($fp, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
if ($fp) {
    fputcsv($fp, array("Code douanier", "Description"), ";");
    if ($res->rowCount() != 0) {

// on insère les données de la table 
        while ($arrSelect = $res->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($fp, $arrSelect, ";");
        }
    }
}

fclose($fp);
