<?php
include('./scripts/dbc.php');
page_protect(); 
include("./scripts/head.php"); 
include("./scripts/menu.php"); 
include("./scripts/banner.php"); 

	$default_lang = 'fr';
	
	if(!isset($_SESSION['lang'])){
		if(isset($_GET['lang'])){
			if(($_GET['lang']=="fr")||($_GET['lang']=="en")){
				$lang = $_GET['lang'];
			}else{
				$lang = $default_lang;
			}
		}
		else{
			$lang = $default_lang;
		}
	}else{
			$lang = $_SESSION['lang'];
	}
	
	$trad_menu = array(
		'fr' => array(
			'title_form' => 'Modifier mon profil',
			'lastname' => 'Nom',
			'firstname' => 'Prénom',
			'username' => 'Nom d\'utilisateur',
			'username_help' => 'Utiliser pour se connecter à la place de l\'adresse email',
			'email' => 'Adresse e-mail',
			'language' => 'Langue',
			'newpwd1' => 'Nouveau mot de passe',
			'newpwd2' => 'Confirmer nouveau mot de passe',
			'save' => 'Enregistrer',
			'errorpwd1' => 'Votre mot de passe doit comporter 6 caractères minimum.',
			'errorpwd2' => 'Vos mots de passe doivent être identiques.',
			'erroremail1' => 'Votre adresse e-mail n\'est pas valide.',
			'erroremail2' => 'Cette adresse e-mail est déjà utilisée.',
			'errorusrnme1' => 'Le nom d\'utilisateur existe déjà.',
			'errorusrnme2' => 'Votre nom d\'utilisateur doit contenir au moins 6 caractères.',
			'errorusrnme3' => 'Uniquement des lettres et chiffres autorisés.',
			'success' => ''
		),
		'en' => array(
			'title_form' => 'Edit profile',
			'lastname' => 'Name',
			'firstname' => 'First name',
			'username' => 'Username',
			'username_help' => 'Use to connect instead of the email address',
			'email' => 'Email address',
			'language' => 'Language',
			'newpwd1' => 'New Password',
			'newpwd2' => 'Confirm New Password',
			'save' => 'Save',
			'errorpwd1' => 'Your password must contain at least 6 characters long.',
			'errorpwd2' => 'Your passwords must match.',
			'erroremail1' => 'Your e-mail address is not valid.',
			'erroremail2' => 'This email address is already in use.',
			'errorusrnme1' => 'Your username is already used.',
			'errorusrnme2' => 'Your username must be at least 6 characters.',
			'errorusrnme3' => 'Only letters and white space allowed.',
			'success' => ''
		)
	);
	
	//Recupération des infos de l'utilisateur
	$pdo_recup_info = $link->query("select pnom_user, nom_user, user_name, user_email, lang from users where id = ".$_SESSION['user_id']);
	list($pnom_user,$nom_user,$user_name,$user_email,$lang) = $pdo_recup_info->fetch(PDO::FETCH_NUM);
	
	if(isset($_SESSION['user_level'])){
?>
<!-- Content Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<div id="result_req" class="lead section-lead has-success"></div>
				
				<form class="form-horizontal" name="formUpdateUser" id="formUpdateUser" data-toggle="validator" role="form">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo $trad_menu[$lang]['title_form']; ?></legend>

				<!-- lastname-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="lastname"><?php echo $trad_menu[$lang]['lastname']; ?></label>  
				  <div class="col-md-4">
				  <input 
					  id="lastname" 
					  name="lastname" 
					  type="text" 
					  placeholder="<?php echo $nom_user; ?>" 
					  class="form-control input-md"
				  >
					
				  </div>
				</div>

				<!-- firstname-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="firstname"><?php echo $trad_menu[$lang]['firstname']; ?></label>  
				  <div class="col-md-4">
				  <input 
					  id="firstname" 
					  name="firstname" 
					  type="text" 
					  placeholder="<?php echo $pnom_user; ?>" 
					  class="form-control input-md"
				  >
				  </div>
				</div>

				<!-- username-->
				<div class="form-group" id="verif-username">
				  <label class="col-md-4 control-label" for="username"><?php echo $trad_menu[$lang]['username']; ?></label>  
				  <div class="col-md-4">
				  <input 
					id="username" 
					name="username" 
					type="text" 
					placeholder="<?php echo $user_name; ?>" 
					class="form-control input-md" 
					data-remote="./scripts/req_username.php" 
					data-remote-error="<?php echo $trad_menu[$lang]['errorusrnme1']; ?>"
					data-minlength="6" 
					data-minlength-error="<?php echo $trad_menu[$lang]['errorusrnme2']; ?>" 
				  >
				  <span class="help-block with-errors"><?php echo $trad_menu[$lang]['username_help']; ?></span>
				  </div>
				</div>

				<!-- email-->
				<div class="form-group" id="verif-email">
				  <label class="col-md-4 control-label" for="email"><?php echo $trad_menu[$lang]['email']; ?></label>
				  <div class="col-md-4">
				  <input 
					  id="email" 
					  name="email" 
					  type="email" 
					  placeholder="<?php echo $user_email; ?>" 
					  class="form-control input-md" 
					  data-error="<?php echo $trad_menu[$lang]['erroremail1']; ?>" 
					  data-remote="./scripts/req_email.php" 
					  data-remote-error="<?php echo $trad_menu[$lang]['erroremail2']; ?>"
				  >
				  <span class="help-block with-errors" id="valid-email"></span>
				  </div>
				</div>

				<!-- language -->
				<div class="form-group" id="language-group">
				  <label class="col-md-4 control-label" for="language"><?php echo $trad_menu[$lang]['language']; ?></label>
				  <div class="col-md-4">
				  <div class="radio">
					<label for="langage-0">
					  <input 
						  type="radio" 
						  name="language" 
						  id="langage-0" 
						  value="fr" 
						  <?php if($lang=="fr"){echo 'checked="checked"';} ?>
					  >
					  Francais
					</label>
					</div>
				  <div class="radio">
					<label for="langage-1">
					  <input 
						  type="radio" 
						  name="language" 
						  id="langage-1" 
						  value="en" 
						  <?php if($lang=="en"){echo 'checked="checked"';} ?>
					  >
					  English
					</label>
					</div>
				  </div>
				</div>

				<!-- Password-->
				<div class="form-group" id="verif-pwd1">
				  <label class="col-md-4 control-label" for="new_pwd1"><?php echo $trad_menu[$lang]['newpwd1']; ?></label>
				  <div class="col-md-4">
					<input 
						id="new_pwd1" 
						name="new_pwd1" 
						type="password" 
						placeholder="" 
						class="form-control input-md" 
						data-minlength="6" 
						data-error="<?php echo $trad_menu[$lang]['errorpwd1']; ?>"
					>
					<span class="help-block with-errors" id="valid-pwd1"></span>
				  </div>
				</div>

				<!-- Confirm-->
				<div class="form-group" id="verif-pwd2">
				  <label class="col-md-4 control-label" for="new_pwd2"><?php echo $trad_menu[$lang]['newpwd2']; ?></label>
				  <div class="col-md-4">
					<input 
						id="new_pwd2" 
						name="new_pwd2" 
						type="password" 
						placeholder="" 
						class="form-control input-md" 
						data-match="#new_pwd1" 
						data-match-error="<?php echo $trad_menu[$lang]['errorpwd2']; ?>"
					>
					<span class="help-block with-errors" id="valid-pwd2"></span>
				  </div>
				</div>

				<!-- submit -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="submit"></label>
				  <div class="col-md-4">
					<button 
						id="submit" 
						name="submit" 
						type="submit" 
						class="btn btn-primary"
					><?php echo $trad_menu[$lang]['save']; ?></button>
				  </div>
				</div>

				</fieldset>
				</form>
            </div>
        </div>
    </div>
</section>
<script src="./js/validator.js"></script>
<script src="./js/jquery-myaccount.js"></script>
<?php 
}else{
	echo "<h2 class='lead section-lead has-error'>".$trad[$lang]['error']."</h2>";
} 
include("./scripts/footer.php");
?>
