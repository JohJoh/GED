<?php

require_once('./scripts/dbc.php');
page_protect();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");
$lang = $_SESSION['lang'];

$trad_ajax_update_products = array(
    'fr' => array(
        'title' => 'Modification réussie',
        'start' => ' Vous avez modifié vos produits',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez le formulaire',
        'retour' => 'Retour',
    ),
    'en' => array(
        'title' => 'Update successful',
        'start' => 'You\'ve updated your products',
        'title_error' => 'An error has occured',
        'error' => 'Check out the form',
        'retour' => 'Cancel',
    )
);

$liste_update_products = $trad_ajax_update_products[$lang]['start'];

$sha1_codes_sap = sha1($_POST["code_actuel"]);
$codes_sap = explode(",", $_POST["code_actuel"]);
foreach ($codes_sap as $i => $code) {
    $codes_sap[$i] = filter_var($code, FILTER_SANITIZE_NUMBER_INT);
}

$req_update_products = "UPDATE produits SET ";

//Fiche DLT
//echo $_FILES['fiche_DLT']['name']."<BR>";
if (isset($_FILES['fiche_DLT']['name'])) {
    $uploaddir_dlt = './uploads/';
    $uploadfile_dlt = $uploaddir_dlt . "dlt_" . $sha1_codes_sap . ".pdf";

    if (file_exists($uploadfile_dlt)) {
        copy($uploadfile_dlt, $uploaddir_dlt . "archives/dlt_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DLT']['tmp_name'], $uploadfile_dlt)) {
        $req_update_products .= "fic_dlt = :fic_dlt, ";

        $sql_params[':fic_dlt'] = $uploadfile_dlt;
    }
}

//Fiche DLT PEMD
//echo $_FILES['fiche_DLT_PEMD']['name']."<BR>";
if (isset($_FILES['fiche_DLT_PEMD']['name'])) {
    $uploaddir_dlt_pemd = './uploads/';
    $uploadfile_dlt_pemd = $uploaddir_dlt_pemd . "dlt_pemd_" . $sha1_codes_sap . ".pdf";

    if (file_exists($uploadfile_dlt_pemd)) {
        copy($uploadfile_dlt_pemd, $uploaddir_dlt_pemd . "archives/dlt_pemd_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DLT_PEMD']['tmp_name'], $uploadfile_dlt_pemd)) {
        $req_update_products .= "fic_dlt_pemd = :fic_dlt_pemd, ";

        $sql_params[':fic_dlt_pemd'] = $uploadfile_dlt_pemd;
    }
}

//Fiche DE
//echo $_FILES['fiche_DE']['name']."<BR>";
if (isset($_FILES['fiche_DE']['name'])) {
    $uploaddir_de = './uploads/';
    $uploadfile_de = $uploaddir_de . "de_" . $sha1_codes_sap . ".pdf";

    if (file_exists($uploadfile_de)) {
        copy($uploadfile_de, $uploaddir_de . "archives/de_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DE']['tmp_name'], $uploadfile_de)) {
        $req_update_products .= "fic_de = :fic_de, ";

        $sql_params[':fic_de'] = $uploadfile_de;
    }
}

//Fiche FDS FR
if (isset($_FILES['fiche_FDS_fr']['name'])) {
    $uploaddir_fds_fr = './uploads/';
    $uploadfile_fds_fr = $uploaddir_fds_fr . "fds_fr_" . $sha1_codes_sap . ".pdf";

    if (file_exists($uploadfile_fds_fr)) {
        copy($uploadfile_fds_fr, $uploaddir_fds_fr . "archives/fds_fr_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_FDS_fr']['tmp_name'], $uploadfile_fds_fr)) {
        $req_update_products .= "fic_fds_fr = :fic_fds_fr, ";

        $sql_params[':fic_fds_fr'] = $uploadfile_fds_fr;
    }
}

//Fiche FDS EN
if (!isset($data['no_english_file'])) {
    if (isset($_FILES['fiche_FDS_en']['name'])) {
        $uploaddir_fds_en = './uploads/';
        $uploadfile_fds_en = $uploaddir_fds_en . "fds_en_" . $sha1_codes_sap . ".pdf";
        if (file_exists($uploadfile_fds_en)) {
            copy($uploadfile_fds_en, $uploaddir_fds_en . "archives/fds_en_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_FDS_en']['tmp_name'], $uploadfile_fds_en)) {
            $req_update_products .= "fic_fds_en = :fic_fds_en, ";

            $sql_params[':fic_fds_en'] = $uploadfile_fds_en;
        }
    }
}

//Fiche DOF
if (isset($_FILES['fiche_DOF']['name'])) {
    $uploaddir_dof = './uploads/';
    $uploadfile_dof = $uploaddir_dof . "dof_" . $sha1_codes_sap . ".pdf";
    if (file_exists($uploadfile_dof)) {
        copy($uploadfile_dof, $uploaddir_dof . "archives/dof_" . $sha1_codes_sap . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DOF']['tmp_name'], $uploadfile_dof)) {
        $req_update_products .= "fic_decl_four = :fic_decl_four, ";

        $sql_params[':fic_decl_four'] = $uploadfile_fds_en;
    }
}


$req_update_products .= "date_dern_modif_prod='" . date('Y-m-d H:i:s') . "' WHERE sap_prod = :sap_prod;";
//$req_update_products=$req_update_products."is_active=1, date_dern_modif_prod='".date('Y-m-d H:i:s')."' WHERE sap_prod ='2' ;";

$i = 0;
$stmt = $link->prepare($req_update_products);
foreach ($codes_sap as $code) {
    $sql_params[':sap_prod'] = $code;
    if ($stmt->execute($sql_params)) {
        $i++;
    }
}

if ($i > 0) {
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_products . '</div>';
    //echo'<meta http-equiv="refresh" content="1; URL=allproduct.php">';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'allproduct.php'" ><?php echo $trad_ajax_update_products[$lang]['retour']; ?></button>
    <?php
} else {
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_update_products[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <center><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = 'allproduct.php'" ><?php echo $trad_ajax_update_products[$lang]['retour']; ?></button></center>
    <?php
}
?>