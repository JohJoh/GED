<?php
include('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_change_fournisseur = array(
    'fr' => array(
        'title' => 'Changer un produit de fournisseur'
    ),
    'en' => array(
        'title' => 'Change provider\'s product'
    )
);


//Partie SQL
$produit = $link->query("SELECT sap_prod, designation, is_active FROM produits");

if (isset($_SESSION['user_level'])) {

    $pdo_nb_produits = $link->query("Select sap_prod from produits");
    $nb_produits = $pdo_nb_produits->rowCount();
    $nb_pages = ceil($nb_produits / 100);
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <legend><?php echo $trad_admin_change_fournisseur[$lang]['title']; ?></legend>
                    <div class="col-md-4" style="float:right;">
                        <div class="input-group" style="text-align:center">
                            <input id="search_code_sap" name="search_code_sap" class="form-control" placeholder="Code SAP" type="text">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                        </div>
                    </div>
                    <div id="content"></div>
                    <script>
                        // init bootpag
                        $(document).ready(function () {
                            $.ajax({
                                url: './scripts/req_admin_changer_fournisseur.php',
                                type: 'GET',
                                data: 'page=1',
                                dataType: 'html',
                                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                                    $('#content').html(code_html);
                                    //console.log('load page: ok');
                                },
                                error: function (resultat, statut, erreur) {
                                    alert('Erreur : ' + erreur);
                                }
                            });
                        });
                        $('#search_code_sap').keyup(function () {
                            $.ajax({
                                url: './scripts/req_admin_changer_fournisseur.php',
                                type: 'GET',
                                data: 'page=1&search=' + $('#search_code_sap').val(),
                                dataType: 'html',
                                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                                    $('#content').html(code_html);
                                    //console.log('load page: ok');
                                },
                                error: function (resultat, statut, erreur) {
                                    alert('Erreur : ' + erreur);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>