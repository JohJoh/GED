<?php
include ('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include('./scripts/head.php');
include('./scripts/menu.php');
include('./scripts/banner.php');

$defaut_lang = 'fr';
if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == 'en')) {
            $lang = $_GET['lang'];
        } else {
            $lang = $defaut_lang;
        }
    } else {
        $lang = $defaut_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_provider_delete = array(
    'fr' => array(
        'title_form' => 'Supprimer des fournisseurs',
        'provider' => 'Fournisseur',
        'code_provider' => 'Code fournisseur ',
        'nom_provider' => 'Nom du fournisseur',
        'delete' => 'Supprimer',
        'suppression_ok' => 'Le fournisseur a été supprimé',
        'suppression_ko' => 'Erreur de suppression'
    ),
    'en' => array(
        'title_form' => 'Delete providers ',
        'provider' => 'Provider',
        'code_provider' => 'Code of prrovider',
        'nom_provider' => 'Name of provider ',
        'delete' => 'Delete',
        'suppression_ok' => 'The provider was deleted',
        'suppression_ko' => 'Error deleting'
    )
);

//Partie SQL
$reponse_fournisseur = $link->query("SELECT * from fournisseurs ORDER BY lib_four ASC");

if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req2" class="lead section-lead has-success"></div>

                    <form class="form-horizontal" name="delete_provider" id="delete_provider" data-toggle="validator" role="form">
                        <fieldset>
                            <legend><?php echo $trad_admin_provider_delete[$lang]['title_form']; ?></legend>
                            <div class="form-group">
                                <label for="select" class="col-md-4 control-label"><?php echo $trad_admin_provider_delete[$lang]['provider']; ?></label>
                                <div class="col-md-4" >
                                    <select id="provider2" class="form-control" name="provider">
    <?php
    while ($donnees_fournisseur = $reponse_fournisseur->fetch(PDO::FETCH_BOTH)) {
        echo "<option value='" . $donnees_fournisseur['code_four'] . "'>" . $donnees_fournisseur['code_four'] . ' - ' . $donnees_fournisseur['lib_four'] . "</option>";
    }
    ?>
                                    </select>
                                </div>
                            </div>
                            </br>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_delete"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_delete" class="btn btn-primary" name="btn_delete"><?php echo $trad_admin_provider_delete[$lang]['delete']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_provider.js"></script>
    <?php
} else {
    echo '<h2 class="lead section-lead-has-error">' . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>