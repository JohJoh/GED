<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';
$code_provider = filter_input(INPUT_POST, 'provider', FILTER_SANITIZE_STRING);

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_provider_create = array(
    'fr' => array(
        'title' => 'Modifier les informations',
        'code' => 'Code fournisseur',
        'name' => 'Nom du fournisseur',
        'save' => 'Enregistrer',
        'error_providercode_1' => 'Le code fournisseur existe déja',
        'error_providercode_2' => 'Le code fournisseur ne peut contenir que des chiffres',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Edit informations',
        'code' => 'Provider Code',
        'name' => 'Name of the provider',
        'save' => 'Save',
        'error_providercode_1' => 'The provider code already exists',
        'error_providercode_2' => 'The provider code may contain numbers',
        'retour' => 'Cancel'
    )
);

$infos_provider = safeParameteredSQLRequestFetch($link, 'SELECT * FROM fournisseurs WHERE code_four = :code_four;', [':code_four' => $code_provider]);
if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req3" class="lead section-lead has-success"></div>
                    <form class="form-horizontal" id="update_provider" name="update_provider" data-toggle="validator" role="form">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_provider_create[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_four"><?php echo $trad_admin_provider_create[$lang]['code']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_four" 
                                        class="form-control input-md" 
                                        name="code_four" 
                                        type="text" 
                                        placeholder="<?php echo $infos_provider[0]['code_four']; ?>" 
                                        data-remote="./scripts/req_providercode.php" 
                                        data-remote-error="<?php echo $trad_admin_provider_create[$lang]['error_providercode_1']; ?>"
                                        pattern="[0-9]+"
                                        data-native-error="<?php echo $trad_admin_provider_create[$lang]['error_providercode_2']; ?>"
                                        maxlength="10"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_four"><?php echo $trad_admin_provider_create[$lang]['name']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom_four" class="form-control input-md" name="nom_four" type="text" placeholder="<?= $infos_provider[0]['lib_four']; ?>">
                                    <input id="code_actuel" name="code_actuel" type="hidden" value="<?= $code_provider; ?>">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">    
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_provider_create[$lang]['save']; ?></button><button type="button" name="retour" class="btn btn-danger" id="retour"onclick="self.location.href = 'admin_provider_update.php'" ><?php echo $trad_admin_provider_create[$lang]['retour']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_provider.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
