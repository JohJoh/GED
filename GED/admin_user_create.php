<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_user_create = array(
    'fr' => array(
        'title' => 'Créer un utilisateur',
        'lastname' => 'Nom',
        'firstname' => 'Prénom',
        'username' => 'Nom d\'utilisateur',
        'username_help' => 'Utilisé pour se connecter à la place de l\'adresse email',
        'email' => 'Adresse e-mail',
        'langage' => 'Langue',
        'newpwd1' => 'Mot de passe',
        'caracteres' => '**6 caractères minimum',
        'newpwd2' => 'Confirmer mot de passe',
        'account' => 'Type de compte',
        'packer' => 'Emballeur',
        'provider' => 'Fournisseur',
        'userlyreco' => 'Utilisateur Lyreco',
        'administrator' => 'Administrateur',
        'valid_email' => '**email valide',
        'save' => 'Enregistrer',
        'errorpwd1' => 'Votre mot de passe doit comporter 6 caractères minimum.',
        'errorpwd2' => 'Vos mots de passe doivent être identiques.',
        'erroremail1' => 'Votre adresse e-mail n\'est pas valide.',
        'erroremail2' => 'Cette adresse e-mail est déjà utilisée.',
        'errorusrnme1' => 'Le nom d\'utilisateur existe déjà.',
        'errorusrnme2' => 'Votre nom d\'utilisateur doit contenir au moins 6 caractères.',
        'errorusrnme3' => 'Uniquement des lettres et chiffres autorisés.',
        'insertion_ok' => 'L\'utilisateur a été créé avec succès',
        'insertion_ko' => 'Erreur de création'
    ),
    'en' => array(
        'title' => 'Create user',
        'lastname' => 'Name',
        'firstname' => 'First name',
        'username' => 'Username',
        'username_help' => 'Use to connect instead of the email address',
        'email' => 'Email address',
        'langage' => 'Language',
        'newpwd1' => 'Password',
        'caracteres' => '**6 characters minimum',
        'newpwd2' => 'Confirm Password',
        'account' => 'Type of account',
        'packer' => 'Packer',
        'provider' => 'Provider',
        'userlyreco' => 'User of Lyreco',
        'administrator' => 'Administrator',
        'valid_email' => '**email valid',
        'save' => 'Save',
        'errorpwd1' => 'Your password must contain at least 6 characters long.',
        'errorpwd2' => 'Your passwords must match.',
        'erroremail1' => 'Your e-mail address is not valid.',
        'erroremail2' => 'This email address is already in use.',
        'errorusrnme1' => 'Your username is already used.',
        'errorusrnme2' => 'Your username must be at least 6 characters.',
        'errorusrnme3' => 'Only letters and white space allowed.',
        'insertion_ok' => 'The user was successfully created',
        'insertion_ko' => 'Error creating'
    )
);
//MYSQL
$reponse = $link->query("SELECT * FROM fournisseurs ORDER BY lib_four;");

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req" class="lead section-lead has-success"></div>

                    <form class="form-horizontal" name="create_user" id="create_user" data-toggle="validator" role="form">
                        <fieldset>
                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_user_create[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom"><?php echo $trad_admin_user_create[$lang]['lastname']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom" class="form-control input-md" name="nom" type="text" placeholder="" required="">
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="prenom"><?php echo $trad_admin_user_create[$lang]['firstname']; ?></label>  
                                <div class="col-md-4">
                                    <input id="prenom" class="form-control input-md" name="prenom" type="text" placeholder="" required="">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group" id="verif-username">
                                <label class="col-md-4 control-label" for="username"><?php echo $trad_admin_user_create[$lang]['username']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="username" 
                                        name="username" 
                                        type="text"
                                        class="form-control input-md" 
                                        data-remote="./scripts/req_username.php"
                                        data-remote-error="<?php echo $trad_admin_user_create[$lang]['errorusrnme1']; ?>"
                                        data-minlength="6" 
                                        data-minlength-error="<?php echo $trad_admin_user_create[$lang]['errorusrnme2']; ?>" 

                                        >
                                    <span class="help-block with-errors"><?php echo $trad_admin_user_create[$lang]['username_help']; ?></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="email"><?php echo $trad_admin_user_create[$lang]['email']; ?></label>  
                                <div class="col-md-4">
                                    <input id="email" class="form-control input-md" name="email" type="text" placeholder="" required=""
                                           data-remote="./scripts/req_email.php"
                                           data-remote-error="<?php echo $trad_admin_user_create[$lang]['erroremail1']; ?>"
                                           data-minlength="6" 
                                           data-minlength-error="<?php echo $trad_admin_user_create[$lang]['erroremail2']; ?>" 
                                           >
                                    <span class="help-block"><?php echo $trad_admin_user_create[$lang]['valid_email']; ?></span>
                                </div>
                            </div>
                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password1"><?php echo $trad_admin_user_create[$lang]['newpwd1']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="password1" 
                                        class="form-control input-md" 
                                        name="password1" 
                                        type="password" placeholder="" 
                                        required="" 
                                        data-minlength="6" 
                                        data-error="<?php echo $trad_admin_user_create[$lang]['errorpwd1']; ?>"
                                        >
                                    <span class="help-block with-errors" id="valid-pwd1"></span>
                                </div>
                            </div>
                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password2"><?php echo $trad_admin_user_create[$lang]['newpwd2']; ?></label>
                                <div class="col-md-4">
                                    <input 
                                        id="password2" 
                                        class="form-control input-md" 
                                        name="password2" 
                                        type="password" 
                                        placeholder="" 
                                        required="" 
                                        data-error="<?php echo $trad_admin_user_create[$lang]['errorpwd2']; ?>"
                                        data-match="#password1" 
                                        data-match-error="<?php echo $trad_admin_user_create[$lang]['errorpwd2']; ?>"
                                        >
                                    <span class="help-block with-errors" id="valid-pwd2"></span>
                                </div>
                            </div>

                            <div class="form-group" id="language-group">
                                <label class="col-md-4 control-label" for="language"><?php echo $trad_admin_user_create[$lang]['langage']; ?></label>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label for="langage-0">
                                            <input type="radio" name="language" id="langage" value="fr" <?php
                                            if ($lang == "fr") {
                                                echo 'checked="checked"';
                                            }
                                            ?>>
                                            Francais
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label for="langage-1">
                                            <input type="radio" name="language" id="langage" value="en" <?php
                                            if ($lang == "en") {
                                                echo 'checked="checked"';
                                            }
                                            ?>>
                                            English
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="type_compte"><?php echo $trad_admin_user_create[$lang]['account']; ?></label>
                                <div class="col-md-4">
                                    <select id="type_compte" class="form-control" name="type_compte">
                                        <option></option>
                                        <?php
                                        if ($_SESSION['user_level'] == 5) {
                                            echo '<option value="0">' . $trad_admin_user_create[$lang]['packer'] . '</option>';
                                            echo '<option value="1">' . $trad_admin_user_create[$lang]['provider'] . '</option>';
                                            echo '<option value="2">' . $trad_admin_user_create[$lang]['userlyreco'] . '</option>';
                                            echo '<option value="5">' . $trad_admin_user_create[$lang]['administrator'] . '</option>';
                                        } else {
                                            echo '<option value="0">' . $trad_admin_user_create[$lang]['packer'] . '</option>';
                                            echo '<option value="1">' . $trad_admin_user_create[$lang]['provider'] . '</option>';
                                            echo '<option value="2">' . $trad_admin_user_create[$lang]['userlyreco'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="form-group" id="group_fournisseur">
                                <label class="col-md-4 control-label" for="fournisseur"><?php echo $trad_admin_user_create[$lang]['provider']; ?></label>
                                <div class="col-md-4">
                                    <select id="fournisseur" class="form-control" name="fournisseur">
                                        <?php
                                        while ($donnees = $reponse->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees['code_four'] . "'>" . $donnees['code_four'] . " - " . $donnees['lib_four'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_user_create[$lang]['save']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_user.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>