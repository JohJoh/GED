<?php

include('./scripts/dbc.php');
page_protect();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_menu = array(
'fr' => array(
'Gestion_site' => 'Gestion du site',
 'Gestion_utilisateurs' => 'Gestion des utilisateurs',
 'Gestion_zones' => 'Gestion des zones préférentielles',
 'Gestion_pays' => 'Gestion des pays',
 'Gestion_codes' => 'Gestion des codes douaniers',
 'Gestion_fournisseurs' => 'Gestion des fournisseurs',
 'Gestion_produits' => 'Gestion des produits',
 'Export_donnees' => 'Exportation des données',
 'Fichier_dangereux' => 'Exporter le fichier des dangereux',
 'Fichier_zones_1000' => 'Exporter le fichier des zones division 1000',
 'Fichier_zones_2000' => 'Exporter le fichier des zones division 2000'
),
 'en' => array(
'Gestion_site' => 'Site Management',
 'Gestion_utilisateurs' => 'User Management',
 'Gestion_zones' => 'Management of preferential zones',
 'Gestion_pays' => 'Country Management',
 'Gestion_codes' => 'Management of customs codes',
 'Gestion_fournisseurs' => 'Supplier Management',
 'Gestion_produits' => 'Product Management',
 'Export_donnees' => 'Export data',
 'Fichier_dangereux' => 'Export dangerous file',
 'Fichier_zones_1000' => 'Export the file division zones 1000',
 'Fichier_zones_2000' => 'Export the file division zones 2000',
 )
);
?>