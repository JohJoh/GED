<?php
include('./scripts/dbc.php');
page_protect();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

//var_dump($_SESSION['user_code_four']); die();

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_help = array(
    'fr' => array(
        'title_page' => '<B><U>Guide</B></U>',
        'text_page' => '(Vous trouverez ci-dessous les définitions et explications sur les données à renseigner sur cette plateforme)<br><br><a href="GuideExterne_GED_Import_2017.pdf">Guide d\'utilisation</a><br>',
        'title_p1' => '1/ <U>La provenance</U>',
        'p1' => 'Il s’agira de préciser depuis quel endroit (usine ou dépôt logistique) la marchandise est approvisionnée sur le CDD de Lyreco <br>Notion géographique/logistique = pays à partir duquel les marchandises ont été expédiées vers notre site
            </br>
            <U>Exemple</U>: Un produit est fabriqué en Chine, mais est livré à Lyreco depuis votre entrepôt aux Pays-Bas. Le pays d’origine est la Chine, Le pays de provenance est les Pays-Bas.',
        'title_p2' => '2/ <U>L\'origine des produits</U>',
        'p2' => 'Il s\'agit du pays où le produit est fabriqué ou du pays dans lequel le produit a subi une « ouvraison substantielle économiquement justifiée ».
			</br>
			Il faudra attester de l\'origine de votre produit via un document officiel : une déclaration d\'orgine.
			</br>
			<a href="./Declaration_dorigine.docx">Modèle ici</a>
			</br>
			Ce document est obligatoire lorsque le produit ne bénéficie pas d\'origine préférentielle et/ou est fabriqué en dehors de l\'Europe. Une déclaration d\'origine doit être remplie par type d\'origine.
			</br>
			<U>Exemple</U>:Les produits A,B et C sont fabriqués en Chine et le produit D est fabriqué à Taiwan. Une déclaration pour les produits de Chine sera nécessaire ainsi qu\'une déclaration pour le produit fabriqué à Taiwan.
			</br>
			A noter, un document prouvant l\'origine est obligatoire:
			<br/>
				-DLT dans le cadre d\'origine préférentielle;
			<br/>
				-Déclaration d\'origine si le produit n\'a pas d\'origine préférentielle.
                        <br />
                                IMPORTANT : Dans le cas où un ou plusieurs de vos produits peuvent avoir plusieurs origines (Ex : vous avez une usine en Chine et une usine au Japon qui fabriquent le même produit), merci de nous l’indiquer en cochant la case « origines multiples » sur la fiche produit.',
        'title_p3' => '3/ <U>L\'origine préférentielle des produits</U>',
        'p3' => 'Tous les produits ont une origine de droit commun.
			</br>
			Mais certains produits peuvent en plus bénéficier d\'une <U>origne préférentielle</U>, dans le cadre des relations commerciales préférentielles établies par l\'UE avec un certain nombre de pays tiers ayant conclu un accord préférentiel avec la CEE.
			</br>
			Dans le cadre des relations préférentielles existant entre l\'UE et certains pays tiers, les avantages tarifaires (taux de droit de douane réduit ou nul) sont réservés aux produits qui peuvent être réputés "originaires" des parties contractantes.
			</br>
			En conséquence, dans chaque accord, figure un protocole définissant la notion des produits originaires.La liste des accords conclus par l\'UE et actuellement en vigueur est consultable dans l\'annexe ci-jointe ou récupérables sur le site de la douane où vous pourrez consulter directement les dispositions.
			</br>
			</br>
			Site de la douane en France :
			</br>
			<U><a href="http://www.douane.gouv.fr/Portals/0/fichiers/professionnel/dedouanement/relations-preferentielles-23042014.pdf">http://www.douane.gouv.fr/Portals/0/fichiers/professionnel/dedouanement/relations-preferentielles-23042014.pdf</a></U>
			</br>
			La liste des accords préférentiels est disponible ici :
			</br>
			<U><a href="http://www.douane.gouv.fr/articles/a11987-liste-des-accords-et-preferences-unilaterales-de-l-union-europeenne">http://www.douane.gouv.fr/articles/a11987-liste-des-accords-et-preferences-unilaterales-de-l-union-europeenne</a></U>
			</br>
			Site de la douane européenne :
			</br>
			<U><a href="http://ec.europa.eu/taxation_customs/customs/customs_duties/rules_origin/preferential/article_779_en.htm">http://ec.europa.eu/taxation_customs/customs/customs_duties/rules_origin/preferential/article_779_en.htm</a></U>
			</br>
			</br>
			Même s\'il existe des dispositions commmunes à tous ces systèmes préférentiels concernant l\'acquisition de l\'origine communautaire, il vous appartient d\'aller vérifier dans chaque accord et protocole, les règles définies pour acquérir l\'origine préférentielle et de nous indiquer sur quelle zone cette origine préférentielle peut s\'appliquer.
			</br>
			</br>
			Dans chaque accord, vous trouverez un tableau récapitulatif qui s\'étale sur plusieurs pages. Il vous suffit de repérer les 4 premiers chiffres du ou des codes douaniers de vos produits (colonne 1). En colonne 3 se situe le critère qui permet de définir si le produit relatif à ce code douanier bénéficie d\'une origine préférentielle.Si votre produit rentre dans ce critère, alors il bénéficie d\une origine préférentielle avec le pays de l\'accord sur lequel vous êtes.
			</br>
			</br>
			Vous trouverez ci-dessous un exemple avec l\'accord de la Suisse.
			</br>
			</br>
			<img src="images/Aide-01.png"/>
			</br>
			</br>
			<I>Exemple : Mon produit a un code douanier commençant par 0403</I>
			</br>
			</br>
			1.Dans l\'accord de la Suisse, je repère le tableau récapitulatif et les 4 premiers chiffres du code douanier de mes produits.
			</br>
			2.Je lis le critère associé au code douanier en colonne.Si mon produit qui a un code douanier commençant par 0403 rentre dans le cadre de cette définition, alors mon produit a une origine préférentielle avec la Suisse.
			</br>
			3.Répéter cet exercice avec chaque accord la listeprésent sur le lien indiqué ci-dessus.
			</br>
			</br>
			Enfin le bénéfice d\'un régime préférentiel est subordonné à la présentation d\une preuve de l\'origine. En tant que fabricant ou commerçant du produit, nous vous demandons de faire cette déclaration d\'origine préférentielle sous forme d\une DLT, Déclaration à Long Terme qui devra être renouvelée tous les ans.
			</br>
			</br>
			Vous trouverez un modèle de DLT en annexe 2 du document suivant:
			</br>
			<U><a href="http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2006:300:0005:0008:FR:PDF">http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2006:300:0005:0008:FR:PDF</a></U>
			</br>
			</br>
			Pour maîtriser les règles d\'origine préférentielle, les éyapes à suivre :
			</br>
			</br>
			1.Connaitre les pays entrant dans la relation préférentielle.
			</br>
			</br>
			<img src="images/Aide-03.png"/>
			</br>
			</br>
			2.Consulter l\'accord conclu entre le pays exportateur et le pays importateur.
			</br>
			</br>
			<img src="images/Aide-04.png"/>
			</br>
			</br>
			3.Identifier les règles d\'origine applicables.
			</br>
			</br>
			<img src="images/Aide-05.png"/>
			</br>
			</br>
			Si vos produits ne bénéficient pas d\'orgine préférentielle, vous n\'avez donc pas à remplir ces champs. Cependant, il faut tout de même justifier de l\'origine de vos produits en joignant une déclaration d\'origine (un seul document pour l\'ensemble de vos produits conviendra) ou un certificat d\'origine
                        <br /><br />
                        IMPORTANT : 
                        <br />Merci de noter que plusieurs pays peuvent avoir les mêmes accords.
                        <br />Nous avons donc regroupé les pays ayant les mêmes accords et créé 12 zones : RAMA, ACP, PEMD, PTOM, BALK, ASIE, BOHE, AMCE, MEXI, CHIL, PERO, COLO.
                        <br />Vous trouverez la liste exhaustive des pays par zone dans le fichier excel ici : <a href="./liste_des_pays_par_zone_preferentielle.xlsx" >Liste</a>
                        <br />Par exemple, si votre produit bénéficie d’une origine préférentielle avec Andorre (suite à votre consultation de l’accord en question), alors votre produit a une origine préférentielle avec tous les pays qui sont dans la même zone que l’Andorre, ici la zone PEMD.
                        <br />Vous pouvez donc indiquer sur votre déclaration long terme, la liste de tous ces pays.
        ',
        'title_p4' => '4/ <U>Les fiches de données de sécurité</U>',
        'p4' => 'Dans le cas où un produit est considéré comme dangereux, vous devez remplir les champs concernés dans la fiche produit : </br> -Numero UN</br> -Classe de danger</br> -Groupe d\'emballage</br> -Mode de transport pour lequel le produit est considéré comme dangereux</br>Vous trouverez toutes ces informations dans le paragraphe 14 de la fiche de données de sécurité du produit en question</br>Vous devez également joindre la fiche de données de sécurité en question en français et en anglais.',
        'title_p5' => '5/ <u>Emballage</u>',
        'p5' => '-Intérieur : récipient qui contient et permet la rétention du produit dit dangereux<br><img src="images/Aide-07.png"><br>
			-Combiné : emballage constitué par un ou plusieurs emballages intérieurs contenus dans un emballage extérieur<br><img src="images/Aide-06.png"><br>
                        La quantité qui devra être indiquée est bien la quantité contenue dans l’emballage intérieur.',
		'title_p6' => '6/ <u>Signature manuscrite originale du fournisseur</u>',
        'p6' => 'La déclaration du fournisseur doit porter <strong>la signature manuscrite originale du fournisseur.</strong>
				Toutefois, lorsque la déclaration du fournisseur sont établies par voie électronique, la déclaration ne doit pas nécessairement être signée à la main,
				à condition que le fournisseur remette <strong>au client un engagement écrit par lequel il accepte la responsabilité entière de toute déclaration du fournisseur l\'identifiant comme si elle avait été signée de sa propre main, 
				conformément à l’article 63 des AE</strong>. Dans la mesure où cette disposition évoque l\'établissement de déclarations du fournisseur par voie électronique, 
				elle prévoit la possibilité que celles-ci soient présentées sous format dématérialisé.',
		'title_p7' => '7/ <u>Le fournisseur déclarera comment le produit livré a obtenu l’origine préférentielle</u>',
        'p7' => '<strong>A)</strong>
				Pour qu’Un produit soit considéré comme originaire de la communauté ou d’un pays lié à la communauté par un accord préférentiel, 
				il doit avoir été obtenu dans la communauté ou dans un pays partenaire<br>
					--> Soit entièrement parce qu’il est issu de la terre ou incorporant des matières exclusivement européennes<br>
					--> Soit par transformation dite suffisante pour les matières tierces non originaires (exemple si matière venant de chine)
						– la notion de transformation suffisante est décrite pr chaque produit dans une liste annexée au protocole accord<br>
					<strong>Le caractère originaire de l’UE</strong> pour la matière ou du produit fini livré se fera par <strong><u>une Déclaration à Long terme Fournisseur pour des produits ayant le caractère originaire à titre préférentiel <a href="formulaire/Formulaire_6b_FR.pdf" onclick="window.open(this.href); return false;">model 6b</a>.</u></strong><br><br>
				<strong>B)</strong>
				Des assouplissements au principe de transformation suffisante existent pour tous les accords de libre échange entre pays partenaires<br>
				Au sein de l’accord <a href="pdf/PANEUROMED_FR.pdf" onclick="window.open(this.href); return false;">PAN EUROMED</a> L’origine préférentielle UE peut être obtenue<br>
					--> Par Une tolérance d’incorporation qui permet l’utilisation de matières non originaires qui ne satisfont pas à la règle de transformation suffisante, 
						à condition toutefois que leur valeur n’excède pas un certain pourcentage du prix départ usine du produit final (10 ou 15 % selon le cadre juridique)<br>
					--> Grâce aux mécanismes des cumul d’origine qui permettent, sous réserve du respect de certaines conditions, de considérer certaines matières non originaires comme originaires du pays où a lieu leur utilisation. 
						En application des règles de cumul <strong>dans la zone pan-euro-méditerranéenne</strong>, l\'origine pourra alors être soit UE, soit celle d\'un des pays de la zone PEMD ayant participé à la fabrication du produit à partir du moment où l’opération effectuée dans un des pays de la zone va au-delà des règles de tolérance d’incorporation. 
						Ces matières n’ayant plus à respecter la règle de transformation suffisante prévue pour les matières tierces originaires d\'un pays qui ne fait pas parti de la zone paneuromed,  
						l’origine préférentielle devient plus facile à acquérir. En d’autre terme l’origine du produit est donnée par le dernier pays où a lieu une ouvraison ou une opération qui va au-delà d’une opération insuffisante<br><br>
						
						Un fournisseur pourra donc faire état <strong>du caractère non originaire d’un produit fourni ayant subi une première transformation dans l’UE</strong>, 
						dans le cas où cette transformation, ajoutée à des opérations à venir dans l’UE, permet d’acquérir une origine préférentielle. 
						Cette attestation se fera par le biais d’une <strong><u>Déclaration à long terme du fournisseur concernant les produits n\'ayant pas le caractère originaire à titre préférentiel <a href="formulaire/Formulaire_6d_FR.pdf" onclick="window.open(this.href); return false;">model 6d</a></u></strong>.
						Lorsque la déclaration se rapporte à des marchandises de différents types ou à des marchandises ne comportant pas la même proportion de matières non originaires, 
						le fournisseur est tenu de les distinguer clairement dans ce formulaire<br><br>
						Particulièrement pour la zone de libre échange PAN EUROMED, le fournisseur se reportera au protocole PAN EUROMED pour bien définir l’origine préférentielle de son produit et si cumul (cas B), 
						il devra aussi vérifier les règles définies dans le protocole UE/Pays de destination de la zone de libre échange PAN EUROMED<br><br>
						<img src="images/Aide-08_FR.png">'
    ),
    'en' => array(
        'title_page' => '<B><U>Guideline</U></B>',
        'text_page' => '(You will find the definitions and explanations of the data to fill in this platform )<br><br><a href="GuideExterne_GED_Import_2017.pdf">User guide</a><br>',
        'title_p1' => '1/ <U>Source</U>',
        'p1' => 'you must mention from where / any location (factory or logistic warehouses) the goods are supplied on CDD Lyreco.
                        <br>It is about a Geographical concept / logistics = Country from which the goods were shipped to our site
			</br>
			<U>Example</U>: A product is made in China and is delivered to Lyreco via your warehouse based in The Netherlands. The country of origin is China. The country of sourcing is the Netherlands.',
        'title_p2' => '2/ <U>Origin of product</U>',
        'p2' => 'A product is originating in the country where it has been wholly obtained or the one where he suffered a “substantial working economically justified"
			</br>
			It will certify the origin of your product via an official document : a declaration of origin.
			</br>
			<a href="./DECLARATION OF ORIGIN.docx">Draft here</a>
			</br>
			This document is mandatory when the product does not benefit from preferential origin and / or manufactured outside of Europe. A declaration of origin should be completed by type of origin
			</br>
			<U>Example</U>: Products A, B and C are made in China and Product D is manufactured in Taiwan. A statement for Chinese products is necessary and a statement for the product manufactured in Taiwan will be also necessary.
			</br>
			Please note, a document proving origin is mandatory :
			<br/>
			-LTD under preferential origin;
			<br/>
			-Declaration of origin if the product has no preferential origin</li></ul>
                        <br />
                        IMPORTANT: Some of your products may have several origins (I.E : the production of 1 product is split between your factory in China and your second factory in Japan), then please tick “Multiples origins” on the product sheet.',
        'title_p3' => '3/ <U>Preferential origin of products</U>',
        'p3' => 'All products have an <U>origin of common law</U>
			</br>
			</br>
			But some can additionally benefit from <U>preferential original</U> under preferential trade relations established by the EU with a number of third countries that have concluded a preferential agreement with the EEC.
			</br>
			</br>
			Under the existing preferential relations between the EU and third countries, the tariff preferences (duty reduced or zero tariff) are reserved for products that may be deemed "originating from the Contracting Parties.
			</br>
			</br>
			Accordingly, in each Agreement, there is a specifc protocol defining the concept of origin. The list of agreements concluded by the EU and current, is avaliable in the attached doc or recoverable on the site of the customs where you can check directly all the items & protocols.
			</br>
			</br>
			French custom site :
			</br>
			<U><a href="http://www.douane.gouv.fr/Portals/0/fichiers/professionnel/dedouanement/relations-preferentielles-23042014.pdf">http://www.douane.gouv.fr/Portals/0/fichiers/professionnel/dedouanement/relations-preferentielles-23042014.pdf</a></U>
			</br>
			</br>
			European customs site in several languages :
			</br>
			<U><a href="http://ec.europa.eu/taxation_customs/customs/customs_duties/rules_origin/preferential/article_779_en.htm">http://ec.europa.eu/taxation_customs/customs/customs_duties/rules_origin/preferential/article_779_en.htm</a></U>
			</br>
			</br>
			Although there are common provisions for all these preferential schemes for the acquisition of Community origin, it is your responsibility to go and check in each agreement and protocols, the rules defined for acquiring preferential origin and to tell us on which area that may apply preferential origin.
			</br>
			</br>
			In each agreement, you will find a recap table. You need to go and check on that table the line(s) that refer to the first 4 digits of your products custom codes(column 1).
			</br>
			</br>
			In column 3, you will find a definition. If your product fit in that definition, then it has a preferential origin with the country for which the agreement stands for.
			</br>
			</br>
			You will find hereafter an example with the agreement for Switzerland.
			</br>
			</br>
			<img src="images/Aide-02.png">
			</br>
			</br>
			<I>Example : My product has a custom code beginning with 0403</I>
			</br>
			</br>
			1.In the agreement of Switzerland, I have to find the recap table. In the reacp table, I have to find the first 4 digits of my custom code : 0403
			</br>
			2.I carefully read the definition in column 3. If my product, which has a custom code beginning with 0403, matches the definition, then it has a preferential origin with Switzerland.
			</br>
			3.Do it again with every agreement in the link above.
			</br>
			</br>
			Finally the benefit of a preferential scheme is subject to the submission of a proof of origin. As a manufacturer or dealer of the product, we ask you to make this declaration of preferential origin as a LTD, Long Term Declaration which must be renewed every year.
			</br>
			</br>
			You will find a model for LTD in annex 2 of the document blow: 
			</br>
			<U><a href="http://www.google.com/url?sa=t&rct=j&q=&esrc=s&frm=1&source=web&cd=1&ved=0CB8QFjAA&url=http%3A%2F%2Fwww.ferona.cz%2Ffile.php%3Fid%3D313&ei=KupxVJr7HoTqOPalgfgP&usg=AFQjCNEF1ZpIZeTo7yieZAykarqTBfE7sw&bvm=bv.80185997,d.ZWU"/> http://www.google.com/url?sa=t&rct=j&q=&esrc=s&frm=1&source=web&cd=1&ved=0CB8QFjAA&url=http%3A%2F%2Fwww.ferona.cz%2Ffile.php%3Fid%3D313&ei=KupxVJr7HoTqOPalgfgP&usg=AFQjCNEF1ZpIZeTo7yieZAykarqTBfE7sw&bvm=bv.80185997,d.ZWU</a></U>
                        <br /><br />IMPORTANT
<br />Please note that several countries may have the same requirements.
<br />Therefore, we grouped countries with same requirements for preferential origin, and created 12 zones: RAMA, ACP, PEMD, PTOM, BALK, ASIE, BOHE, AMCE, MEXI, CHIL, PERO, COLO.
<br />You will find the full list in the excel file here : <a href="./liste_des_pays_par_zone_preferentielle.xlsx" >List</a>
<br />For example, if your product has a preferential origin with Andorra (because you checked on the Andorra Origin agreement as specified earlier), then you can be sure that your product has a preferential origin with all the countries in the same zone, in this example, we are talking about PEMD.
<br />You can therefore indicate in your long term declaration the list of all the countries in the PEMD zone.
',
        'title_p4' => '4/ <U>Safety data sheets</U>',
        'p4' => 'In case one of your products is considered as hazardous, please fill in the following data in the product sheet : </br> -UN number</br> -Hazard class</br> -Packing group</br> -Transport mode</br>You will find the according data in the paragraph 14 of the safety data sheet.</br>You will also have to attach the safety data sheet both in English and French.',
        'title_p5' => '5/ <u>Packaging</u>',
        'p5' => '-Inside : container that contains and allows the product retention dangerous told<br><img src="images/Aide-07.png"><br>
			-Combined: packaging consisting of one or more inner packagings in an outer packaging<br><img src="images/Aide-06.png"><br>
                        The volume that will be shown is the amount contained in the inner packaging.',
		'title_p6' => '6/ <u>Original handwritten signature of the supplier</u>',
        'p6' => 'The supplier\'s declaration must bear <strong>the original handwritten signature of the supplier</strong>.
				However, where the supplier\'s declaration is made electronically, the declaration need not be signed by hand, 
				provided that the supplier gives the customer <strong>a written undertaking whereby he accepts full responsibility for any declaration by the supplier identifying it as if it had been signed by its own hand, 
				in accordance with Article 63 of the EA</strong>. 
				Insofar as this provision refers to the establishment of supplier\'s declarations by electronic means, 
				it provides the possibility that they be presented in dematerialized format.',
		'title_p7' => '7/ <u>The supplier will declare how the delivered product has obtained the preferential origin</u>',
        'p7' => '<strong>A)</strong>
				To be considered as originating in the community or a country linked to the community by a preferential agreement, 
				it must have been obtained in the community or in a partner country<br>
					--> Either fully because it comes from the land or incorporating exclusively European materials<br>
					-- Either by a transformation known as sufficient for non-originating third-party materials (eg if material coming from China) - 
						the concept of sufficient transformation is described for each product in a list annexed to the agreement protocol<br>
					The originating status of the EU for the material or finished product delivered shall be issued through <strong><u>a Long-term Supplier Declaration for products having a preferential origin status <a href="formulaire/Formulaire_6b_EN.pdf" onclick="window.open(this.href); return false;">model 6b</a></u></strong><br><br>

				<strong>B)</strong>
				Some arrangement  to the principle of sufficient transformation exist for all free trade agreements between partner countries<br>
				Within the <a href="pdf/PANEUROMED_EN.pdf" onclick="window.open(this.href); return false;">PAN EURMED</a> agreement EU preferential origin can be obtained<br>
					--> By an allowance for incorporation which allows the use of non-originating materials which do not satisfy the sufficient processing rule, 
						provided, however, that their value does not exceed a certain percentage of the ex-works price of the final product (10 or 15% according to the legal framework)<br>
					--> Through the mechanisms of cumulation of origin which allow, subject to the fulfillment of certain conditions, 
						to consider certain non-originating materials as originating in the country where their use takes place. 
						In application of cumulation rules in the pan-Euro-Mediterranean zone, the origin may then be either EU or that of one of the countries in the PEMD zone that participated in the manufacture of the product from the moment the operation performed in one of the countries of the zone goes beyond the rules of tolerance of incorporation.
						As these materials no longer have to comply with the sufficient processing rule for third-country materials originating in a country which is not part of the paneuromed zone,
						the preferential origin becomes easier to acquire. In other words, the origin of the product is given by the last country where a working or an operation is carried out which goes beyond an insufficient operation<br><br>
					
					A supplier may therefore declare the non-originating nature of a supplied product which has undergone first processing in the EU, 
						in the event that such processing, added to future operations in the EU, makes it possible to acquire a preferential origin.
						This certification will be done <strong><u>through a long-term Supplier Declaration for products not having preferential origin <a href="formulaire/Formulaire_6d_EN.pdf" onclick="window.open(this.href); return false;">model 6d</a></u></strong>.
						Where the declaration relates to goods of different types or to goods which do not have the same proportion of non-originating materials, 
						the supplier is obliged to distinguish them clearly in this form<br><br>
					<img src="images/Aide-08_EN.png">')
);
if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <center><h1 style='color:#FF0000;'><?php echo $trad_help[$lang]['title_page']; ?></h1></center>
                    <center><h5><?php echo $trad_help[$lang]['text_page']; ?></h5></center>
                    <h2><?php echo $trad_help[$lang]['title_p1'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p1']; ?></p>
                    <h2><?php echo $trad_help[$lang]['title_p2'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p2']; ?></p>
                    <h2><?php echo $trad_help[$lang]['title_p3'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p3']; ?></p>
                    <h2><?php echo $trad_help[$lang]['title_p4'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p4']; ?></p>
                    <h2><?php echo $trad_help[$lang]['title_p5'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p5']; ?></p>
					<h2><?php echo $trad_help[$lang]['title_p6'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p6']; ?></p>
					<h2><?php echo $trad_help[$lang]['title_p7'] ?></h2>
                    <p class="lead section-lead"><?php echo $trad_help[$lang]['p7']; ?></p>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>