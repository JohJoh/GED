<?php

// Connexion à la base de données 
include('./scripts/dbc.php');
$division = filter_input(INPUT_GET, "divi", FILTER_SANITIZE_STRING);

$resQuery = $link->prepare("SELECT p.sap_prod, "
        . "p.code_pays_origine, "
        . "z.code_zone_pref, "
        . "DAY(p.date_deb_validite) as jour, "
        . "MONTH(p.date_deb_validite) as mois, "
        . "YEAR(p.date_deb_validite), "
        . "p.code_douanier "
        . "FROM produits p, "
        . "lien_produit_zone_pays z "
        . "WHERE z.sap_prod = p.sap_prod "
        . "AND p.is_active = 1 "
        . "AND p.division_prod = :division");
$resQuery->execute([':division' => $division]);
//var_dump($resQuery);die;

//Header permettant la création d'un txt 
//header("Content-type: application/octet-stream");
header('Content-Type: text/html; charset=utf-8');
header("Content-Disposition: attachment; filename=\"zones " . $division . " " . date("d-m-Y") . ".txt\"");

if ($resQuery->rowCount() != 0) {
// on insère les titres des colonnes 
    echo "code article	code pays d'origine	zone preferentielle	jour	mois	annee	NDP code";
    echo "\r\n";

// on insère les données de la table 
    while ($arrSelect = $resQuery->fetch(PDO::FETCH_ASSOC)) {
        foreach ($arrSelect as $elem) {
            echo "$elem	";
        }
        echo "\r\n";
    }
}
?>