<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$allCountries = loadPays();
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_product_create = array(
    'fr' => array(
        'title' => 'Créer un produit',
        'code_sap' => 'Code SAP',
        'provider' => 'Fournisseur',
        'code_prod' => 'Code Produit Fournisseur',
        'designation' => 'Désignation',
        'division' => 'Division',
        'pays_prov' => 'Pays de provenance',
        'code_post' => 'Code Postal',
        'ville' => 'Ville',
        'pays_origine' => 'Pays d\'origine',
        'produit_non_pref' => 'Ce produit ne bénéficie pas de préférence',
        'date_debut' => 'Date de début de validité',
        'date_fin' => 'Date de fin de validité',
        'zone_pref' => 'Zones préférentielles',
        'pays_zone' => 'Pays par zone',
        'code_douane' => 'Code douanier',
        'definition_code' => 'Définition du code ',
        'danger' => 'DANGER',
		'produit_dlt_concerne' => 'Signature manuscrite présente ?',
        'produit_concerne' => 'Ce produit est concerné par cette partie ?',
		'produit_concerne0' => 'NON',
		'produit_concerne1' => 'OUI',
        'classe_danger' => 'Classe de danger',
        'code_onu' => 'Code ONU',
        'code_emballage' => 'Code emballage',
        'mode_transport' => 'Mode de transport',
        'conditionnement' => 'Conditionnement du fournisseur',
        'DLT' => 'Fiche DLT',
		'DLT_PEMD' => 'Fiche DLT PEMD',
		'DE_Radio' => '',
		'DE_PEMD_Radio' => '',
		'DE' => 'Fiche DE',
		'FDS_fr' => 'Fiche FDS Français',
        'info_fds_en' => 'Je ne dispose pas de cette fiche en anglais',
        'FDS_en' => 'Fiche FDS Anglais',
        'DOF' => 'Fiche DOF',
        'error_sapcode_1' => 'Le code SAP existe déjà.',
        'error_sapcode_2' => 'Le code SAP ne doit pas être vide',
		'error_fournisseur' => 'Sélectionnez un fournisseur dans la liste.',
		'error_code_fournisseur' => 'Le code Produit Fournisseur ne doit pas être vide.',
		'error_designation' => 'La désignation ne doit pas être vide.',
		'error_division' => 'Sélectionnez une division dans le liste.',
		'error_pays_provenance' => 'Sélectionnez un pays de provenance.',
		'error_code_postal' => 'Le code postal ne doit pas être vide.',
		'error_ville' => 'La ville ne doit pas être vide.',
		'error_pays_origine' => 'Sélectionnez un pays d\'origine.',
		'error_date_debut' => 'La date de début ne doit pas être vide.',
		'error_date_fin' => 'La date de fin ne doit pas être vide.',
		'error_code_douanier' => 'Sélectionnez un code douanier dans la liste.',
		'error_classe_danger' => 'Sélectionnez une classe de danger dans la liste.',
		'error_code_onu' => 'Sélectionnez un code onu dans la liste.',
		'error_code_emballage' => 'Sélectionnez un code emballage dans la liste.',
		'error_quantite' => 'La quantité par emballage intérieur ne doit pas être vide.',
		'error_type_emballage' => 'Le type d\'emballage ne doit pas être vide.',
		'error_conditionnement_fournisseur' => 'Le conditionnement du fournisseur ne doit pas être vide.',
		'error_dlt' => 'L\'ajout de la fiche DLT est obligatoire.',
		'error_dlt_pemd' => 'L\'ajout de la fiche DLT PEMD est obligatoire.',
		'error_de' => 'L\'ajout de la fiche DE est obligatoire.',
		'error_fds_fr' => 'L\'ajout de la fiche FDS est obligatoire.',
		'error_fds_en' => 'L\'ajout de la fiche FDS est obligatoire.',
		'error_dof' => 'L\'ajout de la fiche DOF est obligatoire.',
		'save' => 'Enregistrer',
        'contenance' => 'Quantité par emballage intérieur',
		'type' => 'Type d\'emballage',
        'explication_contenance' => 'Fournir la quantité de produit dangereux (en mL, L, g, kg) contenu par emballage - exemple : 500 mL, 5L',
		'explication_type' => 'Fournir la typologie de l\'emballage - exemple : aérosol métal, bidon plastique, flacon de verre',
        'explication_conditionnement' => 'Ex: Si paquet de 4 stylos, indiquer 4+',
        'add' => 'Ajouter'
    ),
    'en' => array(
        'title' => 'Create product',
        'code_sap' => 'SAP code',
        'provider' => 'Provider',
        'code_prod' => 'Supplier product code',
        'designation' => 'Designation',
        'division' => 'Division',
        'pays_prov' => 'Country of provenance',
        'code_post' => 'Postal Code',
        'ville' => 'City',
        'pays_origine' => 'Country of origin',
        'produit_non_pref' => 'This product does not benefit from preference',
        'date_debut' => 'Starting date of validity',
        'date_fin' => 'Expiration date',
        'zone_pref' => 'Preferential area',
        'pays_zone' => 'Area\'s countries',
        'code_douane' => 'Customs code',
        'definition_code' => 'Definition of the code',
        'danger' => 'DANGER',
		'produit_dlt_concerne' => 'Handwritten signature present ?',
        'produit_concerne' => 'This product is affected by this part ?',
		'produit_concerne0' => 'NO',
		'produit_concerne1' => 'YES',
        'classe_danger' => 'Class of danger',
        'code_onu' => 'UN code',
        'code_emballage' => 'Packaging code',
        'mode_transport' => 'Mode of transport',
        'conditionnement' => 'Packaging provider',
        'DLT' => 'LTH File',
		'DLT_PEMD' => 'DLT PEMD File',
		'DE_Radio' => '',
		'DE_PEMD_Radio' => '',
		'DE' => 'DC File',
		'FDS_fr' => 'French SDS File',
        'info_fds_en' => 'I do not have to this file in English',
        'FDS_en' => 'English SDS File',
        'DOF' => 'DOF File',
        'error_sapcode_1' => 'The SAP code already exists.',
        'error_sapcode_2' => 'The SAP code should not be empty',
		'error_fournisseur' => 'Select a provider from the list.',
		'error_code_fournisseur' => 'The Supplier Product Code must not be empty.',
		'error_designation' => 'The designation must not be empty.',
		'error_division' => 'Select a division from the list.',
		'error_pays_provenance' => 'Select a country of origin.',
		'error_code_postal' => 'The postal code must not be empty.',
		'error_ville' => 'The city must not be empty.',
		'error_pays_origine' => 'Select a country of origin',
		'error_date_debut' => 'The start date must not be empty.',
		'error_date_fin' => 'The end date must not be empty.',
		'error_code_douanier' => 'Select a customs code from the list.',
		'error_classe_danger' => 'Select a hazard class from the list.',
		'error_code_onu' => 'Select a ONU code from the list.',
		'error_code_emballage' => 'Select a classSelect a packaging code from the hazard list.',
		'error_quantite' => 'The quantity per inner package must not be empty.',
		'error_type_emballage' => 'The type of packaging must not be empty.',
		'error_conditionnement_fournisseur' => 'The supplier\'s packaging must not be empty.',
		'error_dlt' => 'The addition of the DLT form is mandatory.',
		'error_dlt_pemd' => 'The addition of the DLT PEMD form is mandatory.',
		'error_de' => 'The addition of the DE form is mandatory.',
		'error_fds_fr' => 'The addition of the FDS form is mandatory.',
		'error_fds_en' => 'The addition of the FDS form is mandatory.',
		'error_dof' => 'The addition of the DOF form is mandatory.',
        'save' => 'Save',
        'contenance' => 'Quantity per inner packaging',
		'type' => 'Type of packaging',
        'explication_contenance' => 'Provide the amount of hazardous material ( in mL , L, g, kg) content by packaging - example: 500 mL, 5L',
		'explication_type' => 'Provide the typology of the packaging - example: metal aerosol canister, plastic , glass bottle',
        'explication_conditionnement' => 'Ex : If pack of 4 pens, indicate 4+',
        'add' => 'Add'
    )
);

//Partie SQL
$reponse_fournisseur = $link->query("SELECT * FROM fournisseurs ORDER BY lib_four");
$reponse_division = $link->query('SELECT sap_prod, division_prod FROM produits');
$reponse_pays_provenance = $link->query('SELECT * from pays ORDER BY lib_pays');
$reponse_pays_origine = $link->query('SELECT * from pays ORDER BY lib_pays');
$reponse_zone_pref = $link->query('SELECT * from zone');
$reponse_mode_transport = $link->query("SELECT * from mode_tpt");
$reponse_code_douanier = $link->query('SELECT * from douane');
$reponse_classe_danger = $link->query("SELECT * from danger");
$reponse_code_onu = $link->query('SELECT * from onu');
$reponse_code_emballage = $link->query("SELECT * from emballage");

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="result_req" class="lead section-lead has-success"></div>
                    <script>
                        $(document).ready(function () {
                            var popover = $("[data-toggle=popover]").popover({
                                trigger: 'hover',
                                placement: 'bottom',
                                html: 'true'
                            }).on('show.bs.popover', function () {
                                $.ajax({
                                    url: 'admin_provider_create.php',
                                    success: function (html) {
                                        popover.attr('data-content', html);
                                    }
                                });
                            });
                        });
                    </script>
                    <form class="form-horizontal" name="create_product" action="./ajax_admin_product_create.php"  method="post" data-toggle="validator" role="form" id="create_product" enctype="multipart/form-data">
                        <fieldset>

                            <!-- Créer un produit -->
                            <legend><?php echo $trad_admin_product_create[$lang]['title']; ?></legend>

                            <!-- Code SAP-->
                            <div class="form-group">	
                                <label class="col-md-4 control-label" for="code_sap"><?php echo $trad_admin_product_create[$lang]['code_sap']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_sap" 
                                        name="code_sap" 
                                        class="form-control input-md" 
                                        type="text" 
                                        placeholder="" 
                                        data-remote="./scripts/req_sapcode.php" 
                                        data-remote-error="<?php echo $trad_admin_product_create[$lang]['error_sapcode_1']; ?>"
                                        required=""
                                        data-native-error="<?php echo $trad_admin_product_create[$lang]['error_sapcode_2']; ?>"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <style>
                                .btn-glyphicon {
                                    padding:8px;
                                    background:#ffffff;
                                    margin-right:4px;
                                }
                                .icon-btn {
                                    padding: 1px 15px 3px 2px;
                                    border-radius:50px;
                                }
                            </style>

                            <!-- Fournisseur -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fournisseur"><?php echo $trad_admin_product_create[$lang]['provider']; ?></label>
                                <div class="col-md-4">
                                    <select id="fournisseur" class="form-control" name="fournisseur" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_fournisseur']; ?>">
                                        <option></option>
                                        <?php
                                        while ($donnees_fournisseur = $reponse_fournisseur->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_fournisseur['code_four'] . "'>" . $donnees_fournisseur['lib_four'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Code produit fournisseur-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_produit_fournisseur"><?php echo $trad_admin_product_create[$lang]['code_prod']; ?></label>  
                                <div class="col-md-4">
                                    <input id="code_produit_fournisseur" class="form-control input-md" name="code_produit_fournisseur" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_code_fournisseur']; ?>">
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Designation -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="designation"><?php echo $trad_admin_product_create[$lang]['designation']; ?></label>  
                                <div class="col-md-4">
                                    <input id="designation" class="form-control input-md" name="designation" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_designation']; ?>">
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Division -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="division"><?php echo $trad_admin_product_create[$lang]['division']; ?></label>
                                <div class="col-md-4">
                                    <select id="division" class="form-control" name="division" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_division']; ?>">
                                        <option></option>
                                        <option value="1000">France</option>
                                        <option value="2000">Group</option>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Pays de provenance -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pays_provenance"><?php echo $trad_admin_product_create[$lang]['pays_prov']; ?></label>
                                <div class="col-md-4">
                                    <select id="pays_provenance" class="form-control" name="pays_provenance" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_pays_provenance']; ?>">
                                        <option></option>
                                        <?php
                                        while ($donnees_pays_provenance = $reponse_pays_provenance->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_pays_provenance['code_pays'] . "'>" . $donnees_pays_provenance['lib_pays'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Code postal -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_postal"><?php echo $trad_admin_product_create[$lang]['code_post']; ?></label>  
                                <div class="col-md-4">
                                    <input id="code_postal" class="form-control input-md" name="code_postal" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_code_postal']; ?>">
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>

                            <!-- Ville -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="ville"><?php echo $trad_admin_product_create[$lang]['ville']; ?></label>  
                                <div class="col-md-4">
                                    <input id="ville" class="form-control input-md" name="ville" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_ville']; ?>">
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>

                            <!-- Pays d'origine -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pays_origine"><?php echo $trad_admin_product_create[$lang]['pays_origine']; ?></label>  
                                <div class="col-md-4">
                                    <select id="pays_origine" class="form-control" name="pays_origine" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_pays_origine']; ?>">
                                        <option></option>
                                        <?php
                                        while ($donnees_pays_origine = $reponse_pays_origine->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_pays_origine['code_pays'] . "'>" . $donnees_pays_origine['lib_pays'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>
                            <div class="form-group" id="group_europe">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="no_prefer"></label>  
                                    <div class="col-md-4">
                                        <label class="checkbox-inline" for="prefer-0">
                                            <input id="prefer-0" name="prefer-0" type="checkbox" value="0" checked>
                                            <?php echo $trad_admin_product_create[$lang]['produit_non_pref']; ?>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="group-prefer">
                                    <!-- Date de debut -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="date_debut_validite"><?php echo $trad_admin_product_create[$lang]['date_debut']; ?></label>  
                                        <div class="col-md-4">
                                            <input id="date_debut_validite" class="form-control input-md" name="date_debut_validite" type="date" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_date_debut']; ?>">
											<span class="help-block with-errors"></span>
                                        </div>										
                                    </div>

                                    <!-- Date de fin -->
                                    <div class="form-group">	
                                        <label class="col-md-4 control-label" for="date_fin_validite"><?php echo $trad_admin_product_create[$lang]['date_fin']; ?></label>  
                                        <div class="col-md-4">
                                            <input id="date_fin_validite" class="form-control input-md" name="date_fin_validite" type="date" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_date_fin']; ?>">
											<span class="help-block with-errors"></span>
                                        </div>										
                                    </div>

                                    <!-- Zones préférentielles -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="zone_pref"><?php echo $trad_admin_product_create[$lang]['zone_pref']; ?></label>
                                        <div class="col-md-4">
                                            <?php
											$zp = '';
                                            $donnees_zone_pref = $reponse_zone_pref->fetchAll();
                                            foreach ($donnees_zone_pref as $requete2) {
                                                echo '<label class="checkbox-inline" for="zone_pref-' . $requete2['code_zone_pref'] . '">';
                                                echo '<input name="zone_' . $requete2['code_zone_pref'] . '" id="zone_' . $requete2['code_zone_pref'] . '" value="' . $requete2['code_zone_pref'] . '" type="checkbox">';
                                                echo $requete2['code_zone_pref'];
												$zp .= '#zone_'.$requete2['code_zone_pref'] . ' ';
												?>
												<script type="text/javascript">
													var zp='<?php echo $zp;?>'
												</script>
												<?php
                                                echo '</label>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!-- Pays par zone préférentielle -->
                                    <div class="form-group" id="group_pays">
                                        <label class="col-md-4 control-label" for="zone_pays"><?php echo $trad_admin_product_create[$lang]['pays_zone']; ?></label>
                                        <div class="col-md-4">
                                            <span id="pays_par_zone" name="pays_par_zone"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Code douanier -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_douanier"><?php echo $trad_admin_product_create[$lang]['code_douane']; ?></label>
                                <div class="col-md-4">
                                    <select id="code_douanier" class="form-control" name="code_douanier" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_code_douanier']; ?>">
                                        <option></option>
                                        <?php
                                        while ($donnees_code_douanier = $reponse_code_douanier->fetch(PDO::FETCH_BOTH)) {
                                            echo "<option value='" . $donnees_code_douanier['code_douanier'] . "'>" . $donnees_code_douanier['code_douanier'] . "</option>";
                                        }
                                        ?>
                                    </select>
									<span class="help-block with-errors"></span>
                                    <span class="help-block" id="definition_code_douanier" name="definition_code_douanier"></span>
                                </div>
                            </div>

                            <!-- Partie Danger -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="danger"><?php echo $trad_admin_product_create[$lang]['danger']; ?></label>
                                <div class="col-md-4">
                                    <label class="checkbox-inline" for="danger-0">
										<?php echo $trad_admin_product_create[$lang]['produit_concerne']; ?> <br>
                                        <input id="danger-0" name="danger-0" value="0" type="radio">
                                        <?php echo $trad_admin_product_create[$lang]['produit_concerne0']; ?> <br>
										<input id="danger-1" name="danger-0" required="" value="0" type="radio">
                                        <?php echo $trad_admin_product_create[$lang]['produit_concerne1']; ?>
										<span class="help-block with-errors"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row" id="group_danger">
                                <section class="col-sm-12 table-responsive">
                                    <table class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th><?php echo $trad_admin_product_create[$lang]['classe_danger']; ?></th>
                                                <th><?php echo $trad_admin_product_create[$lang]['code_onu']; ?></th>
                                                <th><?php echo $trad_admin_product_create[$lang]['code_emballage']; ?></th>
                                                <th><?php echo $trad_admin_product_create[$lang]['mode_transport']; ?></th>
                                                <th><?php echo $trad_admin_product_create[$lang]['contenance']; ?></th>
												<th><?php echo $trad_admin_product_create[$lang]['type']; ?></th>
                                                <th><?php echo $trad_admin_product_create[$lang]['conditionnement']; ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="classe_danger" class="form-control" name="classe_danger" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_classe_danger']; ?>">
                                                                <option></option>
                                                                <?php
                                                                while ($donnees_classe_danger = $reponse_classe_danger->fetch(PDO::FETCH_BOTH)) {
                                                                    echo "<option value='" . $donnees_classe_danger['code_danger'] . "'>" . $donnees_classe_danger['code_danger'] . "</option>";
                                                                }
                                                                ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="code_onu" class="form-control" name="code_onu" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_code_onu']; ?>">
                                                                <option></option>
                                                                <?php
                                                                while ($donnees_code_onu = $reponse_code_onu->fetch(PDO::FETCH_BOTH)) {
                                                                    echo "<option value='" . $donnees_code_onu['code_onu'] . "'>" . $donnees_code_onu['code_onu'] . "</option>";
                                                                }
                                                                ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>	
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select id="code_emballage" class="form-control" name="code_emballage" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_code_emballage']; ?>">
                                                                <option></option>
                                                                <?php
                                                                while ($donnees_code_emballage = $reponse_code_emballage->fetch(PDO::FETCH_BOTH)) {
                                                                    echo "<option value='" . $donnees_code_emballage['code_embal'] . "'>" . $donnees_code_emballage['code_embal'] . "</option>";
                                                                }
                                                                ?>
                                                            </select>
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12" id="transporter">
                                                            <?php
															$tpt = ''; 
                                                            $donnees_mode_transport = $reponse_mode_transport->fetchAll();
                                                            foreach ($donnees_mode_transport as $requete3) {
                                                                echo '<label class="checkbox-inline" for="mode-' . $requete3['code_tpt'] . '">';
                                                                echo '<input name="mode_' . $requete3['code_tpt'] . '" id="id_mode_' . $requete3['code_tpt'] . '" value="' . $requete3['code_tpt'] . '" type="checkbox">';
                                                                echo $requete3['code_tpt'];
																$tpt .= '#id_mode_'.$requete3['code_tpt'] . ' ';
																?>
																<script type="text/javascript">
																	var tpt='<?php echo $tpt;?>'
																</script>
																<?php
                                                                echo '</label>';
                                                            }
                                                            ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-8">
                                                            <input id="contenance" class="form-control input-md" name="contenance" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_quantite']; ?>">
															<span class="help-block with-errors"></span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select id="unite" class="form-control" name="unite" required="">
                                                                <option value=''></option>
																<option value='ml'>ml</option>
                                                                <option value="l">l</option>
                                                                <option value='g'>g</option>
                                                                <option value="kg">kg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </td>
												<td>
                                                    <div class="form-group">
                                                        <div class="col-md-8">
                                                            <input id="type" class="form-control input-md" name="type" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_type_emballage']; ?>">
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <input id="conditionnement_fournisseur" class="form-control input-md" name="conditionnement_fournisseur" type="text" placeholder="" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_conditionnement_fournisseur']; ?>">
															<span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="help-block" id="definition_classe_danger" name="definition_classe_danger"></span></td>
                                                <td><span class="help-block" id="definition_code_onu" name="definition_code_onu"></span></td>
                                                <td><span class="help-block" id="definition_code_emballage" name="definition_code_emballage"></span></td>
                                                <td>
                                                    <?php
                                                    foreach ($donnees_mode_transport as $requete4) {
                                                        echo $requete4['code_tpt'] . '=' . $requete4['lib_tpt'] . '</br>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $trad_admin_product_create[$lang]['explication_contenance'];
                                                    ?>
                                                </td>
												<td>
                                                    <?php
                                                    echo $trad_admin_product_create[$lang]['explication_type'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $trad_admin_product_create[$lang]['explication_conditionnement'];
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </div>

                            <!-- Fichier DLT --> 
                            <div class="form-group" id="groupDLT">
                                <label class="col-md-4 control-label" for="fiche_DLT"><?php echo $trad_admin_product_create[$lang]['DLT']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DLT" class="input-file" name="fiche_DLT" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_dlt']; ?>" type="file" accept="application/pdf"/>
									<span class="help-block with-errors"></span>																		
                                </div>
                            </div>

							<!-- Fichier DE Radio --> 
                            <div class="form-group" id="groupDE_Radio">
                                <label class="col-md-4 control-label" for="fiche_DE_Radio"><?php echo $trad_admin_product_create[$lang]['DE_Radio']; ?></label>
                                <div class="col-md-4">
                                    <?php echo $trad_admin_product_create[$lang]['produit_dlt_concerne']; ?> <br>
									<input id="dlt-0" name="dlt-0" value="0" type="radio">
									<?php echo $trad_admin_product_create[$lang]['produit_concerne0']; ?> <br>
									<input id="dlt-1" name="dlt-0" required="" value="1" type="radio">
									<?php echo $trad_admin_product_create[$lang]['produit_concerne1']; ?>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>
							
							<!-- Fichier DLT PEMD --> 
                            <div class="form-group" id="groupDLTPEMD">
                                <label class="col-md-4 control-label" for="fiche_DLT_PEMD"><?php echo $trad_admin_product_create[$lang]['DLT_PEMD']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DLT_PEMD" class="input-file" name="fiche_DLT_PEMD" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_dlt_pemd']; ?>" type="file" accept="application/pdf"/>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>

							<!-- Fichier DE_PEMD Radio --> 
                            <div class="form-group" id="groupDE_PEMD_Radio">
                                <label class="col-md-4 control-label" for="fiche_DE_PEMD_Radio"><?php echo $trad_admin_product_create[$lang]['DE_PEMD_Radio']; ?></label>
                                <div class="col-md-4">
                                    <?php echo $trad_admin_product_create[$lang]['produit_dlt_concerne']; ?> <br>
									<input id="dlt_pemd-0" name="dlt_pemd-0" value="0" type="radio">
									<?php echo $trad_admin_product_create[$lang]['produit_concerne0']; ?> <br>
									<input id="dlt_pemd-1" name="dlt_pemd-0" required="" value="1" type="radio">
									<?php echo $trad_admin_product_create[$lang]['produit_concerne1']; ?>
									<span class="help-block with-errors"></span>
                                </div>								
                            </div>
							
							<!-- Fichier DE --> 
                            <div class="form-group" id="groupDE">
                                <label class="col-md-4 control-label" for="fiche_DE"><?php echo $trad_admin_product_create[$lang]['DE']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DE" class="input-file" name="fiche_DE" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_de']; ?>" type="file" accept="application/pdf"/>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>

							<!-- Fichier FDS français --> 
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_FDS_fr"><?php echo $trad_admin_product_create[$lang]['FDS_fr']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_FDS_fr" class="input-file" name="fiche_FDS_fr" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_fds_fr']; ?>" type="file" accept="application/pdf"/>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="no_english_file"></label>  
                                <div class="col-md-4">
                                    <label class="checkbox-inline" for="no_english_file">
                                        <input id="no_english_file" name="no_english_file" type="checkbox" value="0">
										<span class="help-block with-errors"></span>
                                        <?php echo $trad_admin_product_create[$lang]['info_fds_en']; ?>
                                    </label>
                                </div>
                            </div>

                            <!-- Fichier FDS anglais --> 
                            <div class="form-group" id="fds_en">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="fiche_FDS_en"><?php echo $trad_admin_product_create[$lang]['FDS_en']; ?></label>
                                    <div class="col-md-4">
                                        <input id="fiche_FDS_en" class="input-file" name="fiche_FDS_en" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_fds_en']; ?>" type="file" accept="application/pdf"/>
										<span class="help-block with-errors"></span>
                                    </div>
                                </div>
                            </div>

                            <!-- Fichier DOF --> 
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fiche_DOF"><?php echo $trad_admin_product_create[$lang]['DOF']; ?></label>
                                <div class="col-md-4">
                                    <input id="fiche_DOF" class="input-file" name="fiche_DOF" required="" data-native-error="<?php echo $trad_admin_product_create[$lang]['error_dof']; ?>" type="file" accept="application/pdf"/>
									<span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <!-- Bouton Valider -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4" id="groupVAL">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_product_create[$lang]['save']; ?></button>
                                </div>
                            </div>

                            <!--<button type="button" id="id_test" class="btn btn-primary" name="id_test">Test</button>-->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/jquery-admin_product.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>
<script type="text/javascript">
	var ma_lang='<?php echo $lang;?>'
</script>