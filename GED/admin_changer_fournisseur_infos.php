<?php
include('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_changer_fournisseur_infos = array(
    'fr' => array(
        'title' => 'Modifier le produit n°',
        'code' => 'Fournisseur',
        'name' => 'Code article fournisseur',
        'save' => 'Enregistrer',
        'error_providercode_1' => 'Le code fournisseur existe déja',
        'error_providercode_2' => 'Le code fournisseur ne peut contenir que des chiffres'
    ),
    'en' => array(
        'title' => 'Edit product n°',
        'code' => 'Provider ',
        'name' => 'Code of the provider\'s article',
        'save' => 'Save',
        'error_providercode_1' => 'The provider code already exists',
        'error_providercode_2' => 'The provider code may contain numbers'
    )
);

$code_sap = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_NUMBER_INT);
$req_provider = $link->query('SELECT * FROM fournisseurs;');

$req_infos_produit = $link->prepare('SELECT * FROM produits WHERE sap_prod = :code_sap;');
if ($req_infos_produit->execute([':code_sap' => $code_sap]) === false) {
    die('Code incorrect. / Wrong code.');
}

$req_infos_produit_check = $req_infos_produit->fetch(PDO::FETCH_ASSOC);
//var_dump($req_infos_produit_check); die;

if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req2" class="lead section-lead has-success"></div>
                    <form class="form-horizontal" id="change_product" name="change_product" data-toggle="validator" role="form">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_changer_fournisseur_infos[$lang]['title'] . $req_infos_produit_check['sap_prod']; ?></legend>

                            <div class="alert alert-info" role="alert">
                                <b>Attention, cette action réinitialise les points suivants :</b>
                                <ul>
                                    <li>Code postal et ville</li>
                                    <li>Pays de provenance et d'origine</li>
                                    <li>Dates de validité</li>
                                    <li>Codes danger, douanier, ONU et emballage</li>
                                    <li>Conditionnement et contenance fournisseur</li>
                                    <li>Fiches</li>
                                </ul>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_four"><?php echo $trad_admin_changer_fournisseur_infos[$lang]['code']; ?></label>
                                <div class="col-md-4">
                                    <select required="" id="provider" name="provider" class="form-control">
                                        <option></option>
                                        <?php
                                        while ($donnees_fournisseur = $req_provider->fetch(PDO::FETCH_BOTH)) {
										?>
                                            <option value="<?= $donnees_fournisseur['code_four'] ?>"<?= $donnees_fournisseur['code_four'] == $req_infos_produit_check['code_four'] ? ' selected' : '' ?>><?= $donnees_fournisseur['lib_four'] ?></option>
										<?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_zone"><?php echo $trad_admin_changer_fournisseur_infos[$lang]['name']; ?></label>
                                <div class="col-md-4">
                                    <input id="code_art_four" class="form-control input-md" name="code_art_four" type="text" placeholder="<?php echo $req_infos_produit_check['code_art_four_prod']; ?>">
                                    <input id="code_actuel" name="code_actuel" type="hidden" value="<?php echo $code_sap; ?>">
                                    <input id="designation_actuelle" name="designation_actuelle" type="hidden" value="<?php echo $req_infos_produit_check['designation']; ?>">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_changer_fournisseur_infos[$lang]['save']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_product.js"></script>
    <?php
		} else {
			echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
		}
		include("./scripts/footer.php");
	?>