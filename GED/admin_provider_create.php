<?php
include('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_provider_create = array(
    'fr' => array(
        'title' => 'Créer un fournisseur',
        'code' => 'Code fournisseur',
        'name' => 'Nom du fournisseur',
        'save' => 'Enregistrer',
        'error_providercode_1' => 'Le code fournisseur existe déja',
        'error_providercode_2' => 'Le code fournisseur ne doit pas être vide et ne peut contenir que des chiffres',
        'insertion_ok' => 'Le fournisseur a été créé avec succès',
        'insertion_ko' => 'Erreur de création'
    ),
    'en' => array(
        'title' => 'Create a provider',
        'code' => 'Provider Code',
        'name' => 'Name of the provider',
        'save' => 'Save',
        'error_providercode_1' => 'The provider code already exists',
        'error_providercode_2' => 'The provider code should not be empty and can contain only numbers',
        'insertion_ok' => 'The provider was successfully created',
        'insertion_ko' => 'Error creating'
    )
);
if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req" class="lead section-lead has-success"></div>

                    <form class="form-horizontal" name="create_provider" data-toggle="validator" role="form" id="create_provider">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_provider_create[$lang]['title']; ?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_four"><?php echo $trad_admin_provider_create[$lang]['code']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_four" 
                                        class="form-control input-md" 
                                        name="code_four" 
                                        type="text" 
                                        placeholder="" 
                                        required=""
                                        data-remote="./scripts/req_providercode.php" 
                                        data-remote-error="<?php echo $trad_admin_provider_create[$lang]['error_providercode_1']; ?>"
                                        pattern="[0-9]+"
                                        data-native-error="<?php echo $trad_admin_provider_create[$lang]['error_providercode_2']; ?>"
                                        maxlength="10"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_four"><?php echo $trad_admin_provider_create[$lang]['name']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom_four" class="form-control input-md" name="nom_four" type="text" placeholder="" required="">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">    
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_provider_create[$lang]['save']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_provider.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>