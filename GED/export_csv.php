<?php 
// Connexion à la base de données 
include('./scripts/dbc.php');

$resQuery = $link->query("SELECT "
        . "p.sap_prod, "
        . "t.code_tpt, "
        . "p.code_onu, "
        . "p.code_danger, "
        . "p.code_embal "
        . "FROM produits p, "
        . "transporter t "
        . "WHERE p.sap_prod = t.sap_prod "); 

//Header permettant la création d'un CSV 
header('Content-Type: application/octet-stream; charset=iso-8859-1'); 
header("Content-disposition: filename=dangereux ".date("d-m-Y").".csv"); 

if ($resQuery->rowCount()!=0) 
{	
	
// on insère les titres des colonnes 
echo "Article;Reglementation;UN;Classe;Groupe emb;Code supp ;";
echo "\n"; 

// on insère les données de la table 
while ($arrSelect = $resQuery->fetch(PDO::FETCH_ASSOC)) 
{ 
foreach($arrSelect as $elem) {echo "$elem;";} 
echo "\n"; 

} 
} 
?>