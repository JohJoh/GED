<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
$code_four = filter_input(INPUT_POST, 'provider', FILTER_SANITIZE_STRING);

$trad_ajax_admin_delete_provider = array(
    'fr' => array(
        'title' => 'Suppression réussie.',
        'start' => 'Vous avez supprimé votre fournisseur ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Delete successful.',
        'start' => 'You\'ve deleted your provider ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_delete_provider = "DELETE FROM fournisseurs WHERE code_four = :code_four;";
$liste_delete_provider = $trad_ajax_admin_delete_provider[$lang]['start'];

//$req_update_user = substr($req_update_user, 0, (strlen($req_update_user)-2));
//$req_update_user = $req_update_user." WHERE id=".$_SESSION['user_id'];
//$liste_modif = substr($liste_modif, 0, (strlen($liste_modif)-2));
if (safeParameteredSQLRequestExecute($link, $req_delete_provider, [':code_four' => $code_four])) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_provider[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_delete_provider . '</div>';
    echo '</div>';
    echo'<meta http-equiv="refresh" content="1; URL=admin_provider_delete.php">';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_provider[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_delete_provider[$lang]['error'] . '</div>';
    echo '</div>';
}
?>