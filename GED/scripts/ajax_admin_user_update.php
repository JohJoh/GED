<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_update_user = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez mis à jour votre utilisateur ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve update your user',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_update_user = "UPDATE users SET ";
$liste_update_user = $trad_ajax_admin_update_user[$lang]['start'];

if (isset($data_post['nom']) && !empty($data_post['nom'])) {
    $req_update_user .= "nom_user = :nom_user, ";
    $params[':nom_user'] = $data_post['nom'];
}

if (isset($data_post['prenom']) && !empty($data_post['prenom'])) {
    $req_update_user .= "pnom_user = :pnom_user, ";
    $params[':pnom_user'] = $data_post['prenom'];
}

if (isset($data_post['email']) && !empty($data_post['email'])) {
    $req_update_user .= "user_email = :user_email, ";
    $params[':user_email'] = $data_post['email'];
}

if (isset($data_post['username']) && !empty($data_post['username'])) {
    $req_update_user .= "user_name = :user_name, ";
    $params[':user_name'] = $data_post['username'];
}

if (isset($data_post['password1']) && !empty($data_post['password1'])) {
    $req_update_user .= "pwd = :pwd, ";
    $params[':pwd'] = PwdHash($data_post['password1']);
}

if (isset($data_post['language']) && !empty($data_post['language'])) {
    $req_update_user .= "lang = :lang, ";
    $params[':lang'] = $data_post['language'];
}
if (isset($data_post['type_compte']) && !empty($data_post['type_compte'])) {
    $req_update_user .= "user_level = :user_level, ";
    $params[':user_level'] = $data_post['type_compte'];
}

if ($data_post['type_compte'] == 1) {
    if (isset($data_post['fournisseur']) && !empty($data_post['fournisseur'])) {
        $req_update_user .= "code_four = :code_four, ";
        $params[':code_four'] = $data_post['fournisseur'];
    }
} else {
    $req_update_user .= "code_four = NULL, ";
}

//on supprime la derniere virgule et l'espace
$req_update_user = mb_substr($req_update_user, 0, -2);

$req_update_user .= " WHERE id = :id;";
$params[':id'] = $data_post['id_user'];

if (safeParameteredSQLRequestExecute($link, $req_update_user, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_user[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_user . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_user[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_update_user[$lang]['error'] . '</div>';
    echo '</div>';
}