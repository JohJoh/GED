<?php

session_start(); 
$lang = $_SESSION['lang'] ;

require_once("dbc.php");

$g_zone = filter_input(INPUT_GET, 'zone', FILTER_SANITIZE_STRING);
$params = array();
$i = 1;
$tab_zones = explode(',', $g_zone);
$sql_zone_pays = "SELECT * FROM zone WHERE ";

foreach ($tab_zones as $zone) {
    $sql_zone_pays.= "code_zone_pref = :zone$i OR ";
    $params[':zone'.$i] = $zone;
    $i++;
}

$sql_zone_pays = mb_substr($sql_zone_pays, 0, -4, "UTF-8");
$sql_zone_pays.= ";";

$req_zone_pays = safeParameteredSQLRequestFetch($link, $sql_zone_pays, $params);

foreach ($req_zone_pays as $result) {
    echo $result["code_zone_pref"]." = ".$result["lib_zone_pref"] . ' <br><br>';
}
