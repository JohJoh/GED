<?php

require_once("dbc.php");

$classe = filter_input(INPUT_GET, 'classe', FILTER_SANITIZE_STRING);

$req_classe = "SELECT lib_danger FROM danger WHERE code_danger = :code_danger;";
$result = safeParameteredSQLRequestFetch($link, $req_classe, [':code_danger' => $classe]);

if (count($result) > 0) {
    echo $result[0]['lib_danger'];
} else {
    echo '';
}