<?php

require_once("dbc.php");

$codesap = filter_input(INPUT_GET, 'code_sap', FILTER_SANITIZE_STRING);

$execute_sap_prod = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT sap_prod FROM produits WHERE sap_prod = :sap_prod;", 
    [':sap_prod' => $codesap]
);

if (count($execute_sap_prod) > 0) {
    http_response_code(418);
} else {
    http_response_code(200);
}