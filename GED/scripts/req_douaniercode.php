<?php

require_once("dbc.php");

$codedouanier = filter_input(INPUT_GET, 'code_douanier', FILTER_SANITIZE_STRING);

$req_codedouanier = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT code_douanier FROM douane WHERE code_douanier = :code_douanier;",
    [':code_douanier' => $codedouanier]
);

$nb_codedouanier = count($req_codedouanier);

if ($nb_codedouanier > 0) {
    http_response_code(418);// HTTP ERROR
} else {
    http_response_code(200);//HTTP OK
}