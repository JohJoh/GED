<?php

require_once("dbc.php");

$code = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRING);

$req_code = "SELECT lib_onu FROM onu WHERE code_onu = :code;";
$result = safeParameteredSQLRequestFetch($link, $req_code, [':code' => $code]);

if (count($result) > 0) {
    echo $result[0]['lib_onu'];
} else {
    echo '';
}