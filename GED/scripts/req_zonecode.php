<?php
require_once("dbc.php");

$codezone = filter_input(INPUT_GET, 'code_zone', FILTER_SANITIZE_STRING);
$req_codezone = safeParameteredSQLRequestFetch(
    $link, 
    "select * from zone where code_zone_pref = :code_zone_pref;", 
    [':code_zone_pref' => $codezone]
);

$nb_codezone = count($req_codezone);

if ($nb_codezone > 0) {
    http_response_code(418);
} else {
    http_response_code(200);
}