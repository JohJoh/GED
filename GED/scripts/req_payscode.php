<?php

require_once("dbc.php");

$codepays = filter_input(INPUT_GET, 'code_pays', FILTER_SANITIZE_STRING);

$req_codepays = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT code_pays FROM pays WHERE code_pays = :code_pays;", 
    [':code_pays' => $codepays]
);

$nb_codepays = count($req_codepays);

//echo $usrnme;
if ($nb_codepays > 0) {
    http_response_code(418);
} else {
    http_response_code(200);
}