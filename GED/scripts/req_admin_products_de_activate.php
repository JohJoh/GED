<?php
require_once("dbc.php");
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_product_de_re_activate = array(
    'fr' => array(
        'code_sap' => 'Code SAP',
        'designation' => 'Désignation',
        'activation' => 'Activer',
        'desactivation' => 'Désactiver'
    ),
    'en' => array(
        'code_sap' => 'SAP code',
        'designation' => 'Designation',
        'desactivation' => 'Desactivate',
        'activation' => 'Activate'
    )
);

$page = $_GET['page'];
if (isset($_GET['search'])) {
    $search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
} else {
    $search = "";
}
$deb_limit = (($page * 100) - 100);

$req_search_sap_nbpage = "SELECT sap_prod, designation FROM produits WHERE sap_prod LIKE :search;";
$sap_code_nbpage = safeParameteredSQLRequestFetch($link, $req_search_sap_nbpage, [':search' => "%" . $search . "%"]);

$nb_sap = count($sap_code_nbpage);
$nb_pages = ceil($nb_sap / 100);

$req_search_sap = "SELECT sap_prod, designation, is_active FROM produits WHERE sap_prod LIKE :search LIMIT " . $deb_limit . ",100 ";
$produit = safeParameteredSQLRequestFetch($link, $req_search_sap, [':search' => "%" . $search . "%"]);
?>
<script src="./js/jquery.bootpag.min.js"></script>
<div class="pages_top" style="text-align:center;"></div>

<div>
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th width="10%"><?php echo $trad_admin_product_de_re_activate[$lang]['code_sap'] ?></th>
                <th><?php echo $trad_admin_product_de_re_activate[$lang]['designation'] ?></th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($produit as $donnees_produit) {
                echo '<tr>';
                echo '<td>' . $donnees_produit['sap_prod'] . '</td>';
                echo '<td>' . $donnees_produit['designation'] . '</td>';
                if ($donnees_produit['is_active'] == 0) {
                    echo "<td><button type='button' name='activate' value='" . $donnees_produit['sap_prod'] . "'>" . $trad_admin_product_de_re_activate[$lang]['activation'] . "</button></td>";
                } else {
                    echo "<td><button type='button' name='activate' value='" . $donnees_produit['sap_prod'] . "'>" . $trad_admin_product_de_re_activate[$lang]['desactivation'] . "</button></td>";
                }
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<div class="pages_bottom" style="text-align:center;"></div>

<script>
    $("button").click(function () {
        $.post("scripts/req_admin_product_de_activate_mysql.php", {
            sap: this.value,
        }, function (data) {
            $.ajax({
                url: './scripts/req_admin_products_de_activate.php',
                type: 'GET',
                data: 'page=' + <?php echo $_GET["page"]; ?> + "&search=" + $('#search_code_sap').val(),
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#content').html(code_html);
//                    console.log('load page: ok');
                },
                error: function (resultat, statut, erreur) {
                    alert('Erreur : ' + erreur);
                }
            });
        });
    });
    $('.pages_top,.pages_bottom').bootpag({
        total: <?php echo $nb_pages; ?>,
        page: <?php echo $page; ?>,
        maxVisible: 10,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        //$(".content4").html("Page " + num);
        $.ajax({
            url: './scripts/req_admin_products_de_activate.php',
            type: 'GET',
            data: 'page=' + num + "&search=" + $('#search_code_sap').val(),
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#content').html(code_html);
//                console.log('load page: ok');
            },
            error: function (resultat, statut, erreur) {
                alert('Erreur : ' + erreur);
            }
        });
    });
</script>
