<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_update_code = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez mis à jour votre code douanier ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve update your customs ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_update_code = "UPDATE douane SET ";
$liste_update_code = $trad_ajax_admin_update_code[$lang]['start'];

if (isset($data_post['code_douanier']) && !empty($data_post['code_douanier'])) {
    $req_update_code .= "code_douanier = :code_douanier, ";
    $params[':code_douanier'] = $data_post['code_douanier'];
}

if (isset($data_post['nom_douanier']) && !empty($data_post['nom_douanier'])) {
    $req_update_code .= "lib_douanier = :nom_douanier, ";
    $params[':nom_douanier'] = $data_post['nom_douanier'];
}

$req_update_code = substr($req_update_code, 0, (strlen($req_update_code) - 2));
$req_update_code .= " WHERE code_douanier = :code_actuel;";
$params[':code_actuel'] = $data_post['code_actuel'];

if (safeParameteredSQLRequestExecute($link, $req_update_code, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_code[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_code . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_code[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_update_code[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}
?>