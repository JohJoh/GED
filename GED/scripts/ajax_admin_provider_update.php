<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
$code_four = filter_input(INPUT_POST, 'code_four', FILTER_SANITIZE_STRING);
$lib_four = filter_input(INPUT_POST, 'nom_four', FILTER_SANITIZE_STRING);
$code_actuel = filter_input(INPUT_POST, 'code_actuel', FILTER_SANITIZE_STRING);
$params = array();

$trad_ajax_admin_update_provider = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez mis à jour votre fournisseur ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve update your provider',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_update_provider = "UPDATE fournisseurs SET ";
$liste_update_provider = $trad_ajax_admin_update_provider[$lang]['start'];

if (isset($_POST['code_four']) && !empty($_POST['code_four'])) {
    $req_update_provider .= "code_four = :code_four, ";
    $params[':code_four'] = $code_four;
}

if (isset($_POST['nom_four']) && !empty($_POST['nom_four'])) {
    $req_update_provider .= "lib_four = :lib_four, ";
    $params[':lib_four'] = $lib_four;
}

$req_update_provider = mb_substr($req_update_provider, 0, -2, "UTF-8");
$req_update_provider .= " WHERE code_four = :code_actuel;";
$params[':code_actuel'] = $code_actuel;

if (safeParameteredSQLRequestExecute($link, $req_update_provider, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_provider[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_provider . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_provider[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_update_provider[$lang]['error'] . '</div>';
    echo '</div>';
}
?>