<?php

require_once("dbc.php");

$code = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRING);

$req_code = "SELECT lib_douanier FROM douane WHERE code_douanier = :code;";
$result = safeParameteredSQLRequestFetch($link, $req_code, [':code' => $code]);

if (count($result) > 0) {
    echo $result[0]['lib_douanier'];
} else {
    echo '';
}
