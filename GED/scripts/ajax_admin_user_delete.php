<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
$user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);

$trad_ajax_admin_delete_user = array(
    'fr' => array(
        'title' => 'Suppression réussie.',
        'start' => 'Vous avez supprimé votre utilisateur ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Delete successful.',
        'start' => 'You\'ve deleted your user ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$liste_delete_user = $trad_ajax_admin_delete_user[$lang]['start'];

if (isset($user) && !empty($user)) {
    $req_delete_user = "DELETE FROM users WHERE id = :user;";
}else{
    die("Utilisateur introuvable.");
}

if (safeParameteredSQLRequestExecute($link, $req_delete_user, [':user' => $user])) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_user[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_delete_user . '</div>';
    echo '</div>';
    echo'<meta http-equiv="refresh" content="1; URL=admin_user_delete.php">';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_user[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_delete_user[$lang]['error'] . '</div>';
    echo '</div>';
}
?>