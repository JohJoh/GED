<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_delete_code = array(
    'fr' => array(
        'title' => 'Suppression réussie.',
        'start' => 'Vous avez supprimé votre code douanier ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Delete successful.',
        'start' => 'You\'ve deleted your customs ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_delete_code = "DELETE FROM douane WHERE code_douanier = ";
$liste_delete_code = $trad_ajax_admin_delete_code[$lang]['start'];

if (isset($data_post['code']) && !empty($data_post['code'])) {
    $req_delete_code .= ":code_douanier ;";
    $params[':code_douanier'] = $data_post['code'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['lastname'];
}

//$req_update_user = substr($req_update_user, 0, (strlen($req_update_user)-2));
//$req_update_user = $req_update_user." WHERE id=".$_SESSION['user_id'];
//$liste_modif = substr($liste_modif, 0, (strlen($liste_modif)-2));

if (safeParameteredSQLRequestExecute($link, $req_delete_code, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_code[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_delete_code . '</div>';
    echo '</div>';
    echo'<meta http-equiv="refresh" content="1; URL=admin_zone_delete.php">';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_code[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_delete_code[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}
?>