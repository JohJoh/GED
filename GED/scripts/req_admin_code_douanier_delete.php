<?php
require_once("dbc.php");
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_code_douanier = array(
    'fr' => array(
        'code_douanier' => 'Code douanier',
        'lib_douanier' => 'Désignation',
        'supprimer' => 'Supprimer'
    ),
    'en' => array(
        'code_douanier' => 'Customs Code',
        'lib_douanier' => 'Designation',
        'supprimer' => 'Supprimer'
    )
);

$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
$deb_limit = (($page * 100) - 100);

$req_search_code_nbpage = "SELECT code_douanier, lib_douanier FROM douane WHERE code_douanier LIKE :search ;";
$code_douanier_nbpage = safeParameteredSQLRequestFetch($link, $req_search_code_nbpage, [':search' => "%" . $search . "%"]);

$nb_codes = count($code_douanier_nbpage);
$nb_pages = ceil($nb_codes / 100);

$req_search_code = "SELECT code_douanier, lib_douanier FROM douane WHERE code_douanier LIKE :search LIMIT " . $deb_limit . ",100 ";
$code_douaniers = safeParameteredSQLRequestFetch($link, $req_search_code, [':search' => "%" . $search . "%"]);
?>

<script src="./js/jquery.bootpag.min.js"></script>
<div class="pages_top" style="text-align:center;"></div>
<div>
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th width="10%"><?php echo $trad_admin_code_douanier[$lang]['code_douanier'] ?></th>
                <th><?php echo $trad_admin_code_douanier[$lang]['lib_douanier'] ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($code_douaniers as $code_douanier) {
                echo '<tr>';
                echo '<td>' . $code_douanier['code_douanier'] . '</td>';
                echo '<td>' . $code_douanier['lib_douanier'] . '</td>';
                echo "<td><button type='button' name='activate' value='" . $code_douanier['code_douanier'] . "' >" . $trad_admin_code_douanier[$lang]['supprimer'] . "</button></td>";
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<div class="pages_bottom" style="text-align:center;"></div>
<script>
    $("button").click(function () {
        if (confirm("Voulez-vous vraiment supprimer ce code douanier ?")) {  // Clic sur OK
            $.post("scripts/req_admin_code_douanier_delete_mysql.php", {
                code_douanier: this.value,
            }, function (data) {
                $.ajax({
                    url: './scripts/req_admin_code_douanier_delete.php',
                    type: 'GET',
                    data: 'page=' + <?php echo $_GET["page"]; ?> + "&search=" + $('#search_code_douanier').val(),
                    dataType: 'html',
                    success: function (code_html, statut) { // code_html contient le HTML renvoyé
                        $('#content').html(code_html);
                    },
                    error: function (resultat, statut, erreur) {
                        alert('Erreur : ' + erreur);
                    }
                });

            });
        }
    });
    $('.pages_top,.pages_bottom').bootpag({
        total: <?php echo $nb_pages; ?>,
        page: <?php echo $page; ?>,
        maxVisible: 10,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        $.ajax({
            url: './scripts/req_admin_code_douanier_delete.php',
            type: 'GET',
            data: 'page=' + num + "&search=" + $('#search_code_douanier').val(),
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#content').html(code_html);
            },
            error: function (resultat, statut, erreur) {
                alert('Erreur : ' + erreur);
            }
        });
    });
</script>