<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_update_country = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez mis à jour votre pays ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve update your country',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_update_country = "UPDATE pays SET ";
$liste_update_country = $trad_ajax_admin_update_country[$lang]['start'];

if (isset($data_post['code_pays']) && !empty($data_post['code_pays'])) {
    $req_update_country .= "code_pays = :code_pays, ";
    $params[':code_pays'] = $data_post['code_pays'];
}

if (isset($data_post['nom_pays']) && !empty($data_post['nom_pays'])) {
    $req_update_country .= "lib_pays = :nom_pays, ";
    $params[':nom_pays'] = $data_post['nom_pays'];
}

if (isset($data_post['pays_europeen'])) {
    if (empty($data_post['pays_europeen'])) {
        $req_update_country .= "estEurope = 0";
    } else {
        $req_update_country .= "estEurope = 1";
    }
}

$req_update_country .= " WHERE code_pays = :code_actuel;";
$params[':code_actuel'] = $data_post['code_actuel'];

if (safeParameteredSQLRequestExecute($link, $req_update_country, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_country[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_country . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_country[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_update_country[$lang]['error'] . '</div>';
    echo '</div>';
}