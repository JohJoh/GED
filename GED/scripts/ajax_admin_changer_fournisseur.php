<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];

$trad_ajax_admin_changer_fournisseur = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez changé de fournisseur sur ce produit ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You changed provider with this product ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$liste_update_product = $trad_ajax_admin_changer_fournisseur[$lang]['start'];

$req_update_product = "UPDATE produits SET ";
$params = [];

if (isset($_POST['fournisseur']) && !empty($_POST['fournisseur'])) {
    $req_update_product .= "code_four = :code_four,";
    $params[':code_four'] = filter_input(INPUT_POST, 'fournisseur', FILTER_SANITIZE_STRING);
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['lastname'];
}

if (isset($_POST['code_art_four']) && !empty($_POST['code_art_four'])) {
    $params[':code_art_four'] = filter_input(INPUT_POST, 'code_art_four', FILTER_SANITIZE_STRING);
    $req_update_product .= "code_art_four_prod = :code_art_four_prod,";
}

//$req_update_product = substr($req_update_product, 0, (strlen($req_update_product)-1));
$params[':designation'] = filter_input(INPUT_POST, 'designation', FILTER_SANITIZE_STRING);
$params[':sap_prod'] = filter_input(INPUT_POST, 'code_actuel', FILTER_SANITIZE_STRING);
$req_update_product .= "date_dern_modif_prod = NOW(), "
            . "code_postal_prov=NULL, "
            . "date_deb_validite=NULL, "
            . "date_fin_validite=NULL, "
            . "code_pays=NULL, "
            . "code_danger=NULL, "
            . "code_douanier=NULL, "
            . "code_onu=NULL, "
            . "code_embal=NULL, "
            . "code_pays_origine=NULL, "
            . "designation = :designation, "
            . "ville_prov=NULL, "
            . "fic_dlt=NULL, "
			. "fic_de=NULL, "
			. "fic_pemd=NULL, "
			. "fic_de_pemd=NULL, "
            . "fic_fds_fr=NULL, "
            . "fic_fds_en=NULL, "
            . "fic_decl_four=NULL, "
            . "condi_four=NULL, "
            . "contenance_prod=NULL, "
            . "commentaire=NULL, "
            . "com_ged_auto='Changement de fournisseur' "
        . "WHERE sap_prod = :sap_prod;";

//$liste_modif = substr($liste_modif, 0, (strlen($liste_modif)-2));

$prep = $link->prepare($req_update_product);

if ($prep->execute($params) !== false) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_changer_fournisseur[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_product . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_changer_fournisseur[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_changer_fournisseur[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}
