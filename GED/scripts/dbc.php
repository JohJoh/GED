<?php

/* * ************* PHP LOGIN SCRIPT V 2.3*********************
  (c) Balakrishnan 2010. All Rights Reserved

  Usage: This script can be used FREE of charge for any commercial or personal projects. Enjoy!

  Limitations:
  - This script cannot be sold.
  - This script should have copyright notice intact. Dont remove it please...
  - This script may not be provided for download except from its original site.

  For further usage, please contact me.

  /******************** MAIN SETTINGS - PHP LOGIN SCRIPT V2.1 **********************
  Please complete wherever marked xxxxxxxxx

  /************* MYSQL DATABASE SETTINGS *****************
  1. Specify Database name in $dbname
  2. MySQL host (localhost or remotehost)
  3. MySQL user name with ALL previleges assigned.
  4. MySQL password

  Note: If you use cpanel, the name will be like account_database
 * *********************************************************** */

/* PDO CONV */

//LOCALHOST
/* define("DB_HOST", "localhost"); // set database host
  define("DB_USER", "root"); // set database user
  define("DB_PASS", ""); // set database password
  define("DB_NAME", "ged_pdo"); // set database name
 */

//SERVOR
define ("DB_HOST", "localhost"); // set database host
define ("DB_USER", "root"); // set database user
define ("DB_PASS",""); // set database password
define ("DB_NAME","ged"); // set database name

/* $link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die("Connection impossible.");
  mysqli_query ($link, 'SET NAMES UTF8'); */

try {
    //;charset=utf8
    $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $link->exec("SET NAMES UTF8");
} catch (PDOException $e) {
    echo 'Erreur : ' . $e->getMessage();
}


/* Registration Type (Automatic or Manual)
  1 -> Automatic Registration (Users will receive activation code and they will be automatically approved after clicking activation link)
  0 -> Manual Approval (Users will not receive activation code and you will need to approve every user manually)
 */
$user_registration = 1;  // set 0 or 1

define("COOKIE_TIME_OUT", 10); //specify cookie timeout in days (default is 10 days)
define('SALT_LENGTH', 9); // salt for password
//define ("ADMIN_NAME", "admin"); // sp

/* Specify user levels */
define("ADMIN_LEVEL", 5);
define("USER_LEVEL", 2);
define("FOURNISSEUR_LEVEL", 1);
define("GUEST_LEVEL", 0);

/* * ************* reCAPTCHA KEYS*************** */
$publickey = "6LcQnvMSAAAAADjNojIgw3TSBtHSXqR7kooKwGHV";
$privatekey = "6LcQnvMSAAAAAPHhs70W6h_84d6_x24MaadOYAFD";

//Creer un fichier CSV
/*
  Exemple :
  $fichier = new FichierExcel();
  $fichier->Colonne("Col1;Col2;Col3");
  $fichier->Insertion("COUCOU;SALUT;BONJOUR");//ici on peut faire une boucle si nécessaire.
  $fichier->output('NomFichier');
 */
class FichierExcel {

    private
            $csv = Null;

    /**
     * Cette ligne permet de créer les colonnes du fichers Excel
     * Cette fonction est totalement faculative, on peut faire la même chose avec la
     * fonction insertion, c'est juste une clarté pour moi
     */
    function Colonne($file) {

        $this->csv.=$file . "\n";
        return $this->csv;
    }

    /**
     * Insertion des lignes dans le fichiers Excel, il faut introduire les données sous formes de chaines
     * de caractère.
     * Attention a séparer avec une virgule.
     */
    function Insertion($file) {

        $this->csv.=$file . "\n";
        return $this->csv;
    }

    /**
     * fonction de sortie du fichier avec un nom spécifique.
     *
     */
    function output($NomFichier) {

        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=$NomFichier.csv");
        print $this->csv;
        exit;
    }

}

/* * ** PAGE PROTECT CODE  ********************************
  This code protects pages to only logged in users. If users have not logged in then it will redirect to login page.
  If you want to add a new page and want to login protect, COPY this from this to END marker.
  Remember this code must be placed on very top of any html or php page.
 * ****************************************************** */

function page_protect() {
    session_start();

    global $db;

    /* Secure against Session Hijacking by checking user agent */
    if (isset($_SESSION['HTTP_USER_AGENT'])) {
        if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])) {
            logout();
            exit;
        }
    }

// before we allow sessions, we need to check authentication key - ckey and ctime stored in database

    /* If session not set, check for cookies set by Remember me */
    if (!isset($_SESSION['user_id']) && !isset($_SESSION['user_name'])) {
        if (isset($_COOKIE['user_id']) && isset($_COOKIE['user_key'])) {
            /* we double check cookie expiry time against stored in database */

            $cookie_user_id = filter($_COOKIE['user_id']);
            //$rs_ctime = mysqli_query($link, "select `ckey`,`ctime` from `users` where `id` ='$cookie_user_id'") or die(mysql_error());
            $rs_ctime = $link->query("select `ckey`,`ctime` from `users` where `id` ='$cookie_user_id'");

            //list($ckey,$ctime) = mysqli_fetch_row($rs_ctime);
            list($ckey, $ctime) = $rs_ctime->fetch(PDO::FETCH_NUM);

            // coookie expiry
            if ((time() - $ctime) > 60 * 60 * 24 * COOKIE_TIME_OUT) {

                logout();
            }
            /* Security check with untrusted cookies - dont trust value stored in cookie.
              /* We also do authentication check of the `ckey` stored in cookie matches that stored in database during login */

            if (!empty($ckey) && is_numeric($_COOKIE['user_id']) && isUserID($_COOKIE['user_name']) && $_COOKIE['user_key'] == sha1($ckey)) {
                session_regenerate_id(); //against session fixation attacks.

                $_SESSION['user_id'] = $_COOKIE['user_id'];
                $_SESSION['user_name'] = $_COOKIE['user_name'];
                /* query user level from database instead of storing in cookies */
                //list($user_level) = mysqli_fetch_row(mysql_query("select user_level from users where id='$_SESSION[user_id]'"));
                $prep_req = $link->prepare("select `ckey`,`ctime` from `users` where `id` ='$cookie_user_id'");
                list($user_level) = $prep_req->fetch(PDO::FETCH_NUM);


                $_SESSION['user_level'] = $user_level;
                $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
            } else {
                logout();
            }
        } else {
            header("Location: login.php");
            exit();
        }
    }
}

function date_eu($date_fr) {
    $date_eu = explode("/", $date_fr);
    return $date_eu[2] . "-" . $date_eu[1] . "-" . $date_eu[0];
}

function date_fr($date_eu) {
    $date_fr = explode("-", $date_eu);
    return $date_fr[2] . "/" . $date_fr[1] . "/" . $date_fr[0];
}

function filter($data) {

//    try {
//        //;charset=utf8
//        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
//        $link->exec("SET NAMES UTF8");
//    } catch (PDOException $e) {
//        echo 'Erreur : ' . $e->getMessage();
//    }
    //$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die("Connection impossible.");
    $data = trim(htmlentities(strip_tags($data)));
//
    if (get_magic_quotes_gpc())
        $data = stripslashes($data);

//    $data = filter_var($data, FILTER_SANITIZE_STRING);
    //$data = mysqli_real_escape_string($link, $data);

    return $data;
}

function EncodeURL($url) {
    $new = strtolower(ereg_replace(' ', '_', $url));
    return($new);
}

function DecodeURL($url) {
    $new = ucwords(ereg_replace('_', ' ', $url));
    return($new);
}

function ChopStr($str, $len) {
    if (strlen($str) < $len)
        return $str;

    $str = substr($str, 0, $len);
    if ($spc_pos = strrpos($str, " "))
        $str = substr($str, 0, $spc_pos);

    return $str . "...";
}

function isEmail($email) {
    return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}

function isUserID($username) {
    if (preg_match('/^[a-z\d_]{5,20}$/i', $username)) {
        return true;
    } else {
        return false;
    }
}

function isURL($url) {
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
        return true;
    } else {
        return false;
    }
}

function checkPwd($x, $y) {
    if (empty($x) || empty($y)) {
        return false;
    }
    if (strlen($x) < 4 || strlen($y) < 4) {
        return false;
    }

    if (strcmp($x, $y) != 0) {
        return false;
    }
    return true;
}

function GenPwd($length = 7) {
    $password = "";
    $possible = "0123456789bcdfghjkmnpqrstvwxyz"; //no vowels

    $i = 0;

    while ($i < $length) {


        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);


        if (!strstr($password, $char)) {
            $password .= $char;
            $i++;
        }
    }

    return $password;
}

function GenKey($length = 7) {
    $password = "";
    $possible = "0123456789abcdefghijkmnopqrstuvwxyz";

    $i = 0;

    while ($i < $length) {


        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);


        if (!strstr($password, $char)) {
            $password .= $char;
            $i++;
        }
    }

    return $password;
}

function logout(PDO $link) {
    
//    $link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die("Connection impossible.");
    global $db;
    session_start();

//    $sess_user_id = strip_tags(mysqli_real_escape_string($link, $_SESSION['user_id']));
//    $cook_user_id = strip_tags(mysqli_real_escape_string($link, $_COOKIE['user_id']));

    if (isset($sess_user_id) || isset($cook_user_id)) {
        $update = $link->prepare("UPDATE users SET ckey = '', ctime = '' WHERE id = :sess_id ;");
        $params[':sess_id'] = $_SESSION['user_id'];
//        $params[':cook_id'] = $cook_user_id;
        $update->execute($params);
        
//        mysqli_query($link, "update `users`
//			set `ckey`= '', `ctime`= ''
//			where `id`='$sess_user_id' OR  `id` = '$cook_user_id'") or die(mysqli_error($link));
    }

    /*     * ********** Delete the sessions*************** */
    unset($_SESSION['user_id']);
    unset($_SESSION['user_name']);
    unset($_SESSION['user_level']);
    unset($_SESSION['HTTP_USER_AGENT']);
    session_unset();
    session_destroy();

    /* Delete the cookies****************** */
//    setcookie("user_id", '', time() - 60 * 60 * 24 * COOKIE_TIME_OUT, "/");
//    setcookie("user_name", '', time() - 60 * 60 * 24 * COOKIE_TIME_OUT, "/");
//    setcookie("user_key", '', time() - 60 * 60 * 24 * COOKIE_TIME_OUT, "/");

    header("Location: login.php");
}

// Password and salt generation
function PwdHash($pwd, $salt = null) {
    if ($salt === null) {
        $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
    } else {
        $salt = substr($salt, 0, SALT_LENGTH);
    }
    return $salt . sha1($pwd . $salt);
}

function checkAdmin() {
    if (isset($_SESSION['user_level'])) {
        if ($_SESSION['user_level'] >= ADMIN_LEVEL) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function checkUserLyreco() {
    if (isset($_SESSION['user_level'])) {
        if ($_SESSION['user_level'] >= USER_LEVEL) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function checkFourniseur() {
    if (isset($_SESSION['user_level'])) {
        if ($_SESSION['user_level'] >= FOURNISSEUR_LEVEL) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function loadPays() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allCountry = $link->query("select * from pays");

    for ($allCountry = array(); $row = $req_allCountry->fetch(PDO::FETCH_ASSOC); $allCountry[array_shift($row)] = $row)
        ;
    return $allCountry;
}

function loadSupplier() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allSupplier = $link->query("select * from fournisseurs ORDER BY lib_four");

    for ($allSupplier = array(); $row = $req_allSupplier->fetch(PDO::FETCH_ASSOC); $allSupplier[array_shift($row)] = $row)
        ;
    return $allSupplier;
}

function loadZone() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allZones = $link->query("select * from zone");

    for ($allZones = array(); $row = $req_allZones->fetch(PDO::FETCH_ASSOC); $allZones[array_shift($row)] = $row)
        ;
    return $allZones;
}

function loadMode() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allModes = $link->query("select * from mode_tpt");

    for ($allModes = array(); $row = $req_allModes->fetch(PDO::FETCH_ASSOC); $allModes[array_shift($row)] = $row)
        ;
    return $allModes;
}

function loadEmbal() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allEmbal = $link->query("select * from emballage");

    for ($allEmbal = array(); $row = $req_allEmbal->fetch(PDO::FETCH_ASSOC); $allEmbal[array_shift($row)] = $row)
        ;
    return $allEmbal;
}

function loadDang() {

    try {
        $link = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', '' . DB_USER . '', '' . DB_PASS . '');
        $link->exec("SET NAMES UTF8");
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }

    $req_allEmbal = $link->query("select * from danger");

    for ($allEmbal = array(); $row = $req_allEmbal->fetch(PDO::FETCH_ASSOC); $allEmbal[array_shift($row)] = $row)
        ;
    return $allEmbal;
}

function mb_ucfirst($str, $encoding = "UTF-8") {
    return mb_strtoupper(mb_substr($str, 0, 1, $encoding)) . mb_substr($str, 1, null, $encoding);
}

function fullDebug($max_data = 2048) {
    ini_set('xdebug.var_display_max_depth', 10);
    ini_set('xdebug.var_display_max_children', 512);
    ini_set('xdebug.var_display_max_data', $max_data);
}

function isAssoc(array $arr) {
    if (array() === $arr)
        return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

function safeParameteredSQLRequestFetch(PDO $link, $request, array $fieldsAndValues) {
    /**
     * Checks
     */
    if (($requestIsEmpty = $request === '') || ($arrayIsNotAssoc = !isAssoc($fieldsAndValues))) {
        if ($requestIsEmpty) {
            throw new InvalidArgumentException("La requête est une chaîne vide.");
        }
        if ($arrayIsNotAssoc && !empty($fieldsAndValues)) {
            throw new InvalidArgumentException("Votre tableau est mal formé. Veuillez passer un tableau associatif (dictionnaire).");
        }
    }

    /**
     * Request
     */
    try {
        $prep = $link->prepare($request);
        if ($prep instanceof PDOStatement) {
            if ($prep->execute($fieldsAndValues) !== false) {
                return $prep->fetchAll(PDO::FETCH_ASSOC);
            } else {
                throw new Exception("La requête ne s'est pas bien exécutée.");
            }
        }
    } catch (PDOException $ex) {
        throw $ex;
    }
}

function safeParameteredSQLRequestExecute(PDO $link, $request, array $fieldsAndValues) {
    /**
     * Checks
     */
    if (($requestIsEmpty = $request === '') || ($arrayIsNotAssoc = !isAssoc($fieldsAndValues))) {
        if ($requestIsEmpty) {
            throw new InvalidArgumentException("La requête est une chaîne vide.");
        }
        if ($arrayIsNotAssoc && !empty($fieldsAndValues)) {
            throw new InvalidArgumentException("Votre tableau est mal formé. Veuillez passer un tableau associatif (dictionnaire).");
        }
    }

    /**
     * Request
     */
    try {
        $prep = $link->prepare($request);
        if ($prep instanceof PDOStatement) {
            $prep->execute($fieldsAndValues);
            return true;
        }
    } catch (PDOException $ex) {
        throw $ex;
    }
}

function checkFourniseurSpool(PDO $link, $sap) {
    $code_fournisseur = safeParameteredSQLRequestFetch($link, "SELECT code_four FROM produits WHERE sap_prod = :sap_prod", [':sap_prod' => $sap]);
    return isset($_SESSION['user_level']) && (($_SESSION['user_level'] >= FOURNISSEUR_LEVEL) && ($code_fournisseur[0]["code_four"] === $_SESSION['user_code_four']));
}
