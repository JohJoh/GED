<?php
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_menu = array(
    'fr' => array(
        'title' => '<span class="glyphicon glyphicon-home" aria-hidden="true"></span> Accueil',
        'home' => 'Accueil',
        'profil' => '<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Mon profil',
        'products' => '<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mes produits',
        'admin' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Administration',
        'logout' => '<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Déconnexion',
        'dangerous' => 'Voir les produits dangereux',
        'help' => '<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Aide',
        'gestion_site' => 'Gestion du site',
        'gestion_utilisateurs' => 'Gestion des utilisateurs',
        'gestion_zones' => 'Gestion des zones préférentielles',
        'gestion_pays' => 'Gestion des pays',
        'gestion_codes' => 'Gestion des codes douaniers',
        'gestion_fournisseurs' => 'Gestion des fournisseurs',
        'gestion_produits' => 'Gestion des produits',
        'create' => 'Création',
        'update' => 'Modification',
        'delete' => 'Suppression',
        'extract' => 'Extraction',
        'create_product' => 'Créer un produit',
        'create_products' => 'Charger des produits',
        'change_provider' => 'Changer de fournisseur',
        'desactivate_reactivate_products' => 'Désactiver/Réactiver les produits',
        'export_donnees' => '<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Exportation des données',
        'fichier_dangereux' => 'Exporter le fichier des dangereux',
        'fichier_zones_1000' => 'Exporter le fichier des zones division 1000',
        'fichier_zones_2000' => 'Exporter le fichier des zones division 2000',
        'download_dlt' => 'Telecharger les fichiers DLT',
		'download_de' => 'Telecharger les fichiers DE',
		'download_pemd' => 'Telecharger les fichiers PEMD',
		'download_de_pemd' => 'Telecharger les fichiers DE_PEMD',
        'download_fds_fr' => 'Telecharger les fichiers FDS FR',
        'download_fds_en' => 'Telecharger les fichiers FDS EN',
        'download_df' => 'Telecharger les fichiers Déclaration d’origine'
    ),
    'en' => array(
        'title' => '<span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home',
        'home' => 'Home',
        'profil' => '<span class="glyphicon glyphicon-user" aria-hidden="true"></span> My profil',
        'products' => '<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> My products',
        'admin' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Administration',
        'logout' => '<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout',
        'dangerous' => 'See Hazard Products',
        'help' => '<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Help',
        'gestion_site' => 'Site Management',
        'gestion_utilisateurs' => 'User Management',
        'gestion_zones' => 'Management of preferential zones',
        'gestion_pays' => 'Country Management',
        'gestion_codes' => 'Management of customs codes',
        'gestion_fournisseurs' => 'Supplier Management',
        'gestion_produits' => 'Product Management',
        'create' => 'Create',
        'update' => 'Update',
        'delete' => 'Delete',
        'extract' => 'Extract',
        'create_product' => 'Create product',
        'create_products' => 'Create products',
        'change_provider' => 'Load provider',
        'desactivate_reactivate_products' => 'Desactivate/Reactivate products',
        'export_donnees' => '<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Export data',
        'fichier_dangereux' => 'Export dangerous file',
        'fichier_zones_1000' => 'Export the file division zones 1000',
        'fichier_zones_2000' => 'Export the file division zones 2000',
        'download_dlt' => 'Download the DLT files',
		'download_de' => 'Download the DC files',
		'download_pemd' => 'Download the PEMD files',
		'download_de_pemd' => 'Download the DC PEMD files',
        'download_fds_fr' => 'Download the FDS FR files',
        'download_fds_en' => 'Download the FDS EN files',
        'download_df' => 'Download the Declaration of origin files'
    )
);
?>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.php"><?php echo $trad_menu[$lang]['title']; ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                if (isset($_SESSION['user_level'])) {
                    if ($_SESSION['user_level'] > 0) {
                        ?>
                        <li>
                            <a href="myaccount.php"><?= $trad_menu[$lang]['profil'] ?></a>
                        </li>
                        <li>
                            <a href="allproduct.php"><?= $trad_menu[$lang]['products'] ?></a>
                        </li>
                        <?php if (checkAdmin()) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $trad_menu[$lang]['admin'] ?> <b class="caret"></b></a>

                                <ul class="dropdown-menu">
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_utilisateurs'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_user_create.php"><?= $trad_menu[$lang]['create'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_user_update.php"><?= $trad_menu[$lang]['update'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_user_delete.php"><?= $trad_menu[$lang]['delete'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_utilisateurs.php">
                                                    <span class="glyphicon glyphicon-save"></span>  <?= $trad_menu[$lang]['extract'] ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_zones'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_zone_create.php"><?= $trad_menu[$lang]['create'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_zone_update.php"><?= $trad_menu[$lang]['update'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_zone_delete.php"><?= $trad_menu[$lang]['delete'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_zone.php">
                                                    <span class="glyphicon glyphicon-save"></span> <?= $trad_menu[$lang]['extract'] ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_pays'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_country_create.php"><?= $trad_menu[$lang]['create'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_country_update.php"><?= $trad_menu[$lang]['update'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_country_delete.php"><?= $trad_menu[$lang]['delete'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_pays.php"><span class="glyphicon glyphicon-save"></span> <?= $trad_menu[$lang]['extract'] ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_codes'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_code_douanier_create.php"><?= $trad_menu[$lang]['create'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_code_douanier_update.php"><?= $trad_menu[$lang]['update'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_code_douanier_delete.php"><?= $trad_menu[$lang]['delete'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_douanier.php"><span class="glyphicon glyphicon-save"></span> <?= $trad_menu[$lang]['extract'] ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_fournisseurs'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_provider_create.php"><?= $trad_menu[$lang]['create'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_provider_update.php"><?= $trad_menu[$lang]['update'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_provider_delete.php"><?= $trad_menu[$lang]['delete'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_fournisseurs.php"><span class="glyphicon glyphicon-save"></span> <?= $trad_menu[$lang]['extract'] ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['gestion_produits'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="admin_product_create.php"><?= $trad_menu[$lang]['create_product'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_import_csv_products.php"><?= $trad_menu[$lang]['create_products'] ?></a>
                                            </li>
                                            <li>
                                                <a href="changer_fournisseur.php"><?= $trad_menu[$lang]['change_provider'] ?></a>
                                            </li>
                                            <li>
                                                <a href="admin_products_de_activate.php"><?= $trad_menu[$lang]['desactivate_reactivate_products'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_produits.php"><span class="glyphicon glyphicon-save"></span> <?= $trad_menu[$lang]['extract'] ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1"><?= $trad_menu[$lang]['export_donnees'] ?></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="export_csv.php"><?= $trad_menu[$lang]['fichier_dangereux'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_txt.php?divi=1000"><?= $trad_menu[$lang]['fichier_zones_1000'] ?></a>
                                            </li>
                                            <li>
                                                <a href="export_txt.php?divi=2000"><?= $trad_menu[$lang]['fichier_zones_2000'] ?></a>
                                            </li>
                                            <li>
                                                <a href="create_zip.php?fic=fic_dlt"><?= $trad_menu[$lang]['download_dlt'] ?></a>
                                            </li>
											<li>
                                                <a href="create_zip.php?fic=fic_de"><?= $trad_menu[$lang]['download_de'] ?></a>
                                            </li>
											<li>
                                                <a href="create_zip.php?fic=fic_pemd"><?= $trad_menu[$lang]['download_pemd'] ?></a>
                                            </li>
											<li>
                                                <a href="create_zip.php?fic=fic_de_pemd"><?= $trad_menu[$lang]['download_de_pemd'] ?></a>
                                            </li>
                                            <li>
                                                <a href="create_zip.php?fic=fic_fds_fr"><?= $trad_menu[$lang]['download_fds_fr'] ?></a>
                                            </li>
                                            <li>
                                                <a href="create_zip.php?fic=fic_fds_en"><?= $trad_menu[$lang]['download_fds_en'] ?></a>
                                            </li>
                                            <li>
                                                <a href="create_zip.php?fic=fic_decl_four"><?= $trad_menu[$lang]['download_df'] ?></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <li><a href="help.php"><?= $trad_menu[$lang]['help'] ?></a></li>
                    <?php } else { ?>
                        <li>
                            <a href="myaccount.php"><?= $trad_menu[$lang]['home'] ?></a>
                        </li>
                        <li>
                            <a href="alldangerous.php"><?= $trad_menu[$lang]['dangerous'] ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['user_level']) && $_SESSION['user_level'] > 0) { ?>
                    <li>
                        <a href="logout.php"><?= $trad_menu[$lang]['logout']; ?></a>
                    </li>
                <?php } ?>
                <?php if (!isset($_SESSION['lang'])) { ?>
                    <li>
                        <img src="" id="flag_fr" class="flag flag-fr" lang="fr">
                    </li>
                    <li>
                        <img src="" id="flag_en" class="flag flag-gb" lang="en">
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
