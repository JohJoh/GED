<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_delete_country = array(
    'fr' => array(
        'title' => 'Suppression réussie.',
        'start' => 'Vous avez supprimé votre pays ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Delete successful.',
        'start' => 'You\'ve deleted your country ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$liste_delete_country = $trad_ajax_admin_delete_country[$lang]['start'];

if (isset($data_post['country']) && !empty($data_post['country'])) {
    $req_delete_country = "DELETE FROM pays WHERE code_pays = :country;";
    $params[':country'] = $data_post['country'];
}

if (safeParameteredSQLRequestExecute($link, $req_delete_country, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_country[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_delete_country . '</div>';
    echo '</div>';
    echo'<meta http-equiv="refresh" content="1; URL=admin_country_delete.php">';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_country[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_delete_country[$lang]['error'] . '</div>';
    echo '</div>';
}