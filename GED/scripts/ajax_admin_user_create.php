<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING); 
}
$params = array();

$trad_ajax_admin_create_user = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre utilisateur ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your user ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_create_user = "INSERT INTO users(nom_user, pnom_user, user_name, user_email, lang, pwd, user_level, code_four) VALUES(";
$liste_create_user = $trad_ajax_admin_create_user[$lang]['start'];

if (isset($data_post['nom']) && !empty($data_post['nom'])) {
    $req_create_user .= ":nom, ";
    $params[':nom'] = $data_post['nom'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['lastname'];
}

if (isset($data_post['prenom']) && !empty($data_post['prenom'])) {
    $req_create_user .= ":prenom, ";
    $params[':prenom'] = $data_post['prenom'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['firstname'];
}

if (isset($data_post['username']) && !empty($data_post['username'])) {
    $req_create_user .= ":username, ";
    $params[':username'] = $data_post['username'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['username'];
} else {
    $req_create_user = $req_create_user . "NULL, ";
}

if (isset($data_post['email']) && !empty($data_post['email'])) {
    $req_create_user .= ":email, ";
    $params['email'] = $data_post['email'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['email'];
}

if (isset($data_post['language']) && !empty($data_post['language'])) {
    $req_create_user .= ":language, ";
    $params[':language'] = $data_post['language'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['language'];
    //$_SESSION['lang'] = $data_post['language'];
}
if (isset($data_post['password1']) && !empty($data_post['password1'])) {
    $req_create_user .= ":password, ";
    $params[':password'] = PwdHash($data_post['password1']);
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['password'];
}

if (isset($data_post['type_compte']) && !empty($data_post['type_compte'])) {
    $req_create_user .= ":type_compte, ";
    $params[':type_compte'] = $data_post['type_compte'];
}
if ($data_post['type_compte'] === '1') {
    if (isset($data_post['fournisseur']) && !empty($data_post['fournisseur'])) {
        $req_create_user .= ":fournisseur );";
        $params[':fournisseur'] = $data_post['fournisseur'];
    }
} else {
    $req_create_user .= "NULL);";
}

if (safeParameteredSQLRequestExecute($link, $req_create_user, $params)) {
    $user_id = $link->LastInsertId();
    $md5_id = md5($user_id);
    safeParameteredSQLRequestExecute($link, "UPDATE users SET md5_id = :md5_id WHERE id = :user_id;", [":md5_id" => $md5_id, ":user_id" => $user_id]);
   
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_user[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_user . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_user[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_user[$lang]['error'] . '</div>';
    echo '</div>';
}