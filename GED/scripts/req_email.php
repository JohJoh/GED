<?php
require_once("dbc.php");

$email = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_STRING);

$req_email = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT id FROM users WHERE user_email = :user_email;", 
    [':user_email' => $email]
);

$nb_email = count($req_email);

if ($nb_email > 0) {
    http_response_code(418); // I'm a teapot. Email is bad.
} else {
    http_response_code(200); // Email is good
}