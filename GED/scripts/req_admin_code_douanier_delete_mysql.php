<?php
require_once("dbc.php");
$code_douanier = filter_input(INPUT_POST, 'code_douanier', FILTER_SANITIZE_STRING);
safeParameteredSQLRequestExecute(
        $link, 
        'DELETE '
        . 'FROM douane '
        . 'where code_douanier = :code;', 
        [':code' => $code_douanier]);