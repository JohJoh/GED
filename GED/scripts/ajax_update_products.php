<?php
session_start();
require_once('dbc.php');
$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_update_products = array(
    'fr' => array(
        'title' => 'Modification réussie',
        'start' => ' Vous avez modifié vos produits',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez le formulaire'
    ),
    'en' => array(
        'title' => 'Update successful',
        'start' => 'You\'ve updated your products',
        'title_error' => 'An error has occured',
        'error' => 'Check out the form'
    )
);

if ($data_post['pays_origine']) {
    $pays_europeen_check = $link->query("SELECT * FROM pays WHERE code_pays='" . $data_post['pays_origine'] . "';");
    $pays_europeen = $pays_europeen_check->fetch();
}
$liste_update_products = $trad_ajax_update_products[$lang]['start'];

$codes_sap = explode(",", $data_post["code_actuel"]);
$i = 0;

foreach ($_POST as $key => $value) {
    var_dump($key);
    $data[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}

foreach ($codes_sap as $code) {

    $req_update_products = "UPDATE produits SET ";
    $nb_zones = array();
    $nb_modes = array();
    $exist = false;
    //$champs_obligatoires=false;

    if (isset($data['fournisseur'])) {
        if ($data['fournisseur'] !== "") {
            $req_update_products .= "code_four ='" . $data['fournisseur'] . "', ";
        }
    }

    if (isset($data['code_produit_fournisseur'])) {
        if ($data['code_produit_fournisseur'] !== "") {
            $req_update_products = $req_update_products . "code_art_four_prod ='" . $data['code_produit_fournisseur'] . "', ";
        }
    }

    if (isset($data['designation'])) {
        if ($data['designation'] !== "") {
            $req_update_products = $req_update_products . "designation ='" . $data['designation'] . "', ";
        }
    }

    if (isset($data['division'])) {
        if ($data['division'] !== "") {
            $req_update_products = $req_update_products . "division_prod ='" . $data['division'] . "', ";
        }
    }

    if (isset($data['pays_provenance'])) {
        if ($data['pays_provenance'] !== "") {
            $req_update_products = $req_update_products . "code_pays='" . $data['pays_provenance'] . "', ";
        }
    }

    if (isset($data['code_postal'])) {
        if ($data['code_postal'] !== "") {
            $req_update_products = $req_update_products . "code_postal_prov='" . $data['code_postal'] . "', ";
        }
    }

    if (isset($data['ville'])) {
        if ($data['ville'] !== "") {
            $req_update_products = $req_update_products . "ville_prov='" . $data['ville'] . "', ";
        }
    }

    if (isset($data['pays_origine'])) {
        if ($data['pays_origine'] !== "") {
            $req_update_products = $req_update_products . "code_pays_origine='" . $data['pays_origine'] . "', ";
        }
    }

    if (isset($data['date_debut_validite'])) {
        if ("" !== $data['date_debut_validite']) {
            $req_update_products = $req_update_products . "date_deb_validite=";
            if (($pays_europeen['estEurope'] == "0") or ( $data['no_prefer'] == "0")) {
                $req_update_products = $req_update_products . "'NULL', ";
            } else {
                $req_update_products = $req_update_products . "'" . date($data['date_debut_validite']) . "', ";
            }
        }
    } else {
        $req_update_products = $req_update_products . "date_deb_validite='NULL', ";
    }

    if (isset($data['date_fin_validite'])) {
        if ("" !== $data['date_fin_validite']) {
            $req_update_products = $req_update_products . "date_fin_validite=";
            if (($pays_europeen['estEurope'] == "0") or ( $data['no_prefer'] == "0")) {
                $req_update_products = $req_update_products . "'NULL', ";
            } else {
                $req_update_products = $req_update_products . "'" . date($data['date_fin_validite']) . "', ";
            }
        }
    } else {
        $req_update_products = $req_update_products . "date_fin_validite='NULL', ";
    }

    if (isset($data['code_douanier'])) {
        if ($data['code_douanier'] !== "") {
            $req_update_products = $req_update_products . "code_douanier='" . $data['code_douanier'] . "', ";
        }
    }

    if ($data['danger'] !== "0") {
        if (isset($data['classe_danger'])) {
            if ($data['classe_danger'] !== "") {
                $req_update_products = $req_update_products . "classe_danger='" . $data['classe_danger'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "classe_danger=NULL, ";
        }

        if (isset($data['code_onu'])) {
            if ($data['code_onu'] !== "") {
                $req_update_products = $req_update_products . "code_onu='" . $data['code_onu'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "code_onu=NULL, ";
        }

        if (isset($data['code_emballage'])) {
            if ($data['code_emballage'] !== "") {
                $req_update_products = $req_update_products . "code_embal='" . $data['code_emballlage'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "code_embal=NULL, ";
        }

        if (isset($data['contenance'])) {
            if ($data['contenance'] !== "") {
                $req_update_products = $req_update_products . "contenance_prod='" . $data['contenance'] . " " . $data['unite'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "contenance_prod=NULL, ";
        }

		if (isset($data['type'])) {
            if ($data['type'] !== "") {
                $req_update_products = $req_update_products . "type_prod='" . $data['type'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "type_prod=NULL, ";
        }

        if (isset($data['conditionnement'])) {
            if ($data['conditionnement'] !== "") {
                $req_update_products = $req_update_products . "condi_four='" . $data['conditionnement'] . "', ";
            }
        } else {
            $req_update_products = $req_update_products . "condi_four=NULL, ";
        }

        $req_delete_modes = "delete from transporter where sap_prod = :code";
        $stmt_delete_modes = $link->prepare($req_delete_modes);
        $stmt_delete_modes->bindValue(":code", $code);
        $stmt_delete_modes->execute();
        if ($data['mode_transport'] != "") {
            $modes = explode(",", $data['mode_transport']);
            foreach ($modes as $mode) {
                $req_ajout_modes = "INSERT INTO transporter (`id_transporter`,`sap_prod`, `code_tpt`) VALUES (NULL, :code , :mode);";
                $stmt_ajout_modes = $link->prepare($req_ajout_modes);
                $stmt_ajout_modes->bindValue(":code", $code);
                $stmt_ajout_modes->bindValue(":mode", $mode);
                $stmt_ajout_modes->execute();
            }
        }
    }
	
	//Fiche DLT
	echo $_FILES['fiche_dlt']['name']."<BR>";
	if(isset($_FILES['fiche_dlt']['name'])){
		$uploaddir_dlt = './uploads/';
		$uploadfile_dlt = $uploaddir_dlt ."dlt_".$data['code_sap'].".pdf";
		if(file_exists($uploadfile_dlt)){
			copy($uploadfile_dlt, $uploaddir_dlt."dlt_".$data['code_sap']."_".date("YmdHis").".pdf");
		}
		if (move_uploaded_file($_FILES['fiche_dlt']['tmp_name'], $uploadfile_dlt)) {
			$req_update_products = $req_update_products . "fic_dlt='" . $uploaddir_dlt . "', ";
		}
	}else{
		$req_update_products = $req_update_products . "fic_dlt=NULL, ";
	}

	//Fiche DLT PEMD
	echo $_FILES['fiche_dlt_pemd']['name']."<BR>";
	if(isset($_FILES['fiche_pemd']['name'])){
		$uploaddir_dlt_pemd = './uploads/';
		$uploadfile_dlt_pemd = $uploaddir_dlt_pemd ."dlt_pemd_".$data['code_sap'].".pdf";
		if(file_exists($uploadfile_dlt_pemd)){
			copy($uploadfile_dlt_pemd, $uploaddir_dlt_pemd."dlt_pemd_".$data['code_sap']."_".date("YmdHis").".pdf");
		}
		if (move_uploaded_file($_FILES['fiche_dlt_pemd']['tmp_name'], $uploadfile_dlt_pemd)) {
			$req_update_products = $req_update_products . "fic_dlt_pemd='" . $uploaddir_dlt_pemd . "', ";
		}
	}else{
		$req_update_products = $req_update_products . "fic_dlt_pemd=NULL, ";
	}
	
	//Fiche DE
	echo $_FILES['fiche_de']['name']."<BR>";
	if(isset($_FILES['fiche_de']['name'])){
		$uploaddir_de = './uploads/';
		$uploadfile_de = $uploaddir_de ."de_".$data['code_sap'].".pdf";
		if(file_exists($uploadfile_de)){
			copy($uploadfile_de, $uploaddir_de."de_".$data['code_sap']."_".date("YmdHis").".pdf");
		}
		if (move_uploaded_file($_FILES['fiche_de']['tmp_name'], $uploadfile_de)) {
			$req_update_products = $req_update_products . "fic_de='" . $uploaddir_de . "', ";
		}
	}else{
		$req_update_products = $req_update_products . "fic_de=NULL, ";
	}

	//Fiche FDS FR
    if(isset($_FILES['fiche_fds_fr']['name'])){
		$uploaddir_fds_fr = './uploads/';
		$uploadfile_fds_fr = $uploaddir_fds_fr ."fds_fr_".$data['code_sap'].".pdf";
		if(file_exists($uploadfile_fds_fr)){
			copy($uploadfile_fds_fr, $uploaddir_fds_fr."archives/fds_fr_".$data['code_sap']."_".date("YmdHis").".pdf");
		}
		if (move_uploaded_file($_FILES['fiche_fds_fr']['tmp_name'], $uploadfile_fds_fr)) {
			$req_update_products = $req_update_products . "fic_fds_fr='" . $uploaddir_fds_fr . "', ";
		}
	}else {
        $req_update_products = $req_update_products . "fic_fds_fr='" . $data['fiche_fds_fr'] . "', ";
    }

	//Fiche FDS EN 
    if ($data['no_english_fds_file'] !== "0") {
        if(isset($_FILES['fiche_fds_en']['name'])){
			$uploaddir_fds_en = './uploads/';
			$uploadfile_fds_en = $uploaddir_fds_en ."fds_en_".$data['code_sap'].".pdf";
			if(file_exists($uploadfile_fds_en)){
				copy($uploadfile_fds_en, $uploaddir_fds_en."archives/fds_en_".$data['code_sap']."_".date("YmdHis").".pdf");
			}
			if (move_uploaded_file($_FILES['fiche_fds_en']['tmp_name'], $uploadfile_fds_en)) {
				$req_update_products = $req_update_products . "fic_fds_en='" . $uploaddir_fds_en . "', ";
			}
		}else {
            $req_update_products = $req_update_products . "fic_fds_en=NULL, ";
        }
    } else {
        $req_update_products = $req_update_products . "fic_fds_en=NULL, ";
    }
	
	//Fiche DOF
    if(isset($_FILES['fiche_dof']['name'])){
		$uploaddir_dof = './uploads/';
		$uploadfile_dof = $uploaddir_dof ."dof_".$data['code_sap'].".pdf";
		if(file_exists($uploadfile_dof)){
			copy($uploadfile_dof, $uploaddir_dof."archives/dof_".$data['code_sap']."_".date("YmdHis").".pdf");
		}
		if (move_uploaded_file($_FILES['fiche_dof']['tmp_name'], $uploadfile_dof)) {
			$req_update_products = $req_update_products . "fic_decl_four='" . $data['fiche_dof'] . "', ";
		}
    }else{
        $req_update_products = $req_update_products . "fic_decl_four=NULL, ";
    }

    $req_update_products = $req_update_products . "is_active=1, date_dern_modif_prod='" . date('Y-m-d H:i:s') . "' WHERE sap_prod =:code;";
    //$req_update_products=$req_update_products."is_active=1, date_dern_modif_prod='".date('Y-m-d H:i:s')."' WHERE sap_prod ='2' ;";

    $stmt = $link->prepare($req_update_products);
    $stmt->bindValue(":code", $code);
    $stmt->execute();

    if ($data['no_prefer'] != "0") {
        if ($data['zone_pref'] != "") {
            if ($data['zone_pays'] != "") {
                //CONDITION SUR LES ZONES COCHEES
                $req_delete_pays = "delete from lien_produit_zone_pays where sap_prod=:code;";
                $stmt_delete_pays = $link->prepare($req_delete_pays);
                $stmt_delete_pays->bindValue(":code", $code);
                $stmt_delete_pays->execute();
                $zone_pays = explode(";", $data['zone_pays']);
                $zones = explode(",", $data['zone_pref']);

                foreach ($zones as $zone) {
                    foreach ($zone_pays as $pays) {
                        $req_ajout_pays = "INSERT INTO lien_produit_zone_pays (`sap_prod`, `code_zone_pref`, `code_pays`) VALUES(:code, :zone, :pays);";
                        $stmt_ajout_pays = $link->prepare($req_ajout_pays);
                        $stmt_ajout_pays->bindValue(":code", $code);
                        $stmt_ajout_pays->bindValue(":zone", $zone);
                        $stmt_ajout_pays->bindValue(":pays", $pays);
                        $stmt_ajout_pays->execute();
                    }
                }
            }
        } else {
            $req_check_pays_zones = $link->query("SELECT * from lien_zone_pays_defaut where code_zone_pref='ACP';");
            While ($zones_pays = $req_check_pays_zones->fetch(PDO::FETCH_BOTH)) {
                $req_ajouts_pays = "INSERT INTO lien_produit_zone_pays(`sap_prod`, `code_zone_pref`, `code_pays`) VALUES(:code, 'ACP', '" . $zones_pays['code_pays'] . "');";
                $stmt_ajouts_pays = $link->prepare($req_ajouts_pays);
                $stmt_ajouts_pays->bindValue(":code", $code);
                $stmt_ajouts_pays->execute();
            }
        }
    } else {
        $req_check_pays_zone = $link->query("SELECT * from lien_zone_pays_defaut where code_zone_pref='ACP';");
        While ($zone_pays = $req_check_pays_zone->fetch(PDO::FETCH_BOTH)) {
            $req_ajout_pays = "INSERT INTO lien_produit_zone_pays(`sap_prod`, `code_zone_pref`, `code_pays`) VALUES(:code, 'ACP', '" . $zone_pays['code_pays'] . "');";
            $stmt_ajout_pays = $link->prepare($req_ajout_pays);
            $stmt_ajout_pays->bindValue(":code", $code);
            $stmt_ajout_pays->execute();
        }
    }
    $i = $i + 1;
}
if ($i > 0) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_products . '</div>';
	echo'<meta http-equiv="refresh" content="1; URL=allproduct.php">';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_update_products[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}
?>