<?php
require_once("dbc.php");

$codefour = filter_input(INPUT_GET, 'code_four', FILTER_SANITIZE_STRING);

$req_codefour = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT code_four FROM fournisseurs WHERE code_four = :code_four;", 
    [':code_four' => $codefour]
);

$nb_codefour = count($req_codefour);

if ($nb_codefour > 0) {
    http_response_code(418);
} else {
    http_response_code(200);
}