<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_myaccount = array(
    'fr' => array(
        'title' => 'Mise à jour réussie.',
        'start' => 'Vous avez mis à jour votre ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve updated your ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_update_user = "UPDATE users SET ";
$liste_modif = $trad_ajax_myaccount[$lang]['start'];

if (isset($data_post['lastname']) && !empty($data_post['lastname'])) {
    $req_update_user .= "nom_user = :nom_user, ";
    $params[':nom_user'] = $data_post['lastname'];
    $liste_modif .= $trad_ajax_myaccount[$lang]['lastname'];
}

if (isset($data_post['firstname']) && !empty($data_post['firstname'])) {
    $req_update_user .= "pnom_user = :pnom_user, ";
    $params[':pnom_user'] = $data_post['firstname'];
    $liste_modif .= $trad_ajax_myaccount[$lang]['firstname'];
}

if (isset($data_post['username']) && !empty($data_post['username'])) {
    $req_update_user .= "user_name = :user_name, ";
    $params[':user_name'] = $data_post['username'];
    $liste_modif .= $trad_ajax_myaccount[$lang]['username'];
}

if (isset($data_post['email']) && !empty($data_post['email'])) {
    $req_update_user .= "user_email = :user_email, ";
    $params[':user_email'] = $data_post['email'];
    $liste_modif .= $trad_ajax_myaccount[$lang]['email'];
}

if (isset($data_post['language']) && ($lang <> $data_post['language'])) {
    $req_update_user .= "lang  = :lang, ";
    $params[':lang'] = $data_post['language'];
    $liste_modif .= $trad_ajax_myaccount[$lang]['language'];
    $_SESSION['lang'] = $data_post['language'];
}

if (isset($data_post['new_pwd1']) && !empty($data_post['new_pwd1'])) {
    $req_update_user .= "pwd = :pwd, ";
    $params[':pwd'] = PwdHash($data_post['new_pwd1']);
    $liste_modif .= $trad_ajax_myaccount[$lang]['password'];
}
$req_update_user = mb_substr($req_update_user, 0, -2, "UTF-8");
$req_update_user .= " WHERE id = :id;";
$params[':id'] = $_SESSION['user_id'];

$liste_modif = mb_substr($liste_modif, 0, -2);

if (safeParameteredSQLRequestExecute($link, $req_update_user, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_myaccount[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_modif . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_myaccount[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_myaccount[$lang]['error'] . '</div>';
    echo '</div>';
}