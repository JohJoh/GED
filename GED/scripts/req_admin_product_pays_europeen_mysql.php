<?php
require_once("dbc.php");

$pays_origine = filter_input(INPUT_POST, 'pays_origine', FILTER_SANITIZE_STRING);

$req_pays_europeen = 'SELECT estEurope FROM pays WHERE code_pays = :code_pays;';

$resultat_pays_europeen = safeParameteredSQLRequestFetch($link, $req_pays_europeen, [':code_pays' => $pays_origine]);

echo $resultat_pays_europeen[0]["estEurope"];
