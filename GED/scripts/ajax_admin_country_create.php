<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_create_country = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre pays ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your country ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_create_country = "INSERT INTO pays(code_pays, lib_pays, estEurope) VALUES(";
$liste_create_country = $trad_ajax_admin_create_country[$lang]['start'];

if (isset($data_post['code_pays']) && !empty($data_post['code_pays'])) {
    $req_create_country .= ":code_pays, ";
    $params[':code_pays'] = $data_post['code_pays'];
}

if (isset($data_post['nom_pays']) && !empty($data_post['nom_pays'])) {
    $req_create_country .= ":nom_pays,";
    $params[':nom_pays'] = $data_post['nom_pays'];
}

if (isset($data_post['pays_europeen'])) {
    if (empty($data_post['pays_europeen'])) {
        $req_create_country .= "0)";
    } else {
        $req_create_country .= "1)";
    }
}
//$req_update_user = substr($req_update_user, 0, (strlen($req_update_user)-2));
//$req_update_user = $req_update_user." WHERE id=".$_SESSION['user_id'];
//$liste_modif = substr($liste_modif, 0, (strlen($liste_modif)-2));

if (safeParameteredSQLRequestExecute($link, $req_create_country, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_country[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_country . ' ' . $data_post['code_pays'] . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_country[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_country[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}
?>