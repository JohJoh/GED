<?php
require_once("dbc.php");

$sap = filter_input(INPUT_POST, 'sap', FILTER_SANITIZE_STRING);
$recup_activate = "SELECT is_active FROM produits WHERE sap_prod = :sap;";
$activate = safeParameteredSQLRequestFetch($link, $recup_activate, [':sap' => $sap]);


if($activate[0]["is_active"] == 1){
    safeParameteredSQLRequestExecute($link, 'UPDATE produits SET is_active=0 WHERE sap_prod = :sap;', [':sap' => $sap]);
}else{
    safeParameteredSQLRequestExecute($link, 'UPDATE produits SET is_active=1 WHERE sap_prod = :sap;', [':sap' => $sap]);
}