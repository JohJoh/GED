<!DOCTYPE html>
<html lang="fr">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Gestion Electronique de Document pour Lyreco France">
        <meta name="author" content="bdhaussy">

        <title>GED Lyreco</title>

        <!-- Bootstrap Core CSS -->
        <!--<link href="http://getbootstrap.com/assets/css/docs.min.css" rel="stylesheet">-->
        <!--<link href="css/highlight.css" rel="stylesheet">-->
        <!--<link href="css/main.css" rel="stylesheet">-->

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sweetalert.css" rel="stylesheet">
        <link href="css/swal-forms.css" rel="stylesheet">
        <link href="css/bootstrap-switch.css" rel="stylesheet">
        <link href="css/full-width-pics.css" rel="stylesheet">
        <link href="css/lang.css" rel="stylesheet">
        <link href="css/bootstrap-multiselect.css" rel="stylesheet">
        <style>
            .dropdown-submenu{position:relative;}
            .dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
            .dropdown-submenu:hover>.dropdown-menu{display:block;}
            .dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
            .dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
            .dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
            
            section {
                padding: 10px 0 0 0;
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="js/jquery-lang.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/validator.js"></script>
        <script src="js/sweetalert.min.js"></script>
        <script src="js/swal-forms.js"></script>
        <script src="js/bootstrap-switch.js"></script>
        <script src="js/jquery.bootpag.min.js"></script>
        <script src='js/table-fixed-header.js'></script>
        <script src="js/highlight.js"></script>
        <script src="js/bootstrap-multiselect.js"></script>

    </head>

    <body>