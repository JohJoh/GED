<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_create_zone = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre zone ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your zone ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_create_zone = "INSERT INTO zone (code_zone_pref, lib_zone_pref) VALUES(";
$liste_create_zone = $trad_ajax_admin_create_zone[$lang]['start'];

if (isset($data_post['code_zone']) && !empty($data_post['code_zone'])) {
    $req_create_zone .= ":code_zone_pref, ";
    $params[':code_zone_pref'] = $data_post['code_zone'];
}

if (isset($data_post['nom_zone']) && !empty($data_post['nom_zone'])) {
    $req_create_zone .= ":nom_zone);";
    $params[':nom_zone'] = $data_post['nom_zone'];
}

if (safeParameteredSQLRequestExecute($link, $req_create_zone, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_zone[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_zone . ' ' . $data_post['code_zone'] . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_zone[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_zone[$lang]['error'] . '</div>';
    echo '</div>';
}
