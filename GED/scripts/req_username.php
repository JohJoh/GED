<?php
require_once("dbc.php");

$lg = filter_input(INPUT_GET, 'lang', FILTER_SANITIZE_STRING);

$req_lg = safeParameteredSQLRequestFetch(
    $link, 
    "SELECT lang FROM users WHERE lang = :lang;", 
    [':lang' => $lg]
);

$nb_lg = count($req_lg);

//echo $lg;
if ($nb_lg > 0) {
    http_response_code(418); // I'm a teapot. Lang is bad.
} else {
    http_response_code(200); // Lang is good
}