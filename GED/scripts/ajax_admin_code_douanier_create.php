<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_create_code = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre code douanier ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your code ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_create_code = "INSERT INTO douane(code_douanier, lib_douanier) VALUES( ";

$liste_create_code = $trad_ajax_admin_create_code[$lang]['start'];

if (isset($data_post['code_douanier']) && !empty($data_post['code_douanier'])) {
    $req_create_code .= ":code_douanier, ";
    $params[':code_douanier'] = $data_post['code_douanier'];
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['lastname'];
}

if (isset($data_post['nom_douanier']) && !empty($data_post['nom_douanier'])) {
    $req_create_code .= ":nom_douanier );";
    $params[':nom_douanier'] = $data_post['nom_douanier'];
}

if (safeParameteredSQLRequestExecute($link, $req_create_code, $params)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_code[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_code . ' ' . $data_post['code_douanier'] . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_code[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_code[$lang]['error'] . '</div>';
    echo '</div>';
}
?>