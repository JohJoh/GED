<?php
require_once("dbc.php");
session_start();
$default_lang = 'fr';
$params = array();

foreach ($_GET as $key => $value) {
    $data[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING); // post variables are filtered
}

if (!isset($_SESSION['lang'])) {
    if (isset($data['lang'])) {
        if (($data['lang'] == "fr") || ($data['lang'] == "en")) {
            $lang = $data['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_code_douanier = array(
    'fr' => array(
        'th_sap' => 'Code SAP',
        'th_four' => 'Fournisseur',
        'th_desi' => 'Designation',
        'th_c_prod' => 'Code produit',
        'th_prov' => 'Provenance',
        'th_prov_pays' => 'Pays',
        'th_prov_cp' => 'Code Postal',
        'th_prov_vill' => 'Ville',
        'th_orig' => 'Pays d\'origine',
        'th_zone' => 'Zones préférentielles',
        'th_preu' => 'Preuve d\'origine préférentielle',
        'th_preu_deb' => 'Début',
        'th_preu_fin' => 'Fin',
        'th_c_doua' => 'Code douanier',
        'th_dang' => 'Danger',
        'th_dang_clas' => 'Classe de danger',
        'th_dang_onu' => 'Numéro ONU',
        'th_dang_emba' => 'Groupe d\'emballage',
        'th_dang_mode' => 'Mode de transport',
        'th_fic' => 'Fichiers',
        'th_fic_dlt' => 'DLT',
		'th_fic_dlt_pemd' => 'DLT PEMD',
		'th_fic_de' => 'DE',
		'th_fic_fds_fr' => 'FDS FR',
        'th_fic_fds_en' => 'FDS EN',
        'th_fic_dof' => 'Déclaration d\'origine',
        'td_donnee_manquante' => 'Donnée manquante',
        'td_donnee_non_communiquee' => 'Donnée non communiquée',
        'td_produit_non_dangereux' => 'Produit non dangereux',
        'td_donnee_non_necessaire' => 'Donnée non nécessaire',
        'new_value' => 'Entrez une nouvelle valeur',
        'valid' => 'Valider',
        'modification' => 'Modifié',
        'no_packing_group' => 'Pas de groupe d\'emballage'
    ),
    'en' => array(
        'th_sap' => 'Code SAP',
        'th_four' => 'Supplier',
        'th_desi' => 'Description',
        'th_c_prod' => 'Supplier art. nber.',
        'th_prov' => 'Shipping',
        'th_prov_pays' => 'Country',
        'th_prov_cp' => 'Zip code',
        'th_prov_vill' => 'Town',
        'th_orig' => 'Country of origin',
        'th_zone' => 'Preferential zones',
        'th_preu' => 'Proof of preferential origin',
        'th_preu_deb' => 'Start',
        'th_preu_fin' => 'End',
        'th_c_doua' => 'Customs code',
        'th_dang' => 'Hazard',
        'th_dang_clas' => 'Hazard class',
        'th_dang_onu' => 'UN Number',
        'th_dang_emba' => 'Packing group',
        'th_dang_mode' => 'Transport mode',
        'th_fic' => 'Files',
        'th_fic_dlt' => 'LTH',
		'th_fic_dlt_pemd' => 'DLT PEMD',
		'th_fic_de' => 'DC',
		'th_fic_fds_fr' => 'SDF FR',
        'th_fic_fds_en' => 'SDF EN',
        'th_fic_dof' => 'Origin declaration',
        'td_donnee_manquante' => 'Missing data',
        'td_donnee_non_communiquee' => 'Data not provided',
        'td_produit_non_dangereux' => 'Product not dangerous',
        'td_donnee_non_necessaire' => 'Data not necessary given',
        'new_value' => 'Enter a new value',
        'valid' => 'Confirm',
        'modification' => 'Updated',
        'no_packing_group' => 'No packing group'
    )
);

$nb_prod_page = 100;

if (isset($data['page'])) {
    $page = $data['page'];
} else {
    $page = 1;
}
$deb_limit = (($page * $nb_prod_page) - $nb_prod_page);

$req_search_code_nbpage = '
SELECT
    produits.sap_prod as code_sap,
    fournisseurs.lib_four as fournisseur,
    produits.code_art_four_prod as code_art_four,
    produits.designation as design,
    pap.lib_pays as pays_prov,
    produits.code_postal_prov as cp,
    produits.ville_prov as ville,
	produits.is_not_pref as not_prefer,
    pao.lib_pays as pays_orig,
    pao.estEurope,
    group_concat(DISTINCT lien_produit_zone_pays.code_zone_pref order by lien_produit_zone_pays.code_zone_pref asc separator ",") as "zone",
    produits.date_deb_validite as date_deb,
    produits.date_fin_validite as date_fin,
    produits.code_douanier as code_doua,
    produits.code_danger as code_dang,
    produits.code_onu as onu,
    produits.code_embal as code_emba,
    group_concat(DISTINCT transporter.code_tpt order by transporter.code_tpt asc separator ",") as "mode",
    produits.fic_dlt as dlt,
	produits.fic_dlt_pemd as dlt_pemd,
	produits.fic_de as de,
	produits.fic_fds_fr as fds_fr,
    produits.fic_fds_en as fds_en,
    produits.fic_decl_four as decl_four,
    produits.is_dangerous as danger,
    produits.is_not_pref as not_pref
FROM
    produits
LEFT JOIN pays pao ON produits.code_pays_origine = pao.code_pays
LEFT JOIN pays pap ON produits.code_pays = pap.code_pays
LEFT JOIN douane ON produits.code_douanier = douane.code_douanier
LEFT JOIN fournisseurs ON produits.code_four = fournisseurs.code_four
LEFT JOIN onu ON produits.code_onu = onu.code_onu
LEFT JOIN transporter ON produits.sap_prod = transporter.sap_prod
LEFT JOIN lien_produit_zone_pays ON produits.sap_prod = lien_produit_zone_pays.sap_prod
WHERE is_active = 1 ';

if (isset($_SESSION["user_level"])) {
    switch ($_SESSION["user_level"]) {
        case GUEST_LEVEL:
            $req_search_code_nbpage .= "";
            break;
        case FOURNISSEUR_LEVEL:
            $req_search_code_nbpage .= "AND (produits.code_four = :code_four) ";
            $params[":code_four"] = $_SESSION['user_code_four'];
            break;
        case USER_LEVEL:
            $req_search_code_nbpage .= "";
            break;
        case ADMIN_LEVEL:
            $req_search_code_nbpage .= "";
            break;
    }
} else {
    logout();
}

if (isset($data['filter_code_art_four_prod'])) {

    if (!empty($data['filter_code_art_four_prod'])) {
        $req_search_code_nbpage .= "AND produits.code_art_four_prod LIKE :code_art_four_prod ";
        $params[':code_art_four_prod'] = "%" . $data['filter_code_art_four_prod'] . "%";
    }
}

if (isset($data['filter_sap'])) {

    if (!empty($data['filter_sap'])) {
        $req_search_code_nbpage .= "AND produits.sap_prod LIKE :sap_prod ";
        $params[':sap_prod'] = "%" . $data['filter_sap'] . "%";
    }
}

if (isset($data['filter_zone'])) {

    if (!empty($data['filter_zone'])) {
        $req_search_code_nbpage .= "AND ((lien_produit_zone_pays.code_zone_pref IN (";
        $zones = explode(",", $data['filter_zone']);
        foreach ($zones as $i => $zone) {
            $params[':zone' . $i] = filter_var($zone, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":zone" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_zone'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (lien_produit_zone_pays.code_zone_pref IS NULL)) ";
    }
}

if (isset($data['filter_tpt'])) {

    if (!empty($data['filter_tpt'])) {
        $req_search_code_nbpage .= "AND ((transporter.code_tpt IN (";
        $modes = explode(",", $data['filter_tpt']);
        foreach ($modes as $i => $mode) {
            $params[':mode' . $i] = filter_var($mode, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":mode" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_tpt'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (transporter.code_tpt IS NULL)) ";
    }
}

if (isset($data['filter_four'])) {

    if (!empty($data['filter_four'])) {
        $req_search_code_nbpage .= "AND ((produits.code_four IN (";
        $fours = explode(",", $data['filter_four']);
        foreach ($fours as $i => $four) {
            $params[':four' . $i] = filter_var($four, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":four" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_four'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (produits.code_four IS NULL)) ";
    }
}

if (isset($data['filter_pao'])) {
    if (!empty($data['filter_pao'])) {
        $req_search_code_nbpage .= "AND ((produits.code_pays_origine IN (";
        $paos = explode(",", $data['filter_pao']);
        foreach ($paos as $i => $pao) {
            $params[':pao' . $i] = filter_var($pao, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":pao" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_pao'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (produits.code_pays_origine IS NULL)) ";
    }
}

if (isset($data['filter_pap'])) {
    if (!empty($data['filter_pap'])) {
        $req_search_code_nbpage .= "AND ((produits.code_pays IN (";
        $paps = explode(",", $data['filter_pap']);
        foreach ($paps as $i => $pap) {
            $params[':pap' . $i] = filter_var($pap, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":pap" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_pap'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (produits.code_pays IS NULL)) ";
    }
}

if (isset($data['filter_danger'])) {
    if (!empty($data['filter_danger'])) {
        $req_search_code_nbpage .= "AND ((produits.code_danger IN (";
        $dangers = explode(",", $data['filter_danger']);
        foreach ($dangers as $i => $danger) {
            $params[':danger' . $i] = filter_var($danger, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":danger" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_danger'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (produits.code_danger IS NULL)) ";
    }
}

if (isset($data['filter_embal'])) {
    if (!empty($data['filter_embal'])) {
        $req_search_code_nbpage .= "AND ((produits.code_embal IN (";
        $embals = explode(",", $data['filter_embal']);
        foreach ($embals as $i => $embal) {
            $params[':embal' . $i] = filter_var($embal, FILTER_SANITIZE_STRING);
            $req_search_code_nbpage .= ":embal" . $i . ",";
        }
        $req_search_code_nbpage = mb_substr($req_search_code_nbpage, 0, -1, "UTF-8") . ")) ";
    }
    if (strpos($data['filter_embal'], "NULL") === false) {
        $req_search_code_nbpage .= ") ";
    } else {
        $req_search_code_nbpage .= "OR (produits.code_embal IS NULL)) ";
    }
}

if (isset($data['filter_cp'])) {
    if ($data['filter_cp'] == "null") {
        $req_search_code_nbpage .= "AND (produits.code_postal_prov IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.code_postal_prov IS NOT NULL) ";
    }
}

if (isset($data['filter_ville'])) {
    if ($data['filter_ville'] == "null") {
        $req_search_code_nbpage .= "AND (produits.ville_prov IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.ville_prov IS NOT NULL) ";
    }
}

if (isset($data['filter_douane'])) {
    if ($data['filter_douane'] == "null") {
        $req_search_code_nbpage .= "AND (produits.code_douanier IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.code_douanier IS NOT NULL) ";
    }
}
if (isset($data['filter_onu'])) {
    if ($data['filter_onu'] == "null") {
        $req_search_code_nbpage .= "AND (produits.code_onu IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.code_onu IS NOT NULL) ";
    }
}

if (isset($data['filter_dlt'])) {
    if ($data['filter_dlt'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_dlt IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_dlt IS NOT NULL) ";
    }
}

if (isset($data['filter_dlt_pemd'])) {
    if ($data['filter_dlt_pemd'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_dlt_pemd IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_dlt_pemd IS NOT NULL) ";
    }
}

if (isset($data['filter_de'])) {
    if ($data['filter_de'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_de IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_de IS NOT NULL) ";
    }
}

if (isset($data['filter_fds_fr'])) {
    if ($data['filter_fds_fr'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_fds_fr IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_fds_fr IS NOT NULL) ";
    }
}

if (isset($data['filter_dof'])) {
    if ($data['filter_dof'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_decl_four IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_decl_four IS NOT NULL) ";
    }
}

if (isset($data['filter_fds_en'])) {
    if ($data['filter_fds_en'] == "null") {
        $req_search_code_nbpage .= "AND (produits.fic_fds_en IS NULL) ";
    } else {
        $req_search_code_nbpage .= "AND (produits.fic_fds_en IS NOT NULL) ";
    }
}

if (isset($data['filter_date_deb'])) {
    switch ($data['filter_date_deb']) {
        case "null":
            $req_search_code_nbpage .= "AND (produits.date_deb_validite IS NULL) ";
            break;
        case "not_null":
            $req_search_code_nbpage .= "AND (produits.date_deb_validite IS NOT NULL) ";
            break;
        case "obsolete";
            $req_search_code_nbpage .= "AND (produits.date_fin_validite < " . date("Y-m-d") . ") ";
            break;
    }
}

if (isset($data['filter_date_fin'])) {
    switch ($data['filter_date_fin']) {
        case "null":
            $req_search_code_nbpage .= "AND (produits.date_fin_validite IS NULL) ";
            break;
        case "not_null":
            $req_search_code_nbpage .= "AND (produits.date_fin_validite IS NOT NULL) ";
            break;
        case "obsolete";
            $req_search_code_nbpage .= "AND (produits.date_fin_validite < " . date("Y-m-d") . ") ";
            break;
    }
}

if (isset($data['orderBy'])) {
    $req_search_code_nbpage .= " ORDER BY :orderby";
    $params[':orderby'] = $data['orderBy'];
    if (isset($data['tri'])) {
        $req_search_code_nbpage .= " ORDER BY :orderby :tri";
        $params[':tri'] = $data['tri'];
    }
}

$req_search_code_nbpage .= ' GROUP BY produits.sap_prod ';

$url = explode("?", $_SERVER['REQUEST_URI']);
$produits_nbpage = safeParameteredSQLRequestFetch($link, $req_search_code_nbpage, $params);

$nb_codes = filter_var(count($produits_nbpage), FILTER_SANITIZE_STRING);
$nb_pages = ceil($nb_codes / $nb_prod_page);

$req_search_code = $req_search_code_nbpage . " LIMIT " . $deb_limit . "," . $nb_prod_page;

$produits = safeParameteredSQLRequestFetch($link, $req_search_code, $params);
?>

<div class="pages_top" style="text-align:center;"></div>
<div>
    <table class="table-bordered table-striped table-fixed-header table-condensed" data-width="800px">
        <thead class="header" style="background-color: #eaeaea;" valign="middle" id="table_header_origin">
            <tr>
                <?php
                if (isset($_SESSION["user_level"])) {
                    if ($_SESSION["user_level"] > 1) {
                        echo '<th id="sap_prod" rowspan="2" name="col_sap">' . $trad_admin_code_douanier[$lang]['th_sap'] . '</th>';
                        echo '<th id="fournisseurs.lib_four" rowspan="2" name="col_four">' . $trad_admin_code_douanier[$lang]['th_four'] . '</th>';
                    }
                } else {
                    logout();
                }
                ?>
                <th id="" rowspan="2" name="col_c_prod"><?= $trad_admin_code_douanier[$lang]['th_c_prod'] ?></th>
                <th id="" rowspan="2" name="col_desi"><?= $trad_admin_code_douanier[$lang]['th_desi'] ?></th>
                <th colspan="3" name="col_prov"><?= $trad_admin_code_douanier[$lang]['th_prov'] ?></th>
                <th id="" rowspan="2" name="col_orig"><?= $trad_admin_code_douanier[$lang]['th_orig'] ?></th>
                <th id="" rowspan="2" name="col_zone"><?= $trad_admin_code_douanier[$lang]['th_zone'] ?></th>
                <th colspan="2" name="col_preu"><?= $trad_admin_code_douanier[$lang]['th_preu'] ?></th>
                <th id="" rowspan="2" name="col_c_doua"><?= $trad_admin_code_douanier[$lang]['th_c_doua'] ?></th>
                <th colspan="4" name="col_dang"><?= $trad_admin_code_douanier[$lang]['th_dang'] ?></th>
                <th colspan="7" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic'] ?></th>
            </tr>
            <tr>
                <th id="" name="col_prov"><?= $trad_admin_code_douanier[$lang]['th_prov_pays'] ?></th>
                <th id="" name="col_prov"><?= $trad_admin_code_douanier[$lang]['th_prov_cp'] ?></th>
                <th id="" name="col_prov"><?= $trad_admin_code_douanier[$lang]['th_prov_vill'] ?></th>
                <th id="" name="col_preu"><?= $trad_admin_code_douanier[$lang]['th_preu_deb'] ?></th>
                <th id="" name="col_preu"><?= $trad_admin_code_douanier[$lang]['th_preu_fin'] ?></th>
                <th id="" name="col_dang"><?= $trad_admin_code_douanier[$lang]['th_dang_clas'] ?></th>
                <th id="" name="col_dang"><?= $trad_admin_code_douanier[$lang]['th_dang_onu'] ?></th>
                <th id="" name="col_dang"><?= $trad_admin_code_douanier[$lang]['th_dang_emba'] ?></th>
                <th id="" name="col_dang"><?= $trad_admin_code_douanier[$lang]['th_dang_mode'] ?></th>
                <th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_dlt'] ?></th>
				<th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_dlt_pemd'] ?></th>
				<th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_de'] ?></th>
				<th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_fds_fr'] ?></th>
                <th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_fds_en'] ?></th>
                <th id="" name="col_fic"><?= $trad_admin_code_douanier[$lang]['th_fic_dof'] ?></th>
            </tr>

        </thead>
        <tbody>
            <?php
            foreach ($produits as $donnees_produit) {
                echo '<tr>';
                if (isset($_SESSION["user_level"])) {
                    if ($_SESSION["user_level"] > 1) {
                        //CODE SAP
                        echo '<td name="col_sap"><a href="fiche_infos_product.php?q=' . $donnees_produit['code_sap'] . '">' . $donnees_produit['code_sap'] . '</a></td>';
                        echo '<td name="col_four" table="produits" field="code_four" title="' . $donnees_produit['fournisseur'] . '">' . mb_strimwidth($donnees_produit['fournisseur'], 0, 17, "...") . '</td>';
                    }
                } else {
                    logout();
                }

                //CODE ARTICLE FOURNISSEUR
                if ($donnees_produit['code_art_four'] !== "") {
                    echo '<td name="col_c_prod" table="produit" field="code_art_four_prod"><a href="fiche_infos_product.php?q=' . $donnees_produit['code_sap'] . '">' . $donnees_produit['code_art_four'] . '</a></td>';
                } else {
                    echo '<td name="col_c_prod" class="danger-td" table="produits" field="code_art_four_prod">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                }

                //DESIGNATION
                echo '<td name="col_desi" table="produits" field="designation" title="' . $donnees_produit['design'] . '">' . mb_strimwidth($donnees_produit['design'], 0, 17, "...") . '</td>';


                //SQL
                $pays = $link->query("select * from pays");
                $donnees_pays = $pays->fetch();
                //PAYS DE PROVENANCE
                if (($donnees_produit['pays_prov'] !== NULL) AND ( $donnees_produit['pays_prov'] !== "")) {
                    echo '<td name="col_prov" table="produits" field="code_pays" title="' . $donnees_produit['pays_prov'] . '">' . mb_strimwidth($donnees_produit['pays_prov'], 0, 17, "...") . '</td>';
                } else {
                    echo '<td name="col_prov" table="produits" class="danger-td" field="code_pays">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                }

                //CODE POSTAL
                if (empty($donnees_produit['cp'])) {
                    echo '<td name="col_prov" table="produits" field="code_postal_prov">' . $trad_admin_code_douanier[$lang]['td_donnee_non_communiquee'] . '</td>';
                } else {
                    echo '<td name="col_prov" table="produits" field="code_postal_prov">' . $donnees_produit['cp'] . '</td>';
                }

                //VILLE
                if (empty($donnees_produit['ville'])) {
                    echo '<td name="col_prov" table="produits" field="ville_prov">' . $trad_admin_code_douanier[$lang]['td_donnee_non_communiquee'] . '</td>';
                } else {
                    echo '<td name="col_prov" table="produits" field="ville_prov">' . $donnees_produit['ville'] . '</td>';
                }

                //PAYS D'ORIGINE
                if ($donnees_produit['pays_orig'] != "") {
                    echo '<td name="col_orig" table="produits" field="code_pays_origine" title="' . $donnees_produit['pays_orig'] . '">' . mb_strimwidth($donnees_produit['pays_orig'], 0, 17, "...") . '</td>';
                } else {
                    echo '<td name="col_orig" class="danger-td" table="produits" field="code_pays_origine">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                }

                //ZONES PREF
                if (($donnees_produit['not_prefer'] === "0" && $donnees_produit['estEurope'] === "1") || $donnees_produit['estEurope'] === null) {
                    if (empty($donnees_produit['zone'])) {
                        echo '<td name="col_zone" class="danger-td" table="lien_produit_zone_pays" field="code_zone_pref">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_zone" table="lien_produit_zone_pays" field="code_zone_pref">' . $donnees_produit['zone'] . '</td>';
                    }
                    //var_dump(empty($donnees_produit['zone']));
                    //DATE DEBUT
                    if (empty($donnees_produit['date_deb']) or ( $donnees_produit['date_deb'] == '0000-00-00')) {
                        echo '<td name="col_preu" class="danger-td" table="produits" field="date_deb_validite">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_preu" table="produits" field="date_deb_validite">' . ($lang === 'fr' ? date_fr($donnees_produit['date_deb']) : $donnees_produit['date_deb']) . '</td>';
                    }

                    //DATE FIN
                    if (empty($donnees_produit['date_fin']) or ( $donnees_produit['date_fin'] == '0000-00-00')) {
                        echo '<td name="col_preu" class="danger-td" table="produits" field="date_fin_validite">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_preu" table="produits" field="date_fin_validite">' . ($lang === 'fr' ? date_fr($donnees_produit['date_fin']) : $donnees_produit['date_fin']) . '</td>';
                    }
                } else {
                    echo '<td name="col_zone" table="lien_produit_zone_pays" field="code_zone_pref">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    echo '<td name="col_preu" table="produits" field="date_deb_validite">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    echo '<td name="col_preu" table="produits" field="date_fin_validite">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                }

                //CODE DOUANIER
                if (($donnees_produit['code_doua'] !== NULL) AND ( $donnees_produit['code_doua'] !== "")) {
                    echo '<td name="col_c_doua" table="produits" field="code_douanier">' . $donnees_produit['code_doua'] . '</td>';
                } else {
                    echo '<td name="col_c_doua" class="danger-td" table="lien_produit_zone_pays" field="code_zone_pref">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                }

                //DANGER
                if ($donnees_produit['danger'] == "1") {
                    if (empty($donnees_produit['code_dang'])) {
                        echo '<td name="col_dang" class="danger-td" table="produits" field="code_danger">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_dang" table="produits" field="code_danger">' . $donnees_produit['code_dang'] . '</td>';
                    }

                    //ONU
                    if (empty($donnees_produit['onu'])) {
                        echo '<td name="col_dang" class="danger-td" table="produits" field="code_onu">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_dang" table="produits" field="code_onu">' . $donnees_produit['onu'] . '</td>';
                    }
                    //EMBALLAGE
                    if (!empty($donnees_produit['code_emba'])) {
                        echo '<td name="col_dang" table="produits" field="code_embal">' . $donnees_produit['code_emba'] . '</td>';
                    } else {
                        echo '<td name="col_dang" table="produits" field="code_embal">' . $trad_admin_code_douanier[$lang]['no_packing_group'] . '</td>';
                    }

                    //MODE TPT
                    if (empty($donnees_produit['mode'])) {
                        echo '<td name="col_dang" class="danger-td" table="transporter" field="code_tpt">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_dang" table="transporter" field="code_tpt">' . $donnees_produit['mode'] . '</td>';
                    }
                } else {
                    echo '<td name="col_dang" table="produits" field="code_danger">' . $trad_admin_code_douanier[$lang]['td_produit_non_dangereux'] . '</td>';
                    echo '<td name="col_dang" table="produits" field="code_onu">' . $trad_admin_code_douanier[$lang]['td_produit_non_dangereux'] . '</td>';
                    echo '<td name="col_dang" table="produits" field="code_embal">' . $trad_admin_code_douanier[$lang]['td_produit_non_dangereux'] . '</td>';
                    echo '<td name="col_dang" table="transporter" field="code_tpt">' . $trad_admin_code_douanier[$lang]['td_produit_non_dangereux'] . '</td>';
                }
                
				//DLT
                if (empty($donnees_produit['dlt'])) {
                    if (($donnees_produit['estEurope'] === "1") && ($donnees_produit['not_pref'] === "0")) {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    }
                } else {
                    //echo '<td name="col_fic" table="produit" field="fic_dlt">' . $donnees_produit['dlt'] . '</td>';
                    echo '<td name="col_fic"><a href="' . $donnees_produit['dlt'] . '" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                }

				//DLT_PEMD
                if (empty($donnees_produit['dlt_pemd'])) {
                    if (($donnees_produit['estEurope'] === "1") && ($donnees_produit['not_pref'] === "0")) {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    }
                } else {
                    //echo '<td name="col_fic" table="produit" field="fic_dlt_pemd">' . $donnees_produit['dlt_pemd'] . '</td>';
                    echo '<td name="col_fic"><a href="' . $donnees_produit['dlt_pemd'] . '" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                }
				
				//DE
                if (empty($donnees_produit['de'])) {
                    if (($donnees_produit['estEurope'] === "1") && ($donnees_produit['not_pref'] === "0")) {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    }
                } else {
                    //echo '<td name="col_fic" table="produit" field="fic_de">' . $donnees_produit['de'] . '</td>';
                    echo '<td name="col_fic"><a href="' . $donnees_produit['de'] . '" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                }

				//FDS FR
                if (empty($donnees_produit['fds_fr'])) {
                    if ($donnees_produit['danger'] === "1") {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    }
                } else {
                    //echo '<td name="col_fic" table="produit" field="fic_fds_fr">'.$donnees_produit['fds_fr'] . '</td>';
                    echo '<td name="col_fic"><a href="' . $donnees_produit['fds_fr'] . '" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                }

                //FDS EN
                if (empty($donnees_produit['fds_en'])) {
                    if ($donnees_produit['danger'] === "1") {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    } else {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    }
                } else {
                    echo '<td name="col_fic"><a href="' . $donnees_produit['fds_en'] . '" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                    //echo '<td name="col_fic" table="produit" field="fic_fds_en">' . $donnees_produit['fds_en'] . '</td>';
                }

                //DECLARATION FOURNISSEUR
                if (empty($donnees_produit['decl_four'])) {
                    if (($donnees_produit['estEurope'] === "1") && ($donnees_produit['not_pref'] === "0")) {
                        echo '<td name="col_fic">' . $trad_admin_code_douanier[$lang]['td_donnee_non_necessaire'] . '</td>';
                    } else {
                        echo '<td name="col_fic" class="danger-td">' . $trad_admin_code_douanier[$lang]['td_donnee_manquante'] . '</td>';
                    }
                } else {
                    echo '<td name="col_fic"><a href="' . $donnees_produit['decl_four'] . '" rel="lightbox" target="_blank"><img src="images/pdf.jpg" border="0" alt="Version PDF"></a></td>';
                    //echo '<td name="col_fic" table="produit" field="fic_decl_four">' . $donnees_produit['decl_four'] . '</td>';
                }
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<div class="pages_bottom" style="text-align:center;"></div>

<?php
$final_goto_update = "'";
$i = 0;
foreach ($produits as $goto_update) {
    $final_goto_update .= $goto_update['code_sap'] . ",";
    $i++;
}

$final_goto_update = substr($final_goto_update, 0, (strlen($final_goto_update) - 1)) . "'";
//echo($final_goto_update);
?>
<script>
    function gestion_url(variable, valeur) {
        url_complet = '<?= $url[1]; ?>';
        var url_fin;

        if ((valeur === undefined) || (valeur.toString().length === 0)) {
            for (i = url_complet.indexOf(variable); i < url_complet.length; i++) {
                if (url_complet[i] === "&") {
                    break;
                }
            }
            url_fin = url_complet.replace(url_complet.substring(url_complet.indexOf(variable), i), "");
        } else {
            if (url_complet.indexOf(variable) > -1) {
                for (i = url_complet.indexOf(variable); i < url_complet.length; i++) {
                    if (url_complet[i] === "&") {
                        break;
                    }
                }
                url_fin = url_complet.replace(url_complet.substring(url_complet.indexOf(variable), i), (variable + "=" + valeur));
                //console.log(url_fin);
            } else {
                url_fin = url_complet.concat("&" + variable + "=" + valeur);
                //console.log(url_complet);
            }
        }
        url_fin = url_fin.replace("&&", "&");
        url_fin = url_fin.replace("&&", "&");
//        console.log("gestion_url");
        return url_fin;
    }

    function getFilterValue() {
        //alert('Changed option ' + $(option).parent().attr('id'));
        var filter_global = [];

        //sap
        filter_global['filter_sap'] = $("#filter_sap").val();

        //Code produit
        filter_global['filter_code_art_four_prod'] = $("#filter_code_art_four_prod").val();

        //fournisseurs
        var filter_four = [];
        $('#filter_four option:selected').each(function (index, brand) {
            filter_four.push(brand.value);
            filter_global['filter_four'] = filter_four;
        });

        //pays pref
        var filter_pap = [];
        $('#filter_pap option:selected').each(function (index, brand) {
            filter_pap.push(brand.value);
            filter_global['filter_pap'] = filter_pap;
        });

        //filter_cp
        var filter_cp = [];
        $('#filter_cp option:selected').each(function (index, brand) {
            filter_cp.push(brand.value);
            filter_global['filter_cp'] = filter_cp;
        });

        //filter_ville
        var filter_ville = [];
        $('#filter_ville option:selected').each(function (index, brand) {
            filter_ville.push(brand.value);
            filter_global['filter_ville'] = filter_ville;
        });

        //filter_pao
        var filter_pao = [];
        $('#filter_pao option:selected').each(function (index, brand) {
            filter_pao.push(brand.value);
            filter_global['filter_pao'] = filter_pao;
        });

        //filter_zone
        var filter_zone = [];
        $('#filter_zone option:selected').each(function (index, brand) {
            filter_zone.push(brand.value);
            filter_global['filter_zone'] = filter_zone;
        });

        //filter_date_deb
        var filter_date_deb = [];
        $('#filter_date_deb option:selected').each(function (index, brand) {
            filter_date_deb.push(brand.value);
            filter_global['filter_date_deb'] = filter_date_deb;
        });

        //filter_date_fin
        var filter_date_fin = [];
        $('#filter_date_fin option:selected').each(function (index, brand) {
            filter_date_fin.push(brand.value);
            filter_global['filter_date_fin'] = filter_date_fin;
        });

        //filter_douane
        var filter_douane = [];
        $('#filter_douane option:selected').each(function (index, brand) {
            filter_douane.push(brand.value);
            filter_global['filter_douane'] = filter_douane;
        });

        //filter_danger
        var filter_danger = [];
        $('#filter_danger option:selected').each(function (index, brand) {
            filter_danger.push(brand.value);
            filter_global['filter_danger'] = filter_danger;
        });

        //filter_onu
        var filter_onu = [];
        $('#filter_onu option:selected').each(function (index, brand) {
            filter_onu.push(brand.value);
            filter_global['filter_onu'] = filter_onu;
        });

        //filter_embal
        var filter_embal = [];
        $('#filter_embal option:selected').each(function (index, brand) {
            filter_embal.push(brand.value);
            filter_global['filter_embal'] = filter_embal;
        });

        //filter_tpt
        var filter_tpt = [];
        $('#filter_tpt option:selected').each(function (index, brand) {
            filter_tpt.push(brand.value);
            filter_global['filter_tpt'] = filter_tpt;
        });

        //filter_dlt
        var filter_dlt = [];
        $('#filter_dlt option:selected').each(function (index, brand) {
            filter_dlt.push(brand.value);
            filter_global['filter_dlt'] = filter_dlt;
        });

		//filter_dlt_pemd
        var filter_dlt_pemd = [];
        $('#filter_dlt_pemd option:selected').each(function (index, brand) {
            filter_dlt_pemd.push(brand.value);
            filter_global['filter_dlt_pemd'] = filter_dlt_pemd;
        });
		
		//filter_de
        var filter_de = [];
        $('#filter_de option:selected').each(function (index, brand) {
            filter_de.push(brand.value);
            filter_global['filter_de'] = filter_de;
        });

		//filter_fds_fr
        var filter_fds_fr = [];
        $('#filter_fds_fr option:selected').each(function (index, brand) {
            filter_fds_fr.push(brand.value);
            filter_global['filter_fds_fr'] = filter_fds_fr;
        });

        //filter_fds_en
        var filter_fds_en = [];
        $('#filter_fds_en option:selected').each(function (index, brand) {
            filter_fds_en.push(brand.value);
            filter_global['filter_fds_en'] = filter_fds_en;
        });

        //filter_dof
        var filter_dof = [];
        $('#filter_dof option:selected').each(function (index, brand) {
            filter_dof.push(brand.value);
            filter_global['filter_dof'] = filter_dof;
        });
        return filter_global;
    }

    function loadFilterResult(filter_id) {
        $('#resume_filter').html(' : <span class="badge"> <?= $nb_codes ?> </span> produit(s)');
        var filter_global = getFilterValue();
        var new_url;
        new_url = gestion_url(filter_id, filter_global[filter_id]);
        $.ajax({
            url: './scripts/allproduct_hide.php?',
            type: 'GET',
            data: new_url,
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé
//                console.log(code_html);
                $('#content').html(code_html);
            },
            complete: function () {
                $('input[name=remote]').each(function () {
                    if ($(this).bootstrapSwitch('state')) {
                        $('td[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                        $('th[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                    } else {
                        $('td[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                        $('th[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                    }
                });
            },
            error: function (resultat, statut, erreur) {
            }
        });
    }

    $("#update_filter").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "update_products.php",
            data: "code_sap=<?= $final_goto_update ?>",
            dataType: "html",
            success: function (data) {
                $('#content_page').html(data);
            }
        });
    });

    $(document).ready(function () {

        $('#resume_filter').html(' : <span class="badge"><?= $nb_codes ?></span> produit(s)');
        $('.table-fixed-header').fixedHeader();
        $('.selection-multiple').each(function () {
            $('#' + $(this).attr('id')).multiselect({
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100px',
                maxHeight: 600,
                multiSelectName: $(this).attr('id'),
                onChange: function (option, checked, select) {
                    loadFilterResult($(option).parent().attr('id'));
                }
            });
        });
    });

    $('.pages_bottom').bootpag({
        total: <?= $nb_pages; ?>,
        page: <?= $page; ?>,
        maxVisible: 10,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        //$(".content4").html("Page " + num);
        var $head = $('#table_header_origin');
        var $win = $(window);
        var new_url;
        new_url = gestion_url("page", num);
        $.ajax({
            url: './scripts/allproduct_hide.php',
            type: 'GET',
            data: new_url,
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#content').html(code_html);
            },
            complete: function () {
                $('input[name=remote]').each(function () {
                    if ($(this).bootstrapSwitch('state')) {
                        $('td[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                        $('th[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                    } else {
                        $('td[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                        $('th[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                    }
                });
            },
            error: function (resultat, statut, erreur) {
                alert('Erreur : ' + erreur);
            }
        });
    });

    $('.pages_top').bootpag({
        total: <?= $nb_pages; ?>,
        page: <?= $page; ?>,
        maxVisible: 10,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        //$(".content4").html("Page " + num);
        var head = $('#table_header_origin');
        var $win = $(window);
        var new_url;
        new_url = gestion_url("page", num);
        $.ajax({
            url: './scripts/allproduct_hide.php',
            type: 'GET',
            data: new_url,
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#content').html(code_html);
                //console.log(code_html);
            },
            complete: function () {
                $('input[name=remote]').each(function () {
                    if ($(this).bootstrapSwitch('state')) {
                        $('td[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                        $('th[name="' + $(this).attr("remote-for") + '"]').removeAttr("style");
                    } else {
                        $('td[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                        $('th[name="' + $(this).attr("remote-for") + '"]').css('display', 'none');
                    }
                });


            },
            error: function (resultat, statut, erreur) {
                alert('Erreur : ' + erreur);
            }
        });
    });

</script>