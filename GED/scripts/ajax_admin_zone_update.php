<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
foreach($_POST as $key => $value){
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

$trad_ajax_admin_update_zone = array(
    'fr' => array(
        'title' => 'Modification réussie.',
        'start' => 'Vous avez mis à jour votre zone préférentielle ',
        'pays' => 'Vous avez inséré des pays dans votre zone préférentielle',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez votre formulaire'
    ),
    'en' => array(
        'title' => 'Update successful.',
        'start' => 'You\'ve update your preferential zone',
        'pays' => 'You\'ve insert countries of your zone préférentielle',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

if ($data_post['code_zone'] !== "" or ( $data_post['nom_zone'] !== "")) {
    $req_update_zone = "UPDATE zone SET ";
    $liste_update_zone = $trad_ajax_admin_update_zone[$lang]['start'];

    if (isset($data_post['code_zone']) && !empty($data_post['code_zone'])) {
        $req_update_zone .= "code_zone_pref = :code_zone_pref, ";
        $params[':code_zone_pref'] = $data_post['code_zone'];
    }

    if (isset($data_post['nom_zone']) && !empty($data_post['nom_zone'])) {
        $req_update_zone .= "lib_zone_pref = :lib_zone_pref, ";
        $params[':lib_zone_pref'] = $data_post['nom_zone'];
    }

    $req_update_zone = mb_substr($req_update_zone, 0, -2, "UTF-8");
    $req_update_zone = $req_update_zone . " WHERE code_zone_pref = :code_zone_actuel;";
    $params[':code_zone_actuel'] = $data_post['code_actuel'];

    if (safeParameteredSQLRequestExecute($link, $req_update_zone, $params)) {
        echo '<div class="panel panel-success">';
        echo '<div class="panel-heading">';
        echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_zone[$lang]['title'] . '</h3>';
        echo '</div>';
        echo '<div class="panel-body" id="contenu_panel">' . $liste_update_zone . '</div>';
        echo '</div>';
    } else {
        echo '<div class="panel panel-danger">';
        echo '<div class="panel-heading">';
        echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_update_zone[$lang]['title_error'] . '</h3>';
        echo '</div>';
        echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_update_zone[$lang]['error'] . '</div>';
        echo '</div>';
    }
}