<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
$code_zone = filter_input(INPUT_POST, 'zone', FILTER_SANITIZE_STRING);

$trad_ajax_admin_delete_zone = array(
    'fr' => array(
        'title' => 'Suppression réussie.',
        'start' => 'Vous avez supprimé votre zone ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Delete successful.',
        'start' => 'You\'ve deleted your zone ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$liste_delete_zone = $trad_ajax_admin_delete_zone[$lang]['start'];

if (isset($code_zone) && !empty($code_zone)) {
    $req_delete_zone = "DELETE FROM zone WHERE code_zone_pref = :code_zone;";
}else{
    die("Zone introuvable.");
}

if (safeParameteredSQLRequestExecute($link, $req_delete_zone, [':code_zone' => $code_zone])) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_zone[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_delete_zone . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_delete_zone[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_delete_zone[$lang]['error'] . '</div>';
    echo '</div>';
}
