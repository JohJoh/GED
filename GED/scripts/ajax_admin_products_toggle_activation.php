<?php

require_once './dbc.php';

$activation = (int)filter_input(INPUT_POST, 'activation', FILTER_SANITIZE_NUMBER_INT);

$fichier = $_FILES['fichier'];

$message = '';
switch ($fichier['error']) {
    case UPLOAD_ERR_OK:
        break;
    case UPLOAD_ERR_INI_SIZE: 
        $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
        break; 
    case UPLOAD_ERR_FORM_SIZE: 
        $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
        break; 
    case UPLOAD_ERR_PARTIAL: 
        $message = "The uploaded file was only partially uploaded"; 
        break; 
    case UPLOAD_ERR_NO_FILE: 
        $message = "No file was uploaded"; 
        break; 
    case UPLOAD_ERR_NO_TMP_DIR: 
        $message = "Missing a temporary folder"; 
        break; 
    case UPLOAD_ERR_CANT_WRITE: 
        $message = "Failed to write file to disk"; 
        break; 
    case UPLOAD_ERR_EXTENSION: 
        $message = "File upload stopped by extension"; 
        break; 
    default: 
        $message = "Unknown upload error"; 
        break; 
}
if ($message !== '') {
    die($message);
}

$handle = fopen($fichier['tmp_name'], "r");
if ($handle) {
    $data = fgetcsv($handle, 0, ';');
    
//    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $prep = $link->prepare('UPDATE produits SET is_active = :activation WHERE sap_prod = :sap_prod;');
    
    while (($data = fgetcsv($handle, 0, ';')) !== false) {
        if ($data[0] !== null) {
            $sap_prod = str_replace(' ', '', $data[0]);
            if (false === $prep->execute([
                ':activation' => $activation,
                ':sap_prod' => $sap_prod
            ])) {
                die('Désolé, une erreur s’est produite.');
            }
        }
    }

    fclose($handle);
} else {
    die('Le fichier n’a malheureusement pas pu être ouvert.');
} 

