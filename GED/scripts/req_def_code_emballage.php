<?php

require_once("dbc.php");

$code = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRING);

$req_code = "SELECT lib_embal FROM emballage WHERE code_embal = :code;";
$result = safeParameteredSQLRequestFetch($link, $req_code, [':code' => $code]);

if (count($result) > 0) {
    echo $result[0]['lib_embal'];
} else {
    echo '';
}