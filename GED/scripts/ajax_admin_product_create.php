<?php
require_once('dbc.php');
session_start();
include("./head.php");
include("./menu.php");
include("./banner.php");

$lang = $_SESSION['lang'];
foreach ($_POST as $key => $value) {
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();

//var_dump($data_post);
die;

fullDebug(4096);
$trad_ajax_admin_create_product = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre produit ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your product ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);
//$pays_europeen = $link->query("SELECT * FROM pays WHERE code_pays='" . $data_post['pays_origine']);
$pays_europeen = safeParameteredSQLRequestFetch($link, "SELECT * FROM pays WHERE code_pays = :code_pays;", [':code_pays' => $data_post['pays_origine']]);
$liste_create_product = $trad_ajax_admin_create_product[$lang]['start'];
$req_create_product = "insert into produits(";
$suite_req_product = ")VALUES(";
$nb_zones = array();
$nb_modes = array();
$nb_pays = array();
$exist = false;
$champs_obligatoires = false;

$req_recup_infos_prod = "select * from produits;";
$req_recup_infos_prod_check = $link->query($req_recup_infos_prod);
while ($donnees = $req_recup_infos_prod_check->fetch(PDO::FETCH_BOTH)) {
    if ($donnees['sap_prod'] == $data_post['code_sap']) {
        $exist = true;
    }
}
if ((trim($data_post["code_sap"]) == "") or ( trim($data_post['fournisseur']) == "") or ( trim($data_post['code_produit_fournisseur']) == "") or ( trim($data_post['designation']) == "") or ( trim($data_post['division']) == "")) {
    $champs_obligatoires = true;
}

if (!$exist and ! $champs_obligatoires) {
    //SAP
    if (isset($data_post['code_sap'])) {
        if ($data_post['code_sap'] !== "") {
            $req_create_product .= "sap_prod, ";
            $suite_req_product .= ":sap_prod, ";
            $params[':sap_prod'] = $data_post['code_sap'];
        }
    }

    //FOURNISSEUR
    if (isset($data_post['fournisseur']) && ($data_post['fournisseur'] !== "")) {
        $req_create_product .= "code_four, ";
        $suite_req_product .= ":code_four, ";
        $params[':code_four'] = $data_post['fournisseur'];
    }

    //CODE PRODUIT FOURNISSEUR
    if (isset($data_post['code_produit_fournisseur']) && ($data_post['code_produit_fournisseur'] !== "")) {
        $req_create_product .= "code_art_four_prod, ";
        $suite_req_product .= ":code_art_four_prod, ";
        $params[':code_art_four_prod'] = data_post['code_produit_fournisseur'];
    }

    //DESIGNATION
    if (isset($data_post['designation'])) {
        if ($data_post['designation'] !== "") {
            $req_create_product .= "designation, ";
            $suite_req_product .= ":designation, ";
            $params[':designation'] = $data_post['designation'];
        }
    }

    //DIVISION
    if (isset($data_post['division'])) {
        if ($data_post['division'] !== "") {
            $req_create_product .= "division_prod, ";
            $suite_req_product .= ":division_prod, ";
            $params[':division_prod'] = $data_post['division'];
        }
    }

    //PAYS DE PROVENANCE
    if (isset($data_post['pays_provenance'])) {
        if ($data_post['pays_provenance'] !== "") {
            $req_create_product .= "code_pays, ";
            $suite_req_product .= ":code_pays, ";
            $params[':code_pays'] = $data_post['pays_provenance'];
        }
    }

    //CODE POSTAL
    if (isset($data_post['code_postal'])) {
        if ($data_post['code_postal'] !== "") {
            $req_create_product .= "code_postal_prov, ";
            $suite_req_product .= ":code_postal_prov, ";
            $params[':code_postal_prov'] = $data_post['code_postal'];
        }
    }

    //VILLE
    if (isset($data_post['ville'])) {
        if ($data_post['ville'] !== "") {
            $req_create_product .= "ville_prov, ";
            $suite_req_product .= ":ville_prov, ";
            $params[':ville_prov'] = $data_post['ville'];
        }
    }

    //PAYS D'ORIGINE
    if (isset($data_post['pays_origine'])) {
        if ($data_post['pays_origine'] !== "") {
            $req_create_product .= "code_pays_origine, ";
            $suite_req_product .= ":code_pays_origine, ";
            $params[':code_pays_origine'] = $data_post['pays_origine'];
        }
    }

    //DATE DEBUT
    if (isset($data_post['date_debut_validite'])) {
        if ("" !== $data_post['date_debut_validite']) {
            $req_create_product .= "date_deb_validite, ";
            if (($pays_europeen[0]['estEurope'] == "0") or ( $data_post['no_prefer'] == "0")) {
                $suite_req_product .= "NULL, ";
            } else {
                $suite_req_product .= ":date_deb_validite, ";
                $params[':date_deb_validite'] = date($data_post['date_debut_validite']);
            }
        }
    } else {
        $req_create_product .= "date_deb_validite, ";
        $suite_req_product .= "NULL, ";
    }

    //DATE FIN
    if (isset($data_post['date_fin_validite'])) {
        if ("" !== $data_post['date_fin_validite']) {
            $req_create_product .= "date_fin_validite, ";
            if (($pays_europeen[0]['estEurope'] == "0") or ( $data_post["no_prefer"] == "0")) {
                $suite_req_product .= "NULL, ";
            } else {
                $suite_req_product .= ":date_fin_validite, ";
                $params[':date_fin_validite'] = date($data_post['date_fin_validite']);
            }
        }
    } else {
        $req_create_product .= "date_fin_validite, ";
        $suite_req_product .= "NULL, ";
    }

    //CODE DOUANIER
    if (isset($data_post['code_douanier'])) {
        if ($data_post['code_douanier'] !== "") {
            $req_create_product .= "code_douanier, ";
            $suite_req_product .= ":code_douanier, ";
            $params[':code_douanier'] = $data_post['code_douanier'];
        }
    }

    if ($data_post['danger'] !== "0") {
        //CLASSE DANGER
        if (isset($data_post['classe_danger'])) {
            if ($data_post['classe_danger'] !== "") {
                $req_create_product .= "code_danger, ";
                $suite_req_product .= ":code_danger, ";
                $params[':code_danger'] = $data_post['classe_danger'];
            }
        } else {
            $req_create_product .= "code_danger, ";
            $suite_req_product .= "NULL, ";
        }


        //CODE ONU
        if (isset($data_post['code_onu'])) {
            if ($data_post['code_onu'] !== "") {
                $req_create_product .= "code_onu, ";
                $suite_req_product .= ":code_onu, ";
                $params[':code_onu'] = $data_post['code_onu'];
            }
        } else {
            $req_create_product .= "code_onu, ";
            $suite_req_product .= "NULL, ";
        }

        //CODE EMBALLAGE
        if (isset($data_post['code_emballage'])) {
            if ($data_post['code_emballage'] !== "") {
                $req_create_product .= "code_embal, ";
                $suite_req_product .= ":code_embal, ";
                $params[':code_embal'] = $data_post['code_emballage'];
            }
        } else {
            $req_create_product .= "code_embal, ";
            $suite_req_product .= "NULL, ";
        }


        //CONTENANCE PRODUIT
        if (isset($data_post['contenance'])) {
            if ($data_post['contenance'] !== "") {
                $req_create_product .= "contenance_prod, ";
                $suite_req_product .= ":contenance_prod, ";
                $params[':contenance_prod'] = $data_post['contenance'] . " " . $data_post['unite'];
            }
        } else {
            $req_create_product .= "contenance_prod, ";
            $suite_req_product .= "NULL, ";
        }

		//TYPE PRODUIT
        if (isset($data_post['type'])) {
            if ($data_post['type'] !== "") {
                $req_create_product .= "type_prod, ";
                $suite_req_product .= ":type_prod, ";
                $params[':type_prod'] = $data_post['type'];
            }
        } else {
            $req_create_product .= "type_prod, ";
            $suite_req_product .= "NULL, ";
        }

        //CONDITIONNEMENT FOURNISSEUR
        if (isset($data_post['conditionnement'])) {
            if ($data_post['conditionnement'] !== "") {
                $req_create_product .= "condi_four, ";
                $suite_req_product .= ":condi_four, ";
                $params[':condi_four'] = $data_post['conditionnement'];
            }
        } else {
            $req_create_product .= "condi_four, ";
            $suite_req_product .= "NULL, ";
        }

        //mode_tpt
        $params_mode = array();
        safeParameteredSQLRequestExecute($link, "DELETE FROM transporter WHERE sap_prod = :sap_prod;", [':sap_prod' => $data_post['code_sap']]);
        if ($data_post['mode_transport'] != "") {
            $modes = explode(";", $data_post['mode_transport']);
            $requete_insert = $link->prepare("INSERT INTO transporter (`sap_prod`, `code_tpt`) VALUES (:sap_prod ,:mode);");
            foreach ($modes as $mode) {
                $params_mode[':sap_prod'] = $data_post['code_sap'];
                $params_mode[':mode'] = $mode;
                $requete_insert->execute($params_mode);
            }
        }
    }

    //Fiche DLT	
    if (isset($_FILES['fiche_dlt']['name'])) {
        $uploaddir_dlt = './uploads/';
        $uploadfile_dlt .= "dlt_" . $data_post['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dlt)) {
            copy($uploadfile_dlt, $uploaddir_dlt . "dlt_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_dlt']['tmp_name'], $uploadfile_dlt)) {
            $req_create_product .= "fic_dlt, ";
            $suite_req_product .= ":fic_dlt, ";
            $params[":fic_dlt"] = $uploaddir_dlt;
        }
    } else {
        $req_create_product .= "fic_dlt, ";
        $suite_req_product .= "NULL, ";
    }

	//Fiche DLT PEMD	
    if (isset($_FILES['fiche_dlt_pemd']['name'])) {
        $uploaddir_dlt_pemd = './uploads/';
        $uploadfile_dlt_pemd .= "dlt_pemd_" . $data_post['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dlt_pemd)) {
            copy($uploadfile_dlt_pemd, $uploaddir_dlt_pemd . "dlt_pemd_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_dlt_pemd']['tmp_name'], $uploadfile_dlt_pemd)) {
            $req_create_product .= "fic_dlt_pemd, ";
            $suite_req_product .= ":fic_dlt_pemd, ";
            $params[":fic_dlt_pemd"] = $uploaddir_dlt_pemd;
        }
    } else {
        $req_create_product .= "fic_dlt_pemd, ";
        $suite_req_product .= "NULL, ";
    }
	
	//Fiche DE	
    if (isset($_FILES['fiche_de']['name'])) {
        $uploaddir_de = './uploads/';
        $uploadfile_de .= "de_" . $data_post['code_sap'] . ".pdf";
        if (file_exists($uploadfile_de)) {
            copy($uploadfile_de, $uploaddir_de . "de_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_de']['tmp_name'], $uploadfile_de)) {
            $req_create_product .= "fic_de, ";
            $suite_req_product .= ":fic_de, ";
            $params[":fic_de"] = $uploaddir_de;
        }
    } else {
        $req_create_product .= "fic_de, ";
        $suite_req_product .= "NULL, ";
    }

	//Fiche FDS FR
    if (isset($_FILES['fiche_fds_fr']['name'])) {
        $uploaddir_fds_fr = './uploads/';
        $uploadfile_fds_fr .= "fds_fr_" . $data_post['code_sap'] . ".pdf";
        if (file_exists($uploadfile_fds_fr)) {
            copy($uploadfile_fds_fr, $uploaddir_fds_fr . "archives/fds_fr_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_fds_fr']['tmp_name'], $uploadfile_fds_fr)) {
            $req_create_product .= "fic_fds_fr, ";
            $suite_req_product .= ":fic_fds_fr, ";
            $params[':fic_fds_fr'] = $uploaddir_fds_fr;
        }
    } else {
        $req_create_product .= "fic_fds_fr, ";
        $suite_req_product .= "NULL, ";
    }

    //Fiche FDS EN 
    if ($data_post['no_english_fds_file'] !== "0") {
        if (isset($_FILES['fiche_fds_en']['name'])) {
            $uploaddir_fds_en = './uploads/';
            $uploadfile_fds_en .= "fds_en_" . $data_post['code_sap'] . ".pdf";
            if (file_exists($uploadfile_fds_en)) {
                copy($uploadfile_fds_en, $uploaddir_fds_en . "archives/fds_en_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
            }
            if (move_uploaded_file($_FILES['fiche_fds_en']['tmp_name'], $uploadfile_fds_en)) {
                $req_create_product .= "fic_fds_en, ";
                $suite_req_product .= ":fic_fds_en, ";
                $params[':fic_fds_en'] = $uploaddir_fds_en;
            }
        } else {
            $req_create_product .= "fic_fds_en, ";
            $suite_req_product .= "NULL, ";
        }
    } else {
        $req_create_product .= "fic_fds_en, ";
        $suite_req_product .= "NULL, ";
    }

    //Fiche DOF
    if (isset($_FILES['fiche_dof']['name'])) {
        $uploaddir_dof = './uploads/';
        $uploadfile_dof .= "dof_" . $data_post['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dof)) {
            copy($uploadfile_dof, $uploaddir_dof . "archives/dof_" . $data_post['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_dof']['tmp_name'], $uploadfile_dof)) {
            $req_create_product .= "fic_decl_four, ";
            $suite_req_product .= ":fic_decl_four, ";
            $params[':fic_decl_four'] = $uploaddir_dof;
        }
    } else {
        $req_create_product .= "fic_decl_four, ";
        $suite_req_product .= "NULL, ";
    }

    //Si case "pas de zone pref" coché
    if (!isset($data_post['prefer-0'])) {
        $req_create_product .= "is_not_pref, ";
        $suite_req_product .= "1, ";
    }else{
        $req_create_product .= "is_not_pref, ";
        $suite_req_product .= "0, ";
    }

    $req_create_product .= "date_dern_modif_prod, is_active";
    $suite_req_product .= "'" . date("Y-m-d H:i:s") . "', 1";
}

//ZONES
if (safeParameteredSQLRequestExecute($link, $req_create_product . $suite_req_product . ");", $params)) {
    $req_delete_pays = "DELETE FROM lien_produit_zone_pays WHERE sap_prod = :code;";
    $stmt_delete_pays = $link->prepare($req_delete_pays);
    $stmt_delete_pays->bindValue(":code", $data_post['code_sap']);
    $stmt_delete_pays->execute();

    if (!isset($data_post['prefer-0'])) {
        if ($data_post['zone_pref'] != "") {
            $zones = explode(";", $data_post['zone_pref']);
            $zone_pays = explode(";", $data_post['zone_pays']);
            foreach ($zones as $zone) {
                foreach ($zone_pays as $pays) {
                    $req_ajout_pays = "INSERT INTO lien_produit_zone_pays (`sap_prod`, `code_zone_pref`) VALUES(:code, :zone);";
                    $stmt_ajout_pays = $link->prepare($req_ajout_pays);
                    $stmt_ajout_pays->bindValue(":code", $data_post['code_sap']);
                    $stmt_ajout_pays->bindValue(":zone", $zone);
                    $stmt_ajout_pays->execute();
                }
            }
        }
    }

    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_product[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_product . ' ' . $data_post['code_sap'] . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_product[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_product[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}