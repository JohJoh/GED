<?php

session_start();
require_once('dbc.php');

$lang = $_SESSION['lang'];
$code_four = filter_input(INPUT_POST, "code_four", FILTER_SANITIZE_STRING);
$lib_four = filter_input(INPUT_POST, "nom_four", FILTER_SANITIZE_STRING);
$data = [];

$trad_ajax_admin_create_provider = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre fournisseur ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your provider ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.'
    )
);

$req_create_provider = "INSERT INTO fournisseurs(code_four, lib_four) VALUES( ";
$liste_create_provider = $trad_ajax_admin_create_provider[$lang]['start'];

if (isset($_POST['code_four']) && !empty($_POST['code_four'])) {
    $req_create_provider .= ":code_four, ";
    $data[':code_four'] = $code_four;
    //$liste_modif = $liste_modif.$trad_ajax_myaccount[$lang]['lastname'];
} else {
    die("Code fournisseur invalide");
}

if (isset($_POST['nom_four']) && !empty($_POST['nom_four'])) {
    $req_create_provider .= ":lib_four);";
    $data[':lib_four'] = $lib_four;
} else {
    die("Nom du fournisseur invalide");
}

if (safeParameteredSQLRequestExecute($link, $req_create_provider, $data)) {
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_provider[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_provider . ' ' . $_POST['code_four'] . '</div>';
    echo '</div>';
} else {
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_provider[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_provider[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
}