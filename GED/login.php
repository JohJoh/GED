<?php
/*
  LANGAGE
 */
$default_lang = 'fr';

if (isset($_GET['lang'])) {
    if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
        $lang = $_GET['lang'];
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $default_lang;
}

$trad_login = array(
    'fr' => array(
        'login' => 'Identifiez-vous',
        'user' => 'Nom d\'utilisateur',
        'password' => 'Mot de passe',
        'send' => 'Se connecter',
        'error1' => 'Compte non activé. Regardez vos e-mail pour le code d\'activation',
        'error2' => 'Nom d\'utilisateur invalide. Essayez de nouveau avec un login et mot de passe correct.'
    ),
    'en' => array(
        'login' => 'Login',
        'user' => 'User',
        'password' => 'Password',
        'send' => 'Send',
        'error1' => 'Unactivated account. Watch your email for the activation code',
        'error2' => 'Invalid username. Try again with a correct username and password.'
    )
);

/*
  LANGAGE
 */
include 'scripts/dbc.php';

//$err = array();

foreach ($_GET as $key => $value) {
    $get[$key] = filter($value); //get variables are filtered.
}

if (isset($_POST['doLogin'])) {
    foreach ($_POST as $key => $value) {
        $data[$key] = filter($value); // post variables are filtered
    }

    $user_email = $data['usr_email'];
    $pass = $data['pwd'];

    if (strpos($user_email, '@') === false) {
        $user_cond = "user_name=:usrname";
    } else {
        $user_cond = "user_email=:usremail";
    }

    $result = $link->prepare("SELECT `id`,`pwd`,`pnom_user`,`nom_user`,`user_level`, `code_four`, `lang` FROM users WHERE " . $user_cond);
    if (strpos($user_email, '@') === false) {
        $result->bindValue(':usrname', $user_email);
    } else {
        $result->bindValue(':usremail', $user_email);
    }
    $result->execute();


    //$result = $link->query("SELECT `id`,`pwd`,`pnom_user`,`nom_user`,`user_level`, `code_four`, `lang` FROM users WHERE " . $user_cond);
    $num = $result->rowCount();

    // Match row found with more than 1 results  - the user is authenticated.
    if ($num > 0) {
        //list($id,$pwd,$pnom_user,$nom_user,$approved,$user_level, $user_code_four) = mysqli_fetch_row($result);
        list($id, $pwd, $pnom_user, $nom_user, $user_level, $user_code_four, $lang) = $result->fetch(PDO::FETCH_NUM);
        // var_dump($result);
        $full_name = mb_ucfirst(mb_strtolower($pnom_user, "UTF-8")) . ' ' . mb_strtoupper($nom_user, "UTF-8");

        //check against salt
        // echo $pwd;
        //echo "<br>" . PwdHash($pass, substr($pwd, 0, 9));
        if ($pwd === PwdHash($pass, substr($pwd, 0, 9))) {
            if (empty($err)) {

                // this sets session and logs user in
                session_start();
                session_regenerate_id(true); //prevent against session fixation attacks.
                // this sets variables in the session
                $_SESSION['user_id'] = $id;
                $_SESSION['user_name'] = $full_name;
                $_SESSION['user_level'] = $user_level;
                $_SESSION['user_code_four'] = $user_code_four;
                $_SESSION['lang'] = $lang;
                $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);

                //update the timestamp and key for cookie
                $stamp = time();
                $ckey = GenKey();
                //mysqli_query($link, "update users set `ctime`='$stamp', `ckey` = '$ckey' where id='$id'") or die(mysqli_error($link));
                $sth = $link->prepare("update users set `ctime`=:stamp, `ckey` = :ckey where id=:id");
                $sth->bindParam(':stamp', $stamp, PDO::PARAM_STR, 220);
                $sth->bindParam(':ckey', $ckey, PDO::PARAM_STR, 220);
                $sth->bindParam(':id', $id, PDO::PARAM_INT);
                $sth->execute();

                //header("Location: home.php");
            }
            header("Location: home.php");
        } else {
            //$msg = urlencode("Invalid Login. Please try again with correct user email and password. ");
            $err = $trad_login[$lang]['error2'];
            //header("Location: login.php?msg=$msg");
        }
    } else {
        $err = $trad_login[$lang]['error2'];
    }
}
?>

<?php include("./scripts/head.php"); ?>
<?php include("./scripts/menu.php"); ?>
<?php include("./scripts/banner.php"); ?>

<!-- Content Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" action="login.php" method="post" name="logForm">
                    <fieldset>
                        <!-- Form Name -->
                        <legend><?php echo $trad_login[$lang]['login']; ?></legend>
                        <?php
                        if (!empty($err)) {
                            echo '<center>' . $err . '</center>';
                        }
                        ?>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput"></label>
                            <div class="col-md-4">
                                <input id="textinput" name="usr_email" type="text" placeholder="<?php echo $trad_login[$lang]['user']; ?>" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="passwordinput"></label>
                            <div class="col-md-4">
                                <input id="passwordinput" name="pwd" type="password" placeholder="<?php echo $trad_login[$lang]['password']; ?>" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton"></label>
                            <div class="col-md-4">
                                <button id="singlebutton" name="doLogin" class="btn btn-default"><?php echo $trad_login[$lang]['send']; ?></button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include("./scripts/footer.php"); ?>
