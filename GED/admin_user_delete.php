<?php
include('./scripts/dbc.php');
page_protect(); 
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php"); 
include("./scripts/menu.php"); 
include("./scripts/banner.php"); 

	$default_lang = 'fr';
	
	if(!isset($_SESSION['lang'])){
		if(isset($_GET['lang'])){
			if(($_GET['lang']=="fr")||($_GET['lang']=="en")){
				$lang = $_GET['lang'];
			}else{
				$lang = $default_lang;
			}
		}
		else{
			$lang = $default_lang;
		}
	}else{
			$lang = $_SESSION['lang'];
	}
	
	$trad_admin_user_delete = array(
		'fr' => array(
			'title_form' => 'Supprimer des utilisateurs',
			'user'=>'Utilisateur',
			'delete'=>'Supprimer',
			'suppression_ok'=>'L\'utilisateur a été supprimé',
			'suppression_ko'=>'Erreur de suppression'
		),
		'en' => array(
			'title_form' => 'Delete users',
			'user'=>'User',
			'delete'=>'Delete',
			'suppression_ok'=>'The user was deleted',
			'suppression_ko'=>'Error deleting'
		)
	);
	//Partie SQL
	$reponse_user=$link->query("SELECT * from users order by nom_user ASC");
	$reponse_user_admin=$link->query("SELECT *, users.id as new_id2 from users where user_level=0 or user_level=1 or user_level=2 order by nom_user ASC;");
	if(isset($_SESSION['user_level'])){
	
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<div id="result_req2" class="lead section-lead has-success"></div>
				
				<form class="form-horizontal" name="delete_user" data-toggle="validator" role="form" id="delete_user" >
					<fieldset>
					<!-- Form Name -->
						<legend><?php echo $trad_admin_user_delete[$lang]['title_form']; ?></legend>
						<div class="row">
							<label for="select" class="col-md-4 control-label"><?php echo $trad_admin_user_delete[$lang]['user'];?> : </label>
							<div class="col-md-4">
								<select id="user" class="form-control"name="user_delete">
								<?php
								if ($_SESSION['user_level']==5){
									while($donnees_user = $reponse_user ->fetch(PDO::FETCH_BOTH))
									{
										echo "<option value='".$donnees_user['id']."'>".$donnees_user['nom_user']." ".$donnees_user['pnom_user']."</option>";
									}
								}
								else{
									while($donnees_user_admin = $reponse_user_admin ->fetch(PDO::FETCH_BOTH))
									{
										echo "<option value='".$donnees_user_admin['id']."'>".$donnees_user_admin['nom_user']." ".$donnees_user_admin['pnom_user']."</option>";
									}
								}
								?>
								</select>
							</div>
						</div>
						</br>
						<div class="form-group">
							<label class="col-md-4 control-label" for="btn_delete"></label>
							<div class="col-md-4">
								<input type="submit" id="btn_delete" class="btn btn-primary" name="btn_delete" value="<?php echo $trad_admin_user_delete[$lang]['delete'];?>"/>
							</div>
						</div>
					</fieldset>
				</form>
            </div>
        </div>
    </div>
</section>
<script src="./js/validator.js"></script>
<script src="./js/jquery-admin_user.js"></script>
<?php 

}else{
	echo "<h2 class='lead section-lead has-error'>".$trad[$lang]['error']."</h2>";
} 

include("./scripts/footer.php");
?>
