<?php
include ('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include('./scripts/head.php');
include('./scripts/menu.php');
include('./scripts/banner.php');

$defaut_lang = 'fr';
if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == 'en')) {
            $lang = $_GET['lang'];
        } else {
            $lang = $defaut_lang;
        }
    } else {
        $lang = $defaut_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_zone_update = array(
    'fr' => array(
        'title_form' => 'Modifier des zones',
        'code_zone' => 'Code de la zone',
        'nom_zone' => 'Nom de la zone',
        'pays' => 'Pays de la zone',
        'update' => 'Modifier'
    ),
    'en' => array(
        'title_form' => 'Update zones',
        'code_zone' => 'Code of the zone',
        'nom_zone' => 'Name of the zone',
        'update' => 'Update'
    )
);

//Partie SQL
$reponse_zone = $link->query("SELECT * FROM zone ORDER BY code_zone_pref ASC");

if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-horizontal" name="formUpdateZone" data-toggle="validator" role="form" action="admin_zone_update_infos.php" method="post">
                        <fieldset>
                            <legend><?php echo $trad_admin_zone_update[$lang]['title_form']; ?></legend>
                            <div class="form-group">
                                <label for="select" class="col-md-4 control-label">Zone : </label>
                                <div class="col-md-4">
                                    <select id="zone" name="zone" class="form-control">
                                        <?php
                                        while ($donnees_zone = $reponse_zone->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<option value='" . $donnees_zone['code_zone_pref'] . "'>" . $donnees_zone['code_zone_pref'] . " - " . $donnees_zone['lib_zone_pref'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            </br>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_update"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_update" class="btn btn-primary" name="btn_update"><?php echo $trad_admin_zone_update[$lang]['update']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
} else {
    echo '<h2 class="lead section-lead-has-error">' . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>