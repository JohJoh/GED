<?php
include ('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include('./scripts/head.php');
include('./scripts/menu.php');
include('./scripts/banner.php');

$defaut_lang = 'fr';
if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == 'en')) {
            $lang = $_GET['lang'];
        } else {
            $lang = $defaut_lang;
        }
    } else {
        $lang = $defaut_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_zone_delete = array(
    'fr' => array(
        'title_form' => 'Supprimer des zones',
        'code_zone' => 'Code de la zone',
        'nom_zone' => 'Nom de la zone',
        'delete' => 'Supprimer',
        'suppression_ok' => 'La zone a été supprimée',
        'suppression_ko' => 'Erreur de suppression'
    ),
    'en' => array(
        'title_form' => 'Delete zones',
        'code_zone' => 'Code of zone',
        'nom_zone' => 'Name of zone',
        'delete' => 'Delete',
        'suppression_ok' => 'The zone was deleted',
        'suppression_ko' => 'Error deleting'
    )
);

//Partie SQL
$reponse_zone = $link->query("SELECT * FROM zone ORDER BY code_zone_pref ASC");

if (isset($_SESSION['user_level'])) {
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req2" class="lead section-lead has-success"></div>
                    <form class="form-horizontal" name="delete_zone" data-toggle="validator" role="form" id="delete_zone">
                        <fieldset>
                            <legend><?php echo $trad_admin_zone_delete[$lang]['title_form']; ?></legend>
                            <div class="form-group">
                                <label for="select" class="col-md-4 control-label">Zone : </label>
                                <div class="col-md-4">
                                    <select id="zone" class="form-control" name="zone_delete">
                                        <?php
                                        while ($donnees_zone = $reponse_zone->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<option value='" . $donnees_zone['code_zone_pref'] . "'>" . $donnees_zone['code_zone_pref'] . " - " . $donnees_zone['lib_zone_pref'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            </br>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_delete"></label>
                                <div class="col-md-4">
                                    <button type="submit" id="btn_delete" class="btn btn-primary" name="btn_delete"><?php echo $trad_admin_zone_delete[$lang]['delete']; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_zone.js"></script>
    <?php
} else {
    echo '<h2 class="lead section-lead-has-error">' . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>