<?php

// Connexion à la base de données 
include('./scripts/dbc.php');

$res = $link->query('SELECT pnom_user, nom_user, user_email, lang, 
                        CASE
                            WHEN user_level=0 THEN "Emballeur"
                            WHEN user_level=1 THEN "Fournisseur"
                            WHEN user_level=2 THEN "Utilisateur Lyreco"
                            WHEN user_level=5 THEN "Administrateur"
                        END as Type
                        FROM users
                        WHERE code_four IS NULL;');

//headers
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');
header('Content-Type: text/csv');
header("Content-disposition: attachment; filename=utilisateurs " . date("d-m-Y") . ".csv");
header('Content-Transfer-Encoding: binary');

//open file pointer to standard output
$fp = fopen('php://output', 'w');

//add BOM to fix UTF-8 in Excel
fputs($fp, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
if ($fp) {
    fputcsv($fp, array("Prénom", "Nom", "Email", "Langue", "Type de compte"), ";");
    if ($res->rowCount() != 0) {

// on insère les données de la table 
        while ($arrSelect = $res->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($fp, $arrSelect, ";");
        }
    }
}

fclose($fp);
