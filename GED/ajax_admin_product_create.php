﻿<?php
require_once('./scripts/dbc.php');
session_start();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$lang = $_SESSION['lang'];
foreach ($_POST as $key => $value) {
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
}
$params = array();
var_dump($data_post);

$trad_ajax_admin_create_product = array(
    'fr' => array(
        'title' => 'Création réussie.',
        'start' => 'Vous avez créé votre produit ',
        'lastname' => 'nom, ',
        'firstname' => 'prénom, ',
        'username' => 'nom d\'utilisateur, ',
        'email' => 'adresse e-mail, ',
        'language' => 'langage, ',
        'password' => 'mot de passe, ',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Verifiez le formulaire.',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Create successful.',
        'start' => 'You\'ve created your product ',
        'lastname' => 'last name, ',
        'firstname' => 'first name, ',
        'username' => 'username, ',
        'email' => 'email address, ',
        'language' => 'language, ',
        'password' => 'password, ',
        'title_error' => 'An error has occurred, ',
        'error' => 'Check out the form.',
        'retour' => 'Cancel'
    )
);
if (isset($data_post['pays_origine'])) {
    $pays_europeen_check = $link->query("SELECT * FROM pays WHERE code_pays='" . $data_post['pays_origine'] . "';");
    $pays_europeen = $pays_europeen_check->fetch();
}
$liste_create_product = $trad_ajax_admin_create_product[$lang]['start'];
$req_create_product = "insert into produits(";
$suite_req_product = ")VALUES(";
$nb_zones = array();
$nb_modes = array();
$nb_pays = array();
$exist = false;
$champs_obligatoires = false;
//$PI = explode("|", $data_post['pays_origine']);

foreach ($_POST as $key => $value) {
    $data[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING); // post variables are filtered
    if (substr($key, 0, 4) == "zone") {
        $nb_zones[] = $key;
    }
    if (substr($key, 0, 4) == "mode") {
        $nb_modes[] = $key;
    }
//    if (substr($key, 0, 4) == "pays") {
//        $nb_pays[] = $key;
//    }
}

$req_recup_infos_prod = "SELECT sap_prod FROM produits;";
$req_recup_infos_prod_check = $link->query($req_recup_infos_prod);
while ($donnees = $req_recup_infos_prod_check->fetch(PDO::FETCH_ASSOC)) {
    if ($donnees['sap_prod'] == $data_post['code_sap']) {
        $exist = true;
    }
}
if ((trim($data_post["code_sap"]) == "") or ( trim($data_post['fournisseur']) == "") or ( trim($data_post['code_produit_fournisseur']) == "") or ( trim($data_post['designation']) == "") or ( trim($data_post['division']) == "")) {
    $champs_obligatoires = true;
}

if (!$exist and ! $champs_obligatoires) {
    //SAP
    if (isset($data['code_sap'])) {
        if ($data['code_sap'] !== "") {
            $req_create_product .= "sap_prod, ";
            $suite_req_product .= ":sap_prod, ";
            $params[':sap_prod'] = $data['code_sap'];
        }
    }

    //FOURNISSEUR
    if (isset($data['fournisseur'])) {
        if ($data['fournisseur'] !== "") {
            $req_create_product .= "code_four, ";
            $suite_req_product .= ":code_four, ";
            $params[':code_four'] = $data['fournisseur'];
        }
    }

    //CODE PRODUIT FOURNISSEUR
    if (isset($data['code_produit_fournisseur'])) {
        if ($data['code_produit_fournisseur'] !== "") {
            $req_create_product .= "code_art_four_prod, ";
            $suite_req_product .= ":code_art_four_prod, ";
            $params[':code_art_four_prod'] = $data['code_produit_fournisseur'];
        }
    }

    //DESIGNATION
    if (isset($data['designation'])) {
        if ($data['designation'] !== "") {
            $req_create_product .= "designation, ";
            $suite_req_product .= ":designation, ";
            $params[':designation'] = $data['designation'];
        }
    }

    //DIVISION
    if (isset($data['division'])) {
        if ($data['division'] !== "") {
            $req_create_product .= "division_prod, ";
            $suite_req_product .= ":division_prod, ";
            $params[':division_prod'] = $data['division'];
        }
    }

    //PAYS DE PROVENANCE
    if (isset($data['pays_provenance'])) {
        if ($data['pays_provenance'] !== "") {
            $req_create_product .= "code_pays, ";
            $suite_req_product .= ":code_pays, ";
            $params[':code_pays'] = $data['pays_provenance'];
        }
    }

    //CODE POSTAL
    if (isset($data['code_postal'])) {
        if ($data['code_postal'] !== "") {
            $req_create_product .= "code_postal_prov, ";
            $suite_req_product .= ":code_postal_prov, ";
            $params[':code_postal_prov'] = $data['code_postal'];
        }
    }

    //VILLE
    if (isset($data['ville'])) {
        if ($data['ville'] !== "") {
            $req_create_product .= "ville_prov, ";
            $suite_req_product .= ":ville_prov, ";
            $params[':ville_prov'] = $data['ville'];
        }
    }

    //PAYS D'ORIGINE
    if (isset($data['pays_origine'])) {
        if ($data['pays_origine'] !== "") {
            $req_create_product .= "code_pays_origine, ";
            $suite_req_product .= ":code_pays_origine, ";
            $params[':code_pays_origine'] = $data['pays_origine'];
        }
    }

    //DATE DEBUT
    if ($pays_europeen['estEurope'] == "1") {
        if (!isset($data['prefer-0'])) {
            if (isset($data['date_debut_validite'])) {
                $req_create_product .= "date_deb_validite, ";
                $suite_req_product .= ":date_deb_validite, ";
                $params[':date_deb_validite'] = date($data['date_debut_validite']);
            } else {
                $req_create_product .= "date_deb_validite, ";
                $suite_req_product .= "NULL, ";
            }
        } else {
            $req_create_product .= "date_deb_validite, ";
            $suite_req_product .= "NULL, ";
        }
    } else {
        $req_create_product .= "date_deb_validite, ";
        $suite_req_product .= "NULL, ";
    }

    //DATE FIN	
    if ($pays_europeen['estEurope'] == "1") {
        if (!isset($data['prefer-0'])) {
            if (isset($data['date_fin_validite'])) {
                $req_create_product .= "date_fin_validite, ";
                $suite_req_product .= ":date_fin_validite, ";
                $params[':date_fin_validite'] = date($data['date_fin_validite']);
            } else {
                $req_create_product .= "date_fin_validite, ";
                $suite_req_product .= "NULL, ";
            }
        } else {
            $req_create_product .= "date_fin_validite, ";
            $suite_req_product .= "NULL, ";
        }
    } else {
        $req_create_product .= "date_fin_validite, ";
        $suite_req_product .= "NULL, ";
    }

    //CODE DOUANIER
    if (isset($data['code_douanier'])) {
        if ($data['code_douanier'] !== "") {
            $req_create_product .= "code_douanier, ";
            $suite_req_product .= ":code_douanier, ";
            $params[':code_douanier'] = $data['code_douanier'];
        }
    }

    
        $req_create_product .= "is_dangerous, ";
        $suite_req_product .= "1, ";
        //CLASSE DANGER
        if (isset($data['classe_danger'])) {
            if ($data['classe_danger'] !== "") {
                $req_create_product .= "code_danger, ";
                $suite_req_product .= ":code_danger, ";
                $params[':code_danger'] = $data['classe_danger'];
            }
        } else {
            $req_create_product .= "code_danger, ";
            $suite_req_product .= "NULL, ";
        }


        //CODE ONU
        if (isset($data['code_onu'])) {
            if ($data['code_onu'] !== "") {
                $req_create_product .= "code_onu, ";
                $suite_req_product .= ":code_onu, ";
                $params[':code_onu'] = $data['code_onu'];
            }
        } else {
            $req_create_product .= "code_onu, ";
            $suite_req_product .= "NULL, ";
        }

        //CODE EMBALLAGE
        if (isset($data['code_emballage'])) {
            if ($data['code_emballage'] !== "") {
                $req_create_product .= "code_embal, ";
                $suite_req_product .= ":code_embal, ";
                $params[':code_embal'] = $data['code_emballage'];
            }
        } else {
            $req_create_product .= "code_embal, ";
            $suite_req_product .= "NULL, ";
        }


        //CONTENANCE PRODUIT
        if (isset($data['contenance'])) {
            if ($data['contenance'] !== "") {
                $req_create_product .= "contenance_prod, ";
                $suite_req_product .= ":contenance_prod, ";
                $params[':contenance_prod'] = $data['contenance'] . " " . $data['unite'];
            }
        } else {
            $req_create_product .= "contenance_prod, ";
            $suite_req_product .= "NULL, ";
        }

		//TYPE PRODUIT
        if (isset($data['type'])) {
            if ($data['type'] !== "") {
                $req_create_product .= "type_prod, ";
                $suite_req_product .= ":type_prod, ";
                $params[':type_prod'] = $data['type'];
            }
        } else {
            $req_create_product .= "type_prod, ";
            $suite_req_product .= "NULL, ";
        }

        //CONDITIONNEMENT FOURNISSEUR
        if (isset($data['conditionnement_fournisseur'])) {
            if ($data['conditionnement_fournisseur'] !== "") {
                $req_create_product .= "condi_four, ";
                $suite_req_product .= ":condi_four, ";
                $params[':condi_four'] = $data['conditionnement_fournisseur'];
            }
        } else {
            $req_create_product .= "condi_four, ";
            $suite_req_product .= "NULL, ";
        }
    

    //Fiche DLT	
    //echo $data_post['fiche_dlt']."<BR>";
    if (isset($_FILES['fiche_DLT']['name'])) {
        $uploaddir_dlt = './uploads/';
        $uploadfile_dlt = $uploaddir_dlt . "dlt_" . $data['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dlt)) {
            copy($uploadfile_dlt, $uploaddir_dlt . "archives/dlt_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_DLT']['tmp_name'], $uploadfile_dlt)) {
            $req_create_product .= "fic_dlt, ";
            $suite_req_product .= ":fic_dlt, ";
            $params[':fic_dlt'] = $uploadfile_dlt;
        }
    } else {
        $req_create_product .= "fic_dlt, ";
        $suite_req_product .= "NULL, ";
    }

	//Fiche DLT PEMD	
    //echo $data_post['fiche_dlt_pemd']."<BR>";
    if (isset($_FILES['fiche_DLT_PEMD']['name'])) {
        $uploaddir_dlt_pemd = './uploads/';
        $uploadfile_dlt_pemd = $uploaddir_dlt_pemd . "dlt_pemd_" . $data['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dlt_pemd)) {
            copy($uploadfile_dlt_pemd, $uploaddir_dlt_pemd . "archives/dlt_pemd_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_DLT_PEMD']['tmp_name'], $uploadfile_dlt_pemd)) {
            $req_create_product .= "fic_dlt_pemd, ";
            $suite_req_product .= ":fic_dlt_pemd, ";
            $params[':fic_dlt_pemd'] = $uploadfile_dlt_pemd;
        }
    } else {
        $req_create_product .= "fic_dlt_pemd, ";
        $suite_req_product .= "NULL, ";
    }
	
	//Fiche DE	
    //echo $data_post['fiche_de']."<BR>";
    if (isset($_FILES['fiche_DE']['name'])) {
        $uploaddir_de = './uploads/';
        $uploadfile_de = $uploaddir_de . "de_" . $data['code_sap'] . ".pdf";
        if (file_exists($uploadfile_de)) {
            copy($uploadfile_de, $uploaddir_de . "archives/de_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_DE']['tmp_name'], $uploadfile_de)) {
            $req_create_product .= "fic_de, ";
            $suite_req_product .= ":fic_de, ";
            $params[':fic_de'] = $uploadfile_de;
        }
    } else {
        $req_create_product .= "fic_de, ";
        $suite_req_product .= "NULL, ";
    }

	//Fiche FDS FR
    if (isset($_FILES['fiche_FDS_fr']['name'])) {
        $uploaddir_fds_fr = './uploads/';
        $uploadfile_fds_fr = $uploaddir_fds_fr . "fds_fr_" . $data['code_sap'] . ".pdf";
        if (file_exists($uploadfile_fds_fr)) {
            copy($uploadfile_fds_fr, $uploaddir_fds_fr . "archives/fds_fr_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_FDS_fr']['tmp_name'], $uploadfile_fds_fr)) {
            $req_create_product .= "fic_fds_fr, ";
            $suite_req_product .= ":fic_fds_fr, ";
            $params[':fic_fds_fr'] = $uploadfile_fds_fr;
        }
    } else {
        $req_create_product .= "fic_fds_fr, ";
        $suite_req_product .= "NULL, ";
    }

    //Fiche FDS EN 
    if (!isset($data['no_english_file'])) {
        if (isset($_FILES['fiche_FDS_en']['name'])) {
            $uploaddir_fds_en = './uploads/';
            $uploadfile_fds_en = $uploaddir_fds_en . "fds_en_" . $data['code_sap'] . ".pdf";
            if (file_exists($uploadfile_fds_en)) {
                copy($uploadfile_fds_en, $uploaddir_fds_en . "archives/fds_en_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
            }
            if (move_uploaded_file($_FILES['fiche_FDS_en']['tmp_name'], $uploadfile_fds_en)) {
                $req_create_product .= "fic_fds_en, ";
                $suite_req_product .= ":fic_fds_en, ";
                $params[':fic_fds_en'] = $uploadfile_fds_en;
            }
        } else {
            $req_create_product .= "fic_fds_en, ";
            $suite_req_product .= "NULL, ";
        }
    } else {
        $req_create_product .= "fic_fds_en, ";
        $suite_req_product .= "NULL, ";
    }

    //Fiche DOF
    if (isset($_FILES['fiche_DOF']['name'])) {
        $uploaddir_dof = './uploads/';
        $uploadfile_dof = $uploaddir_dof . "dof_" . $data['code_sap'] . ".pdf";
        if (file_exists($uploadfile_dof)) {
            copy($uploadfile_dof, $uploaddir_dof . "archives/dof_" . $data['code_sap'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_DOF']['tmp_name'], $uploadfile_dof)) {
            $req_create_product .= "fic_decl_four, ";
            $suite_req_product .= ":fic_decl_four, ";
            $params[':fic_decl_four'] = $uploadfile_dof;
        }
    } else {
        $req_create_product .= "fic_decl_four, ";
        $suite_req_product .= "NULL, ";
    }
    
    //PAS DE PREFERENCES
    if (isset($data['prefer-0'])) {
        $req_create_product .= 'is_not_pref, ';
        $suite_req_product .= '1, ';
    } else {
        $req_create_product .= 'is_not_pref, ';
        $suite_req_product .= '0, ';
    }
    $req_create_product .= "date_dern_modif_prod, is_active";
    $suite_req_product .= "'" . date("Y-m-d H:i:s") . "', 1";
}

if (safeParameteredSQLRequestExecute($link, $req_create_product . $suite_req_product . ");", $params)) {
    $req_delete_pays = "DELETE FROM lien_produit_zone_pays WHERE sap_prod = :code;";
    $stmt_delete_pays = $link->prepare($req_delete_pays);
    $stmt_delete_pays->bindValue(":code", $data['code_sap']);
    $stmt_delete_pays->execute();

    if (!isset($data['prefer-0'])) {
        foreach ($nb_zones as $zone_cochee) {
            $req_ajout_pays = "INSERT INTO lien_produit_zone_pays (`sap_prod`, `code_zone_pref`) VALUES(:code, :zone,);";
            $stmt_ajout_pays = $link->prepare($req_ajout_pays);
            $stmt_ajout_pays->bindValue(":code", $data['code_sap']);
            $stmt_ajout_pays->bindValue(":zone", $data_post[$zone_cochee]);
            //$stmt_ajout_pays->execute();
        }
        if (!isset($data["danger-0"])) {
            $sap = $data['code_sap'];
            //mode_tpt
            $req_delete_modes = "DELETE FROM transporter WHERE sap_prod = :code";
            $stmt_delete_modes = $link->prepare($req_delete_modes);
            $stmt_delete_modes->bindValue(":code", $sap);
            $stmt_delete_modes->execute();
            foreach ($nb_modes as $mode_coche) {
                $req_ajout_modes = "INSERT INTO transporter (`sap_prod`, `code_tpt`) VALUES (:code , :mode);";
                $stmt_ajout_modes = $link->prepare($req_ajout_modes);
                $stmt_ajout_modes->bindValue(":code", $sap);
                $stmt_ajout_modes->bindValue(":mode", $data_post[$mode_coche]);
                $stmt_ajout_modes->execute();
            }
        }
    }
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_product[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_create_product . ' ' . $data['code_sap'] . '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <center><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = './admin_product_create.php'" ><?php echo $trad_ajax_admin_create_product[$lang]['retour']; ?></button></center>
    <?php
} else {
    echo $req_create_product . $suite_req_product . ");";
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_admin_create_product[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_admin_create_product[$lang]['error'] . '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <center><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = './admin_product_create.php'" ><?php echo $trad_ajax_admin_create_product[$lang]['retour']; ?></button></center>
    <?php
}
?>