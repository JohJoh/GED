<?php
require_once('./scripts/dbc.php');
session_start();
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$params = array();
$lang = $_SESSION['lang'];
foreach ($_POST as $key => $value) {
    $data_post[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING); // post variables are filtered
}

$trad_ajax_update_products = array(
    'fr' => array(
        'title' => 'Modification réussie',
        'start' => ' Vous avez modifié vos produits',
        'title_error' => 'Une erreur s\'est produite',
        'error' => 'Vérifiez le formulaire',
        'retour' => 'Retour'
    ),
    'en' => array(
        'title' => 'Update successful',
        'start' => 'You\'ve updated your products',
        'title_error' => 'An error has occured',
        'error' => 'Check out the form',
        'retour' => 'Cancel'
    )
);
if ($data_post['pays_origine']) {
    $pays_europeen_check = $link->prepare("SELECT * FROM pays WHERE code_pays = :pays_origine;");
    $pays_europeen_check -> execute([":pays_origine" => $data_post['pays_origine']]);
    $pays_europeen = $pays_europeen_check->fetch();
}
//var_dump($data['pays_origine']);die;
$liste_update_products = $trad_ajax_update_products[$lang]['start'];

$sap = $data_post['code_actuel'];
$i = 0;
$nb_zones = array();
$nb_modes = array();
$nb_pays = array();
foreach ($_POST as $key => $value) {
    $data[$key] = filter($value); // post variables are filtered
    if (substr($key, 0, 4) == "zone") {
        $nb_zones[] = $key;
    }
    if (substr($key, 0, 4) == "mode") {
        $nb_modes[] = $key;
    }
    if (substr($key, 0, 4) == "pays") {
        $nb_pays[] = $key;
    }
}

//var_dump($_POST); die;

$req_update_products = "UPDATE produits SET ";

$exist = false;
//$champs_obligatoires=false;

if (isset($data['fournisseur'])) {
    if ($data['fournisseur'] !== "") {
        $req_update_products .= "code_four = :fournisseur, ";
        $params[':fournisseur'] = $data['fournisseur'];
    }
}

if (isset($data['code_produit_fournisseur'])) {
    if ($data['code_produit_fournisseur'] !== "") {
        $req_update_products .= "code_art_four_prod = :code_produit_fournisseur, ";
        $params[':code_produit_fournisseur'] = $data['code_produit_fournisseur'];
    }
}

if (isset($data['designation'])) {
    if ($data['designation'] !== "") {
        $req_update_products .= "designation = :designation, ";
        $params[':designation'] = $data['designation'];
    }
}

if (isset($data['division'])) {
    if ($data['division'] !== "") {
        $req_update_products .= "division_prod = :division, ";
        $params[':division'] = $data['division'];
    }
}

if (isset($data['pays_provenance'])) {
    if ($data['pays_provenance'] !== "") {
        $req_update_products .= "code_pays = :pays_provenance, ";
        $params[':pays_provenance'] = $data['pays_provenance'];
    }
}

if (isset($data['code_postal'])) {
    if ($data['code_postal'] !== "") {
        $req_update_products .= "code_postal_prov = :code_postal, ";
        $params[':code_postal'] = $data['code_postal'];
    }
}

if (isset($data['ville']) && ($data['ville'] !== "")) {
    $req_update_products .= "ville_prov = :ville, ";
    $params[':ville'] = $data['ville'];
}

if (isset($data['pays_origine']) && ($data['pays_origine'] !== "")) {
    $req_update_products .= "code_pays_origine = :pays_origine, ";
    $params[':pays_origine'] = $data['pays_origine'];
}
if (isset($pays_europeen)) {
    if ($pays_europeen['estEurope'] == "1") {
        if (!isset($data['prefer-0'])) {
            if (isset($data['date_debut_validite']) && $data['date_debut_validite'] !== '') {
                $req_update_products .= "date_deb_validite = :date_debut_validite, ";
                $params[':date_debut_validite'] = $data['date_debut_validite'];
            } else {
                $req_update_products .= "date_deb_validite = NULL, ";
            }
        } else {
            $req_update_products .= "date_deb_validite = NULL, ";
        }
    } else {
        $req_update_products .= "date_deb_validite = NULL, ";
    }
}

if (isset($pays_europeen)) {
    if ($pays_europeen['estEurope'] == "1") {
        if (!isset($data['prefer-0'])) {
            if (isset($data['date_fin_validite']) && $data['date_fin_validite'] !== '') {
                $req_update_products .= "date_fin_validite = :date_fin_validite, ";
                $params[':date_fin_validite'] = $data['date_fin_validite'];
            } else {
                $req_update_products .= "date_fin_validite = NULL, ";
            }
        } else {
            $req_update_products .= "date_fin_validite = NULL, ";
        }
    } else {
        $req_update_products .= "date_fin_validite = NULL, ";
    }
}

if (isset($data['code_douanier']) && ($data['code_douanier'] !== "")) {
    $req_update_products .= "code_douanier = :code_douanier, ";
    $params[':code_douanier'] = $data['code_douanier'];
}

if (!isset($data['danger-0'])) {
    $req_update_products .= "is_dangerous = 1, ";
    if (isset($data['classe_danger'])) {
        if ($data['classe_danger'] !== "") {
            $req_update_products .= "code_danger = :classe_danger, ";
            $params[':classe_danger'] = $data['classe_danger'];
        }
    } else {
        $req_update_products .= "code_danger = NULL, ";
    }

    if (isset($data['code_onu'])) {
        if ($data['code_onu'] !== "") {
            $req_update_products .= "code_onu = :code_onu , ";
            $params[':code_onu'] = $data['code_onu'];
        }
    } else {
        $req_update_products .= "code_onu = NULL, ";
    }

    if (isset($data['code_emballage'])) {
        if ($data['code_emballage'] !== "") {
            $req_update_products .= "code_embal = :code_emballage, ";
            $params[':code_emballage'] = $data['code_emballage'];
        }
    } else {
        $req_update_products .= "code_embal = NULL, ";
    }

    if (isset($data['contenance'])) {
        if ($data['contenance'] !== "") {
            $req_update_products .= "contenance_prod = :contenance, ";
            $params[':contenance'] = $data['contenance'] . " " . $data['unite'];
        }
    } else {
        $req_update_products .= "contenance_prod = NULL, ";
    }

	if (isset($data['type'])) {
        if ($data['type'] !== "") {
            $req_update_products .= "type_prod = :type, ";
            $params[':type'] = $data['type'];
        }
    } else {
        $req_update_products .= "type_prod = NULL, ";
    }

    if (isset($data['conditionnement'])) {
        if ($data['conditionnement'] !== "") {
            $req_update_products .= "condi_four = :conditionnement, ";
            $params[':conditionnement'] = $data['conditionnement'];
        }
    } else {
        $req_update_products .= "condi_four = NULL, ";
    }

    $req_delete_modes = "delete from transporter where sap_prod = :code";
    $stmt_delete_modes = $link->prepare($req_delete_modes);
    $stmt_delete_modes->bindValue(":code", $sap);
    $stmt_delete_modes->execute();
    foreach ($nb_modes as $mode_coche) {
        $req_ajout_modes = "INSERT INTO transporter (`id_transporter`,`sap_prod`, `code_tpt`) VALUES (NULL, :code , :mode);";
        $stmt_ajout_modes = $link->prepare($req_ajout_modes);
        $stmt_ajout_modes->bindValue(":code", $sap);
        $stmt_ajout_modes->bindValue(":mode", $_POST[$mode_coche]);
        $stmt_ajout_modes->execute();
    }
} else {
    $req_update_products .= "is_dangerous = 0, ";
}
if (isset($data['classe_danger'])) {
        if ($data['classe_danger'] !== "") {
            $req_update_products .= "code_danger = :classe_danger, ";
            $params[':classe_danger'] = $data['classe_danger'];
        }
    } else {
        $req_update_products .= "code_danger = NULL, ";
    }

    if (isset($data['code_onu'])) {
        if ($data['code_onu'] !== "") {
            $req_update_products .= "code_onu = :code_onu , ";
            $params[':code_onu'] = $data['code_onu'];
        }
    } else {
        $req_update_products .= "code_onu = NULL, ";
    }

    if (isset($data['code_emballage'])) {
        if ($data['code_emballage'] !== "") {
            $req_update_products .= "code_embal = :code_emballage, ";
            $params[':code_emballage'] = $data['code_emballage'];
        }
    } else {
        $req_update_products .= "code_embal = NULL, ";
    }

    if (isset($data['contenance'])) {
        if ($data['contenance'] !== "") {
            $req_update_products .= "contenance_prod = :contenance, ";
            $params[':contenance'] = $data['contenance'] . " " . $data['unite'];
        }
    } else {
        $req_update_products .= "contenance_prod = NULL, ";
    }

	if (isset($data['type'])) {
        if ($data['type'] !== "") {
            $req_update_products .= "type_prod = :type, ";
            $params[':type'] = $data['type'];
        }
    } else {
        $req_update_products .= "type_prod = NULL, ";
    }

    if (isset($data['conditionnement_fournisseur'])) {
        if ($data['conditionnement_fournisseur'] !== "") {
            $req_update_products .= "condi_four = :conditionnement_fournisseur, ";
            $params[':conditionnement_fournisseur'] = $data['conditionnement_fournisseur'];
        }
    } else {
        $req_update_products .= "condi_four = :conditionnement_fournisseur, ";
    }

	$req_delete_modes = "delete from transporter where sap_prod = :code";
    $stmt_delete_modes = $link->prepare($req_delete_modes);
    $stmt_delete_modes->bindValue(":code", $sap);
    $stmt_delete_modes->execute();
    foreach ($nb_modes as $mode_coche) {
        $req_ajout_modes = "INSERT INTO transporter (`id_transporter`,`sap_prod`, `code_tpt`) VALUES (NULL, :code , :mode);";
        $stmt_ajout_modes = $link->prepare($req_ajout_modes);
        $stmt_ajout_modes->bindValue(":code", $sap);
        $stmt_ajout_modes->bindValue(":mode", $_POST[$mode_coche]);
        $stmt_ajout_modes->execute();
    }
//Fiche DLT
if (isset($_FILES['fiche_DLT']['name'])) {
    $uploaddir_dlt = 'uploads/';
    $uploadfile_dlt = $uploaddir_dlt . "dlt_" . $data['code_actuel'] . ".pdf";
    if (file_exists($uploadfile_dlt)) {
        copy($uploadfile_dlt, $uploaddir_dlt . "archives/dlt_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DLT']['tmp_name'], $uploadfile_dlt)) {
        $req_update_products .= "fic_dlt = :dlt, ";
        $params[':dlt'] = $uploadfile_dlt;
    }
} else {
    $req_update_products .= "fic_dlt = NULL, ";
}

//Fiche DLT PEMD
if (isset($_FILES['fiche_DLT_PEMD']['name'])) {
    $uploaddir_dlt_pemd = 'uploads/';
    $uploadfile_dlt_pemd = $uploaddir_dlt_pemd . "dlt_pemd_" . $data['code_actuel'] . ".pdf";
    if (file_exists($uploadfile_dlt_pemd)) {
        copy($uploadfile_dlt_pemd, $uploaddir_dlt_pemd . "archives/dlt_pemd_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DLT_PEMD']['tmp_name'], $uploadfile_dlt_pemd)) {
        $req_update_products .= "fic_dlt_pemd = :dlt_pemd, ";
        $params[':dlt_pemd'] = $uploadfile_dlt_pemd;
    }
} else {
    $req_update_products .= "fic_dlt_pemd = NULL, ";
}

//Fiche DE
if (isset($_FILES['fiche_DE']['name'])) {
    $uploaddir_de = 'uploads/';
    $uploadfile_de = $uploaddir_de . "de_" . $data['code_actuel'] . ".pdf";
    if (file_exists($uploadfile_de)) {
        copy($uploadfile_de, $uploaddir_de . "archives/de_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DE']['tmp_name'], $uploadfile_de)) {
        $req_update_products .= "fic_de = :de, ";
        $params[':de'] = $uploadfile_de;
    }
} else {
    $req_update_products .= "fic_de = NULL, ";
}

//Fiche FDS FR
if (isset($_FILES['fiche_FDS_fr']['name'])) {
    $uploaddir_fds_fr = 'uploads/';
    $uploadfile_fds_fr = $uploaddir_fds_fr . "fds_fr_" . $data['code_actuel'] . ".pdf";
    if (file_exists($uploadfile_fds_fr)) {
        copy($uploadfile_fds_fr, $uploaddir_fds_fr . "archives/fds_fr_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_FDS_fr']['tmp_name'], $uploadfile_fds_fr)) {
        $req_update_products .= "fic_fds_fr = :fds_fr, ";
        $params[':fds_fr'] = $uploadfile_fds_fr;
    }
} else {
    $req_update_products .= "fic_fds_fr = NULL, ";
}

//Fiche FDS EN
if (!isset($data['no_english_file'])) {
    if (isset($_FILES['fiche_FDS_en']['name'])) {
        $uploaddir_fds_en = 'uploads/';
        $uploadfile_fds_en = $uploaddir_fds_en . "fds_en_" . $data['code_actuel'] . ".pdf";
        if (file_exists($uploadfile_fds_en)) {
            copy($uploadfile_fds_en, $uploaddir_fds_en . "archives/fds_en_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
        }
        if (move_uploaded_file($_FILES['fiche_FDS_en']['tmp_name'], $uploadfile_fds_en)) {
            $req_update_products .= "fic_fds_en = :fds_en, ";
            $params[':fds_en'] = $uploadfile_fds_en;
        }
    } else {
        $req_update_products .= "fic_fds_en = NULL, ";
    }
} else {
    $req_update_products .= "fic_fds_en = NULL, ";
}

//Fiche DOF
if (isset($_FILES['fiche_DOF']['name'])) {
    $uploaddir_dof = 'uploads/';
    $uploadfile_dof = $uploaddir_dof . "dof_" . $data['code_actuel'] . ".pdf";
    if (file_exists($uploadfile_dof)) {
        copy($uploadfile_dof, $uploaddir_dof . "archives/dof_" . $data['code_actuel'] . "_" . date("YmdHis") . ".pdf");
    }
    if (move_uploaded_file($_FILES['fiche_DOF']['tmp_name'], $uploadfile_dof)) {
        $req_update_products .= "fic_decl_four = :dof, ";
        $params[':dof'] = $uploadfile_dof;
    }
} else {
    $req_update_products .= "fic_decl_four = NULL, ";
}

if (isset($data['prefer-0'])) {
    $req_update_products .= "is_not_pref=1, ";
} else {
    $req_update_products .= "is_not_pref=0, ";
}


$req_update_products .= "is_active=1, date_dern_modif_prod='" . date('Y-m-d H:i:s') . "' WHERE sap_prod =:code;";
//$req_update_products=$req_update_products."is_active=1, date_dern_modif_prod='".date('Y-m-d H:i:s')."' WHERE sap_prod ='2' ;";
$params[':code'] = $sap;
//var_dump($req_update_products, $params);die;
safeParameteredSQLRequestExecute($link, $req_update_products, $params);

$stmt_delete_pays = $link->prepare("DELETE FROM lien_produit_zone_pays WHERE sap_prod = :code;");
$stmt_delete_pays->execute([':code' => $sap]);
if (!isset($data['prefer-0'])) {
    //CONDITION SUR LES ZONES COCHEES

    $req_ajout_pays = "INSERT INTO lien_produit_zone_pays (`sap_prod`, `code_zone_pref`) VALUES(:code, :zone);";
    $stmt_ajout_pays = $link->prepare($req_ajout_pays);
    foreach ($nb_zones as $zone_cochee) {
        $stmt_ajout_pays->bindValue(":code", $sap);
        $stmt_ajout_pays->bindValue(":zone", $_POST[$zone_cochee]);
        $stmt_ajout_pays->execute();
    }
}
$i++;

if ($i > 0) {
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-success">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $liste_update_products . '</div>';
    //echo'<meta http-equiv="refresh" content="0; URL=../allproduct.php">';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <center><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = './allproduct.php'" ><?php echo $trad_ajax_update_products[$lang]['retour']; ?></button></center>
    <?php
} else {
    echo '<section>';
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-lg-12">';
    echo '<div class="panel panel-danger">';
    echo '<div class="panel-heading">';
    echo '<h3 class="panel-title" id="titre_panel">' . $trad_ajax_update_products[$lang]['title_error'] . '</h3>';
    echo '</div>';
    echo '<div class="panel-body" id="contenu_panel">' . $trad_ajax_update_products[$lang]['error'] . '</div>';
    //echo '<div class="panel-body" id="contenu_panel">'.$req_update_user.'</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</section>';
    ?>
    <center><button type="button" name="retour" class="btn btn-danger" id="retour" onclick="self.location.href = './allproduct.php'" ><?php echo $trad_ajax_update_products[$lang]['retour']; ?></button></center>
    <?php
}
?>
