<?php
include('./scripts/dbc.php');
page_protect(); 
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php"); 
include("./scripts/menu.php"); 
include("./scripts/banner.php"); 

	$default_lang = 'fr';
	
	if(!isset($_SESSION['lang'])){
		if(isset($_GET['lang'])){
			if(($_GET['lang']=="fr")||($_GET['lang']=="en")){
				$lang = $_GET['lang'];
			}else{
				$lang = $default_lang;
			}
		}
		else{
			$lang = $default_lang;
		}
	}else{
			$lang = $_SESSION['lang'];
	}
	
	$trad_admin_user_update = array(
		'fr' => array(
			'title_form' => 'Modifier des utilisateurs',
			'user'=>'Utilisateur',
			'lastname' => 'Nom',
			'firstname' => 'Prénom',
			'username' => 'Nom d\'utilisateur',
			'username_help' => 'Utiliser pour se connecter à la place de l\'adresse email',
			'email' => 'Adresse e-mail',
			'language' => 'Langue',
			'newpwd1' => 'Nouveau mot de passe',
			'newpwd2' => 'Confirmer nouveau mot de passe',
			'save' => 'Enregistrer',
			'update'=>'Modifier',
			'errorpwd1' => 'Votre mot de passe doit comporter 6 caractères minimum.',
			'errorpwd2' => 'Vos mots de passe doivent être identiques.',
			'erroremail1' => 'Votre adresse e-mail n\'est pas valide.',
			'erroremail2' => 'Cette adresse e-mail est déjà utilisée.',
			'errorusrnme1' => 'Le nom d\'utilisateur existe déjà.',
			'errorusrnme2' => 'Votre nom d\'utilisateur doit contenir au moins 6 caractères.',
			'errorusrnme3' => 'Uniquement des lettres et chiffres autorisés.'
		),
		'en' => array(
			'title_form' => 'Update users',
			'user'=>'User',
			'lastname' => 'Name',
			'firstname' => 'First name',
			'username' => 'Username',
			'username_help' => 'Use to connect instead of the email address',
			'email' => 'Email address',
			'language' => 'Language',
			'newpwd1' => 'New Password',
			'newpwd2' => 'Confirm New Password',
			'save' => 'Save',
			'update'=>'Update',
			'errorpwd1' => 'Your password must contain at least 6 characters long.',
			'errorpwd2' => 'Your passwords must match.',
			'erroremail1' => 'Your e-mail address is not valid.',
			'erroremail2' => 'This email address is already in use.',
			'errorusrnme1' => 'Your username is already used.',
			'errorusrnme2' => 'Your username must be at least 6 characters.',
			'errorusrnme3' => 'Only letters and white space allowed.'
		)
	);
	//Partie SQL
	$reponse_user=$link->query("SELECT *, users.id as new_id from users order by users.nom_user ASC");
	$reponse_user_admin=$link->query("SELECT *, users.id as new_id2 from users where user_level=0 or user_level=1 or user_level=2 order by users.nom_user ASC;");
	if(isset($_SESSION['user_level'])){
?>

<!-- Content Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<form class="form-horizontal" name="update_user" data-toggle="validator" role="form" action="admin_user_update_infos.php" method="post">
					<fieldset>
					<!-- Form Name -->
						<legend><?php echo $trad_admin_user_update[$lang]['title_form']; ?></legend>
						<div class="row">
							<label for="select" class="col-md-4 control-label"><?php echo $trad_admin_user_update[$lang]['user'];?> : </label>
							<div class="col-md-4">
								<select name="user" id="user" class="form-control">
								<?php
								if ($_SESSION['user_level']==5){
									while($donnees_user = $reponse_user ->fetch(PDO::FETCH_BOTH))
									{
										echo "<option value='".$donnees_user['id']."'>".$donnees_user['nom_user']." ".$donnees_user['pnom_user']."</option>";
									}
								}
								else{
									while($donnees_user_admin = $reponse_user_admin ->fetch(PDO::FETCH_BOTH))
									{
										echo "<option value='".$donnees_user_admin['id']."'>".$donnees_user_admin['nom_user']." ".$donnees_user_admin['pnom_user']."</option>";
									}
								}
								?>
								</select>
							</div>
						</div>
						</br>
						<div class="form-group">
							<label class="col-md-4 control-label" for="btn_update"></label>
							<div class="col-md-4">
								<button type="submit" id="btn_update" class="btn btn-primary" name="btn_update"><?php echo $trad_admin_user_update[$lang]['update'];?></button>
							</div>
						</div>
					</fieldset>
				</form>
            </div>
        </div>
    </div>
</section>
<script src="./js/validator.js"></script>
<?php 
}else{
	echo "<h2 class='lead section-lead has-error'>".$trad[$lang]['error']."</h2>";
} 

include("./scripts/footer.php");
?>
