<?php
include('./scripts/dbc.php');
page_protect();
if (!checkAdmin()) {
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");

$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_code_create = array(
    'fr' => array(
        'title' => 'Créer un code douanier',
        'code' => 'Code douanier',
        'name' => 'Libellé associé',
        'save' => 'Enregistrer',
        'error_douanecode_1' => 'Le code douanier existe déja',
        'error_douanecode_2' => 'Le code douanier ne peut contenir que des chiffres',
        'insertion_ok' => 'Le code douanier a été créé avec succès',
        'insertion_ko' => 'Error de création'
    ),
    'en' => array(
        'title' => 'Create a customs code',
        'code' => 'Customs Code',
        'name' => 'Label',
        'save' => 'Save',
        'error_douanecode_1' => 'The customs code already exists',
        'error_douanecode_2' => 'The customs code may contain numbers',
        'insertion_ok' => 'The customs code was successfully created',
        'insertion_ko' => 'Error creating'
    )
);
if (isset($_SESSION['user_level'])) {
    ?>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="result_req" class="lead section-lead has-success"></div>

                    <form class="form-horizontal" id="create_code" name="create_code" data-toggle="validator" role="form">
                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo $trad_admin_code_create[$lang]['title']; ?></legend>

                            <!-- Code douanier -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="code_douanier"><?php echo $trad_admin_code_create[$lang]['code']; ?></label>  
                                <div class="col-md-4">
                                    <input 
                                        id="code_douanier" 
                                        class="form-control input-md" 
                                        name="code_douanier" 
                                        type="text" 
                                        placeholder="" 
                                        required=""
                                        data-remote="./scripts/req_douaniercode.php" 
                                        data-remote-error="<?php echo $trad_admin_code_create[$lang]['error_douanecode_1']; ?>"
                                        pattern="[0-9]+"
                                        data-native-error="<?php echo $trad_admin_code_create[$lang]['error_douanecode_2']; ?>"
                                        maxlength="8"
                                        >
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <!-- Nom code douanier -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="nom_douanier"><?php echo $trad_admin_code_create[$lang]['name']; ?></label>  
                                <div class="col-md-4">
                                    <input id="nom_douanier" class="form-control input-md" name="nom_douanier" type="text" placeholder="" required="">
                                </div>
                            </div>

                            <!-- Bouton enregistrer -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_registrer"></label>
                                <div class="col-md-4">    
                                    <button type="submit" id="btn_registrer" class="btn btn-primary" name="btn_registrer"><?php echo $trad_admin_code_create[$lang]['save']; ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <script src="./js/jquery-admin_code_douanier.js"></script>

    <?php
    include("./scripts/footer.php");
}
?>