test('global()', function () {
    ok(global(true), 'global ok');
    ok(!global(false), 'global ok');
})

test('ready()', function () {
    ok(ready(true), 'ready ok');
    ok(!ready(false), 'ready ok');
})

test('fct_req_admin_product_pays_europeen_mysql()', function () {
    ok(fct_req_admin_product_pays_europeen_mysql(true), 'fct_req_admin_product_pays_europeen_mysql ok');
    ok(!fct_req_admin_product_pays_europeen_mysql(false), 'fct_req_admin_product_pays_europeen_mysql ok');
})

test('chg_code_douanier()', function () {
    ok(chg_code_douanier(true), 'chg_code_douanier ok');
    test('fct_definition_code_douanier()', function () {
        ok(fct_definition_code_douanier(true), 'fct_definition_code_douanier ok');
        ok(!fct_definition_code_douanier(false), 'fct_definition_code_douanier ok');
    })
    ok(!chg_code_douanier(false), 'chg_code_douanier ok');
})

test('chg_classe_danger()', function () {
    ok(chg_classe_danger(true), 'chg_classe_danger ok');
    test('fct_definition_classe_danger()', function () {
        ok(fct_definition_classe_danger(true), 'fct_definition_classe_danger ok');
        ok(!fct_definition_classe_danger(false), 'fct_definition_classe_danger ok');
    })
    ok(!chg_classe_danger(false), 'chg_classe_danger ok');
})

test('chg_code_onu()', function () {
    ok(chg_code_onu(true), 'chg_code_onu ok');
    test('fct_definition_code_onu()', function () {
        ok(fct_definition_code_onu(true), 'fct_definition_code_onu ok');
        ok(!fct_definition_code_onu(false), 'fct_definition_code_onu ok');
    })
    ok(!chg_code_onu(false), 'chg_code_onu ok');
})

test('chg_code_emballage()', function () {
    ok(chg_code_emballage(true), 'chg_code_emballage ok');
    test('fct_definition_code_emballage()', function () {
        ok(fct_definition_code_emballage(true), 'fct_definition_code_onu ok');
        ok(!fct_definition_code_emballage(false), 'fct_definition_code_onu ok');
    })
    ok(!chg_code_emballage(false), 'chg_code_emballage ok');
})

ma_lang = '';
test('chg_pays_origine()', function () {
    ok(chg_pays_origine(true), 'chg_pays_origine ok');
    ok(!chg_pays_origine(false), 'chg_pays_origine ok');
})

test('clk_danger()', function () {
    ok(clk_danger(true), 'clk_danger ok');
    ok(!clk_danger(false), 'clk_danger ok');
})

zp = '';
test('clk_prefer()', function () {
    ok(clk_prefer(true), 'clk_prefer ok');
    test('zp_element()', function () {
        ok(zp_element(true), 'zp_element ok');
        test('clk_zp_element()', function () {
            ok(clk_zp_element(true), 'clk_zp_element ok');
            ok(!clk_zp_element(false), 'clk_zp_element ok');
        })
        ok(!zp_element(false), 'zp_element ok');
    })
    ok(!clk_prefer(false), 'clk_prefer ok');
})

test('clk_zone_PEMD()', function () {
    ok(clk_zone_PEMD(true), 'clk_zone_PEMD ok');
    ok(!clk_zone_PEMD(false), 'clk_zone_PEMD ok');
})

test('chg_req_zone_pays()', function () {
    ok(chg_req_zone_pays(true), 'chg_req_zone_pays ok');
    test('fct_req_zone_pays()', function () {
        ok(fct_req_zone_pays(true), 'fct_req_zone_pays ok');
        ok(!fct_req_zone_pays(false), 'fct_req_zone_pays ok');
    })
    ok(!chg_req_zone_pays(false), 'chg_req_zone_pays ok');
})

test('clk_no_english_file()', function () {
    ok(clk_no_english_file(true), 'clk_no_english_file ok');
    ok(!clk_no_english_file(false), 'clk_no_english_file ok');
})