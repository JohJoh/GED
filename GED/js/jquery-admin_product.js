$(global = function (rep_global) { 
    $('#group-prefer').css('display', 'none');    
    $("#group_danger").css("display", "none");
            
    /*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
     *	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
     *
     *	SELECTEURS
     *	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
     *								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
     *	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
     *	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
     *
     *
     *
     * vérification de la validité d'un mot de passe saisi par l'utilisateur.
     * Lorsque l'utilisateur saisis des lettres dans le champ mot de passe
     * @return {[type]} [description]
     */

    $(document).ready(ready = function (rep_ready) {
        $.ajax({
            url: './scripts/req_admin_product_pays_europeen_mysql.php',
            type: 'POST',
            data: 'pays_origine=' + $("#pays_origine").val(),
            dataType: 'html',
            success: fct_req_admin_product_pays_europeen_mysql = function (code_html, statut) { // code_html contient le HTML renvoyé
                if (code_html !== 1) {
                    $("#group_europe").css('display', 'none');
                    if ($("#prefer-0").prop('checked')) {
                        $('#groupVAL').css('display', 'block');
                        $('#group-prefer').css('display', 'none');
                        $('#groupDLT').css('display', 'none');
                        $('#groupDE').css('display', 'none');
                        $('#groupDLTPEMD').css('display', 'none');
                        $('#groupDE_Radio').css('display', 'none');
                        $('#groupDE_PEMD_Radio').css('display', 'none');
                    } else {
                        $('#groupVAL').css('display', 'none');
                        $('#group-prefer').css('display', 'block');
                        if (!$("#zone_PEMD").prop('checked')) {
                            $('#groupDLTPEMD').css('display', 'none');
                        }
                        var nbCheck = 0;
                    }
                }
                return code_html;
//                console.log('OK : ' + statut);
            },
            error: function (resultat, statut, erreur) {
//                console.log('Erreur : ' + erreur);                
            },
            complete: function () {                
            }            
        });
        if (!$("#zone_ACP").prop('checked') && !$("#zone_AMCE").prop('checked') && !$("#zone_ASIE").prop('checked') && !$("#zone_BOHE").prop('checked') && !$("#zone_CA").prop('checked') && !$("#zone_CHIL").prop('checked') && !$("#zone_COLO").prop('checked') && !$("#zone_MEXI").prop('checked') && !$("#zone_PEMD").prop('checked') && !$("#zone_PERO").prop('checked') && !$("#zone_PTOM").prop('checked') && !$("#zone_RAMA").prop('checked')) {
            $('#groupDLT').css('display', 'none');
            $('#groupDLTPEMD').css('display', 'none');
            $('#groupDE_Radio').css('display', 'none');
            $('#groupDE_PEMD_Radio').css('display', 'none');
            $('#groupDE').css('display', 'none');
        }
        $("input[name*='zone']").change();
        $("#code_douanier").change();
        $("#classe_danger").change();
        $("#code_onu").change();
        $("#code_emballage").change();
        //$("#danger-0").trigger("click");
        $("#pays_origine").change();
        //$("#prefer-0").click();
        //$("#no_english_file").click();
        return rep_ready;
    });

    //Code douanier
    $("#code_douanier").change(chg_code_douanier = function (rep_chg_code_douanier) {
        $.ajax({
            url: './scripts/req_def_code_douanier.php',
            type: 'GET',
            data: 'code=' + $("#code_douanier").val(),
            dataType: 'html',
            success: fct_definition_code_douanier = function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#definition_code_douanier').html(code_html);
                //console.log(code_html);
                return code_html;
                //                console.log('Definition code douanier : ok');
            },            
            error: function (resultat, statut, erreur) {
                //                console.log('Erreur : ' + erreur);
            },
            complete: function () {
            }
        });
        return rep_chg_code_douanier;
    });

    //Classe danger
    $("#classe_danger").change(chg_classe_danger = function (rep_chg_classe_danger) {
        $.ajax({
            url: './scripts/req_def_classe_danger.php',
            type: 'GET',
            data: 'classe=' + $("#classe_danger").val(),
            dataType: 'html',
            success: fct_definition_classe_danger = function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#definition_classe_danger').html(code_html);
                //console.log(code_html);
                return code_html;
//                console.log('Definition classe danger : ok');
                //console.log($("#classe_danger").val());
            },
            error: function (statut, erreur) {
//                console.log('Erreur : ' + erreur);
            },
            complete: function () {
            }
        });
        return rep_chg_classe_danger;
    });

    //Code ONU
    $("#code_onu").change(chg_code_onu = function (rep_chg_code_onu) {
        $.ajax({
            url: './scripts/req_def_code_onu.php',
            type: 'GET',
            data: 'code=' + $("#code_onu").val(),
            dataType: 'html',
            success: fct_definition_code_onu = function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#definition_code_onu').html(code_html);
                //console.log(code_html);
                return code_html;
//                console.log('Definition code onu : ok');
            },
            error: function (resultat, statut, erreur) {
//                console.log('Erreur : ' + erreur);
            },
            complete: function () {
            }
        });
        return rep_chg_code_onu;
    });

    //Code emballage
    $("#code_emballage").change(chg_code_emballage = function (rep_chg_code_emballage) {
        $.ajax({
            url: './scripts/req_def_code_emballage.php',
            type: 'GET',
            data: 'code=' + $("#code_emballage").val(),
            dataType: 'html',
            success: fct_definition_code_emballage = function (code_html, statut) { // code_html contient le HTML renvoyé
                $('#definition_code_emballage').html(code_html);
                //console.log(code_html);
                return code_html;
//                console.log('Definition code emballage : ok');
            },
            error: function (resultat, statut, erreur) {
//                console.log('Erreur : ' + erreur);
            },
            complete: function () {
            }
        });
        return rep_chg_code_emballage;
    });

    //Parie danger
    $("#danger-1").click(clk_danger = function (rep_clk_danger) {
        if ($("#danger-1").prop('checked')) {
            $("#group_danger").css("display", "block");
//            $("#classe_danger").attr('required', '');
//            $("#code_ONU").attr('required', '');
//            $("#code_emballage").attr('required', '');
//            $("#contenance").attr('required', '');
//            $("#type").attr('required', '');
//            $("#conditionnement_fournisseur").attr('required', '');
            var m = tpt.split(" ", tpt.split(" ").length - 1);
            var nbCheck = 0;
            
            m.forEach(function (element) {
                $(element).click(function () {
                    if ($(element).prop('checked')) {
                        nbCheck += 1;
                        // si une checkbox est coché alors on affiche le groupVAL
                        $('#groupVAL').css('display', 'block');
                    } else {
                        nbCheck -= 1;
                        if (nbCheck > 0) {
                            $('#groupVAL').css('display', 'block');
                        } else {
                            $('#groupVAL').css('display', 'none');
                        }
                    }
                });
                if ($(element).prop('checked')) {
                    // s'il y a une checkbox de coché alors on affiche le groupVAL
                    nbCheck += 1;
                }
                if (nbCheck > 0) {
                    $('#groupVAL').css('display', 'block');
                } else {
                    $('#groupVAL').css('display', 'none');
                }
            });
        }
        //console.log(rep_clk_danger);
        return rep_clk_danger;
    });

    $("#danger-0").click(clk_danger = function (rep_clk_danger) {
        if ($("#danger-0").prop('checked')) {
            $("#group_danger").css("display", "none");            
        }
        return rep_clk_danger;
    });

    //Pays d'origine
    $("#pays_origine").change(chg_pays_origine = function (rep_chg_pays_origine) {
        $.ajax({
            url: './scripts/req_admin_product_pays_europeen_mysql.php',
            type: 'POST',
            data: 'pays_origine=' + $("#pays_origine").val(),
            dataType: 'html',
            success: function (code_html, statut) { // code_html contient le HTML renvoyé                
                if (code_html == 1) {                    
                    $("#group_europe").css('display', 'block');
                    if ($("#prefer-0").prop('checked')) {
                        $('#group-prefer').css('display', 'none');
                        $('#groupDLT').css('display', 'none');
                        $('#groupDLTPEMD').css('display', 'none');
                        $('#groupDE_Radio').css('display', 'none');
                        $('#groupDE_PEMD_Radio').css('display', 'none');
                        $('#groupDE').css('display', 'none');
                    }  
                    
                    if (ma_lang == "fr") {
                        $("#fiche_DLT").attr("onClick", "alert('Signature manuscrite obligatoire sur le document ou joignez un déclaration d’engagement écrite du fournisseur. Pour plus de renseignement : Consulter la page Aide');");
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('Pour savoir quel formulaire vous devez nous envoyer, merci de vous référencer à la page aide (paragraphe 7)');");
                    } else {
                        $("#fiche_DLT").attr("onClick", "alert('Mandatory handwritten signature on the document or attach a written statement of commitment from the supplier.For more information: Consult the Help page');");
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('To find out which form you have to send us, please refer to the help page (paragraph 7)');");
                    }
                    
                } else {
                    $("#group_europe").css('display', 'none');
                    $("#groupDLTPEMD").css('display', 'none');
                    if (ma_lang == "fr") {
                        $("#fiche_DLT").attr("onClick", "alert('Signature manuscrite obligatoire sur le document ou joignez un déclaration d’engagement écrite du fournisseur. Pour plus de renseignement : Consulter la page Aide');");
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('Pour savoir quel formulaire vous devez nous envoyer, veuillez vous référer à la page d'aide (paragraphe 7)');");
                    } else {
                        $("#fiche_DLT").attr("onClick", "alert('Mandatory handwritten signature on the document or attach a written statement of commitment from the supplier.For more information: Consult the Help page');");
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('To find out which form you have to send us, please refer to the help page (paragraph 7)');");
                    }
                    
                }
                value = document.getElementById("pays_origine").value;
                //console.log(document.getElementById("pays_origine").value);
                if (value == "AD" || value == "CH" || value == "EG" || value == "IL" || value == "JO" || value == "LB" || value == "MA" || value == "NO" || value == "PS" || value == "SY" || value == "TN" || value == "TR" || value == "DZ" || value == "FO" || value == "IS" || value == "LB" || value == "SY" || value == "PS") {
                    $("#groupDLTPEMD").css('display', 'block');
                    $('#groupDE_PEMD_Radio').css('display', 'block');
                    $("#dlt_pemd-1").click(function () {
                        console.log('1');
                        if ($("#dlt_pemd-1").prop('checked')) {
                            $('#groupDE').css('display', 'none');
                        }
                        if ($("#dlt_pemd-0").prop('checked')) {
                            $('#groupDE').css('display', 'block');
                        }
                    });
                    if (ma_lang == "fr") {
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('Pour savoir quel formulaire vous devez nous envoyer, merci de vous référencer à la page aide (paragraphe 7)');");
                    } else {
                        $("#fiche_DLT_PEMD").attr("onClick", "alert('To find out which form you have to send us, please refer to the help page (paragraph 7)');");
                    }
                }
            },
            error: function (resultat, statut, erreur) {
//                console.log('Erreur : ' + erreur);
            }            
        });
        return rep_chg_pays_origine;
    });    

    //Partie préférentielle
    $("#prefer-0").click(clk_prefer = function (rep_clk_prefer) {
        if ($("#prefer-0").prop('checked')) {
            $('#groupVAL').css('display', 'block');
            $('#group-prefer').css('display', 'none');
            $('#groupDLT').css('display', 'none');
            $('#groupDE').css('display', 'none');
            $('#groupDLTPEMD').css('display', 'none');
            $('#groupDE_Radio').css('display', 'none');
            $('#groupDE_PEMD_Radio').css('display', 'none');
        } else {
            $('#groupVAL').css('display', 'none');
            $('#group-prefer').css('display', 'block');
            if (!$("#zone_PEMD").prop('checked')) {
                $('#groupDLTPEMD').css('display', 'none');                
            }
            var m = zp.split(" ", zp.split(" ").length - 1);
            var nbCheck = 0;

            m.forEach(zp_element = function (element) {
                $(element).click(clk_zp_element = function (rep_clk_zp_element) {
                    if ($(element).prop('checked')) {
                        nbCheck = nbCheck + 1;
                        //console.log(nbCheck);
                        if (nbCheck > 0) {
                            if ($("#zone_PEMD").prop('checked') && nbCheck == 1) {
                                $('#groupDLT').css('display', 'none');
                                $('#groupDE_Radio').css('display', 'none');
                                $('#groupDLTPEMD').css('display', 'block');
                                $('#groupDE_PEMD_Radio').css('display', 'block');
                            }
                            if ($("#zone_PEMD").prop('checked') && nbCheck > 1) {
                                $('#groupDLT').css('display', 'block');
                                $('#groupDE_Radio').css('display', 'block');
                                $('#groupDLTPEMD').css('display', 'block');
                                $('#groupDE_PEMD_Radio').css('display', 'block');
                            }
                            if ($(element).prop('checked') && !$("#zone_PEMD").prop('checked')) {
                                $('#groupDLT').css('display', 'block');
                                $('#groupDE_Radio').css('display', 'block');
                            }
                        } else {
                            $('#groupDLT').css('display', 'none');
                            $('#groupDE').css('display', 'none');
                            $('#groupVAL').css('display', 'none');
                        }

                        $("#dlt-0").click(function () {
                            if ($("#dlt-0").prop('checked')) {
                                $('#groupDE').css('display', 'block');
                            }
                        });

                        $("#dlt_pemd-0").click(function () {
                            if ($("#dlt_pemd-0").prop('checked')) {
                                $('#groupDE').css('display', 'block');
                            }
                        });

                        // si une checkbox est coché alors on affiche le groupVAL
                        $('#groupVAL').css('display', 'block'); 
                    } else {
                        nbCheck = nbCheck - 1;
                        if (nbCheck < 0) {
                            nbCheck = 0;
                        }
                        //console.log(nbCheck);
                        if (nbCheck > 0) {
                            if ($("#zone_PEMD").prop('checked') && nbCheck == 1) {
                                $('#groupDLT').css('display', 'none');
                                $('#groupDE_Radio').css('display', 'none');
                                $('#groupDLTPEMD').css('display', 'block');
                                $('#groupDE_PEMD_Radio').css('display', 'block');
                            }
                            if ($("#zone_PEMD").prop('checked') && nbCheck > 1) {
                                $('#groupDLT').css('display', 'block');
                                $('#groupDE_Radio').css('display', 'block');
                                $('#groupDLTPEMD').css('display', 'block');
                                $('#groupDE_PEMD_Radio').css('display', 'block');
                            }
                        } else {
                            $('#groupDLT').css('display', 'none');
                            $('#groupDE').css('display', 'none');
                            $('#groupVAL').css('display', 'none');
                            $('#groupDLT').css('display', 'none');
                            $('#groupDLTPEMD').css('display', 'none');
                            $('#groupDE_Radio').css('display', 'none');
                            $('#groupDE_PEMD_Radio').css('display', 'none');
                        }
                        $("#dlt-1").click(function () {
                            if ($("#dlt-1").prop('checked') && $('#groupDLTPEMD').css('display', 'none')) {
                                $('#groupDE').css('display', 'none');
                            }                            
                        });

                        $("#dlt-0").click(function () {
                            if ($("#dlt-0").prop('checked')) {
                                $('#groupDE').css('display', 'block');
                            }
                        });

                        $("#dlt_pemd-1").click(function () {
                            console.log('2');
                            if ($("#dlt_pemd-1").prop('checked') && $('#groupDLT').css('display', 'none')) {
                                $('#groupDE').css('display', 'none');
                            }
                        });

                        $("#dlt_pemd-0").click(function () {
                            if ($("#dlt_pemd-0").prop('checked')) {
                                $('#groupDE').css('display', 'block');
                            }
                        });
                    }
                    return rep_clk_zp_element;
                });
                if ($(element).prop('checked')) {
                    // s'il y a une checkbox de coché alors on affiche le groupVAL
                    nbCheck += 1;
                    //console.log(nbCheck);
                }
                if (nbCheck > 0) {
                    if ($("#zone_PEMD").prop('checked') && nbCheck == 1) {
                        $('#groupDLT').css('display', 'none');
                        $('#groupDLTPEMD').css('display', 'block');
                    }
                    $('#groupDLT').css('display', 'block');
                    $('#groupDE_Radio').css('display', 'block');
                } else {
                    $('#groupDLT').css('display', 'none');
                    $('#groupDE').css('display', 'none');
                    $('#groupVAL').css('display', 'none');
                }
                return element;
            });
        }
        return rep_clk_prefer;
    });

    //Partie PEMD
    $("#zone_PEMD").click(clk_zone_PEMD = function (rep_clk_zone_PEMD) {
        if (!$("#zone_PEMD").prop('checked')) {
            $('#groupDLTPEMD').css('display', 'none');
            $('#groupDE_PEMD_Radio').css('display', 'none');
            console.log(rep_clk_zone_PEMD);
            return rep_clk_zone_PEMD;
        } else {
            $('#groupDLTPEMD').css('display', 'block');
            $('#groupDE_PEMD_Radio').css('display', 'block');
            console.log(rep_clk_zone_PEMD);
            return rep_clk_zone_PEMD;
        }
    });

    $("#dlt-1").click(function () {
        if ($("#dlt-1").prop('checked') && $("#dlt_pemd-1").prop('checked')) {
            $('#groupDE').css('display', 'none');
        }
        if ($("#dlt-1").prop('checked') && $("#dlt_pemd-0").prop('checked')) {
            $('#groupDE').css('display', 'block');
        }
        if ($("#dlt-1").prop('checked') && !$("#dlt_pemd-0").prop('checked') && !$("#dlt_pemd-1").prop('checked')) {
            $('#groupDE').css('display', 'none');
        }
    });
    
    $("#dlt-0").click(function () {
        if ($("#dlt-0").prop('checked')) {
            $('#groupDE').css('display', 'block');
        }
    });

    $("#dlt_pemd-1").click(function () {
        if ($("#dlt_pemd-1").prop('checked') && $("#dlt-1").prop('checked')) {
            $('#groupDE').css('display', 'none');
        }
        if ($("#dlt_pemd-1").prop('checked') && $("#dlt-0").prop('checked')) {
            $('#groupDE').css('display', 'block');
        }
        if ($("#dlt_pemd-1").prop('checked') && !$("#dlt-0").prop('checked') && !$("#dlt-1").prop('checked')) {
            $('#groupDE').css('display', 'none');
        }
    });

    $("#dlt_pemd-0").click(function () {
        if ($("#dlt_pemd-0").prop('checked')) {
            $('#groupDE').css('display', 'block');
        }
    });

    //Afficher pays par zone
    $("input[name*='zone']").change(chg_req_zone_pays = function (rep_chg_req_zone_pays) {
        var x = $("input[name*='zone']:checked");
        var ret = '';
        $.each(x, function (i, field) {
            ret = ret + "," + x[i].value;
        });
        ret = ret.substring(1);
        $.ajax({
            url: './scripts/req_zone_pays.php',
            type: 'GET',
            data: 'zone=' + ret,
            dataType: 'html',
            success: fct_req_zone_pays = function (code_html, statut) {
                $('#pays_par_zone').html(code_html);
//                console.log('Pays par zone : ok')
                return code_html;
            },
            error: function (resultat, statut, erreur) {
//                console.log('Erreur : ' + erreur);
            },
            complete: function () {

            }
        });
        return rep_chg_req_zone_pays;
    });

    //Partie fichier FDS anglais
    $("#no_english_file").click(clk_no_english_file = function (rep_clk_no_english_file) {
        if ($("#no_english_file").prop('checked')) {
            $('#fds_en').css('display', 'none');
            return rep_clk_no_english_file;
        } else {
            $('#fds_en').css('display', 'block');
            return rep_clk_no_english_file;
        }
    });

    function dump(obj) {
        var out = '';
        for (var i in obj) {
            out += i + ": " + obj[i] + "\n";
        }

        alert(out);

        // or, if you wanted to avoid alerts...

        var pre = document.createElement('pre');
        pre.innerHTML = out;
        document.body.appendChild(pre)
    }    

//    $('#change_product').validator().on('submit', function (e) {

//        if (e.isDefaultPrevented()) {
//            alert("Check the form please.")
//        } else {
//            var fournisseur = $('#provider option:selected').val()
//            //alert(language);
//            e.preventDefault();
//            $.ajax({
//                url: './scripts/ajax_admin_changer_fournisseur.php',
//                type: 'POST',
//                data: 'fournisseur=' + fournisseur +
//                        '&code_art_four=' + $('#code_art_four').val() +
//                        '&code_actuel=' + $('#code_actuel').val() +
//                        '&designation_actuelle=' + $('#designation_actuelle').val(),
//                dataType: 'html',
//                success: function (code_html, statut) { // code_html contient le HTML renvoyé
//                    $('#result_req2').html(code_html);
////                    console.log('call update product: ok');
//                },
//                error: function (resultat, statut, erreur) {
//                    $('#result_req2').html(erreur);
//                },
//                complete: function () {
//                }
//            });
//        }
//    });


    // Variable to store your files
    /*var files;
     
     // Add events
     $('form').on('submit', uploadFiles);
     
     // Catch the form submit and upload the files
     function uploadFiles(event)
     {
     event.stopPropagation(); // Stop stuff happening
     event.preventDefault(); // Totally stop stuff happening
     
     // START A LOADING SPINNER HERE
     
     // Create a formdata object and add the files
     var data = new FormData();
     $.each(files, function(key, value)
     {
     data.append(key, value);
     });
     
     $.ajax({
     url: 'submit.php?files',
     type: 'POST',
     data: data,
     cache: false,
     dataType: 'json',
     processData: false, // Don't process the files
     contentType: false, // Set content type to false as jQuery will tell the server its a query string request
     success: function(data, textStatus, jqXHR)
     {
     if(typeof data.error === 'undefined')
     {
     // Success so call function to process the form
     submitForm(event, data);
     }
     else
     {
     // Handle errors here
     console.log('ERRORS: ' + data.error);
     }
     },
     error: function(jqXHR, textStatus, errorThrown)
     {
     // Handle errors here
     console.log('ERRORS: ' + textStatus);
     // STOP LOADING SPINNER
     }
     });
     }
     function submitForm(event, data)
     {
     // Create a jQuery object from the form
     $form = $(event.target);
     
     // Serialize the form data
     var formData = $form.serialize();
     
     // You should sterilise the file names
     $.each(data.files, function(key, value)
     {
     formData = formData + '&filenames[]=' + value;
     });
     
     $.ajax({
     url: 'submit.php',
     type: 'POST',
     data: formData,
     cache: false,
     dataType: 'json',
     success: function(data, textStatus, jqXHR)
     {
     if(typeof data.error === 'undefined')
     {
     // Success so call function to process the form
     console.log('SUCCESS: ' + data.success);
     }
     else
     {
     // Handle errors here
     console.log('ERRORS: ' + data.error);
     }
     },
     error: function(jqXHR, textStatus, errorThrown)
     {
     // Handle errors here
     console.log('ERRORS: ' + textStatus);
     },
     complete: function()
     {
     // STOP LOADING SPINNER
     }
     });
     }*/
    return rep_global;
});