$(function () {

    /*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
     *	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
     *
     *	SELECTEURS
     *	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
     *								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
     *	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
     *	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
     *
     *
     *
     * vérification de la validité d'un mot de passe saisi par l'utilisateur.
     * Lorsque l'utilisateur saisis des lettres dans le champ mot de passe
     * @return {[type]} [description]
     */
    $(document).ready(function () {
        $("#zone_update_pays").multiselect({
            //noneSelectedText: 'Vide',
            //noneSelectedText: $(this).data('selected-text'),
            //noneSelectedText: select.data('selected-text'),
            enableFiltering: true,
            enableFullValueFiltering: false,
            enableCaseInsensitiveFiltering: true
        });
//        console.log("ready!");
        $("#zone_create_pays").multiselect({
            enableFiltering: true,
            enableFullValueFiltering: false,
            enableCaseInsensitiveFiltering: true
        });
//        console.log("ready!");
    });
    $('#create_zone').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            //Pays par zone 
            /*var pays=""
             $("input[name=pays]:checked").each(function (){
             pays=pays + $(this).val() + ";";
             });
             if(pays !=""){ pays = pays.substring(pays, (pays.length - 1)) }*/
            var pays = $('#zone_create_pays').val()

            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_zone_create.php',
                type: 'POST',
                data: 'code_zone=' + $('#code_zone').val() +
                        '&nom_zone=' + $('#nom_zone').val() +
                        '&pays=' + pays,
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#result_req').html(code_html);
//                    console.log('call create zone: ok');
                    $('#create_zone')[0].reset();
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req').html(erreur);
                },
                complete: function () {
                }
            });
        }
    });

    $('#delete_zone').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            var zone = $('#zone option:selected').val()
            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_zone_delete.php',
                type: 'POST',
                data: 'zone=' + zone,
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
//                    $('#result_req2').html(code_html);
//                    console.log('call delete zone: ok');
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req2').html(erreur);
                },
                complete: function () {
                    location.reload(true);
                }
            });
        }
    });

    $('#update_zone').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            var code = $('#code_zone').val()
            var nom = $('#nom_zone').val()

            //Pays par zone 
            /*var pays=""
             $("input[name=pays]:checked").each(function (){
             pays=pays + $(this).val() + ";";
             });
             if(pays !=""){ pays = pays.substring(pays, (pays.length - 1)) }*/
            var pays = $('#zone_update_pays').val()

            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_zone_update.php',
                type: 'POST',
                data: 'code_zone=' + code +
                        '&nom_zone=' + nom +
                        '&code_actuel=' + $('#code_actuel').val(),
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#result_req3').html(code_html);
                    if (code != "") {
                        $('#code_zone').attr('placeholder', code);
                    }
                    if (nom != "") {
                        $('#nom_zone').attr('placeholder', nom);
                    }
//                    console.log('call update zone : ok');
                    $('#update_zone')[0].reset();
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req3').html(erreur);
                },
                complete: function () {
                }
            });
        }
    });
    /*
     
     $("#email").keyup(function(){
     $.ajax({
     url : './scripts/req_email.php',
     type : 'GET',
     data : 'email=' + $('#email').val(),
     dataType : 'html',
     success : function(code_html, statut){ // code_html contient le HTML renvoyé
     $('#valid-email').html(code_html);
     console.log('Check email: ok');
     },
     error : function(resultat, statut, erreur){
     alert('Erreur : ' + erreur);
     },
     complete : function(){
     var msg_err = $("#valid-email").text();
     
     if(msg_err.length>0)
     {
     //alert(msg_err.length);
     $("#verif-email").removeClass().addClass("form-group has-error");// supprime une classe CSS 
     }else{
     if($("#email").val().length===0){
     $("#verif-email").removeClass().addClass("form-group");// supprime une classe CSS 
     }else{
     $("#verif-email").removeClass().addClass("form-group has-success");// supprime une classe CSS 
     }
     }
     }
     });
     });
     
     function isValidEmailAddress(emailAddress) {
     var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
     return pattern.test(emailAddress);
     }
     
     $("#new_pwd1").keyup(function()
     { 	
     inputValue = $("#new_pwd1").val(); 
     longueurStr = inputValue.length;
     carMin = 6;
     
     // en fonction du nombre de caractères saisis 
     if(longueurStr < carMin) 
     { 
     if(longueurStr < 1) 
     { 
     $("#verif-pwd1").removeClass().addClass("form-group");// supprime une classe CSS 
     $("#valid-pwd1").css('display', 'none');
     
     }else{
     $("#verif-pwd1").removeClass().addClass("form-group has-error");// supprime une classe CSS 
     $("#valid-pwd1").css('display', 'block');
     }
     }else{
     $("#verif-pwd1").removeClass().addClass("form-group has-success");// supprime et ajoute une classe CSS
     $("#valid-pwd1").css('display', 'none');		
     }
     
     }); 
     
     $("#new_pwd2").keyup(function()
     { 	
     inputValue1 = $("#new_pwd1").val(); 
     inputValue2 = $("#new_pwd2").val(); 
     
     // en fonction du nombre de caractères saisis 
     if(inputValue1 === inputValue2) 
     { 
     $("#verif-pwd2").removeClass().addClass("form-group has-success");// supprime une classe CSS 
     $("#valid-pwd2").css('display', 'none');
     } else{
     $("#verif-pwd2").removeClass().addClass("form-group has-error");// supprime une classe CSS 
     $("#valid-pwd2").css('display', 'block');
     }
     
     }); 
     
     $(document).keydown(function(e){
     if (e.keyCode === 13){
     if($('#f_sap').is(":focus")){;
     e.preventDefault();
     $('#f_sap').trigger('change');
     };
     };
     });*/

});