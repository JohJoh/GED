$(function() {

	/*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
	*	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
	*
	*	SELECTEURS
	*	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
	*								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
	*	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
	*	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
	*
	*
	*
	* vérification de la validité d'un mot de passe saisi par l'utilisateur.
	* Lorsque l'utilisateur saisis des lettres dans le champ mot de passe
	* @return {[type]} [description]
	*/
	
	
	
	$("#code_douanier").change(function(){
		$.ajax({
			url : './scripts/req_def_code_douanier.php',
			type : 'GET',
			data : 'code=' + $("#code_douanier").val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#definition_code_douanier').html(code_html);
//				console.log('Definition code douanier : ok');
			},
			error : function(resultat, statut, erreur){
//				console.log('Erreur : ' + erreur);
			},
			complete : function(){
			}
		});
	});
	
	$("#classe_danger").change(function(){
		$.ajax({
			url : './scripts/req_def_classe_danger.php',
			type : 'GET',
			data : 'classe=' + $("#classe_danger").val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#definition_classe_danger').html(code_html);
//				console.log('Definition classe danger : ok');
			},
			error : function(resultat, statut, erreur){
//				console.log('Erreur : ' + erreur);
			},
			complete : function(){
			}
		});
	});
	
	$("#code_onu").change(function(){
		$.ajax({
			url : './scripts/req_def_code_onu.php',
			type : 'GET',
			data : 'code=' + $("#code_onu").val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#definition_code_onu').html(code_html);
//				console.log('Definition code onu : ok');
			},
			error : function(resultat, statut, erreur){
//				console.log('Erreur : ' + erreur);
			},
			complete : function(){
			}
		});
	});
	
	$("#code_emballage").change(function(){
		$.ajax({
			url : './scripts/req_def_code_emballage.php',
			type : 'GET',
			data : 'code=' + $("#code_emballage").val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#definition_code_emballage').html(code_html);
//				console.log('Definition code emballage : ok');
			},
			error : function(resultat, statut, erreur){
//				console.log('Erreur : ' + erreur);
			},
			complete : function(){
			}
		});
	});
	
	$("#danger-0").click(function(){
		if($("#danger-0").prop('checked')){
			$("#group_danger").css("display","none");
			$("#classe_danger").removeAttr('required');
			$("#code_ONU").removeAttr('required');
			$("#code_emballage").removeAttr('required');
		}else{
			$("#group_danger").css("display","block")
			$("#classe_danger").attr('required', '');
			$("#code_ONU").attr('required', '');
			$("#code_emballage").attr('required', '');
		}
	});
	
	$("#id_test").click(function(){
		var favorite = [];
		$.each($("input[name='mode_transport']:checked"), function(){            
		favorite.push($(this).val());
		});
		alert(favorite.join(", "));
	});
	
    $('#create_product').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            var fournisseur = $('#fournisseur option:selected').val()
            var division = $('#division option:selected').val()
            var pays_provenance = $('#pays_provenance option:selected').val()
            var pays_origine = $('#pays_origine option:selected').val()
            var code_douanier = $('#code_douanier option:selected').val()
            var classe_danger = $('#classe_danger option:selected').val()
            var code_onu = $('#code_onu option:selected').val()
            var code_emballage = $('#classe_emballage').val()
            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_product_create.php',
                type: 'POST',
                data: 'code_sap=' + $('#code_sap').val() +
                '&fournisseur=' + fournisseur +
                '&code_produit_fournisseur=' + $('#code_produit_fournisseur').val() +
                '&designation=' + $('#designation').val() +
                '&division=' + division +
                '&pays_provenance=' + pays_provenance +
                '&code_postal=' + $('#code_postal').val() +
                '&ville=' + $('#ville').val() +
                '&pays_origine=' + pays_origine +
                '&date_debut_validite=' + $('#date_debut_validite').val() +
                '&date_fin_validite=' + $('#date_fin_validite').val() +
                '&code_douanier=' + code_douanier +
                '&classe_danger' + classe_danger +
                '&code_onu=' + code_onu +
                '&code_emballage=' + code_emballage +
                '&contenance=' + $('#contenance').val() +
                '&type=' + $('#type').val() +
                '&conditionnement=' + $('#conditionnement').val() +
                '&fiche_dlt=' + $('#fiche_dlt').val() +
                '&fiche_dlt_pemd=' + $('#fiche_dlt_pemd').val() +
                '&fiche_de=' + $('#fiche_de').val() +
                '&fiche_fds=' + $('#fiche_fds').val() +
                '&fiche_dof=' + $('#fiche_dof').val(),
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
				$('#result_req').html(code_html);
//				console.log('call create product ok');
			},
			error : function(resultat, statut, erreur){
				$('#result_req').html(erreur);
			},
			complete : function(){
			}
		});
	  }
	});
	/*

	$("#email").keyup(function(){
		$.ajax({
			url : './scripts/req_email.php',
			type : 'GET',
			data : 'email=' + $('#email').val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#valid-email').html(code_html);
				console.log('Check email: ok');
			},
			error : function(resultat, statut, erreur){
				alert('Erreur : ' + erreur);
			},
			complete : function(){
				var msg_err = $("#valid-email").text();
				
				if(msg_err.length>0)
				{
					//alert(msg_err.length);
					$("#verif-email").removeClass().addClass("form-group has-error");// supprime une classe CSS 
				}else{
					if($("#email").val().length===0){
						$("#verif-email").removeClass().addClass("form-group");// supprime une classe CSS 
					}else{
						$("#verif-email").removeClass().addClass("form-group has-success");// supprime une classe CSS 
					}
				}
			}
		});
	});

	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	}
	
	$("#new_pwd1").keyup(function()
	{ 	
		inputValue = $("#new_pwd1").val(); 
		longueurStr = inputValue.length;
		carMin = 6;

		// en fonction du nombre de caractères saisis 
		if(longueurStr < carMin) 
		{ 
			if(longueurStr < 1) 
			{ 
				$("#verif-pwd1").removeClass().addClass("form-group");// supprime une classe CSS 
				$("#valid-pwd1").css('display', 'none');
				
			}else{
				$("#verif-pwd1").removeClass().addClass("form-group has-error");// supprime une classe CSS 
				$("#valid-pwd1").css('display', 'block');
			}
		}else{
			$("#verif-pwd1").removeClass().addClass("form-group has-success");// supprime et ajoute une classe CSS
			$("#valid-pwd1").css('display', 'none');		
		}
		 
	}); 
	
	$("#new_pwd2").keyup(function()
	{ 	
		inputValue1 = $("#new_pwd1").val(); 
		inputValue2 = $("#new_pwd2").val(); 

		// en fonction du nombre de caractères saisis 
		if(inputValue1 === inputValue2) 
		{ 
			$("#verif-pwd2").removeClass().addClass("form-group has-success");// supprime une classe CSS 
			$("#valid-pwd2").css('display', 'none');
		} else{
			$("#verif-pwd2").removeClass().addClass("form-group has-error");// supprime une classe CSS 
			$("#valid-pwd2").css('display', 'block');
		}
			 
	}); 
	
	$(document).keydown(function(e){
		if (e.keyCode === 13){
			if($('#f_sap').is(":focus")){;
				e.preventDefault();
				$('#f_sap').trigger('change');
			};
		};
	});*/
		
});