$(function() {

	/*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
	*	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
	*
	*	SELECTEURS
	*	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
	*								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
	*	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
	*	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
	*
	*
	*
	* vérification de la validité d'un mot de passe saisi par l'utilisateur.
	* Lorsque l'utilisateur saisis des lettres dans le champ mot de passe
	* @return {[type]} [description]
	*/
	
	$('#formUpdateUser').validator().on('submit', function (e) {
		
	  if (e.isDefaultPrevented()) {
		alert("Check the form please.")
	  } else {
		var language = $('#language-group input:radio:checked').val()
		//alert(language);
		e.preventDefault();
		$.ajax({
			url : './scripts/ajax_myaccount.php',
			type : 'POST',
			data : 'lastname=' + $('#lastname').val() + '&firstname=' + $('#firstname').val() + '&username=' + $('#username').val() + '&email=' + $('#email').val() + '&new_pwd1=' + $('#new_pwd1').val() + '&language=' + language,
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#result_req').html(code_html);
//				console.log('call update user: ok');
			},
			error : function(resultat, statut, erreur){
				$('#result_req').html(erreur);
			},
			complete : function(){
				$(this).find('formUpdateUser')[0].reset();
			}
		});
	  }
	});
	
	/*$("#username").keyup(function(){
		$.ajax({
			url : './scripts/req_username.php',
			type : 'GET',
			data : 'usrnme=' + $('#username').val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#valid-username').html(code_html);
				console.log('Check username: ok');
			},
			error : function(resultat, statut, erreur){
				alert('Erreur : ' + erreur);
			},
			complete : function(){
				var msg_err = $("#valid-username").text();
				
				if(msg_err.length>0)
				{
					//alert(msg_err.length);
					$("#verif-username").removeClass().addClass("form-group has-error");// supprime une classe CSS 
				}else{
					if($("#username").val().length===0){
						$("#verif-username").removeClass().addClass("form-group");// supprime une classe CSS 
					}else{
						$("#verif-username").removeClass().addClass("form-group has-success");// supprime une classe CSS 
					}
				}
			}
		});
		
	});

	$("#email").keyup(function(){
		$.ajax({
			url : './scripts/req_email.php',
			type : 'GET',
			data : 'email=' + $('#email').val(),
			dataType : 'html',
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				$('#valid-email').html(code_html);
				console.log('Check email: ok');
			},
			error : function(resultat, statut, erreur){
				alert('Erreur : ' + erreur);
			},
			complete : function(){
				var msg_err = $("#valid-email").text();
				
				if(msg_err.length>0)
				{
					//alert(msg_err.length);
					$("#verif-email").removeClass().addClass("form-group has-error");// supprime une classe CSS 
				}else{
					if($("#email").val().length===0){
						$("#verif-email").removeClass().addClass("form-group");// supprime une classe CSS 
					}else{
						$("#verif-email").removeClass().addClass("form-group has-success");// supprime une classe CSS 
					}
				}
			}
		});
	});

	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	}
	
	$("#new_pwd1").keyup(function()
	{ 	
		inputValue = $("#new_pwd1").val(); 
		longueurStr = inputValue.length;
		carMin = 6;

		// en fonction du nombre de caractères saisis 
		if(longueurStr < carMin) 
		{ 
			if(longueurStr < 1) 
			{ 
				$("#verif-pwd1").removeClass().addClass("form-group");// supprime une classe CSS 
				$("#valid-pwd1").css('display', 'none');
				
			}else{
				$("#verif-pwd1").removeClass().addClass("form-group has-error");// supprime une classe CSS 
				$("#valid-pwd1").css('display', 'block');
			}
		}else{
			$("#verif-pwd1").removeClass().addClass("form-group has-success");// supprime et ajoute une classe CSS
			$("#valid-pwd1").css('display', 'none');		
		}
		 
	}); 
	
	$("#new_pwd2").keyup(function()
	{ 	
		inputValue1 = $("#new_pwd1").val(); 
		inputValue2 = $("#new_pwd2").val(); 

		// en fonction du nombre de caractères saisis 
		if(inputValue1 === inputValue2) 
		{ 
			$("#verif-pwd2").removeClass().addClass("form-group has-success");// supprime une classe CSS 
			$("#valid-pwd2").css('display', 'none');
		} else{
			$("#verif-pwd2").removeClass().addClass("form-group has-error");// supprime une classe CSS 
			$("#valid-pwd2").css('display', 'block');
		}
			 
	}); 
	
	$(document).keydown(function(e){
		if (e.keyCode === 13){
			if($('#f_sap').is(":focus")){;
				e.preventDefault();
				$('#f_sap').trigger('change');
			};
		};
	});*/
		
});