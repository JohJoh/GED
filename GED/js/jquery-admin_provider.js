$(function () {

    $('#create_provider').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.");
        } else {
            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_provider_create.php',
                type: 'POST',
                data: 'code_four=' + $('#code_four').val() +
                        '&nom_four=' + $('#nom_four').val(),
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#result_req').html(code_html);
                    //console.log('call create fournisseur: ok');
                    $('#create_provider')[0].reset();
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req').html(erreur);
                },
                complete: function () {
                }
            });
        }
    });

    $('#delete_provider').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            var provider = $('#provider2 option:selected').val();
            //alert(provider);
            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_provider_delete.php',
                type: 'POST',
                data: 'provider=' + provider,
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#result_req2').html(code_html);
                    //console.log('call delete provider: ok');
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req2').html(erreur);
                },
                complete: function () {
                }
            });
        }
    });

    $('#update_provider').validator().on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            alert("Check the form please.")
        } else {
            var code = $('#code_four').val()
            var nom = $('#nom_four').val()
            e.preventDefault();
            $.ajax({
                url: './scripts/ajax_admin_provider_update.php',
                type: 'POST',
                data: 'code_four=' + code +
                        '&nom_four=' + nom +
                        '&code_actuel=' + $('#code_actuel').val(),
                dataType: 'html',
                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                    $('#result_req3').html(code_html);
                    //console.log('call update fournisseur: ok');
                    if (code != "") {
                        $('#code_four').attr('placeholder', code);
                    }
                    if (nom != "") {
                        $('#nom_four').attr('placeholder', nom);
                    }
                    $('#update_provider')[0].reset();
                },
                error: function (resultat, statut, erreur) {
                    $('#result_req3').html(erreur);
                },
                complete: function () {
                }
            });
        }
    });
});