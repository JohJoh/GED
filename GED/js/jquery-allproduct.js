function ajax_allproduct() {
    $.ajax({
        url: './req_allproduct.php',
        type: 'GET',
        data: 'all=' + $('#f_hid_all').val() + '&eu=' + $('#f_hid_eu').val() + '&page=' + $('#f_hid_page').val() + '&wrong=' + $('#f_hid_wrong').val() + '&ppp=' + $('#f_hid_ppp').val() + '&org=' + $('#f_hid_org').val() + '&sap=' + $('#f_hid_sap').val() + '&four=' + $('#f_hid_four').val(),
        dataType: 'html',
        success: function (code_html, statut) { // code_html contient le HTML renvoyé
            $('#allproduct').html(code_html);
//            console.log('Chargement pdt : OK');
        },
        error: function (resultat, statut, erreur) {
            alert('Erreur : ' + erreur);
        },
        complete: function () {
        }
    });


}

$(function () {

    /*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
     *	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
     *
     *	SELECTEURS
     *	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
     *								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
     *	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
     *	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
     *
     *
     *
     */

    $(document).keydown(function (e) {
        if (e.keyCode === 13) {
            if ($('#f_sap').is(":focus")) {
                ;
                e.preventDefault();
                $('#f_sap').trigger('change');
            }
            ;
        }
        ;
    });

    //Afficher tous les produits
    $('#f_all').click(function () {
        $('#f_sap').val("");
        $('#f_hid_all').val("1");
        $('#f_hid_eu').val("0");
        $('#f_hid_wrong').val("0");
        $('#f_hid_sap').val("0");
        ajax_allproduct();
        //alert('all=' + $('#f_hid_all').val() + '&eu=' + $('#f_hid_eu').val() + '&wrong=' + $('#f_hid_wrong').val() + '&ppp=' + $('#f_hid_ppp').val() + '&org=' + $('#f_hid_org').val() + '&sap=' + $('#f_hid_sap').val() + '&four=' + $('#f_hid_four').val());
    })

    //pages
    $('#f_page').change(function () {
        $('#f_hid_page').val(this.value);
        ajax_allproduct();
        //alert('all=' + $('#f_hid_all').val() + '&eu=' + $('#f_hid_eu').val() + '&wrong=' + $('#f_hid_wrong').val() + '&ppp=' + $('#f_hid_ppp').val() + '&org=' + $('#f_hid_org').val() + '&sap=' + $('#f_hid_sap').val() + '&four=' + $('#f_hid_four').val());
    })

    //Uniquement les produits d'origine EU
    $("#f_eu").attr("src", "javascript:false");
    $('#f_eu').click(function () {
        $('#f_sap').val("");
        $('#f_hid_all').val("0");
        $('#f_hid_eu').val("1");
        $('#f_hid_wrong').val("0");
        $('#f_hid_sap').val("0");
        ajax_allproduct();
    })

    //Uniquement les produits incorrect
    $('#f_wrong').click(function () {
        $('#f_sap').val("");
        $('#f_hid_all').val("0");
        $('#f_hid_eu').val("0");
        $('#f_hid_wrong').val("1");
        $('#f_hid_sap').val("0");
        ajax_allproduct();
    })

    //Tri par fournisseur
    $('#f_four').change(function () {
        $('#f_hid_four').val(this.value);
        ajax_allproduct();
    })

    //Tri par origine
    $('#f_org').change(function () {
        $('#f_hid_org').val(this.value);
        ajax_allproduct();
    })

    //Produit par page
    $('#f_ppp').change(function () {
        $('#f_hid_ppp').val(this.value);
        $('#f_hid_page').val("1");
        ajax_allproduct();
    })

    //Uniquement les produits incorrect
    $('#f_sap').change(function () {
        $('#f_hid_sap').val(this.value);
        $('#f_hid_all').val("0");
        $('#f_hid_eu').val("0");
        $('#f_hid_wrong').val("0");
        ajax_allproduct();
    })

});