$(function() {

	/*	PETITES NOTES SUR JQuery - Loïc ROBILLARD
	*	$(function(){ ... }); 		= Fonction principale qui va permettre à JQuery de se lancer quand la page est "prête"
	*
	*	SELECTEURS
	*	$('*').méthode(){ ... }); 	= * correspond à un id, une classe ou encore une balise.
	*								= méthode() correspond à un événement (quand on clique, quand on change la valeur, ...)
	*	Niveau temps d'exécution, la sélection par id est la plus rapide, puis celle par classe et celle par balise.
	*	La sélection d'id est la plus rapide grâce à getElementById du langage Javascript
	*
	*
	*
	*/
	
	//Afficher tous les produits
	$('img#flag_fr').click(function(){
		// Get the src of the image
		var lang = $(this).attr("lang");
		window.location.href = 'login.php?lang='+lang;
		//location.reload();
	});	
	
	//Afficher tous les produits
	$('img#flag_en').click(function(){
		var lang = $(this).attr("lang");
		window.location.href = 'login.php?lang='+lang;
		//location.reload();
	});
});