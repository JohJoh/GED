<?php
include('./scripts/dbc.php');
page_protect();
if(!checkAdmin()){
    header("Location:home.php");
}
include("./scripts/head.php");
include("./scripts/menu.php");
include("./scripts/banner.php");
$default_lang = 'fr';

if (!isset($_SESSION['lang'])) {
    if (isset($_GET['lang'])) {
        if (($_GET['lang'] == "fr") || ($_GET['lang'] == "en")) {
            $lang = $_GET['lang'];
        } else {
            $lang = $default_lang;
        }
    } else {
        $lang = $default_lang;
    }
} else {
    $lang = $_SESSION['lang'];
}

$trad_admin_product_de_re_activate = array(
    'fr' => array(
        'title' => 'Désactiver/Activer des produits'
    ),
    'en' => array(
        'title' => 'Desactivate/Activate products'
    )
);


//Partie SQL
$produit = $link->query("SELECT sap_prod, designation, is_active FROM produits");

if (isset($_SESSION['user_level'])) {

    $pdo_nb_produits = $link->query("Select sap_prod from produits");
    $nb_produits = $pdo_nb_produits->rowCount();
    $nb_pages = ceil($nb_produits / 100);
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <legend><?php echo $trad_admin_product_de_re_activate[$lang]['title']; ?></legend>
                    <div class="col-md-4" style="float:right;">
                        <div class="input-group" style="text-align:center">
                            <input id="search_code_sap" name="search_code_sap" class="form-control" placeholder="Code SAP" type="text">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Injection CSV</label>
                            <div class="panel-body" id="contenu_panel"><a href="./exemple_activer_produits_csv.csv">Exemple</a></div>
                            <input type="file" id="fichier">
                            <button type="submit" class="toggleActivation" data-value="1">Activer</button>
                            <button type="submit" class="toggleActivation" data-value="0">Desactiver</button>
                        </div>
                    </div>
                    <div id="content"></div>
                    <script>
                        $(".toggleActivation").click(function(e) {
                            var files = $('#fichier')[0].files;
                            if (files.length <= 0) {
                                e.preventDefault();
                                return;
                            }

                            var data = (window.FormData) ? new FormData(this) : null;
                            if (data === null && files.length > 0) {
                                return alert("Votre navigateur n'intègre pas la fonctionnalité d'upload asynchrone de fichiers utilisée ici. Veuillez réessayer avec un navigateur plus récent.");
                            }
                            data.append('activation', $(this).data('value'));
                            data.append('fichier', files[0], files[0].name);

                            $.ajax({
                                url: 'scripts/ajax_admin_products_toggle_activation.php',
                                type: 'POST',
                                data: data,
                                contentType: false,
                                cache: false,
                                processData: false,
                                dataType: 'html',
                                error: function (resultat, statut, erreur) {
                                    alert(statut);
                                },
                                complete: function() {
                                    location.reload();
                                }
                            });
                        });
                        // init bootpag
                        $(document).ready(function () {
                            $.ajax({
                                url: './scripts/req_admin_products_de_activate.php',
                                type: 'GET',
                                data: 'page=1',
                                dataType: 'html',
                                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                                    $('#content').html(code_html);
                                    //console.log('load page: ok');
                                },
                                error: function (resultat, statut, erreur) {
                                    alert('Erreur : ' + erreur);
                                }
                            });
                        });
                        $('#search_code_sap').keyup(function () {
                            $.ajax({
                                url: './scripts/req_admin_products_de_activate.php',
                                type: 'GET',
                                data: 'page=1&search=' + $('#search_code_sap').val(),
                                dataType: 'html',
                                success: function (code_html, statut) { // code_html contient le HTML renvoyé
                                    $('#content').html(code_html);
                                    //console.log('load page: ok');
                                },
                                error: function (resultat, statut, erreur) {
                                    alert('Erreur : ' + erreur);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </section>
    <script src="./js/validator.js"></script>
    <?php
} else {
    echo "<h2 class='lead section-lead has-error'>" . $trad[$lang]['error'] . "</h2>";
}
include("./scripts/footer.php");
?>